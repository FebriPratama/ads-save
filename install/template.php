<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" itemscope itemtype="http://schema.org/Article"> <!--<![endif]-->
  <head>
    <title>Install AGC</title>
    <!-- Theme Style -->
    <link rel="stylesheet" href="install/css/bootstrap.min.css">
  
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <?php if(!isset($_POST['singlebutton'])){ ?>
            <form class="form-horizontal" method="POST" action="">
            <fieldset>

            <!-- Form Name -->
            <legend>Setting Config</legend>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Nama Database">Nama Database</label>  
              <div class="col-md-4">
              <input id="Nama Database" name="db_name" type="text" placeholder="Nama Database" class="form-control input-md" required="">
              </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="User Database">User Database</label>  
              <div class="col-md-4">
              <input id="User Database" name="db_user" type="text" placeholder="User Database" class="form-control input-md" required=""> 
              </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Password Database">Password Database</label>  
              <div class="col-md-4">
              <input id="Password Database" name="db_password" type="text" placeholder="Password Database" class="form-control input-md" required="">
              </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="CSS Prefix">CSS Prefix</label>  
              <div class="col-md-4">
              <input id="CSS Prefix" name="css_prefix" type="text" value="<?php echo $_SERVER['SERVER_NAME']; ?>" placeholder="CSS Prefix" class="form-control input-md" required="">
              </div>
            </div>

            <!-- Select Basic -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Theme">Pilihan Theme</label>
              <div class="col-md-4">
                <select id="Theme" name="theme" class="form-control">

                  <option value="clubmona">Clubmona</option>
                  <option value="minitheme_v7_save">Mini Theme V7</option>
                  <option value="minitheme_v7_save_left">Mini Theme V7 Left</option>
                  <option value="minitheme_v8_save">Mini Theme V8 ( Noscript )</option>
                  <option value="minitheme_v9">Mini Theme V9</option>
                  <option value="clubmona_noscript">Clubmona ( Noscript )</option>

                </select>
              </div>
            </div>

            <!-- Select Basic -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Niche">Pilihan Niche</label>
              <div class="col-md-4">
                <select id="Niche" name="niche" class="form-control">
                  <option value="wall">Home Design</option>
                  <option value="wiring">Wiring Diagram</option>
                  <option value="fashion">Fashion</option>
                  <option value="coloring">Coloring Page</option>
                </select>
              </div>
            </div>

            <!-- Button -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="singlebutton"></label>
              <div class="col-md-4">
                <button type="submit" name="singlebutton" class="btn btn-success">Simpan</button>
              </div>
            </div>

            </fieldset>
            </form>
          <?php } ?>

        </div>

      </div>
    </div>
  </body>
