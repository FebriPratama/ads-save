<?php
/**
 * Bootstrap file for setting the application environment
 *
 * @package AGC-Luqman
 */

define( 'ABSPATH', dirname(__FILE__) . '/' );

require ABSPATH . 'config.php';

define( 'SITE_URL', trim($domain, '/') . '/' );

define( 'AGCL_INC_PATH', dirname(__FILE__) . "/includes/" );
define( 'AGCL_TEMPLATE_PATH', dirname(__FILE__) . "/templates/{$template_name}/" );
define( 'AGCL_TEMPLATE_URL', $domain . "/templates/{$template_name}/" );
define( 'AGCL_STATIC_TEMPLATE_URL', SITE_SCHEME. 'static.' . $_SERVER['SERVER_NAME'] . "/templates/{$template_name}/" );
define( 'AGCL_URL_SUFFIX', $url_suffix );

// Constants for expressing human-readable intervals
// in their respective number of seconds.
define( 'MINUTE_IN_SECONDS', 60 );
define( 'HOUR_IN_SECONDS',   60 * MINUTE_IN_SECONDS );
define( 'DAY_IN_SECONDS',    24 * HOUR_IN_SECONDS   );
define( 'WEEK_IN_SECONDS',    7 * DAY_IN_SECONDS    );
define( 'YEAR_IN_SECONDS',  365 * DAY_IN_SECONDS    );

/*---------- start session before everything ----------*/
session_start();

/*---------- For security reasons, error must be turned off on live site ----------*/
if( $_SERVER['SERVER_NAME'] == 'localhost' )
  error_reporting( E_ALL );
else
  error_reporting( E_ALL );

/*---------- set PHP to local timezone ----------*/
date_default_timezone_set('America/New_York');

/*---------- Connect to database. ----------*/
if( isset($ismysql) && $ismysql==TRUE ) :
  require_once AGCL_INC_PATH . 'databases/ez_sql_core.php';
  require_once AGCL_INC_PATH . 'databases/ez_sql_mysqli.php';
  require_once AGCL_INC_PATH . 'databases/queries.php';
  $db = new ezSQL_mysqli($db_user, $db_pass, $db_name, $db_host);
endif;

/*---------- Load libraries ----------*/
require_once AGCL_INC_PATH . 'kses.php';
require_once AGCL_INC_PATH . 'fungsi.php';
require_once AGCL_INC_PATH . 'cache.class.php';

use MatthiasMullie\Minify;

// create style
$path = AGCL_TEMPLATE_PATH.'lib/css/'.CSS_PREFIX.'.css';
if(!file_exists($path)){
	
	switch ($template_name) {
		case 'minitheme_v7_save':

			$msg = generateCssBs(CSS_PREFIX);

			break;
		case 'minitheme_v9':

			$msg = generateCssBs(CSS_PREFIX);

			break;
		case 'minitheme_v7_save_left':

			$msg = generateCssBs(CSS_PREFIX);

			break;
		case 'minitheme_v8_save':

			$msg = generateCssBs(CSS_PREFIX);

			break;
		case 'clubmona':

			$msg = generateCssClub(CSS_PREFIX);

			break;
	}

	chmod(AGCL_TEMPLATE_PATH.'lib/css', 0777);

	$minifier = new Minify\CSS($msg);
	$msg = $minifier->minify();;
	
	$f = fopen($path, "wb+");
	fwrite($f, $msg);
	fclose($f);
	chmod($path, 0777);	

}

//require_once ABSPATH . 'bot/inject-txt2mysql.php'; // inject every page load
//require_once ABSPATH . 'bot/sitemap-xml.php'; // inject every page load

/*---------- Start controller and template calling ----------*/
$URL = translate_URL();
