<?php
/**
 * File: template.php
 * Version: 2
 * Last Edit: 8:38 AM 12 Juni 2015
 */

$bad   = file_get_contents(ABSPATH . 'badwords.txt');
$bad   = explode("\n", $bad);
$bad   = array_map('trim', $bad);
$q     = preg_replace('/([^a-z0-9]+)/i', ' ', $URL['controller']);

$blacklist = array(
  
  'steam it carpet cleaning'

  );

$checkBlacklist = false;

foreach($blacklist as $b){
  if(trim(strtolower(removeSpecial($q))) == $b) $checkBlacklist = true;
}

if ( ! is_term_safe( $q ) || $checkBlacklist ) {

  $URL['controller'] = '404';
  $db->query("delete from search_terms where slug = '{$URL['controller']}'");

}

if( $URL['controller'] == 'index' ) {
  require AGCL_TEMPLATE_PATH . 'index.php';
  die();
}

elseif( $URL['controller'] == 'page' ) {
  require AGCL_TEMPLATE_PATH . 'index.php';
  die();
}

elseif( $URL['controller'] == 'info' ) {

  if( ! file_exists(AGCL_TEMPLATE_PATH . 'info/' . $URL['args'][1] . '.php')) {

    header("HTTP/1.0 404 Not Found");
    require AGCL_TEMPLATE_PATH . '404.php';
    die();

  } else {

    require AGCL_TEMPLATE_PATH . 'info/' . $URL['args'][1] . '.php';
    die();

  }
}

elseif( $URL['controller'] == 'imgs' ) {

    $child = $db->get_row( "SELECT b.term as child_term,c.term as parent_term FROM term_images as a join search_terms as b on a.parent_term = b.ID join search_terms as c on c.ID = b.parent_id where a.url = '".$URL['args'][1]."'" );

    if(!is_object($child)){
      
      header("HTTP/1.0 404 Not Found");
      require AGCL_TEMPLATE_PATH . '404.php';
      die();

    }

    header("Location: ".to_attachment($child->parent_term,$child->child_term), true, 301);

    die();

}

elseif( $URL['controller'] == 'fromimage' ) {

    $child = $db->get_row( "SELECT b.term as child_term,c.term as parent_term FROM term_images as a join search_terms as b on a.parent_term = b.ID join search_terms as c on c.ID = b.parent_id where a.url = '".$URL['args'][1]."'" );

    if(!is_object($child)){
      
      header("HTTP/1.0 404 Not Found");
      require AGCL_TEMPLATE_PATH . '404.php';
      die();

    }

    header("Location: ".to_attachment($child->parent_term,$child->child_term), true, 301);

    die();

}

elseif( $URL['controller'] == 'bot' ) {

    require dirname(__FILE__) . '/bot/' . $URL['args'][1];
    die();

}

elseif( $URL['controller'] == 'map' ) {

    require AGCL_TEMPLATE_PATH . 'map.php';
    die();

}

elseif( $URL['controller'] == 'img' ) {

    require AGCL_TEMPLATE_PATH . 'mask.php';
    die();

}

elseif( $URL['controller'] == 'go' ) {
  require AGCL_TEMPLATE_PATH . 'go.php';
  die();
}


elseif( $URL['controller'] == '404' ) {
  header("HTTP/1.0 404 Not Found");
  require AGCL_TEMPLATE_PATH . '404.php';
  die();
}

else {

  // check kw exist or not
  $item = $db->get_row("SELECT * FROM search_terms WHERE slug = '{$URL['controller']}'");

  if( $item ) {

      $datas = $db->get_results( "SELECT * FROM search_terms where parent_id = ".$item->ID." AND type='child'" );
      $datas = is_array($datas) ? $datas : [];

    //check child
    if($URL['args'][1] !== ''){

      $child = $db->get_row( "SELECT * FROM search_terms where slug = '".$URL['args'][1]."'");
      
      if(!$child){

        header("HTTP/1.0 404 Not Found");
        require AGCL_TEMPLATE_PATH . '404.php';
        die();

      }

      $im = $db->get_row("SELECT * FROM term_images where parent_term='".$child->ID."'");
      
      if(!$im){

        header("HTTP/1.0 404 Not Found");
        require AGCL_TEMPLATE_PATH . '404.php';
        die();

      }
      
      $title = trim($child->term) == '' || ( count(explode(' ', $child->term)) < 2 ) ? ucwords($q) : ucwords($child->term);
      
      $childImgs = getChildImages($db,$im);

      $img = array(

          'term'  => $title,
          'url'  => SITE_URL.'imgs/'.$im->url,
          'childs' => $childImgs,            
          'height' =>$im->height,
          'width' => $im->width,
          'thumb' => $im->thumb,
          'type'    => $im->type

          ); 

      require AGCL_TEMPLATE_PATH . 'attachment.php';   

    }else{

      require AGCL_TEMPLATE_PATH . 'single.php';     

    }

    die();

  }else{

    header("HTTP/1.0 404 Not Found");
    require AGCL_TEMPLATE_PATH . '404.php';
    die();

  }

}
