<?php

function header(){
	
	?>

	  <?php if(hasOption($db,'webmaster')) echo getOption($db,'webmaster')->opt_value; ?>

	  <?php if($p=='index' || $p=='page') { ?>

	  <title><?php echo $blogname?> <?php echo trim($URL['args'][1]) !== '' ? 'on Page '.$URL['args'][1] : ''; ?> | <?php echo $blogdesc;?></title>

	  <?php //$kwx = $db->get_results( "SELECT * FROM search_terms AS r1 JOIN (SELECT (RAND() * (SELECT MAX(id) FROM search_terms)) AS id) AS r2 WHERE r1.id >= r2.id AND r1.type='parent'  LIMIT 20" ); ?>

	  <?php $limit = 18; ?>
	  <?php $page = trim($URL['args'][1]) !== '' ? $URL['args'][1] : 1; ?>
	  <?php $offset = ($page - 1)  * $limit; ?>
	  <?php $kwx = $db->get_results( "SELECT * FROM search_terms WHERE type='parent' ORDER BY ID DESC LIMIT ".$offset.",18" ); ?>

	  <?php 

	    $d = ''; 

	    if( $kwx ):       
	      $i=0; 
	      foreach ($kwx as $kv) : 
	         $d .= removeSpecial(ucwords($kv->term)).'. ';
	         if($i > 5) break; 
	         $i++; 
	      endforeach; 
	    endif; 

	  ?>

	  <meta name="description" content="<?php echo $d ;?>">
	  <meta name="keywords" content="<?php echo $d ;?>">
	  <meta name="robots" content="index, follow">
	  <meta name="googlebot" content="index,follow,imageindex">
	  <link rel="canonical" href="http://<?php echo $_SERVER['SERVER_NAME']; ?>">

	  <?php } elseif ($p=='image') { ?>

	  <?php

	        $imgs = array();

	        $cache = new Cache();
	        $key = md5('single_post_'.$item->ID);

	        if($cache->isCached($key)){

	          $imgs = $cache->retrieve($key);

	        }else{

	            foreach($datas as $a) 
	            {

	                $im = $db->get_row("SELECT * FROM term_images where parent_term='".$a->ID."'");

	                if($im){

	                  $title = trim($a->term) == '' || ( count(explode(' ', $a->term)) < 2 ) ? ucwords($q) : ucwords($a->term);
	                  
	                  $childImgs = getChildImages($db,$im);

	                  $sql_fields = array(

	                      'term'  => $title,
	                      'url'  => SITE_URL.'imgs/'.$im->url,  
	                      'childs' => $childImgs,                  
	                      'height' =>$im->height,
	                      'width' => $im->width,
	                      'thumb' => $im->thumb,
	                      'type'    => $im->type

	                      ); 

	                  $imgs[] = $sql_fields;   
	                               
	                }

	            }

	          $imgs = is_term_safe_bulk($imgs);
	          $cache->setCache($key)->store($key, $imgs);

	        }

	  ?>
	  
	  <?php

	    $d = ''; 
	    if( $imgs ): 
	      $i=0; 
	      foreach ($imgs as $kv) : 
	         $d .= ucwords($kv['term']).'. ';
	         if($i > 5) break; 
	         $i++; 
	      endforeach; 
	    endif; 
	  
	  ?>

	  <title><?php echo ucwords(suffleTitleAwal());?> <?php echo ucwords($q);?></title>

	  <meta name="description" content="<?php echo ucwords(suffleTitleAwal());?> <?php echo $q;?>. Gallery images of <?php echo ucwords($q);?> Pictures.">
	  <meta name="keywords" content="<?php echo ucwords(suffleTitleAwal());?> <?php echo $q;?> photos. <?php echo $q;?> images. <?php echo $q;?> pictures.">
	  
	  <meta name="robots" content="all,index,follow">
	  <meta name="googlebot" content="index,follow,imageindex">
	  <meta name="googlebot-Image" content="index,follow">

	  <link rel="canonical" href="<?php echo _a_url_q( $q ); ?>">

	  <?php } elseif ($p=='attachment') { ?>
	  
	    <?php

	        $imgs = array();

	        $cache = new Cache();
	        $key = md5('single_post_'.$item->ID);

	        if($cache->isCached($key)){

	          $imgs = $cache->retrieve($key);

	        }else{

	            foreach($datas as $a) 
	            {

	                $im = $db->get_row("SELECT * FROM term_images where parent_term='".$a->ID."'");

	                if($im){

	                  $title = trim($a->term) == '' || ( count(explode(' ', $a->term)) < 2 ) ? ucwords($q) : ucwords($a->term);
	                  
	                  $childImgs = getChildImages($db,$im);

	                  $sql_fields = array(

	                      'term'  => $title,
	                      'url'  => SITE_URL.'imgs/'.$im->url,                  
	                      'childs' => $childImgs,                  
	                      'height' =>$im->height,
	                      'width' => $im->width,
	                      'thumb' => $im->thumb,
	                      'type'    => $im->type

	                      ); 

	                  $imgs[] = $sql_fields;   
	                               
	                }

	            }

	          $imgs = is_term_safe_bulk($imgs);
	          $cache->setCache($key)->store($key, $imgs);

	        }
	      
	  ?>

	  <title><?php echo ucwords($q);?> - <?php echo ucwords($item->term); ?></title>

	  <meta name="description" content="Photo : <?php echo ucwords($img['term']);?> - <?php echo ucwords($item->term); ?>">
	  <meta name="keywords" content="<?php echo ucwords($img['term']);?> - <?php echo ucwords($item->term); ?>">
	  
	  <meta name="robots" content="index, follow">
	  <meta name="googlebot" content="index,follow,imageindex">
	  <meta name="googlebot-Image" content="index,follow">

	  <link rel="canonical" href="<?php echo to_attachment($item->term,$q); ?>">

	  <?php } elseif ($p=='info') { ?>

	    <title><?php echo $headtitle ?></title>
	    <meta name="robots" content="noindex, follow">

	  <?php } elseif ($p=='map') { ?>

	  <title>Sitemap <?php echo $URL['args'][1]; ?></title>
	  <meta name="robots" content="noindex, follow">

	  <?php } elseif ($p=='go') { ?>

	  <title>Search for <?php echo $URL['args'][1]; ?></title>
	  <meta name="robots" content="noindex, follow">

	  <?php } else {?>

	    <title>Page not found!</title>
	    <meta name="robots" content="noindex, nofollow">
	    
	  <?php } ?>

	<?php
}

?>