<?php
/**
 * Find out if an image is from a badhost
 * 
 * @param  string   $imgsource
 * @return boolean
 */

require_once ABSPATH . 'vendor/autoload.php';
require_once AGCL_INC_PATH . 'bing/src/Bing.php';
require_once AGCL_INC_PATH . 'bing/src/Image.php';

use Buchin\Bing\Image;

function clean_string($string) {
    $to_replace = array('|','-','.','»','’','–','«','_','+','... ');
    $replace_with = array('',' ','','','','','',' ',' ','');
    $pre_clean =  str_replace($to_replace, $replace_with, $string);
    $clean = preg_replace('/\s+/', ' ',$pre_clean);
    return $clean;
}

  function to_prety_url($str){
      if($str !== mb_convert_encoding( mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32') )
      $str = mb_convert_encoding($str, 'UTF-8', mb_detect_encoding($str));
      $str = htmlentities($str, ENT_NOQUOTES, 'UTF-8');
      $str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\1', $str);
      $str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
      $str = preg_replace(array('`[^a-z0-9]`i','`[-]+`'), '-', $str);
      $str = strtolower( trim($str, '-') );
      return $str;
  }
    
 function checkImgNew($url){

      $fileExtentension = pathinfo($url);

      @$fileExtentension = $fileExtentension['extension'];

      if ($fileExtentension == 'jpg' || $fileExtentension == 'jpeg' || $fileExtentension == 'png' || $fileExtentension == 'gif') {

            $info = @getimagesize($url);

            if(is_array($info)){

              $errors = array_filter($info);

              if (empty($errors)) {

                  return false;            
              }

            }else{

              if ($info == false) {

                  return false;              
              }

            }  

        return $fileExtentension;
      }

    return false;
    
}

function is_badhost( $imgsource ) {

  $badhosts = array(
    'akamaiinnovations.com',
    'drwalraven.com',
    'biomedcentral.com',
  );

  $flags = array();
  
  foreach( $badhosts as $bh ):
    if( strpos($imgsource, $bh)!==FALSE ):
      $flags[] = TRUE;
    endif;
  endforeach;
  
  if( in_array(TRUE, $flags) )
    return TRUE;
    
  else
    return FALSE;
}

function checkImg($url) {

    $hdrs = @get_headers($url);

    echo @$hdrs[1]."\n";

    return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false;
    
}

function js_ishere($haystack, $needle) {
  if(!is_array($needle)) $needle = array($needle);
  foreach($needle as $what) {
    $what = strtolower($what);
    $pos = strpos($haystack, $what);
    if($pos !==false) 
    return true;
  }
  return false;
}

function ob_html_compress($buf){
    return preg_replace(array('/<!--(.*)-->/Uis',"/[[:blank:]]+/"),array('',' '),str_replace(array("\n","\r","\t"),'',$buf));
}

function getChildImages($db,$data){

  $imgs = array('large'=>array(),'medium' => array(),'thumb'=> array());

  $datas = $db->get_results( "SELECT * FROM term_images where parent_term = ".$data->ID." AND type != 'full'" );
  $datas = is_array($datas) ? $datas : array();

  foreach($datas as $d){

    switch ($d->type) {
      case 'large':
        
        $imgs['large'] = $d;

        break;
      case 'medium':
        
        $imgs['medium'] = $d;

        break;
      case 'thumb':

        $imgs['thumb'] = $d;

        break;
    }

  }

  return $imgs;

}

function doMagic($url,$proxy=''){
    $curl = curl_init();

    $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
    $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
    $header[] = "Cache-Control: max-age=0";
    $header[] = "Connection: keep-alive";
    $header[] = "Keep-Alive: 300";
    $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
    $header[] = "Accept-Language: en-us,en;q=0.5";
    $header[] = "Pragma: ";

    curl_setopt($curl, CURLOPT_URL, $url);
    if(trim($proxy) !== ''){      
      curl_setopt($curl, CURLOPT_PROXY, $proxy);
      //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
    }
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:7.0.1) Gecko/20100101 Firefox/7.0.12011-10-16 20:23:00");
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_REFERER, "https://www.bing.com");
    curl_setopt($curl, CURLOPT_ENCODING, "gzip,deflate");
    curl_setopt($curl, CURLOPT_AUTOREFERER, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION,true);

    $html = curl_exec($curl);

    if (curl_errno($curl)){

      curl_close($curl);
      return false;

    }
    else {

      curl_close($curl);
      return $html;

    }  

    return false;

  }

  function generateDefaultOptions($db,$val){
    
    $sql = array(

      'opt_name' => $val[0],
      'opt_code' => $val[1],
      'opt_value' => $val[2]

        ); 

    $db->query( construct_query_insert('site_options', $sql) );

  }

  function hasOption($db,$code){
    
    $h = false;
    
    $c = $db->get_row("SELECT * FROM site_options where opt_code='".$code."'");
    
    if(is_object($c)){
      if(trim($c->opt_value) !=='') $h = true;      
    }
    
    return $h;

  }

  function getOption($db,$code){

    $c = $db->get_row("SELECT * FROM site_options where opt_code='".$code."'");

    return $c;

  }

function doMagicDict($word){

  $tmp = array('head'=>array(), 'type' => 0 );

  if(trim($word) !== ''){

    $headers = array('Accept' => 'application/json');
    $query = array('headword' => $word, 'apikey' => KEY_ADDON_DICTIONARY);

    $response = Unirest\Request::get('http://api.pearson.com/v2/dictionaries/entries', $headers, $query);
    
    if(!isset($response->body->results)) return $tmp;
    
    foreach($response->body->results as $r){
      
      $cat = isset($r->part_of_speech) ? $r->part_of_speech : 'NoPart';
      
      if(!in_array($cat, $tmp['head'])) $tmp['head'][] = $cat; 
      
      if(!array_key_exists($cat, $tmp)) $tmp[$cat] = array('definition' => array(), 'examples' => array());

      if(isset($r->senses[0]->definition)){
        
        if(is_array($r->senses[0]->definition)){
          
          foreach($r->senses[0]->definition as $d){

            if(!in_array($d, $tmp[$cat]['definition'])) $tmp[$cat]['definition'][] = $d;

          }

        }else{

          if(!in_array($r->senses[0]->definition, $tmp[$cat]['definition'])) $tmp[$cat]['definition'][] =  $r->senses[0]->definition;

        }
        
        
      }

      if(isset($r->senses[0]->examples) && !in_array($r->senses[0]->examples[0]->text, $tmp[$cat]['examples'])) $tmp[$cat]['examples'][] =  $r->senses[0]->examples[0]->text;

    }

    $tmp['type'] = 1;

  }else{

    $tmp['type'] = 0;

  }

  return $tmp;

}

function doMagicGoogle($kw,$proxy = ''){

    $datas = array();

    $data = doMagic('https://www.google.com/search?q='.urlencode($kw).'&source=lnms&tbm=isch',$proxy);

    if($data){

      $dom = NEW DOMDOcument();
      @$dom->loadHTML($data);
      $link = $dom->getElementsByTagName('div');

        foreach($link as $a) 
        {
          
          if (strpos($a->getAttribute('class'), 'rg_meta') !== false){

            $a_content = json_decode($a->textContent); 

            $datas[] = array(

              'title'  => trim($a_content->s) == '' ? ucwords($kw) : ucwords($a_content->s),
              'url'  => $a_content->ou,
              'height' => $a_content->oh,
              'width' => $a_content->ow,
              'thumb' => $a_content->tu,
              'type'    => 'jpeg'

              ); 

          }

        }    
            
    }

    return $datas;

}

function doMagicBing($kw){

  $imageScraper = new Image;

  $data = array();

  $niche = array(

    'default' => array(
      'name' => 'Home Decor Gallery',
      'desc' => 'Find New Home Decor Design',
      'key' => array('home decorators collection', 'home decorating ideas', 'Pinterest Home Decor'),
      'keygrab' => array("Decorating Ideas", "Design Ideas","Decoration Ideas","Ideas","Design")
      ),
    'wall' => array(
      'name' => 'Home Decor Gallery',
      'desc' => 'Find New Home Decor Design',
      'key' => array('home decorators collection', 'home decorating ideas', 'Pinterest Home Decor'),
      'keygrab' => array("Decorating Ideas", "Design Ideas","Decoration Ideas","Ideas","Design")
      ),
    'wiring' => array(
      'name' => 'Wiring Diagram Gallery',
      'desc' => 'Find New Wiring Diagram Design',
      'key' => array('wiring diagram collection', 'wiring diagram ideas', 'Pinterest Wiring Diagram'),
      'keygrab' => array("Ideas", "Design")
      )

    );

    $nicheindex = array_key_exists(NICHE, $niche) ? NICHE : 'default';

      foreach($niche[$nicheindex]['keygrab'] as $b){
        
          $q = (strpos($kw, $b) !== false) ? $kw : $kw .' '.$b;
          $resuls = $imageScraper->scrape($q);

          foreach($resuls as $fook){
            
            $data[] = array(

              'url' => $fook['mediaurl'],
              'title' => $fook['title'],
              'thumb' => $fook['thumburl'],
              'width' => explode(' ', $fook['size'])[0],
              'height' => explode(' ', $fook['size'])[2],
              'type' => explode(' ', $fook['size'])[3]

              );

          }

      }

  return $data;
  
}

function removeSpecial($string){

   $string = implode(' ',array_unique(explode(' ',str_replace('.','',$string))));
   $string = str_replace(' ', '-', $string);
   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);

   $string = preg_replace('/-+/', '-', $string);
  
   return str_replace('-', ' ', $string);

}

function _a_url_image( $item ) {
  return SITE_URL . 'img/'. $item->ID . '-' .clean($item->term) .'.'. pathinfo($item->url, PATHINFO_EXTENSION);
}

function suffleTitleAwal(){

  $awal = array('Stunning','Dazzling','Delightful','Trendy','Wonderful','Lovely','Charming','Good Looking','Outstanding','Attracktive','Engaging','Impressive','Gorgeous','Fascinating','Magnificent','Alluring','Excelent','Elegant','New', 'Cool', 'Unique', 'Nice', 'Luxury', 'Modest', 'Awesome', 'Amazing', 'Fresh', 'Popular', 'Awesome', 'Custom', 'Modern', 'Inspiring' , 'Simple', 'Classic');

  return $awal[array_rand($awal)];

}

function suffleCat(){

  $c = array(

    'dinning',
    'bedroom',
    'bathroom',
    'kitchen',
    'living',
    'chair',
    'curtain',
    'sofa',
    'bedding',
    'apartment',
    'dresser',
    'table',
    'floor',
    'lamps',
    'fireplace',

    );

  $b = $c[array_rand($c)];

  $d = array(

    '<strong>'.$b.'</strong>',
    '<u>'.$b.'</u>',
    '<em>'.$b.'</em>'

    );

  return $d[array_rand($d)];

}

function suffleTitleIndex($string,$i){

  $h = array('h2','h3','h4','h5','h6','h2','h3','h4','h5','h6','h2','h3','h4','h5','h6','h2','h3','h4','h5','h6');

  $r = array_key_exists($i, $h) ? '<'.$h[$i].' class="hh">'.$string.'</'.$h[$i].'>' : '<h2 class="hh">'.$string.'</h2>';

  return $r;

}

function suffleTitleSingle($string,$i){

  $h = array('h2','h3','h4','h5','h6','h2','h3','h4','h5','h6','h2','h3','h4','h5','h6','h2','h3','h4','h5','h6','h2','h3','h4','h5','h6','h2','h3','h4','h5','h6','h2','h3','h4','h5','h6','h2','h3','h4','h5','h6','h2','h3','h4','h5','h6');

  $r = array_key_exists($i, $h) ? '<'.$h[$i].' itemprop="name description">'.$string.'</'.$h[$i].'>' : '<h2 itemprop="name description">'.$string.'</h2>';

  return $r;

}

function suffleTitle($string,$url){

  $bungkus = array(

    '<h2 class="single-header" itemprop="name">'.$string.'</h2><a target="_blank" href="'.$url.'">.</a>',
    '<h3 class="single-header" itemprop="name">'.$string.'</h3><a target="_blank" href="'.$url.'">.</a>',
    '<h4 class="single-header" itemprop="name">'.$string.'</h4><a target="_blank" href="'.$url.'">.</a>',
    '<h5 class="single-header" itemprop="name">'.$string.'</h5><a target="_blank" href="'.$url.'">.</a>',
    '<h6 class="single-header" itemprop="name">'.$string.'</h6><a target="_blank" href="'.$url.'">.</a>'
    
    );

  return $bungkus[array_rand($bungkus)];

}

function suffleTitleFull($string,$url){

  $bungkus = array(
    '<h2 class="single-header" itemprop="name description">'.$string.'</h2>',
    '<h3 class="single-header" itemprop="name description">'.$string.'</h3>',
    '<h4 class="single-header" itemprop="name description">'.$string.'</h4>',
    '<h5 class="single-header" itemprop="name description">'.$string.'</h5>',
    '<h6 class="single-header" itemprop="name description">'.$string.'</h6>',
    '<p class="caption"><strong itemprop="name description">'.$string.'</strong></p>',
    '<p class="caption"><u itemprop="name description">'.$string.'</u></p>',
    '<p class="caption"><em itemprop="name description">'.$string.'</em></p>'
    );

  return $bungkus[array_rand($bungkus)];

}

function suffleTitleP($string,$url){

  $bungkus = array(
    '<p class="caption" itemprop="name"><strong>'.$string.'</strong></p><a target="_blank" href="'.$url.'">.</a>',
    '<p class="caption" itemprop="name"><u>'.$string.'</u><a target="_blank" href="'.$url.'">.</a></p>',
    '<p class="caption" itemprop="name"><em>'.$string.'</em><a target="_blank" href="'.$url.'">.</a></p>'
    );

  return $bungkus[array_rand($bungkus)];

}

function suffleTitleMask($string,$item){

  $bungkus = array(
    '<h2 class="single-header" itemprop="name">'.$string.'</h2>',
    '<h3 class="single-header" itemprop="name">'.$string.'</h3>',
    '<h4 class="single-header" itemprop="name">'.$string.'</h4>',
    '<h5 class="single-header" itemprop="name">'.$string.'</h5>',
    '<h6 class="single-header" itemprop="name">'.$string.'</h6>',
    '<p class="caption" itemprop="name"><strong>'.$string.'</strong></p>',
    '<p class="caption" itemprop="name"><u>'.$string.'</u>',
    '<p class="caption" itemprop="name"><em>'.$string.'</em>'
    );

  return $bungkus[array_rand($bungkus)];

}

/**
 * Check if term is found in badwords.txt file
 *
 * @return boolean
 */
function is_term_safe( $term ) {

  $badwordt = file_exists(ABSPATH . 'badwords.txt') ? file_get_contents(ABSPATH . 'badwords.txt') : '';
  $badwords = explode("\n", strtolower($badwordt));

  $caughts  = array();
  foreach ( $badwords as $t) {

    $t = trim($t);
    $pattern = "#{$t}#i";
    preg_match($pattern, $term, $matches);
    if ( !empty($matches) )
      $caughts[] = $t;

  }

  // return FALSE if term given is found inside badwords
  if ( empty($caughts) )
    return TRUE;

  return FALSE;

}

function is_term_safe_bulk( $arr ) {

  $badwordt = file_exists(ABSPATH . 'badwords.txt') ? file_get_contents(ABSPATH . 'badwords.txt') : '';
  $badwords = explode("\n", strtolower($badwordt));

  $results = array();
  $i=0;

  foreach($arr as $term){

    $check = false;
    $term = removeSpecial($term['term']);

    $caughts  = array();
    foreach ( $badwords as $t) {

      $t = trim($t);
      $pattern = "#{$t}#i";
      preg_match($pattern, $term, $matches);
      if ( !empty($matches) )
        $caughts[] = $t;

    }

    // return FALSE if term given is found inside badwords
    if ( empty($caughts) ) $results[] = $arr[$i];
  
    $i++;

  }

  return $results;

}


/**
 * Translates URL in the browser's address bar into 
 * controller name and arguments
 * 
 * @return array
 */
function translate_URL() {

  $URL = array(
      'controller'    => '',
      'args' => array(
          1 => '',
          2 => '',
          3 => '',
          4 => '',
          5 => '',
          6 => '',
          7 => '',
          8 => '',
          9 => '',
      ),
  );
  
  $scheme = defined('SITE_SCHEME') ? SITE_SCHEME : 'http';
  $address_URL  = $scheme . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  $slashed_URL  = str_replace( SITE_URL, '', $address_URL);
  
  // unslash it. always
  $request_uri = trim($slashed_URL, '/');

  // remove AGCL_URL_SUFFIX 
  $request_uri = str_replace(AGCL_URL_SUFFIX, '', $request_uri);

  // Clean the URL parameter from its arguments --> http://www.example.com/foo/bar/?redirect=true&logged_in=true
  $posask = strpos($request_uri, '?');
  if( $posask !== FALSE )
    $clean_request_uri = trim( substr ($request_uri, 0, $posask), '/');
  else 
    $clean_request_uri = trim($request_uri, '/');


  // split the request URI segments into array
  $segments = explode('/', $clean_request_uri);

  // Count how many segments are in the URL
  $url_seg_cnt = count($segments);


  if(  isset($segments[0]) && $segments[0] == '' || 
      !isset($segments[0]) ) {

    $URL['controller'] = 'index';

  } else {

    // Set the first array as the controller's name
    $URL['controller'] = $segments[0];

    // Set the rest as controller arguments
    for($i=1; $i<$url_seg_cnt; $i++) {
      $URL['args'][$i] = $segments[$i];
    }

  }

  return $URL;

}


/**
 * Find out if we're working on localhost dev or live production server
 * 
 * @return boolean 
 */
function is_localhost() {
  
  if( $_SERVER['SERVER_NAME'] == 'localhost')
    return TRUE;
  
  return FALSE;
}

function _a_url_q( $str ) {
  return SITE_URL . clean($str) . AGCL_URL_SUFFIX;
}

function to_attachment( $p,$c ) {
  return SITE_URL . clean($p) .'/'.clean($c) . AGCL_URL_SUFFIX;
}

function clean($str, $replace=array(), $delimiter='-') {
  if( !empty($replace) ) {
    $str = str_replace((array)$replace, ' ', $str);
  }

  $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
  $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
  $clean = strtolower(trim($clean, '-'));
  $clean = trim($clean);
  $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

  return trim($clean);
}


function clean2($str, $replace=array(), $delimiter=' ') {
  if( !empty($replace) ) {
    $str = str_replace((array)$replace, ' ', $str);
  }

  //$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
  $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
  $clean = ucfirst(trim($clean, '-'));
  $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

  return trim($clean);
}

require_once AGCL_INC_PATH . '/shared/pq.php';
function fix_json( $j ){
        $j = trim( $j );
        $j = ltrim( $j, '(' );
        $j = rtrim( $j, ')' );
        $a = preg_split('#(?<!\\\\)\"#', $j );
        
        for( $i=0; $i < count( $a ); $i+=2 ){
            $s = $a[$i];
            $s = preg_replace('#([^\s\[\]\{\}\:\,]+):#', '"\1":', $s );
            $a[$i] = $s;
        }
        
        $j = implode( '"', $a );
        
        return $j;
    }

function fetch($url,$limit) {
       $doc = @file_get_contents($url);
       phpQuery::newDocument($doc);
       $images = array();
       $i = 0;
       foreach(pq('div.dg_u a') as $a){
       	if($i==$limit) { break; }
           $raw_image = pq($a)->attr('m');
           $urls = json_decode(fix_json($raw_image), true);
           
           // let's create image properties
           $image['link'] = $urls['surl'];
           $image['mediaurl'] = $urls['imgurl'];
           $image['title'] = pq($a)->attr('t1');
           $image['size'] = pq($a)->attr('t2');
           
           $images[] = $image;
           $i++;
       } 
       return $images;
   }


function base_url($url){
 $parse = parse_url($url);
 return $parse['host'];
}


function tagging($k,$domain){
  $tag = strtolower($k);
  $tag = preg_replace('/([^a-z]+)/i',' ',$tag);
  $tag = trim($tag);
  $tag = explode(' ', $tag);
  $tag = array_unique($tag);
  sort($tag);
  foreach($tag as $tags){
    if(strlen($tags)>3){
    echo "<a class='tags' href='$domain/tag/".clean($tags)."'>$tags</a>&nbsp;";
    }
  }
}

function tagging2($k,$domain){
  $tag = array_unique($k);
  sort($tag);
  foreach($tag as $tags){
    if(strlen($tags)>3){
    echo "<a class='tags' href='$domain/tag/".clean($tags)."'>".strtolower($tags)."</a>&nbsp;";
    }
  }
}

function fwd($kw){
  $fw = explode(' ',trim($kw));
  return $fw[1]; // will print Test
}

function generateCssClub($tag){

  return 'html,
body,
body div,
span,
object,
iframe,
h1,
h2,
h3,
h4,
h5,
h6,
p,
blockquote,
pre,
a,
abbr,
address,
cite,
code,
del,
dfn,
em,
img,
ins,
kbd,
q,
samp,
small,
strong,
sub,
sup,
var,
b,
i,
dl,
dt,
dd,
ol,
ul,
li,
fieldset,
form,
label,
legend,
table,
caption,
tbody,
tfoot,
thead,
tr,
th,
td,
article,
aside,
figure,
footer,
header,
hgroup,
menu,
nav,
section,
time,
mark,
audio,
video,
hr {
    margin: 0;
    padding: 0;
    border: 0;
    outline: 0;
    font-size: 100%;
    vertical-align: baseline;
    background: transparent
}

body {
    background: #E6E7E8;
    font-family: "Roboto", sans-serif;
    color: #555
}

h1,
h2,
h3,
h4,
h5,
h6 {
    font-family: Arial, sans-serif
}

a {
    text-decoration: none
}

#pp {
    padding: 25px;
}

#pp ul li {
    list-style: none;
}

,
a:hover {
    color: #F50057
}

strong,
em,
u {
    font-weight: normal;
    font-style: normal;
    text-decoration: none
}

.'.$tag.'r {
    clear: both
}

#n {
    background: #00AEEF;
    width: 100%
}

.'.$tag.'m0 {
    margin: 0 auto;
    height: 20px;
    padding: 15px 0;
    position: relative
}

.'.$tag.'m0 .'.$tag.'fa {
    float: left;
    padding: 15px;
    font-size: 20px;
    color: #fff;
    cursor: pointer;
    margin-top: -15px
}

.'.$tag.'h1 a {
    color: #fff;
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%
}

.'.$tag.'h1 {
    position: relative;
    color: #fff;
    text-transform: capitalize;
    float: left;
    margin-left: 5px
}

.'.$tag.'mn {
    width: 300px;
    height: auto;
    background: #0698DD;
    position: absolute;
    top: -999px;
    left: 0;
    z-index: 3
}

.'.$tag.'mn ul {
    padding: 15px 5px;
    list-style: none
}

.'.$tag.'mn li {
    padding: 0 15px;
    margin-bottom: 15px
}

.'.$tag.'mn li a {
    color: #fff
}

#mn:hover .'.$tag.'fa,
#s1 .'.$tag.'fa {
    background: #0698DD
}

#mn:hover .'.$tag.'mn {
    top: 50px
}

#s1 {
    float: right;
    position: relative;
    color: #fff
}

#s1 .'.$tag.'fa {
    float: right;
    border: none;
    padding: 15px
}

#s2 {
    width: 270px;
    height: auto;
    background: #0698DD;
    position: absolute;
    top: -999px;
    right: 0;
    z-index: 3;
    padding: 15px
}

#s1:hover #s2 {
    top: 35px
}

#s3 {
    border: none;
    width: 160px;
    padding: 5px 15px
}

#s4 {
    border: none;
    padding: 5px 15px;
    background: #00AEEF;
    color: #fff
}

#a {
    margin: 15px auto 15px
}

.'.$tag.'c,
.'.$tag.'d,
.'.$tag.'cf {
    width: 300px;
    float: left;
    background: #fff;
    margin-right: 15px;
    margin-bottom: 15px
}

.'.$tag.'cf {
    margin-right: 0
}

.'.$tag.'gi {
    position: relative;
    height: 250px;
    overflow: hidden
}

.'.$tag.'gi img {
    width: 300px;
    height: 250px
}

.'.$tag.'is {
    position: relative;
    overflow: hidden
}

.'.$tag.'is img {
    max-width: 100%;
    -moz-transition: all 0.3s;
    -webkit-transition: all 03s;
    transition: all 0.3s
}

.'.$tag.'is:hover img {
    -moz-transform: scale(1.1);
    -webkit-transform: scale(1.1);
    transform: scale(1.1)
}

.'.$tag.'gi a {
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
    z-index: 2
}

.'.$tag.'gi:after {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    background: #222;
    background: -webkit-linear-gradient(rgba(34, 34, 34, 0) 54%, rgba(34, 34, 34, .2) 71%, rgba(34, 34, 34, .4) 80%, rgba(34, 34, 34, .6) 90%, rgba(34, 34, 34, .8));
    background: -o-linear-gradient(rgba(34, 34, 34, 0) 54%, rgba(34, 34, 34, .2) 71%, rgba(34, 34, 34, .4) 80%, rgba(34, 34, 34, .6) 90%, rgba(34, 34, 34, .8));
    background: -moz-linear-gradient(rgba(34, 34, 34, 0) 54%, rgba(34, 34, 34, .2) 71%, rgba(34, 34, 34, .4) 80%, rgba(34, 34, 34, .6) 90%, rgba(34, 34, 34, .8));
    background: linear-gradient(rgba(34, 34, 34, 0) 54%, rgba(34, 34, 34, .2) 71%, rgba(34, 34, 34, .4) 80%, rgba(34, 34, 34, .6) 90%, rgba(34, 34, 34, .8))
}

.'.$tag.'ct {
    position: absolute;
    left: 5px;
    bottom: 5px;
    color: #fff;
    border: 1px solid #00AEEF;
    padding: 5px 10px;
    z-index: 3;
    font-size: 12px;
    letter-spacing: 1px
}

.'.$tag.'li {
    position: absolute;
    right: 0;
    top: -16px
}

.'.$tag.'li a {
    color: #00AEEF
}

.'.$tag.'c:hover .'.$tag.'ct {
    background: #00AEEF;
    color: #fff
}

.'.$tag.'cp {
    position: relative;
    padding: 15px;
    color: #818181
}

.'.$tag.'ch {
    text-align: center
}

.'.$tag.'ch a {
    position: absolute;
    height: 100%;
    width: 100%;
    left: 0;
    top: 0
}

.'.$tag.'hh {
    height: 20px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    color: #555;
    text-transform: capitalize
}

.'.$tag.'c:hover .'.$tag.'hh,
.'.$tag.'d:hover .'.$tag.'hh {
    color: #F50057
}

.'.$tag.'ah {
    font-size: 11px;
    text-align: center;
    margin-top: 15px;
    letter-spacing: 1px
}

.'.$tag.'ah i {
    margin-right: 5px;
    color: #F50057
}

.'.$tag.'m5 {
    margin-left: 10px
}

.'.$tag.'pn {
    width: 300px;
    float: right;
    margin-bottom: 15px
}

.'.$tag.'pn a {
    color: #fff
}

.'.$tag.'nx,
.'.$tag.'pv {
    padding: 10px 20px;
    background: #F50057;
    float: right;
    margin-left: 1px;
    font-style: normal;
    color: #fff;
    font-size: 12px
}

.'.$tag.'nx:hover,
.'.$tag.'pv:hover {
    background: #00AEEF
}

.'.$tag.'vn {
    width: 100px;
    overflow: hidden;
    color: #F50057;
    font-size: 14px;
    position: absolute;
    left: 0;
    top: 0;
    margin-top: -16px
}

.'.$tag.'vn a {
    color: #F50057
}

#i,
#p {
    float: left;
    background: #fff;
    margin-bottom: 15px;
    color: #555
}

#i blockquote {
    margin: 15px;
    font-size: 14px;
    border: 1px solid #dedede;
    padding: 15px;
    text-transform: lowercase;
    font-style: italic
}

#i figure {
    margin-bottom: 15px;
    position: relative
}

a.'.$tag.'al {
    position: absolute;
    bottom: 23px;
    right: 0;
    background: rgba(0, 0, 0, 0.5);
    padding: 5px 10px;
    color: #fff
}

a.'.$tag.'al:hover {
    color: #00AEEF
}

h1.'.$tag.'rs {
    padding: 0 15px;
    text-transform: capitalize
}

#bc {
    padding: 5px 15px 15px;
    font-size: 11px;
    font-style: normal
}

#bc i {
    font-style: normal;
    text-transform: lowercase
}

#bc a {
    color: #00AEEF
}

#i .'.$tag.'h4 {
    margin: 5px 15px;
    font-size: 12px;
    text-transform: lowercase;
    text-align: center;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap
}

#i h1 {
    width: 90%;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap
}

#i img {
    width: 100%;
    height: auto;
    max-height: 345px
}

.'.$tag.'sn {
    float: right;
    margin: 5px 15px;
    height: 30px
}

.'.$tag.'sn a {
    color: #fff;
    font-size: 12px;
    background: #F50057;
    padding: 10px 15px;
    margin-left: 1px
}

.'.$tag.'sn a:hover {
    background: #00AEEF
}

.'.$tag.'ba {
    float: left
}

.'.$tag.'ba a {
    background: #818181
}

.'.$tag.'ba a:hover {
    background: #F50057
}

.'.$tag.'tg {
    margin: 15px;
    padding: 15px;
    border: 1px solid #dedede;
    font-size: 14px
}

.'.$tag.'tg b {
    margin-right: 5px
}

.'.$tag.'tg a {
    margin-right: 5px;
    color: #818181;
    font-size: 14px
}

.'.$tag.'tg a:hover,
.'.$tag.'p a:hover {
    color: #F50057
}

#g {
    width: 100%;
    height: auto;
    margin-bottom: 15px;
    background: #fff
}

#g img {
    width: 100%;
    height: auto
}

#g h1 {
    background: #fff;
    padding: 15px 15px 5px
}

#t {
    float: right;
    background: #fff;
    position: relative;
    margin-bottom: 15px
}

#t ul {
    padding: 0 15px;
    list-style: none
}

#t li {
    padding: 0 15px 15px;
    position: relative
}

#t li:before,
.'.$tag.'p li:before {
    position: absolute;
    content: "\f101";
    font-family: "FontAwesome";
    font-size: 16px;
    color: #818181;
    left: 0px;
    top: 0px
}

#t h4 {
    margin: 15px;
    border-bottom: 1px solid #dedede;
    text-align: center;
    padding-bottom: 15px
}

#t a {
    color: #818181;
    font-size: 14px
}

#v {
    overflow: hidden;
    float: left
}

#v .'.$tag.'d:nth-child(2n) {
    margin-right: 0
}

h1.'.$tag.'rh {
    background: #fff;
    padding: 15px
}

.'.$tag.'bc {
    background: #fff;
    padding: 0 15px 15px !important
}

#fr img,
#rp img,
.'.$tag.'rf img,
#rc img {
    width: 120px;
    height: 120px
}

#rp,
.'.$tag.'rf {
    padding: 15px
}

.'.$tag.'rf .'.$tag.'di,
#rc .'.$tag.'di {
    width: 100%
}

.'.$tag.'rf .'.$tag.'di,
#rc .'.$tag.'di,
#rp .'.$tag.'di {
    height: 120px;
    margin-bottom: 5px;
    position: relative
}

.'.$tag.'di a {
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0
}

#rp p,
.'.$tag.'rf p,
#rc p {
    margin-bottom: 15px;
    text-align: center;
    font-weight: bold;
    border-bottom: 1px solid #dedede;
    padding-bottom: 15px
}

.'.$tag.'rf p,
#rc p {
    text-align: left
}

#rp .'.$tag.'hd,
.'.$tag.'rf .'.$tag.'hd,
#rc .'.$tag.'hd {
    border: none;
    float: right;
    width: 140px;
    color: #818181;
    font-size: 14px;
    font-weight: normal;
    height: 80px;
    overflow: hidden;
    padding: 0;
    text-align: left;
    margin: 0
}

.'.$tag.'rf .'.$tag.'hd {
    font-size: 16px;
    color: #555;
    height: 74px
}

#rp .'.$tag.'rg,
.'.$tag.'rf .'.$tag.'rg,
#rc .'.$tag.'rg {
    float: left;
    width: 120px
}

.'.$tag.'rc-l{
    width: 336px !important;
    margin-left: 3px;
    margin-right: 3px;
    padding: 0 !important;
    height: 280px;
}

.'.$tag.'rc-r{
    width: 270px !important;
    margin-right: 3px;
    padding: 0 !important;
}
#rc {
    width: 270px;
    padding: 15px 0 15px 15px;
    float: left
}

#rp .'.$tag.'di:hover .'.$tag.'hd,
#rc .'.$tag.'di:hover .'.$tag.'hd,
#fr:hover .'.$tag.'hd,
.'.$tag.'rf .'.$tag.'di:hover .'.$tag.'hd,
#t li:hover a,
.'.$tag.'cf li:hover a {
    color: #F50057
}

#fr {
    width: 100%;
    margin-bottom: 15px
}

#fr img {
    width: 100%;
    height: auto
}

#fr .'.$tag.'di {
    background: #fff;
    position: relative
}

#fr .'.$tag.'hd {
    text-align: center;
    padding: 15px;
    color: #777;
    text-transform: capitalize
}

.'.$tag.'p {
    padding: 15px
}

.'.$tag.'p h3 {
    margin: 15px 0
}

.'.$tag.'p ul {
    list-style: none
}

.'.$tag.'p li {
    position: relative
}

.'.$tag.'p li {
    padding-left: 15px
}

.'.$tag.'p a {
    color: #818181
}

#s {
    width: 100%;
    background: #fff;
    position: relative;
    border-top: 1px solid #F50057;
    background: #fff;
    padding: 15px 0
}

.'.$tag.'cf .'.$tag.'mu {
    padding: 15px;
    border-bottom: 1px solid #dedede;
    font-weight: bold
}

.'.$tag.'cf ul {
    padding: 15px;
    list-style: none
}

.'.$tag.'cf li a {
    color: #818181;
    margin-left: 5px
}

#ipt {
    width: 100%;
    padding: 15px 0;
    background: #C5EFF7;
    color: #fff;
    font-size: 12px;
    text-align: center
}

#ipt a {
    font-size: 12px;
    color: #fff
}

#st {
    width: 100%;
    padding: 15px 0;
    background: #00AEEF;
    color: #fff;
    font-size: 12px;
    text-align: center
}

#st a {
    font-size: 12px;
    color: #fff
}

.'.$tag.'s {
    margin: 0 auto;
    text-align: center;
}

.'.$tag.'ab {
    width: 100%;
    height: 378px
}

.'.$tag.'ds {
    width: 100%;
    height: 90px;
    padding: 5px 0
}

.'.$tag.'ki {
    width: 100%;
    margin: 5px 0;
    height: 410px
}

.'.$tag.'kt {
    width: 100%;
    margin: 5px 0;
    height: 600px
}

@media screen and (min-width:320px) {
    #a,
    .'.$tag.'m0,
    .'.$tag.'s,
    #i,
    #v {
        width: 300px
    }
    .'.$tag.'h1 {
        width: 85px;
        height: 20px;
        overflow: hidden
    }
    #t {
        width: 300px
    }
}

@media screen and (min-width:768px) {
    #a,
    .'.$tag.'m0,
    .'.$tag.'s,
    #i,
    #v,
    #t {
        width: 615px
    }
    .'.$tag.'c:nth-child(2n) {
        margin-right: 0
    }
    #rp .'.$tag.'di {
        width: 270px;
        float: left;
        margin-right: 15px
    }
}

@media screen and (min-width:980px) {
    #a,
    .'.$tag.'m0,
    .'.$tag.'s {
        width: 930px
    }
    #i,
    #v,
    #p {
        width: 615px
    }
    .'.$tag.'c:nth-child(2n) {
        margin-right: 15px
    }
    .'.$tag.'c:nth-child(3n) {
        margin-right: 0
    }
    #t {
        width: 300px
    }
    #rp .'.$tag.'di {
        width: 100%
    }
    .'.$tag.'h1 {
        width: 300px
    }
}';

}

function generateCssBs($tag){

    return '
  html {
    font-family: sans-serif;
    -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
  }
  body {
    margin: 0;
  }
  article,
  aside,
  details,
  figcaption,
  figure,
  footer,
  header,
  hgroup,
  main,
  menu,
  nav,
  section,
  summary {
    display: block;
  }
  audio,
  canvas,
  progress,
  video {
    display: inline-block;
    vertical-align: baseline;
  }
  audio:not([controls]) {
    display: none;
    height: 0;
  }
  [hidden],
  template {
    display: none;
  }
  a {
    background-color: transparent;
  }
  a:active,
  a:hover {
    outline: 0;
  }
  abbr[title] {
    border-bottom: 1px dotted;
  }
  b,
  strong {
    font-weight: bold;
  }
  dfn {
    font-style: italic;
  }
  h1 {
    margin: .67em 0;
    font-size: 2em;
  }
  mark {
    color: #000;
    background: #ff0;
  }
  small {
    font-size: 80%;
  }
  sub,
  sup {
    position: relative;
    font-size: 75%;
    line-height: 0;
    vertical-align: baseline;
  }
  sup {
    top: -.5em;
  }
  sub {
    bottom: -.25em;
  }
  img {
    border: 0;
  }
  svg:not(:root) {
    overflow: hidden;
  }
  figure {
    margin: 1em 40px;
  }
  hr {
    height: 0;
    -webkit-box-sizing: content-box;
       -moz-box-sizing: content-box;
            box-sizing: content-box;
  }
  pre {
    overflow: auto;
  }
  code,
  kbd,
  pre,
  samp {
    font-family: monospace, monospace;
    font-size: 1em;
  }
  button,
  input,
  optgroup,
  select,
  textarea {
    margin: 0;
    font: inherit;
    color: inherit;
  }
  button {
    overflow: visible;
  }
  button,
  select {
    text-transform: none;
  }
  button,
  html input[type="button"],
  input[type="reset"],
  input[type="submit"] {
    -webkit-appearance: button;
    cursor: pointer;
  }
  button[disabled],
  html input[disabled] {
    cursor: default;
  }
  button::-moz-focus-inner,
  input::-moz-focus-inner {
    padding: 0;
    border: 0;
  }
  input {
    line-height: normal;
  }
  input[type="checkbox"],
  input[type="radio"] {
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
    padding: 0;
  }
  input[type="number"]::-webkit-inner-spin-button,
  input[type="number"]::-webkit-outer-spin-button {
    height: auto;
  }
  input[type="search"] {
    -webkit-box-sizing: content-box;
       -moz-box-sizing: content-box;
            box-sizing: content-box;
    -webkit-appearance: textfield;
  }
  input[type="search"]::-webkit-search-cancel-button,
  input[type="search"]::-webkit-search-decoration {
    -webkit-appearance: none;
  }
  fieldset {
    padding: .35em .625em .75em;
    margin: 0 2px;
    border: 1px solid #c0c0c0;
  }
  legend {
    padding: 0;
    border: 0;
  }
  textarea {
    overflow: auto;
  }
  optgroup {
    font-weight: bold;
  }
  table {
    border-spacing: 0;
    border-collapse: collapse;
  }
  td,
  th {
    padding: 0;
  }
  /*! Source: https://github.com/h5bp/html5-boilerplate/blob/master/src/css/main.css */
  @media print {
    *,
    *:before,
    *:after {
      color: #000 !important;
      text-shadow: none !important;
      background: transparent !important;
      -webkit-box-shadow: none !important;
              box-shadow: none !important;
    }
    a,
    a:visited {
      text-decoration: underline;
    }
    a[href]:after {
      content: " (" attr(href) ")";
    }
    abbr[title]:after {
      content: " (" attr(title) ")";
    }
    a[href^="#"]:after,
    a[href^="javascript:"]:after {
      content: "";
    }
    pre,
    blockquote {
      border: 1px solid #999;

      page-break-inside: avoid;
    }
    thead {
      display: table-header-group;
    }
    tr,
    img {
      page-break-inside: avoid;
    }
    img {
      max-width: 100% !important;
    }
    p,
    h2,
    h3 {
      orphans: 3;
      widows: 3;
    }
    h2,
    h3 {
      page-break-after: avoid;
    }
    .'.$tag.'navbar {
      display: none;
    }
    .'.$tag.'btn > .'.$tag.'caret,
    .'.$tag.'dropup > .'.$tag.'btn > .'.$tag.'caret {
      border-top-color: #000 !important;
    }
    .'.$tag.'label {
      border: 1px solid #000;
    }
    .'.$tag.'table {
      border-collapse: collapse !important;
    }
    .'.$tag.'table td,
    .'.$tag.'table th {
      background-color: #fff !important;
    }
    .'.$tag.'table-bordered th,
    .'.$tag.'table-bordered td {
      border: 1px solid #ddd !important;
    }
  }
  @font-face {
    font-family: "Glyphicons Halflings";

    src: url("../fonts/glyphicons-halflings-regular.eot");
    src: url("../fonts/glyphicons-halflings-regular.'.$tag.'eot?#iefix") format("embedded-opentype"), url("../fonts/glyphicons-halflings-regular.'.$tag.'woff2") format("woff2"), url("../fonts/glyphicons-halflings-regular.'.$tag.'woff") format("woff"), url("../fonts/glyphicons-halflings-regular.'.$tag.'ttf") format("truetype"), url("../fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular") format("svg");
  }
  .'.$tag.'glyphicon {
    position: relative;
    top: 1px;
    display: inline-block;
    font-family: "Glyphicons Halflings";
    font-style: normal;
    font-weight: normal;
    line-height: 1;

    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  .'.$tag.'glyphicon-asterisk:before {
    content: "\002a";
  }
  .'.$tag.'glyphicon-plus:before {
    content: "\002b";
  }
  .'.$tag.'glyphicon-euro:before,
  .'.$tag.'glyphicon-eur:before {
    content: "\20ac";
  }
  .'.$tag.'glyphicon-minus:before {
    content: "\2212";
  }
  .'.$tag.'glyphicon-cloud:before {
    content: "\2601";
  }
  .'.$tag.'glyphicon-envelope:before {
    content: "\2709";
  }
  .'.$tag.'glyphicon-pencil:before {
    content: "\270f";
  }
  .'.$tag.'glyphicon-glass:before {
    content: "\e001";
  }
  .'.$tag.'glyphicon-music:before {
    content: "\e002";
  }
  .'.$tag.'glyphicon-search:before {
    content: "\e003";
  }
  .'.$tag.'glyphicon-heart:before {
    content: "\e005";
  }
  .'.$tag.'glyphicon-star:before {
    content: "\e006";
  }
  .'.$tag.'glyphicon-star-empty:before {
    content: "\e007";
  }
  .'.$tag.'glyphicon-user:before {
    content: "\e008";
  }
  .'.$tag.'glyphicon-film:before {
    content: "\e009";
  }
  .'.$tag.'glyphicon-th-large:before {
    content: "\e010";
  }
  .'.$tag.'glyphicon-th:before {
    content: "\e011";
  }
  .'.$tag.'glyphicon-th-list:before {
    content: "\e012";
  }
  .'.$tag.'glyphicon-ok:before {
    content: "\e013";
  }
  .'.$tag.'glyphicon-remove:before {
    content: "\e014";
  }
  .'.$tag.'glyphicon-zoom-in:before {
    content: "\e015";
  }
  .'.$tag.'glyphicon-zoom-out:before {
    content: "\e016";
  }
  .'.$tag.'glyphicon-off:before {
    content: "\e017";
  }
  .'.$tag.'glyphicon-signal:before {
    content: "\e018";
  }
  .'.$tag.'glyphicon-cog:before {
    content: "\e019";
  }
  .'.$tag.'glyphicon-trash:before {
    content: "\e020";
  }
  .'.$tag.'glyphicon-home:before {
    content: "\e021";
  }
  .'.$tag.'glyphicon-file:before {
    content: "\e022";
  }
  .'.$tag.'glyphicon-time:before {
    content: "\e023";
  }
  .'.$tag.'glyphicon-road:before {
    content: "\e024";
  }
  .'.$tag.'glyphicon-download-alt:before {
    content: "\e025";
  }
  .'.$tag.'glyphicon-download:before {
    content: "\e026";
  }
  .'.$tag.'glyphicon-upload:before {
    content: "\e027";
  }
  .'.$tag.'glyphicon-inbox:before {
    content: "\e028";
  }
  .'.$tag.'glyphicon-play-circle:before {
    content: "\e029";
  }
  .'.$tag.'glyphicon-repeat:before {
    content: "\e030";
  }
  .'.$tag.'glyphicon-refresh:before {
    content: "\e031";
  }
  .'.$tag.'glyphicon-list-alt:before {
    content: "\e032";
  }
  .'.$tag.'glyphicon-lock:before {
    content: "\e033";
  }
  .'.$tag.'glyphicon-flag:before {
    content: "\e034";
  }
  .'.$tag.'glyphicon-headphones:before {
    content: "\e035";
  }
  .'.$tag.'glyphicon-volume-off:before {
    content: "\e036";
  }
  .'.$tag.'glyphicon-volume-down:before {
    content: "\e037";
  }
  .'.$tag.'glyphicon-volume-up:before {
    content: "\e038";
  }
  .'.$tag.'glyphicon-qrcode:before {
    content: "\e039";
  }
  .'.$tag.'glyphicon-barcode:before {
    content: "\e040";
  }
  .'.$tag.'glyphicon-tag:before {
    content: "\e041";
  }
  .'.$tag.'glyphicon-tags:before {
    content: "\e042";
  }
  .'.$tag.'glyphicon-book:before {
    content: "\e043";
  }
  .'.$tag.'glyphicon-bookmark:before {
    content: "\e044";
  }
  .'.$tag.'glyphicon-print:before {
    content: "\e045";
  }
  .'.$tag.'glyphicon-camera:before {
    content: "\e046";
  }
  .'.$tag.'glyphicon-font:before {
    content: "\e047";
  }
  .'.$tag.'glyphicon-bold:before {
    content: "\e048";
  }
  .'.$tag.'glyphicon-italic:before {
    content: "\e049";
  }
  .'.$tag.'glyphicon-text-height:before {
    content: "\e050";
  }
  .'.$tag.'glyphicon-text-width:before {
    content: "\e051";
  }
  .'.$tag.'glyphicon-align-left:before {
    content: "\e052";
  }
  .'.$tag.'glyphicon-align-center:before {
    content: "\e053";
  }
  .'.$tag.'glyphicon-align-right:before {
    content: "\e054";
  }
  .'.$tag.'glyphicon-align-justify:before {
    content: "\e055";
  }
  .'.$tag.'glyphicon-list:before {
    content: "\e056";
  }
  .'.$tag.'glyphicon-indent-left:before {
    content: "\e057";
  }
  .'.$tag.'glyphicon-indent-right:before {
    content: "\e058";
  }
  .'.$tag.'glyphicon-facetime-video:before {
    content: "\e059";
  }
  .'.$tag.'glyphicon-picture:before {
    content: "\e060";
  }
  .'.$tag.'glyphicon-map-marker:before {
    content: "\e062";
  }
  .'.$tag.'glyphicon-adjust:before {
    content: "\e063";
  }
  .'.$tag.'glyphicon-tint:before {
    content: "\e064";
  }
  .'.$tag.'glyphicon-edit:before {
    content: "\e065";
  }
  .'.$tag.'glyphicon-share:before {
    content: "\e066";
  }
  .'.$tag.'glyphicon-check:before {
    content: "\e067";
  }
  .'.$tag.'glyphicon-move:before {
    content: "\e068";
  }
  .'.$tag.'glyphicon-step-backward:before {
    content: "\e069";
  }
  .'.$tag.'glyphicon-fast-backward:before {
    content: "\e070";
  }
  .'.$tag.'glyphicon-backward:before {
    content: "\e071";
  }
  .'.$tag.'glyphicon-play:before {
    content: "\e072";
  }
  .'.$tag.'glyphicon-pause:before {
    content: "\e073";
  }
  .'.$tag.'glyphicon-stop:before {
    content: "\e074";
  }
  .'.$tag.'glyphicon-forward:before {
    content: "\e075";
  }
  .'.$tag.'glyphicon-fast-forward:before {
    content: "\e076";
  }
  .'.$tag.'glyphicon-step-forward:before {
    content: "\e077";
  }
  .'.$tag.'glyphicon-eject:before {
    content: "\e078";
  }
  .'.$tag.'glyphicon-chevron-left:before {
    content: "\e079";
  }
  .'.$tag.'glyphicon-chevron-right:before {
    content: "\e080";
  }
  .'.$tag.'glyphicon-plus-sign:before {
    content: "\e081";
  }
  .'.$tag.'glyphicon-minus-sign:before {
    content: "\e082";
  }
  .'.$tag.'glyphicon-remove-sign:before {
    content: "\e083";
  }
  .'.$tag.'glyphicon-ok-sign:before {
    content: "\e084";
  }
  .'.$tag.'glyphicon-question-sign:before {
    content: "\e085";
  }
  .'.$tag.'glyphicon-info-sign:before {
    content: "\e086";
  }
  .'.$tag.'glyphicon-screenshot:before {
    content: "\e087";
  }
  .'.$tag.'glyphicon-remove-circle:before {
    content: "\e088";
  }
  .'.$tag.'glyphicon-ok-circle:before {
    content: "\e089";
  }
  .'.$tag.'glyphicon-ban-circle:before {
    content: "\e090";
  }
  .'.$tag.'glyphicon-arrow-left:before {
    content: "\e091";
  }
  .'.$tag.'glyphicon-arrow-right:before {
    content: "\e092";
  }
  .'.$tag.'glyphicon-arrow-up:before {
    content: "\e093";
  }
  .'.$tag.'glyphicon-arrow-down:before {
    content: "\e094";
  }
  .'.$tag.'glyphicon-share-alt:before {
    content: "\e095";
  }
  .'.$tag.'glyphicon-resize-full:before {
    content: "\e096";
  }
  .'.$tag.'glyphicon-resize-small:before {
    content: "\e097";
  }
  .'.$tag.'glyphicon-exclamation-sign:before {
    content: "\e101";
  }
  .'.$tag.'glyphicon-gift:before {
    content: "\e102";
  }
  .'.$tag.'glyphicon-leaf:before {
    content: "\e103";
  }
  .'.$tag.'glyphicon-fire:before {
    content: "\e104";
  }
  .'.$tag.'glyphicon-eye-open:before {
    content: "\e105";
  }
  .'.$tag.'glyphicon-eye-close:before {
    content: "\e106";
  }
  .'.$tag.'glyphicon-warning-sign:before {
    content: "\e107";
  }
  .'.$tag.'glyphicon-plane:before {
    content: "\e108";
  }
  .'.$tag.'glyphicon-calendar:before {
    content: "\e109";
  }
  .'.$tag.'glyphicon-random:before {
    content: "\e110";
  }
  .'.$tag.'glyphicon-comment:before {
    content: "\e111";
  }
  .'.$tag.'glyphicon-magnet:before {
    content: "\e112";
  }
  .'.$tag.'glyphicon-chevron-up:before {
    content: "\e113";
  }
  .'.$tag.'glyphicon-chevron-down:before {
    content: "\e114";
  }
  .'.$tag.'glyphicon-retweet:before {
    content: "\e115";
  }
  .'.$tag.'glyphicon-shopping-cart:before {
    content: "\e116";
  }
  .'.$tag.'glyphicon-folder-close:before {
    content: "\e117";
  }
  .'.$tag.'glyphicon-folder-open:before {
    content: "\e118";
  }
  .'.$tag.'glyphicon-resize-vertical:before {
    content: "\e119";
  }
  .'.$tag.'glyphicon-resize-horizontal:before {
    content: "\e120";
  }
  .'.$tag.'glyphicon-hdd:before {
    content: "\e121";
  }
  .'.$tag.'glyphicon-bullhorn:before {
    content: "\e122";
  }
  .'.$tag.'glyphicon-bell:before {
    content: "\e123";
  }
  .'.$tag.'glyphicon-certificate:before {
    content: "\e124";
  }
  .'.$tag.'glyphicon-thumbs-up:before {
    content: "\e125";
  }
  .'.$tag.'glyphicon-thumbs-down:before {
    content: "\e126";
  }
  .'.$tag.'glyphicon-hand-right:before {
    content: "\e127";
  }
  .'.$tag.'glyphicon-hand-left:before {
    content: "\e128";
  }
  .'.$tag.'glyphicon-hand-up:before {
    content: "\e129";
  }
  .'.$tag.'glyphicon-hand-down:before {
    content: "\e130";
  }
  .'.$tag.'glyphicon-circle-arrow-right:before {
    content: "\e131";
  }
  .'.$tag.'glyphicon-circle-arrow-left:before {
    content: "\e132";
  }
  .'.$tag.'glyphicon-circle-arrow-up:before {
    content: "\e133";
  }
  .'.$tag.'glyphicon-circle-arrow-down:before {
    content: "\e134";
  }
  .'.$tag.'glyphicon-globe:before {
    content: "\e135";
  }
  .'.$tag.'glyphicon-wrench:before {
    content: "\e136";
  }
  .'.$tag.'glyphicon-tasks:before {
    content: "\e137";
  }
  .'.$tag.'glyphicon-filter:before {
    content: "\e138";
  }
  .'.$tag.'glyphicon-briefcase:before {
    content: "\e139";
  }
  .'.$tag.'glyphicon-fullscreen:before {
    content: "\e140";
  }
  .'.$tag.'glyphicon-dashboard:before {
    content: "\e141";
  }
  .'.$tag.'glyphicon-paperclip:before {
    content: "\e142";
  }
  .'.$tag.'glyphicon-heart-empty:before {
    content: "\e143";
  }
  .'.$tag.'glyphicon-link:before {
    content: "\e144";
  }
  .'.$tag.'glyphicon-phone:before {
    content: "\e145";
  }
  .'.$tag.'glyphicon-pushpin:before {
    content: "\e146";
  }
  .'.$tag.'glyphicon-usd:before {
    content: "\e148";
  }
  .'.$tag.'glyphicon-gbp:before {
    content: "\e149";
  }
  .'.$tag.'glyphicon-sort:before {
    content: "\e150";
  }
  .'.$tag.'glyphicon-sort-by-alphabet:before {
    content: "\e151";
  }
  .'.$tag.'glyphicon-sort-by-alphabet-alt:before {
    content: "\e152";
  }
  .'.$tag.'glyphicon-sort-by-order:before {
    content: "\e153";
  }
  .'.$tag.'glyphicon-sort-by-order-alt:before {
    content: "\e154";
  }
  .'.$tag.'glyphicon-sort-by-attributes:before {
    content: "\e155";
  }
  .'.$tag.'glyphicon-sort-by-attributes-alt:before {
    content: "\e156";
  }
  .'.$tag.'glyphicon-unchecked:before {
    content: "\e157";
  }
  .'.$tag.'glyphicon-expand:before {
    content: "\e158";
  }
  .'.$tag.'glyphicon-collapse-down:before {
    content: "\e159";
  }
  .'.$tag.'glyphicon-collapse-up:before {
    content: "\e160";
  }
  .'.$tag.'glyphicon-log-in:before {
    content: "\e161";
  }
  .'.$tag.'glyphicon-flash:before {
    content: "\e162";
  }
  .'.$tag.'glyphicon-log-out:before {
    content: "\e163";
  }
  .'.$tag.'glyphicon-new-window:before {
    content: "\e164";
  }
  .'.$tag.'glyphicon-record:before {
    content: "\e165";
  }
  .'.$tag.'glyphicon-save:before {
    content: "\e166";
  }
  .'.$tag.'glyphicon-open:before {
    content: "\e167";
  }
  .'.$tag.'glyphicon-saved:before {
    content: "\e168";
  }
  .'.$tag.'glyphicon-import:before {
    content: "\e169";
  }
  .'.$tag.'glyphicon-export:before {
    content: "\e170";
  }
  .'.$tag.'glyphicon-send:before {
    content: "\e171";
  }
  .'.$tag.'glyphicon-floppy-disk:before {
    content: "\e172";
  }
  .'.$tag.'glyphicon-floppy-saved:before {
    content: "\e173";
  }
  .'.$tag.'glyphicon-floppy-remove:before {
    content: "\e174";
  }
  .'.$tag.'glyphicon-floppy-save:before {
    content: "\e175";
  }
  .'.$tag.'glyphicon-floppy-open:before {
    content: "\e176";
  }
  .'.$tag.'glyphicon-credit-card:before {
    content: "\e177";
  }
  .'.$tag.'glyphicon-transfer:before {
    content: "\e178";
  }
  .'.$tag.'glyphicon-cutlery:before {
    content: "\e179";
  }
  .'.$tag.'glyphicon-header:before {
    content: "\e180";
  }
  .'.$tag.'glyphicon-compressed:before {
    content: "\e181";
  }
  .'.$tag.'glyphicon-earphone:before {
    content: "\e182";
  }
  .'.$tag.'glyphicon-phone-alt:before {
    content: "\e183";
  }
  .'.$tag.'glyphicon-tower:before {
    content: "\e184";
  }
  .'.$tag.'glyphicon-stats:before {
    content: "\e185";
  }
  .'.$tag.'glyphicon-sd-video:before {
    content: "\e186";
  }
  .'.$tag.'glyphicon-hd-video:before {
    content: "\e187";
  }
  .'.$tag.'glyphicon-subtitles:before {
    content: "\e188";
  }
  .'.$tag.'glyphicon-sound-stereo:before {
    content: "\e189";
  }
  .'.$tag.'glyphicon-sound-dolby:before {
    content: "\e190";
  }
  .'.$tag.'glyphicon-sound-5-1:before {
    content: "\e191";
  }
  .'.$tag.'glyphicon-sound-6-1:before {
    content: "\e192";
  }
  .'.$tag.'glyphicon-sound-7-1:before {
    content: "\e193";
  }
  .'.$tag.'glyphicon-copyright-mark:before {
    content: "\e194";
  }
  .'.$tag.'glyphicon-registration-mark:before {
    content: "\e195";
  }
  .'.$tag.'glyphicon-cloud-download:before {
    content: "\e197";
  }
  .'.$tag.'glyphicon-cloud-upload:before {
    content: "\e198";
  }
  .'.$tag.'glyphicon-tree-conifer:before {
    content: "\e199";
  }
  .'.$tag.'glyphicon-tree-deciduous:before {
    content: "\e200";
  }
  .'.$tag.'glyphicon-cd:before {
    content: "\e201";
  }
  .'.$tag.'glyphicon-save-file:before {
    content: "\e202";
  }
  .'.$tag.'glyphicon-open-file:before {
    content: "\e203";
  }
  .'.$tag.'glyphicon-level-up:before {
    content: "\e204";
  }
  .'.$tag.'glyphicon-copy:before {
    content: "\e205";
  }
  .'.$tag.'glyphicon-paste:before {
    content: "\e206";
  }
  .'.$tag.'glyphicon-alert:before {
    content: "\e209";
  }
  .'.$tag.'glyphicon-equalizer:before {
    content: "\e210";
  }
  .'.$tag.'glyphicon-king:before {
    content: "\e211";
  }
  .'.$tag.'glyphicon-queen:before {
    content: "\e212";
  }
  .'.$tag.'glyphicon-pawn:before {
    content: "\e213";
  }
  .'.$tag.'glyphicon-bishop:before {
    content: "\e214";
  }
  .'.$tag.'glyphicon-knight:before {
    content: "\e215";
  }
  .'.$tag.'glyphicon-baby-formula:before {
    content: "\e216";
  }
  .'.$tag.'glyphicon-tent:before {
    content: "\26fa";
  }
  .'.$tag.'glyphicon-blackboard:before {
    content: "\e218";
  }
  .'.$tag.'glyphicon-bed:before {
    content: "\e219";
  }
  .'.$tag.'glyphicon-apple:before {
    content: "\f8ff";
  }
  .'.$tag.'glyphicon-erase:before {
    content: "\e221";
  }
  .'.$tag.'glyphicon-hourglass:before {
    content: "\231b";
  }
  .'.$tag.'glyphicon-lamp:before {
    content: "\e223";
  }
  .'.$tag.'glyphicon-duplicate:before {
    content: "\e224";
  }
  .'.$tag.'glyphicon-piggy-bank:before {
    content: "\e225";
  }
  .'.$tag.'glyphicon-scissors:before {
    content: "\e226";
  }
  .'.$tag.'glyphicon-bitcoin:before {
    content: "\e227";
  }
  .'.$tag.'glyphicon-btc:before {
    content: "\e227";
  }
  .'.$tag.'glyphicon-xbt:before {
    content: "\e227";
  }
  .'.$tag.'glyphicon-yen:before {
    content: "\00a5";
  }
  .'.$tag.'glyphicon-jpy:before {
    content: "\00a5";
  }
  .'.$tag.'glyphicon-ruble:before {
    content: "\20bd";
  }
  .'.$tag.'glyphicon-rub:before {
    content: "\20bd";
  }
  .'.$tag.'glyphicon-scale:before {
    content: "\e230";
  }
  .'.$tag.'glyphicon-ice-lolly:before {
    content: "\e231";
  }
  .'.$tag.'glyphicon-ice-lolly-tasted:before {
    content: "\e232";
  }
  .'.$tag.'glyphicon-education:before {
    content: "\e233";
  }
  .'.$tag.'glyphicon-option-horizontal:before {
    content: "\e234";
  }
  .'.$tag.'glyphicon-option-vertical:before {
    content: "\e235";
  }
  .'.$tag.'glyphicon-menu-hamburger:before {
    content: "\e236";
  }
  .'.$tag.'glyphicon-modal-window:before {
    content: "\e237";
  }
  .'.$tag.'glyphicon-oil:before {
    content: "\e238";
  }
  .'.$tag.'glyphicon-grain:before {
    content: "\e239";
  }
  .'.$tag.'glyphicon-sunglasses:before {
    content: "\e240";
  }
  .'.$tag.'glyphicon-text-size:before {
    content: "\e241";
  }
  .'.$tag.'glyphicon-text-color:before {
    content: "\e242";
  }
  .'.$tag.'glyphicon-text-background:before {
    content: "\e243";
  }
  .'.$tag.'glyphicon-object-align-top:before {
    content: "\e244";
  }
  .'.$tag.'glyphicon-object-align-bottom:before {
    content: "\e245";
  }
  .'.$tag.'glyphicon-object-align-horizontal:before {
    content: "\e246";
  }
  .'.$tag.'glyphicon-object-align-left:before {
    content: "\e247";
  }
  .'.$tag.'glyphicon-object-align-vertical:before {
    content: "\e248";
  }
  .'.$tag.'glyphicon-object-align-right:before {
    content: "\e249";
  }
  .'.$tag.'glyphicon-triangle-right:before {
    content: "\e250";
  }
  .'.$tag.'glyphicon-triangle-left:before {
    content: "\e251";
  }
  .'.$tag.'glyphicon-triangle-bottom:before {
    content: "\e252";
  }
  .'.$tag.'glyphicon-triangle-top:before {
    content: "\e253";
  }
  .'.$tag.'glyphicon-console:before {
    content: "\e254";
  }
  .'.$tag.'glyphicon-superscript:before {
    content: "\e255";
  }
  .'.$tag.'glyphicon-subscript:before {
    content: "\e256";
  }
  .'.$tag.'glyphicon-menu-left:before {
    content: "\e257";
  }
  .'.$tag.'glyphicon-menu-right:before {
    content: "\e258";
  }
  .'.$tag.'glyphicon-menu-down:before {
    content: "\e259";
  }
  .'.$tag.'glyphicon-menu-up:before {
    content: "\e260";
  }
  * {
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
  }
  *:before,
  *:after {
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
  }
  html {
    font-size: 10px;

    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  }
  body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
  }
  input,
  button,
  select,
  textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
  }
  a {
    color: #337ab7;
    text-decoration: none;
  }
  a:hover,
  a:focus {
    color: #23527c;
    text-decoration: underline;
  }
  a:focus {
    outline: 5px auto -webkit-focus-ring-color;
    outline-offset: -2px;
  }
  figure {
    margin: 0;
  }
  img {
    vertical-align: middle;
  }
  .'.$tag.'img-responsive,
  .'.$tag.'thumbnail > img,
  .'.$tag.'thumbnail a > img,
  .'.$tag.'carousel-inner > .'.$tag.'item > img,
  .'.$tag.'carousel-inner > .'.$tag.'item > a > img {
    display: block;
    max-width: 100%;
    height: auto;
  }
  .'.$tag.'img-rounded {
    border-radius: 6px;
  }
  .'.$tag.'img-thumbnail {
    display: inline-block;
    max-width: 100%;
    height: auto;
    padding: 4px;
    line-height: 1.42857143;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: all .2s ease-in-out;
         -o-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out;
  }
  .'.$tag.'img-circle {
    border-radius: 50%;
  }
  hr {
    margin-top: 20px;
    margin-bottom: 20px;
    border: 0;
    border-top: 1px solid #eee;
  }
  .'.$tag.'sr-only {
    position: absolute;
    width: 1px;
    height: 1px;
    padding: 0;
    margin: -1px;
    overflow: hidden;
    clip: rect(0, 0, 0, 0);
    border: 0;
  }
  .'.$tag.'sr-only-focusable:active,
  .'.$tag.'sr-only-focusable:focus {
    position: static;
    width: auto;
    height: auto;
    margin: 0;
    overflow: visible;
    clip: auto;
  }
  [role="button"] {
    cursor: pointer;
  }
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  .'.$tag.'h1,
  .'.$tag.'h2,
  .'.$tag.'h3,
  .'.$tag.'h4,
  .'.$tag.'h5,
  .'.$tag.'h6 {
    font-family: inherit;
    font-weight: 500;
    line-height: 1.1;
    color: inherit;
  }
  h1 small,
  h2 small,
  h3 small,
  h4 small,
  h5 small,
  h6 small,
  .'.$tag.'h1 small,
  .'.$tag.'h2 small,
  .'.$tag.'h3 small,
  .'.$tag.'h4 small,
  .'.$tag.'h5 small,
  .'.$tag.'h6 small,
  h1 .'.$tag.'small,
  h2 .'.$tag.'small,
  h3 .'.$tag.'small,
  h4 .'.$tag.'small,
  h5 .'.$tag.'small,
  h6 .'.$tag.'small,
  .'.$tag.'h1 .'.$tag.'small,
  .'.$tag.'h2 .'.$tag.'small,
  .'.$tag.'h3 .'.$tag.'small,
  .'.$tag.'h4 .'.$tag.'small,
  .'.$tag.'h5 .'.$tag.'small,
  .'.$tag.'h6 .'.$tag.'small {
    font-weight: normal;
    line-height: 1;
    color: #777;
  }
  h1,
  .'.$tag.'h1,
  h2,
  .'.$tag.'h2,
  h3,
  .'.$tag.'h3 {
    margin-top: 20px;
    margin-bottom: 10px;
  }
  h1 small,
  .'.$tag.'h1 small,
  h2 small,
  .'.$tag.'h2 small,
  h3 small,
  .'.$tag.'h3 small,
  h1 .'.$tag.'small,
  .'.$tag.'h1 .'.$tag.'small,
  h2 .'.$tag.'small,
  .'.$tag.'h2 .'.$tag.'small,
  h3 .'.$tag.'small,
  .'.$tag.'h3 .'.$tag.'small {
    font-size: 65%;
  }
  h4,
  .'.$tag.'h4,
  h5,
  .'.$tag.'h5,
  h6,
  .'.$tag.'h6 {
    margin-top: 10px;
    margin-bottom: 10px;
  }
  h4 small,
  .'.$tag.'h4 small,
  h5 small,
  .'.$tag.'h5 small,
  h6 small,
  .'.$tag.'h6 small,
  h4 .'.$tag.'small,
  .'.$tag.'h4 .'.$tag.'small,
  h5 .'.$tag.'small,
  .'.$tag.'h5 .'.$tag.'small,
  h6 .'.$tag.'small,
  .'.$tag.'h6 .'.$tag.'small {
    font-size: 75%;
  }
  h1,
  .'.$tag.'h1 {
    font-size: 36px;
  }
  h2,
  .'.$tag.'h2 {
    font-size: 30px;
  }
  h3,
  .'.$tag.'h3 {
    font-size: 24px;
  }
  h4,
  .'.$tag.'h4 {
    font-size: 18px;
  }
  h5,
  .'.$tag.'h5 {
    font-size: 14px;
  }
  h6,
  .'.$tag.'h6 {
    font-size: 12px;
  }
  p {
    margin: 0 0 10px;
  }
  .'.$tag.'lead {
    margin-bottom: 20px;
    font-size: 16px;
    font-weight: 300;
    line-height: 1.4;
  }
  @media (min-width: 768px) {
    .'.$tag.'lead {
      font-size: 21px;
    }
  }
  small,
  .'.$tag.'small {
    font-size: 85%;
  }
  mark,
  .'.$tag.'mark {
    padding: .2em;
    background-color: #fcf8e3;
  }
  .'.$tag.'text-left {
    text-align: left;
  }
  .'.$tag.'text-right {
    text-align: right;
  }
  .'.$tag.'text-center {
    text-align: center;
  }
  .'.$tag.'text-justify {
    text-align: justify;
  }
  .'.$tag.'text-nowrap {
    white-space: nowrap;
  }
  .'.$tag.'text-lowercase {
    text-transform: lowercase;
  }
  .'.$tag.'text-uppercase {
    text-transform: uppercase;
  }
  .'.$tag.'text-capitalize {
    text-transform: capitalize;
  }
  .'.$tag.'text-muted {
    color: #777;
  }
  .'.$tag.'text-primary {
    color: #337ab7;
  }
  a.'.$tag.'text-primary:hover,
  a.'.$tag.'text-primary:focus {
    color: #286090;
  }
  .'.$tag.'text-success {
    color: #3c763d;
  }
  a.'.$tag.'text-success:hover,
  a.'.$tag.'text-success:focus {
    color: #2b542c;
  }
  .'.$tag.'text-info {
    color: #31708f;
  }
  a.'.$tag.'text-info:hover,
  a.'.$tag.'text-info:focus {
    color: #245269;
  }
  .'.$tag.'text-warning {
    color: #8a6d3b;
  }
  a.'.$tag.'text-warning:hover,
  a.'.$tag.'text-warning:focus {
    color: #66512c;
  }
  .'.$tag.'text-danger {
    color: #a94442;
  }
  a.'.$tag.'text-danger:hover,
  a.'.$tag.'text-danger:focus {
    color: #843534;
  }
  .'.$tag.'bg-primary {
    color: #fff;
    background-color: #337ab7;
  }
  a.'.$tag.'bg-primary:hover,
  a.'.$tag.'bg-primary:focus {
    background-color: #286090;
  }
  .'.$tag.'bg-success {
    background-color: #dff0d8;
  }
  a.'.$tag.'bg-success:hover,
  a.'.$tag.'bg-success:focus {
    background-color: #c1e2b3;
  }
  .'.$tag.'bg-info {
    background-color: #d9edf7;
  }
  a.'.$tag.'bg-info:hover,
  a.'.$tag.'bg-info:focus {
    background-color: #afd9ee;
  }
  .'.$tag.'bg-warning {
    background-color: #fcf8e3;
  }
  a.'.$tag.'bg-warning:hover,
  a.'.$tag.'bg-warning:focus {
    background-color: #f7ecb5;
  }
  .'.$tag.'bg-danger {
    background-color: #f2dede;
  }
  a.'.$tag.'bg-danger:hover,
  a.'.$tag.'bg-danger:focus {
    background-color: #e4b9b9;
  }
  .'.$tag.'page-header {
    padding-bottom: 9px;
    margin: 40px 0 20px;
    border-bottom: 1px solid #eee;
  }
  ul,
  ol {
    margin-top: 0;
    margin-bottom: 10px;
  }
  ul ul,
  ol ul,
  ul ol,
  ol ol {
    margin-bottom: 0;
  }
  .'.$tag.'list-unstyled {
    padding-left: 0;
    list-style: none;
  }
  .'.$tag.'list-inline {
    padding-left: 0;
    margin-left: -5px;
    list-style: none;
  }
  .'.$tag.'list-inline > li {
    display: inline-block;
    padding-right: 5px;
    padding-left: 5px;
  }
  dl {
    margin-top: 0;
    margin-bottom: 20px;
  }
  dt,
  dd {
    line-height: 1.42857143;
  }
  dt {
    font-weight: bold;
  }
  dd {
    margin-left: 0;
  }
  @media (min-width: 768px) {
    .'.$tag.'dl-horizontal dt {
      float: left;
      width: 160px;
      overflow: hidden;
      clear: left;
      text-align: right;
      text-overflow: ellipsis;
      white-space: nowrap;
    }
    .'.$tag.'dl-horizontal dd {
      margin-left: 180px;
    }
  }
  abbr[title],
  abbr[data-original-title] {
    cursor: help;
    border-bottom: 1px dotted #777;
  }
  .'.$tag.'initialism {
    font-size: 90%;
    text-transform: uppercase;
  }
  blockquote {
    padding: 10px 20px;
    margin: 0 0 20px;
    font-size: 17.5px;
    border-left: 5px solid #eee;
  }
  blockquote p:last-child,
  blockquote ul:last-child,
  blockquote ol:last-child {
    margin-bottom: 0;
  }
  blockquote footer,
  blockquote small,
  blockquote .'.$tag.'small {
    display: block;
    font-size: 80%;
    line-height: 1.42857143;
    color: #777;
  }
  blockquote footer:before,
  blockquote small:before,
  blockquote .'.$tag.'small:before {
    content: "\2014 \00A0";
  }
  .'.$tag.'blockquote-reverse,
  blockquote.'.$tag.'pull-right {
    padding-right: 15px;
    padding-left: 0;
    text-align: right;
    border-right: 5px solid #eee;
    border-left: 0;
  }
  .'.$tag.'blockquote-reverse footer:before,
  blockquote.'.$tag.'pull-right footer:before,
  .'.$tag.'blockquote-reverse small:before,
  blockquote.'.$tag.'pull-right small:before,
  .'.$tag.'blockquote-reverse .'.$tag.'small:before,
  blockquote.'.$tag.'pull-right .'.$tag.'small:before {
    content: "";
  }
  .'.$tag.'blockquote-reverse footer:after,
  blockquote.'.$tag.'pull-right footer:after,
  .'.$tag.'blockquote-reverse small:after,
  blockquote.'.$tag.'pull-right small:after,
  .'.$tag.'blockquote-reverse .'.$tag.'small:after,
  blockquote.'.$tag.'pull-right .'.$tag.'small:after {
    content: "\00A0 \2014";
  }
  address {
    margin-bottom: 20px;
    font-style: normal;
    line-height: 1.42857143;
  }
  code,
  kbd,
  pre,
  samp {
    font-family: Menlo, Monaco, Consolas, "Courier New", monospace;
  }
  code {
    padding: 2px 4px;
    font-size: 90%;
    color: #c7254e;
    background-color: #f9f2f4;
    border-radius: 4px;
  }
  kbd {
    padding: 2px 4px;
    font-size: 90%;
    color: #fff;
    background-color: #333;
    border-radius: 3px;
    -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .25);
            box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .25);
  }
  kbd kbd {
    padding: 0;
    font-size: 100%;
    font-weight: bold;
    -webkit-box-shadow: none;
            box-shadow: none;
  }
  pre {
    display: block;
    padding: 9.5px;
    margin: 0 0 10px;
    font-size: 13px;
    line-height: 1.42857143;
    color: #333;
    word-break: break-all;
    word-wrap: break-word;
    background-color: #f5f5f5;
    border: 1px solid #ccc;
    border-radius: 4px;
  }
  pre code {
    padding: 0;
    font-size: inherit;
    color: inherit;
    white-space: pre-wrap;
    background-color: transparent;
    border-radius: 0;
  }
  .'.$tag.'pre-scrollable {
    max-height: 340px;
    overflow-y: scroll;
  }
  .'.$tag.'container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
  }
  @media (min-width: 768px) {
    .'.$tag.'container {
      width: 750px;
    }
  }
  @media (min-width: 992px) {
    .'.$tag.'container {
      width: 970px;
    }
  }
  @media (min-width: 1200px) {
    .'.$tag.'container {
      width: 1170px;
    }
  }
  .'.$tag.'container-fluid {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
  }
  .'.$tag.'row {
    margin-right: -15px;
    margin-left: -15px;
  }
  .'.$tag.'col-xs-1, .'.$tag.'col-sm-1, .'.$tag.'col-md-1, .'.$tag.'col-lg-1, .'.$tag.'col-xs-2, .'.$tag.'col-sm-2, .'.$tag.'col-md-2, .'.$tag.'col-lg-2, .'.$tag.'col-xs-3, .'.$tag.'col-sm-3, .'.$tag.'col-md-3, .'.$tag.'col-lg-3, .'.$tag.'col-xs-4, .'.$tag.'col-sm-4, .'.$tag.'col-md-4, .'.$tag.'col-lg-4, .'.$tag.'col-xs-5, .'.$tag.'col-sm-5, .'.$tag.'col-md-5, .'.$tag.'col-lg-5, .'.$tag.'col-xs-6, .'.$tag.'col-sm-6, .'.$tag.'col-md-6, .'.$tag.'col-lg-6, .'.$tag.'col-xs-7, .'.$tag.'col-sm-7, .'.$tag.'col-md-7, .'.$tag.'col-lg-7, .'.$tag.'col-xs-8, .'.$tag.'col-sm-8, .'.$tag.'col-md-8, .'.$tag.'col-lg-8, .'.$tag.'col-xs-9, .'.$tag.'col-sm-9, .'.$tag.'col-md-9, .'.$tag.'col-lg-9, .'.$tag.'col-xs-10, .'.$tag.'col-sm-10, .'.$tag.'col-md-10, .'.$tag.'col-lg-10, .'.$tag.'col-xs-11, .'.$tag.'col-sm-11, .'.$tag.'col-md-11, .'.$tag.'col-lg-11, .'.$tag.'col-xs-12, .'.$tag.'col-sm-12, .'.$tag.'col-md-12, .'.$tag.'col-lg-12 {
    position: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
  }
  .'.$tag.'col-xs-1, .'.$tag.'col-xs-2, .'.$tag.'col-xs-3, .'.$tag.'col-xs-4, .'.$tag.'col-xs-5, .'.$tag.'col-xs-6, .'.$tag.'col-xs-7, .'.$tag.'col-xs-8, .'.$tag.'col-xs-9, .'.$tag.'col-xs-10, .'.$tag.'col-xs-11, .'.$tag.'col-xs-12 {
    float: left;
  }
  .'.$tag.'col-xs-12 {
    width: 100%;
  }
  .'.$tag.'col-xs-11 {
    width: 91.66666667%;
  }
  .'.$tag.'col-xs-10 {
    width: 83.33333333%;
  }
  .'.$tag.'col-xs-9 {
    width: 75%;
  }
  .'.$tag.'col-xs-8 {
    width: 66.66666667%;
  }
  .'.$tag.'col-xs-7 {
    width: 58.33333333%;
  }
  .'.$tag.'col-xs-6 {
    width: 50%;
  }
  .'.$tag.'col-xs-5 {
    width: 41.66666667%;
  }
  .'.$tag.'col-xs-4 {
    width: 33.33333333%;
  }
  .'.$tag.'col-xs-3 {
    width: 25%;
  }
  .'.$tag.'col-xs-2 {
    width: 16.66666667%;
  }
  .'.$tag.'col-xs-1 {
    width: 8.33333333%;
  }
  .'.$tag.'col-xs-pull-12 {
    right: 100%;
  }
  .'.$tag.'col-xs-pull-11 {
    right: 91.66666667%;
  }
  .'.$tag.'col-xs-pull-10 {
    right: 83.33333333%;
  }
  .'.$tag.'col-xs-pull-9 {
    right: 75%;
  }
  .'.$tag.'col-xs-pull-8 {
    right: 66.66666667%;
  }
  .'.$tag.'col-xs-pull-7 {
    right: 58.33333333%;
  }
  .'.$tag.'col-xs-pull-6 {
    right: 50%;
  }
  .'.$tag.'col-xs-pull-5 {
    right: 41.66666667%;
  }
  .'.$tag.'col-xs-pull-4 {
    right: 33.33333333%;
  }
  .'.$tag.'col-xs-pull-3 {
    right: 25%;
  }
  .'.$tag.'col-xs-pull-2 {
    right: 16.66666667%;
  }
  .'.$tag.'col-xs-pull-1 {
    right: 8.33333333%;
  }
  .'.$tag.'col-xs-pull-0 {
    right: auto;
  }
  .'.$tag.'col-xs-push-12 {
    left: 100%;
  }
  .'.$tag.'col-xs-push-11 {
    left: 91.66666667%;
  }
  .'.$tag.'col-xs-push-10 {
    left: 83.33333333%;
  }
  .'.$tag.'col-xs-push-9 {
    left: 75%;
  }
  .'.$tag.'col-xs-push-8 {
    left: 66.66666667%;
  }
  .'.$tag.'col-xs-push-7 {
    left: 58.33333333%;
  }
  .'.$tag.'col-xs-push-6 {
    left: 50%;
  }
  .'.$tag.'col-xs-push-5 {
    left: 41.66666667%;
  }
  .'.$tag.'col-xs-push-4 {
    left: 33.33333333%;
  }
  .'.$tag.'col-xs-push-3 {
    left: 25%;
  }
  .'.$tag.'col-xs-push-2 {
    left: 16.66666667%;
  }
  .'.$tag.'col-xs-push-1 {
    left: 8.33333333%;
  }
  .'.$tag.'col-xs-push-0 {
    left: auto;
  }
  .'.$tag.'col-xs-offset-12 {
    margin-left: 100%;
  }
  .'.$tag.'col-xs-offset-11 {
    margin-left: 91.66666667%;
  }
  .'.$tag.'col-xs-offset-10 {
    margin-left: 83.33333333%;
  }
  .'.$tag.'col-xs-offset-9 {
    margin-left: 75%;
  }
  .'.$tag.'col-xs-offset-8 {
    margin-left: 66.66666667%;
  }
  .'.$tag.'col-xs-offset-7 {
    margin-left: 58.33333333%;
  }
  .'.$tag.'col-xs-offset-6 {
    margin-left: 50%;
  }
  .'.$tag.'col-xs-offset-5 {
    margin-left: 41.66666667%;
  }
  .'.$tag.'col-xs-offset-4 {
    margin-left: 33.33333333%;
  }
  .'.$tag.'col-xs-offset-3 {
    margin-left: 25%;
  }
  .'.$tag.'col-xs-offset-2 {
    margin-left: 16.66666667%;
  }
  .'.$tag.'col-xs-offset-1 {
    margin-left: 8.33333333%;
  }
  .'.$tag.'col-xs-offset-0 {
    margin-left: 0;
  }
  @media (min-width: 768px) {
    .'.$tag.'col-sm-1, .'.$tag.'col-sm-2, .'.$tag.'col-sm-3, .'.$tag.'col-sm-4, .'.$tag.'col-sm-5, .'.$tag.'col-sm-6, .'.$tag.'col-sm-7, .'.$tag.'col-sm-8, .'.$tag.'col-sm-9, .'.$tag.'col-sm-10, .'.$tag.'col-sm-11, .'.$tag.'col-sm-12 {
      float: left;
    }
    .'.$tag.'col-sm-12 {
      width: 100%;
    }
    .'.$tag.'col-sm-11 {
      width: 91.66666667%;
    }
    .'.$tag.'col-sm-10 {
      width: 83.33333333%;
    }
    .'.$tag.'col-sm-9 {
      width: 75%;
    }
    .'.$tag.'col-sm-8 {
      width: 66.66666667%;
    }
    .'.$tag.'col-sm-7 {
      width: 58.33333333%;
    }
    .'.$tag.'col-sm-6 {
      width: 50%;
    }
    .'.$tag.'col-sm-5 {
      width: 41.66666667%;
    }
    .'.$tag.'col-sm-4 {
      width: 33.33333333%;
    }
    .'.$tag.'col-sm-3 {
      width: 25%;
    }
    .'.$tag.'col-sm-2 {
      width: 16.66666667%;
    }
    .'.$tag.'col-sm-1 {
      width: 8.33333333%;
    }
    .'.$tag.'col-sm-pull-12 {
      right: 100%;
    }
    .'.$tag.'col-sm-pull-11 {
      right: 91.66666667%;
    }
    .'.$tag.'col-sm-pull-10 {
      right: 83.33333333%;
    }
    .'.$tag.'col-sm-pull-9 {
      right: 75%;
    }
    .'.$tag.'col-sm-pull-8 {
      right: 66.66666667%;
    }
    .'.$tag.'col-sm-pull-7 {
      right: 58.33333333%;
    }
    .'.$tag.'col-sm-pull-6 {
      right: 50%;
    }
    .'.$tag.'col-sm-pull-5 {
      right: 41.66666667%;
    }
    .'.$tag.'col-sm-pull-4 {
      right: 33.33333333%;
    }
    .'.$tag.'col-sm-pull-3 {
      right: 25%;
    }
    .'.$tag.'col-sm-pull-2 {
      right: 16.66666667%;
    }
    .'.$tag.'col-sm-pull-1 {
      right: 8.33333333%;
    }
    .'.$tag.'col-sm-pull-0 {
      right: auto;
    }
    .'.$tag.'col-sm-push-12 {
      left: 100%;
    }
    .'.$tag.'col-sm-push-11 {
      left: 91.66666667%;
    }
    .'.$tag.'col-sm-push-10 {
      left: 83.33333333%;
    }
    .'.$tag.'col-sm-push-9 {
      left: 75%;
    }
    .'.$tag.'col-sm-push-8 {
      left: 66.66666667%;
    }
    .'.$tag.'col-sm-push-7 {
      left: 58.33333333%;
    }
    .'.$tag.'col-sm-push-6 {
      left: 50%;
    }
    .'.$tag.'col-sm-push-5 {
      left: 41.66666667%;
    }
    .'.$tag.'col-sm-push-4 {
      left: 33.33333333%;
    }
    .'.$tag.'col-sm-push-3 {
      left: 25%;
    }
    .'.$tag.'col-sm-push-2 {
      left: 16.66666667%;
    }
    .'.$tag.'col-sm-push-1 {
      left: 8.33333333%;
    }
    .'.$tag.'col-sm-push-0 {
      left: auto;
    }
    .'.$tag.'col-sm-offset-12 {
      margin-left: 100%;
    }
    .'.$tag.'col-sm-offset-11 {
      margin-left: 91.66666667%;
    }
    .'.$tag.'col-sm-offset-10 {
      margin-left: 83.33333333%;
    }
    .'.$tag.'col-sm-offset-9 {
      margin-left: 75%;
    }
    .'.$tag.'col-sm-offset-8 {
      margin-left: 66.66666667%;
    }
    .'.$tag.'col-sm-offset-7 {
      margin-left: 58.33333333%;
    }
    .'.$tag.'col-sm-offset-6 {
      margin-left: 50%;
    }
    .'.$tag.'col-sm-offset-5 {
      margin-left: 41.66666667%;
    }
    .'.$tag.'col-sm-offset-4 {
      margin-left: 33.33333333%;
    }
    .'.$tag.'col-sm-offset-3 {
      margin-left: 25%;
    }
    .'.$tag.'col-sm-offset-2 {
      margin-left: 16.66666667%;
    }
    .'.$tag.'col-sm-offset-1 {
      margin-left: 8.33333333%;
    }
    .'.$tag.'col-sm-offset-0 {
      margin-left: 0;
    }
  }
  @media (min-width: 992px) {
    .'.$tag.'col-md-1, .'.$tag.'col-md-2, .'.$tag.'col-md-3, .'.$tag.'col-md-4, .'.$tag.'col-md-5, .'.$tag.'col-md-6, .'.$tag.'col-md-7, .'.$tag.'col-md-8, .'.$tag.'col-md-9, .'.$tag.'col-md-10, .'.$tag.'col-md-11, .'.$tag.'col-md-12 {
      float: left;
    }
    .'.$tag.'col-md-12 {
      width: 100%;
    }
    .'.$tag.'col-md-11 {
      width: 91.66666667%;
    }
    .'.$tag.'col-md-10 {
      width: 83.33333333%;
    }
    .'.$tag.'col-md-9 {
      width: 75%;
    }
    .'.$tag.'col-md-8 {
      width: 66.66666667%;
    }
    .'.$tag.'col-md-7 {
      width: 58.33333333%;
    }
    .'.$tag.'col-md-6 {
      width: 50%;
    }
    .'.$tag.'col-md-5 {
      width: 41.66666667%;
    }
    .'.$tag.'col-md-4 {
      width: 33.33333333%;
    }
    .'.$tag.'col-md-3 {
      width: 25%;
    }
    .'.$tag.'col-md-2 {
      width: 16.66666667%;
    }
    .'.$tag.'col-md-1 {
      width: 8.33333333%;
    }
    .'.$tag.'col-md-pull-12 {
      right: 100%;
    }
    .'.$tag.'col-md-pull-11 {
      right: 91.66666667%;
    }
    .'.$tag.'col-md-pull-10 {
      right: 83.33333333%;
    }
    .'.$tag.'col-md-pull-9 {
      right: 75%;
    }
    .'.$tag.'col-md-pull-8 {
      right: 66.66666667%;
    }
    .'.$tag.'col-md-pull-7 {
      right: 58.33333333%;
    }
    .'.$tag.'col-md-pull-6 {
      right: 50%;
    }
    .'.$tag.'col-md-pull-5 {
      right: 41.66666667%;
    }
    .'.$tag.'col-md-pull-4 {
      right: 33.33333333%;
    }
    .'.$tag.'col-md-pull-3 {
      right: 25%;
    }
    .'.$tag.'col-md-pull-2 {
      right: 16.66666667%;
    }
    .'.$tag.'col-md-pull-1 {
      right: 8.33333333%;
    }
    .'.$tag.'col-md-pull-0 {
      right: auto;
    }
    .'.$tag.'col-md-push-12 {
      left: 100%;
    }
    .'.$tag.'col-md-push-11 {
      left: 91.66666667%;
    }
    .'.$tag.'col-md-push-10 {
      left: 83.33333333%;
    }
    .'.$tag.'col-md-push-9 {
      left: 75%;
    }
    .'.$tag.'col-md-push-8 {
      left: 66.66666667%;
    }
    .'.$tag.'col-md-push-7 {
      left: 58.33333333%;
    }
    .'.$tag.'col-md-push-6 {
      left: 50%;
    }
    .'.$tag.'col-md-push-5 {
      left: 41.66666667%;
    }
    .'.$tag.'col-md-push-4 {
      left: 33.33333333%;
    }
    .'.$tag.'col-md-push-3 {
      left: 25%;
    }
    .'.$tag.'col-md-push-2 {
      left: 16.66666667%;
    }
    .'.$tag.'col-md-push-1 {
      left: 8.33333333%;
    }
    .'.$tag.'col-md-push-0 {
      left: auto;
    }
    .'.$tag.'col-md-offset-12 {
      margin-left: 100%;
    }
    .'.$tag.'col-md-offset-11 {
      margin-left: 91.66666667%;
    }
    .'.$tag.'col-md-offset-10 {
      margin-left: 83.33333333%;
    }
    .'.$tag.'col-md-offset-9 {
      margin-left: 75%;
    }
    .'.$tag.'col-md-offset-8 {
      margin-left: 66.66666667%;
    }
    .'.$tag.'col-md-offset-7 {
      margin-left: 58.33333333%;
    }
    .'.$tag.'col-md-offset-6 {
      margin-left: 50%;
    }
    .'.$tag.'col-md-offset-5 {
      margin-left: 41.66666667%;
    }
    .'.$tag.'col-md-offset-4 {
      margin-left: 33.33333333%;
    }
    .'.$tag.'col-md-offset-3 {
      margin-left: 25%;
    }
    .'.$tag.'col-md-offset-2 {
      margin-left: 16.66666667%;
    }
    .'.$tag.'col-md-offset-1 {
      margin-left: 8.33333333%;
    }
    .'.$tag.'col-md-offset-0 {
      margin-left: 0;
    }
  }
  @media (min-width: 1200px) {
    .'.$tag.'col-lg-1, .'.$tag.'col-lg-2, .'.$tag.'col-lg-3, .'.$tag.'col-lg-4, .'.$tag.'col-lg-5, .'.$tag.'col-lg-6, .'.$tag.'col-lg-7, .'.$tag.'col-lg-8, .'.$tag.'col-lg-9, .'.$tag.'col-lg-10, .'.$tag.'col-lg-11, .'.$tag.'col-lg-12 {
      float: left;
    }
    .'.$tag.'col-lg-12 {
      width: 100%;
    }
    .'.$tag.'col-lg-11 {
      width: 91.66666667%;
    }
    .'.$tag.'col-lg-10 {
      width: 83.33333333%;
    }
    .'.$tag.'col-lg-9 {
      width: 75%;
    }
    .'.$tag.'col-lg-8 {
      width: 66.66666667%;
    }
    .'.$tag.'col-lg-7 {
      width: 58.33333333%;
    }
    .'.$tag.'col-lg-6 {
      width: 50%;
    }
    .'.$tag.'col-lg-5 {
      width: 41.66666667%;
    }
    .'.$tag.'col-lg-4 {
      width: 33.33333333%;
    }
    .'.$tag.'col-lg-3 {
      width: 25%;
    }
    .'.$tag.'col-lg-2 {
      width: 16.66666667%;
    }
    .'.$tag.'col-lg-1 {
      width: 8.33333333%;
    }
    .'.$tag.'col-lg-pull-12 {
      right: 100%;
    }
    .'.$tag.'col-lg-pull-11 {
      right: 91.66666667%;
    }
    .'.$tag.'col-lg-pull-10 {
      right: 83.33333333%;
    }
    .'.$tag.'col-lg-pull-9 {
      right: 75%;
    }
    .'.$tag.'col-lg-pull-8 {
      right: 66.66666667%;
    }
    .'.$tag.'col-lg-pull-7 {
      right: 58.33333333%;
    }
    .'.$tag.'col-lg-pull-6 {
      right: 50%;
    }
    .'.$tag.'col-lg-pull-5 {
      right: 41.66666667%;
    }
    .'.$tag.'col-lg-pull-4 {
      right: 33.33333333%;
    }
    .'.$tag.'col-lg-pull-3 {
      right: 25%;
    }
    .'.$tag.'col-lg-pull-2 {
      right: 16.66666667%;
    }
    .'.$tag.'col-lg-pull-1 {
      right: 8.33333333%;
    }
    .'.$tag.'col-lg-pull-0 {
      right: auto;
    }
    .'.$tag.'col-lg-push-12 {
      left: 100%;
    }
    .'.$tag.'col-lg-push-11 {
      left: 91.66666667%;
    }
    .'.$tag.'col-lg-push-10 {
      left: 83.33333333%;
    }
    .'.$tag.'col-lg-push-9 {
      left: 75%;
    }
    .'.$tag.'col-lg-push-8 {
      left: 66.66666667%;
    }
    .'.$tag.'col-lg-push-7 {
      left: 58.33333333%;
    }
    .'.$tag.'col-lg-push-6 {
      left: 50%;
    }
    .'.$tag.'col-lg-push-5 {
      left: 41.66666667%;
    }
    .'.$tag.'col-lg-push-4 {
      left: 33.33333333%;
    }
    .'.$tag.'col-lg-push-3 {
      left: 25%;
    }
    .'.$tag.'col-lg-push-2 {
      left: 16.66666667%;
    }
    .'.$tag.'col-lg-push-1 {
      left: 8.33333333%;
    }
    .'.$tag.'col-lg-push-0 {
      left: auto;
    }
    .'.$tag.'col-lg-offset-12 {
      margin-left: 100%;
    }
    .'.$tag.'col-lg-offset-11 {
      margin-left: 91.66666667%;
    }
    .'.$tag.'col-lg-offset-10 {
      margin-left: 83.33333333%;
    }
    .'.$tag.'col-lg-offset-9 {
      margin-left: 75%;
    }
    .'.$tag.'col-lg-offset-8 {
      margin-left: 66.66666667%;
    }
    .'.$tag.'col-lg-offset-7 {
      margin-left: 58.33333333%;
    }
    .'.$tag.'col-lg-offset-6 {
      margin-left: 50%;
    }
    .'.$tag.'col-lg-offset-5 {
      margin-left: 41.66666667%;
    }
    .'.$tag.'col-lg-offset-4 {
      margin-left: 33.33333333%;
    }
    .'.$tag.'col-lg-offset-3 {
      margin-left: 25%;
    }
    .'.$tag.'col-lg-offset-2 {
      margin-left: 16.66666667%;
    }
    .'.$tag.'col-lg-offset-1 {
      margin-left: 8.33333333%;
    }
    .'.$tag.'col-lg-offset-0 {
      margin-left: 0;
    }
  }
  table {
    background-color: transparent;
  }
  caption {
    padding-top: 8px;
    padding-bottom: 8px;
    color: #777;
    text-align: left;
  }
  th {
    text-align: left;
  }
  .'.$tag.'table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
  }
  .'.$tag.'table > thead > tr > th,
  .'.$tag.'table > tbody > tr > th,
  .'.$tag.'table > tfoot > tr > th,
  .'.$tag.'table > thead > tr > td,
  .'.$tag.'table > tbody > tr > td,
  .'.$tag.'table > tfoot > tr > td {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd;
  }
  .'.$tag.'table > thead > tr > th {
    vertical-align: bottom;
    border-bottom: 2px solid #ddd;
  }
  .'.$tag.'table > caption + thead > tr:first-child > th,
  .'.$tag.'table > colgroup + thead > tr:first-child > th,
  .'.$tag.'table > thead:first-child > tr:first-child > th,
  .'.$tag.'table > caption + thead > tr:first-child > td,
  .'.$tag.'table > colgroup + thead > tr:first-child > td,
  .'.$tag.'table > thead:first-child > tr:first-child > td {
    border-top: 0;
  }
  .'.$tag.'table > tbody + tbody {
    border-top: 2px solid #ddd;
  }
  .'.$tag.'table .'.$tag.'table {
    background-color: #fff;
  }
  .'.$tag.'table-condensed > thead > tr > th,
  .'.$tag.'table-condensed > tbody > tr > th,
  .'.$tag.'table-condensed > tfoot > tr > th,
  .'.$tag.'table-condensed > thead > tr > td,
  .'.$tag.'table-condensed > tbody > tr > td,
  .'.$tag.'table-condensed > tfoot > tr > td {
    padding: 5px;
  }
  .'.$tag.'table-bordered {
    border: 1px solid #ddd;
  }
  .'.$tag.'table-bordered > thead > tr > th,
  .'.$tag.'table-bordered > tbody > tr > th,
  .'.$tag.'table-bordered > tfoot > tr > th,
  .'.$tag.'table-bordered > thead > tr > td,
  .'.$tag.'table-bordered > tbody > tr > td,
  .'.$tag.'table-bordered > tfoot > tr > td {
    border: 1px solid #ddd;
  }
  .'.$tag.'table-bordered > thead > tr > th,
  .'.$tag.'table-bordered > thead > tr > td {
    border-bottom-width: 2px;
  }
  .'.$tag.'table-striped > tbody > tr:nth-of-type(odd) {
    background-color: #f9f9f9;
  }
  .'.$tag.'table-hover > tbody > tr:hover {
    background-color: #f5f5f5;
  }
  table col[class*="'.$tag.'col-"] {
    position: static;
    display: table-column;
    float: none;
  }
  table td[class*="'.$tag.'col-"],
  table th[class*="'.$tag.'col-"] {
    position: static;
    display: table-cell;
    float: none;
  }
  .'.$tag.'table > thead > tr > td.'.$tag.'active,
  .'.$tag.'table > tbody > tr > td.'.$tag.'active,
  .'.$tag.'table > tfoot > tr > td.'.$tag.'active,
  .'.$tag.'table > thead > tr > th.'.$tag.'active,
  .'.$tag.'table > tbody > tr > th.'.$tag.'active,
  .'.$tag.'table > tfoot > tr > th.'.$tag.'active,
  .'.$tag.'table > thead > tr.'.$tag.'active > td,
  .'.$tag.'table > tbody > tr.'.$tag.'active > td,
  .'.$tag.'table > tfoot > tr.'.$tag.'active > td,
  .'.$tag.'table > thead > tr.'.$tag.'active > th,
  .'.$tag.'table > tbody > tr.'.$tag.'active > th,
  .'.$tag.'table > tfoot > tr.'.$tag.'active > th {
    background-color: #f5f5f5;
  }
  .'.$tag.'table-hover > tbody > tr > td.'.$tag.'active:hover,
  .'.$tag.'table-hover > tbody > tr > th.'.$tag.'active:hover,
  .'.$tag.'table-hover > tbody > tr.'.$tag.'active:hover > td,
  .'.$tag.'table-hover > tbody > tr:hover > .'.$tag.'active,
  .'.$tag.'table-hover > tbody > tr.'.$tag.'active:hover > th {
    background-color: #e8e8e8;
  }
  .'.$tag.'table > thead > tr > td.'.$tag.'success,
  .'.$tag.'table > tbody > tr > td.'.$tag.'success,
  .'.$tag.'table > tfoot > tr > td.'.$tag.'success,
  .'.$tag.'table > thead > tr > th.'.$tag.'success,
  .'.$tag.'table > tbody > tr > th.'.$tag.'success,
  .'.$tag.'table > tfoot > tr > th.'.$tag.'success,
  .'.$tag.'table > thead > tr.'.$tag.'success > td,
  .'.$tag.'table > tbody > tr.'.$tag.'success > td,
  .'.$tag.'table > tfoot > tr.'.$tag.'success > td,
  .'.$tag.'table > thead > tr.'.$tag.'success > th,
  .'.$tag.'table > tbody > tr.'.$tag.'success > th,
  .'.$tag.'table > tfoot > tr.'.$tag.'success > th {
    background-color: #dff0d8;
  }
  .'.$tag.'table-hover > tbody > tr > td.'.$tag.'success:hover,
  .'.$tag.'table-hover > tbody > tr > th.'.$tag.'success:hover,
  .'.$tag.'table-hover > tbody > tr.'.$tag.'success:hover > td,
  .'.$tag.'table-hover > tbody > tr:hover > .'.$tag.'success,
  .'.$tag.'table-hover > tbody > tr.'.$tag.'success:hover > th {
    background-color: #d0e9c6;
  }
  .'.$tag.'table > thead > tr > td.'.$tag.'info,
  .'.$tag.'table > tbody > tr > td.'.$tag.'info,
  .'.$tag.'table > tfoot > tr > td.'.$tag.'info,
  .'.$tag.'table > thead > tr > th.'.$tag.'info,
  .'.$tag.'table > tbody > tr > th.'.$tag.'info,
  .'.$tag.'table > tfoot > tr > th.'.$tag.'info,
  .'.$tag.'table > thead > tr.'.$tag.'info > td,
  .'.$tag.'table > tbody > tr.'.$tag.'info > td,
  .'.$tag.'table > tfoot > tr.'.$tag.'info > td,
  .'.$tag.'table > thead > tr.'.$tag.'info > th,
  .'.$tag.'table > tbody > tr.'.$tag.'info > th,
  .'.$tag.'table > tfoot > tr.'.$tag.'info > th {
    background-color: #d9edf7;
  }
  .'.$tag.'table-hover > tbody > tr > td.'.$tag.'info:hover,
  .'.$tag.'table-hover > tbody > tr > th.'.$tag.'info:hover,
  .'.$tag.'table-hover > tbody > tr.'.$tag.'info:hover > td,
  .'.$tag.'table-hover > tbody > tr:hover > .'.$tag.'info,
  .'.$tag.'table-hover > tbody > tr.'.$tag.'info:hover > th {
    background-color: #c4e3f3;
  }
  .'.$tag.'table > thead > tr > td.'.$tag.'warning,
  .'.$tag.'table > tbody > tr > td.'.$tag.'warning,
  .'.$tag.'table > tfoot > tr > td.'.$tag.'warning,
  .'.$tag.'table > thead > tr > th.'.$tag.'warning,
  .'.$tag.'table > tbody > tr > th.'.$tag.'warning,
  .'.$tag.'table > tfoot > tr > th.'.$tag.'warning,
  .'.$tag.'table > thead > tr.'.$tag.'warning > td,
  .'.$tag.'table > tbody > tr.'.$tag.'warning > td,
  .'.$tag.'table > tfoot > tr.'.$tag.'warning > td,
  .'.$tag.'table > thead > tr.'.$tag.'warning > th,
  .'.$tag.'table > tbody > tr.'.$tag.'warning > th,
  .'.$tag.'table > tfoot > tr.'.$tag.'warning > th {
    background-color: #fcf8e3;
  }
  .'.$tag.'table-hover > tbody > tr > td.'.$tag.'warning:hover,
  .'.$tag.'table-hover > tbody > tr > th.'.$tag.'warning:hover,
  .'.$tag.'table-hover > tbody > tr.'.$tag.'warning:hover > td,
  .'.$tag.'table-hover > tbody > tr:hover > .'.$tag.'warning,
  .'.$tag.'table-hover > tbody > tr.'.$tag.'warning:hover > th {
    background-color: #faf2cc;
  }
  .'.$tag.'table > thead > tr > td.'.$tag.'danger,
  .'.$tag.'table > tbody > tr > td.'.$tag.'danger,
  .'.$tag.'table > tfoot > tr > td.'.$tag.'danger,
  .'.$tag.'table > thead > tr > th.'.$tag.'danger,
  .'.$tag.'table > tbody > tr > th.'.$tag.'danger,
  .'.$tag.'table > tfoot > tr > th.'.$tag.'danger,
  .'.$tag.'table > thead > tr.'.$tag.'danger > td,
  .'.$tag.'table > tbody > tr.'.$tag.'danger > td,
  .'.$tag.'table > tfoot > tr.'.$tag.'danger > td,
  .'.$tag.'table > thead > tr.'.$tag.'danger > th,
  .'.$tag.'table > tbody > tr.'.$tag.'danger > th,
  .'.$tag.'table > tfoot > tr.'.$tag.'danger > th {
    background-color: #f2dede;
  }
  .'.$tag.'table-hover > tbody > tr > td.'.$tag.'danger:hover,
  .'.$tag.'table-hover > tbody > tr > th.'.$tag.'danger:hover,
  .'.$tag.'table-hover > tbody > tr.'.$tag.'danger:hover > td,
  .'.$tag.'table-hover > tbody > tr:hover > .'.$tag.'danger,
  .'.$tag.'table-hover > tbody > tr.'.$tag.'danger:hover > th {
    background-color: #ebcccc;
  }
  .'.$tag.'table-responsive {
    min-height: .01%;
    overflow-x: auto;
  }
  @media screen and (max-width: 767px) {
    .'.$tag.'table-responsive {
      width: 100%;
      margin-bottom: 15px;
      overflow-y: hidden;
      -ms-overflow-style: -ms-autohiding-scrollbar;
      border: 1px solid #ddd;
    }
    .'.$tag.'table-responsive > .'.$tag.'table {
      margin-bottom: 0;
    }
    .'.$tag.'table-responsive > .'.$tag.'table > thead > tr > th,
    .'.$tag.'table-responsive > .'.$tag.'table > tbody > tr > th,
    .'.$tag.'table-responsive > .'.$tag.'table > tfoot > tr > th,
    .'.$tag.'table-responsive > .'.$tag.'table > thead > tr > td,
    .'.$tag.'table-responsive > .'.$tag.'table > tbody > tr > td,
    .'.$tag.'table-responsive > .'.$tag.'table > tfoot > tr > td {
      white-space: nowrap;
    }
    .'.$tag.'table-responsive > .'.$tag.'table-bordered {
      border: 0;
    }
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > thead > tr > th:first-child,
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > tbody > tr > th:first-child,
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > tfoot > tr > th:first-child,
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > thead > tr > td:first-child,
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > tbody > tr > td:first-child,
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > tfoot > tr > td:first-child {
      border-left: 0;
    }
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > thead > tr > th:last-child,
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > tbody > tr > th:last-child,
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > tfoot > tr > th:last-child,
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > thead > tr > td:last-child,
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > tbody > tr > td:last-child,
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > tfoot > tr > td:last-child {
      border-right: 0;
    }
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > tbody > tr:last-child > th,
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > tfoot > tr:last-child > th,
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > tbody > tr:last-child > td,
    .'.$tag.'table-responsive > .'.$tag.'table-bordered > tfoot > tr:last-child > td {
      border-bottom: 0;
    }
  }
  fieldset {
    min-width: 0;
    padding: 0;
    margin: 0;
    border: 0;
  }
  legend {
    display: block;
    width: 100%;
    padding: 0;
    margin-bottom: 20px;
    font-size: 21px;
    line-height: inherit;
    color: #333;
    border: 0;
    border-bottom: 1px solid #e5e5e5;
  }
  label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: bold;
  }
  input[type="search"] {
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
  }
  input[type="radio"],
  input[type="checkbox"] {
    margin: 4px 0 0;
    margin-top: 1px \9;
    line-height: normal;
  }
  input[type="file"] {
    display: block;
  }
  input[type="range"] {
    display: block;
    width: 100%;
  }
  select[multiple],
  select[size] {
    height: auto;
  }
  input[type="file"]:focus,
  input[type="radio"]:focus,
  input[type="checkbox"]:focus {
    outline: 5px auto -webkit-focus-ring-color;
    outline-offset: -2px;
  }
  output {
    display: block;
    padding-top: 7px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
  }
  .'.$tag.'form-control {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
         -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
  }
  .'.$tag.'form-control:focus {
    border-color: #66afe9;
    outline: 0;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
  }
  .'.$tag.'form-control::-moz-placeholder {
    color: #999;
    opacity: 1;
  }
  .'.$tag.'form-control:-ms-input-placeholder {
    color: #999;
  }
  .'.$tag.'form-control::-webkit-input-placeholder {
    color: #999;
  }
  .'.$tag.'form-control::-ms-expand {
    background-color: transparent;
    border: 0;
  }
  .'.$tag.'form-control[disabled],
  .'.$tag.'form-control[readonly],
  fieldset[disabled] .'.$tag.'form-control {
    background-color: #eee;
    opacity: 1;
  }
  .'.$tag.'form-control[disabled],
  fieldset[disabled] .'.$tag.'form-control {
    cursor: not-allowed;
  }
  textarea.'.$tag.'form-control {
    height: auto;
  }
  input[type="search"] {
    -webkit-appearance: none;
  }
  @media screen and (-webkit-min-device-pixel-ratio: 0) {
    input[type="date"].'.$tag.'form-control,
    input[type="time"].'.$tag.'form-control,
    input[type="datetime-local"].'.$tag.'form-control,
    input[type="month"].'.$tag.'form-control {
      line-height: 34px;
    }
    input[type="date"].'.$tag.'input-sm,
    input[type="time"].'.$tag.'input-sm,
    input[type="datetime-local"].'.$tag.'input-sm,
    input[type="month"].'.$tag.'input-sm,
    .'.$tag.'input-group-sm input[type="date"],
    .'.$tag.'input-group-sm input[type="time"],
    .'.$tag.'input-group-sm input[type="datetime-local"],
    .'.$tag.'input-group-sm input[type="month"] {
      line-height: 30px;
    }
    input[type="date"].'.$tag.'input-lg,
    input[type="time"].'.$tag.'input-lg,
    input[type="datetime-local"].'.$tag.'input-lg,
    input[type="month"].'.$tag.'input-lg,
    .'.$tag.'input-group-lg input[type="date"],
    .'.$tag.'input-group-lg input[type="time"],
    .'.$tag.'input-group-lg input[type="datetime-local"],
    .'.$tag.'input-group-lg input[type="month"] {
      line-height: 46px;
    }
  }
  .'.$tag.'form-group {
    margin-bottom: 15px;
  }
  .'.$tag.'radio,
  .'.$tag.'checkbox {
    position: relative;
    display: block;
    margin-top: 10px;
    margin-bottom: 10px;
  }
  .'.$tag.'radio label,
  .'.$tag.'checkbox label {
    min-height: 20px;
    padding-left: 20px;
    margin-bottom: 0;
    font-weight: normal;
    cursor: pointer;
  }
  .'.$tag.'radio input[type="radio"],
  .'.$tag.'radio-inline input[type="radio"],
  .'.$tag.'checkbox input[type="checkbox"],
  .'.$tag.'checkbox-inline input[type="checkbox"] {
    position: absolute;
    margin-top: 4px \9;
    margin-left: -20px;
  }
  .'.$tag.'radio + .'.$tag.'radio,
  .'.$tag.'checkbox + .'.$tag.'checkbox {
    margin-top: -5px;
  }
  .'.$tag.'radio-inline,
  .'.$tag.'checkbox-inline {
    position: relative;
    display: inline-block;
    padding-left: 20px;
    margin-bottom: 0;
    font-weight: normal;
    vertical-align: middle;
    cursor: pointer;
  }
  .'.$tag.'radio-inline + .'.$tag.'radio-inline,
  .'.$tag.'checkbox-inline + .'.$tag.'checkbox-inline {
    margin-top: 0;
    margin-left: 10px;
  }
  input[type="radio"][disabled],
  input[type="checkbox"][disabled],
  input[type="radio"].'.$tag.'disabled,
  input[type="checkbox"].'.$tag.'disabled,
  fieldset[disabled] input[type="radio"],
  fieldset[disabled] input[type="checkbox"] {
    cursor: not-allowed;
  }
  .'.$tag.'radio-inline.'.$tag.'disabled,
  .'.$tag.'checkbox-inline.'.$tag.'disabled,
  fieldset[disabled] .'.$tag.'radio-inline,
  fieldset[disabled] .'.$tag.'checkbox-inline {
    cursor: not-allowed;
  }
  .'.$tag.'radio.'.$tag.'disabled label,
  .'.$tag.'checkbox.'.$tag.'disabled label,
  fieldset[disabled] .'.$tag.'radio label,
  fieldset[disabled] .'.$tag.'checkbox label {
    cursor: not-allowed;
  }
  .'.$tag.'form-control-static {
    min-height: 34px;
    padding-top: 7px;
    padding-bottom: 7px;
    margin-bottom: 0;
  }
  .'.$tag.'form-control-static.'.$tag.'input-lg,
  .'.$tag.'form-control-static.'.$tag.'input-sm {
    padding-right: 0;
    padding-left: 0;
  }
  .'.$tag.'input-sm {
    height: 30px;
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
  }
  select.'.$tag.'input-sm {
    height: 30px;
    line-height: 30px;
  }
  textarea.'.$tag.'input-sm,
  select[multiple].'.$tag.'input-sm {
    height: auto;
  }
  .'.$tag.'form-group-sm .'.$tag.'form-control {
    height: 30px;
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
  }
  .'.$tag.'form-group-sm select.'.$tag.'form-control {
    height: 30px;
    line-height: 30px;
  }
  .'.$tag.'form-group-sm textarea.'.$tag.'form-control,
  .'.$tag.'form-group-sm select[multiple].'.$tag.'form-control {
    height: auto;
  }
  .'.$tag.'form-group-sm .'.$tag.'form-control-static {
    height: 30px;
    min-height: 32px;
    padding: 6px 10px;
    font-size: 12px;
    line-height: 1.5;
  }
  .'.$tag.'input-lg {
    height: 46px;
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
  }
  select.'.$tag.'input-lg {
    height: 46px;
    line-height: 46px;
  }
  textarea.'.$tag.'input-lg,
  select[multiple].'.$tag.'input-lg {
    height: auto;
  }
  .'.$tag.'form-group-lg .'.$tag.'form-control {
    height: 46px;
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
  }
  .'.$tag.'form-group-lg select.'.$tag.'form-control {
    height: 46px;
    line-height: 46px;
  }
  .'.$tag.'form-group-lg textarea.'.$tag.'form-control,
  .'.$tag.'form-group-lg select[multiple].'.$tag.'form-control {
    height: auto;
  }
  .'.$tag.'form-group-lg .'.$tag.'form-control-static {
    height: 46px;
    min-height: 38px;
    padding: 11px 16px;
    font-size: 18px;
    line-height: 1.3333333;
  }
  .'.$tag.'has-feedback {
    position: relative;
  }
  .'.$tag.'has-feedback .'.$tag.'form-control {
    padding-right: 42.5px;
  }
  .'.$tag.'form-control-feedback {
    position: absolute;
    top: 0;
    right: 0;
    z-index: 2;
    display: block;
    width: 34px;
    height: 34px;
    line-height: 34px;
    text-align: center;
    pointer-events: none;
  }
  .'.$tag.'input-lg + .'.$tag.'form-control-feedback,
  .'.$tag.'input-group-lg + .'.$tag.'form-control-feedback,
  .'.$tag.'form-group-lg .'.$tag.'form-control + .'.$tag.'form-control-feedback {
    width: 46px;
    height: 46px;
    line-height: 46px;
  }
  .'.$tag.'input-sm + .'.$tag.'form-control-feedback,
  .'.$tag.'input-group-sm + .'.$tag.'form-control-feedback,
  .'.$tag.'form-group-sm .'.$tag.'form-control + .'.$tag.'form-control-feedback {
    width: 30px;
    height: 30px;
    line-height: 30px;
  }
  .'.$tag.'has-success .'.$tag.'help-block,
  .'.$tag.'has-success .'.$tag.'control-label,
  .'.$tag.'has-success .'.$tag.'radio,
  .'.$tag.'has-success .'.$tag.'checkbox,
  .'.$tag.'has-success .'.$tag.'radio-inline,
  .'.$tag.'has-success .'.$tag.'checkbox-inline,
  .'.$tag.'has-success.'.$tag.'radio label,
  .'.$tag.'has-success.'.$tag.'checkbox label,
  .'.$tag.'has-success.'.$tag.'radio-inline label,
  .'.$tag.'has-success.'.$tag.'checkbox-inline label {
    color: #3c763d;
  }
  .'.$tag.'has-success .'.$tag.'form-control {
    border-color: #3c763d;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  }
  .'.$tag.'has-success .'.$tag.'form-control:focus {
    border-color: #2b542c;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #67b168;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #67b168;
  }
  .'.$tag.'has-success .'.$tag.'input-group-addon {
    color: #3c763d;
    background-color: #dff0d8;
    border-color: #3c763d;
  }
  .'.$tag.'has-success .'.$tag.'form-control-feedback {
    color: #3c763d;
  }
  .'.$tag.'has-warning .'.$tag.'help-block,
  .'.$tag.'has-warning .'.$tag.'control-label,
  .'.$tag.'has-warning .'.$tag.'radio,
  .'.$tag.'has-warning .'.$tag.'checkbox,
  .'.$tag.'has-warning .'.$tag.'radio-inline,
  .'.$tag.'has-warning .'.$tag.'checkbox-inline,
  .'.$tag.'has-warning.'.$tag.'radio label,
  .'.$tag.'has-warning.'.$tag.'checkbox label,
  .'.$tag.'has-warning.'.$tag.'radio-inline label,
  .'.$tag.'has-warning.'.$tag.'checkbox-inline label {
    color: #8a6d3b;
  }
  .'.$tag.'has-warning .'.$tag.'form-control {
    border-color: #8a6d3b;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  }
  .'.$tag.'has-warning .'.$tag.'form-control:focus {
    border-color: #66512c;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #c0a16b;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #c0a16b;
  }
  .'.$tag.'has-warning .'.$tag.'input-group-addon {
    color: #8a6d3b;
    background-color: #fcf8e3;
    border-color: #8a6d3b;
  }
  .'.$tag.'has-warning .'.$tag.'form-control-feedback {
    color: #8a6d3b;
  }
  .'.$tag.'has-error .'.$tag.'help-block,
  .'.$tag.'has-error .'.$tag.'control-label,
  .'.$tag.'has-error .'.$tag.'radio,
  .'.$tag.'has-error .'.$tag.'checkbox,
  .'.$tag.'has-error .'.$tag.'radio-inline,
  .'.$tag.'has-error .'.$tag.'checkbox-inline,
  .'.$tag.'has-error.'.$tag.'radio label,
  .'.$tag.'has-error.'.$tag.'checkbox label,
  .'.$tag.'has-error.'.$tag.'radio-inline label,
  .'.$tag.'has-error.'.$tag.'checkbox-inline label {
    color: #a94442;
  }
  .'.$tag.'has-error .'.$tag.'form-control {
    border-color: #a94442;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  }
  .'.$tag.'has-error .'.$tag.'form-control:focus {
    border-color: #843534;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
  }
  .'.$tag.'has-error .'.$tag.'input-group-addon {
    color: #a94442;
    background-color: #f2dede;
    border-color: #a94442;
  }
  .'.$tag.'has-error .'.$tag.'form-control-feedback {
    color: #a94442;
  }
  .'.$tag.'has-feedback label ~ .'.$tag.'form-control-feedback {
    top: 25px;
  }
  .'.$tag.'has-feedback label.'.$tag.'sr-only ~ .'.$tag.'form-control-feedback {
    top: 0;
  }
  .'.$tag.'help-block {
    display: block;
    margin-top: 5px;
    margin-bottom: 10px;
    color: #737373;
  }
  @media (min-width: 768px) {
    .'.$tag.'form-inline .'.$tag.'form-group {
      display: inline-block;
      margin-bottom: 0;
      vertical-align: middle;
    }
    .'.$tag.'form-inline .'.$tag.'form-control {
      display: inline-block;
      width: auto;
      vertical-align: middle;
    }
    .'.$tag.'form-inline .'.$tag.'form-control-static {
      display: inline-block;
    }
    .'.$tag.'form-inline .'.$tag.'input-group {
      display: inline-table;
      vertical-align: middle;
    }
    .'.$tag.'form-inline .'.$tag.'input-group .'.$tag.'input-group-addon,
    .'.$tag.'form-inline .'.$tag.'input-group .'.$tag.'input-group-btn,
    .'.$tag.'form-inline .'.$tag.'input-group .'.$tag.'form-control {
      width: auto;
    }
    .'.$tag.'form-inline .'.$tag.'input-group > .'.$tag.'form-control {
      width: 100%;
    }
    .'.$tag.'form-inline .'.$tag.'control-label {
      margin-bottom: 0;
      vertical-align: middle;
    }
    .'.$tag.'form-inline .'.$tag.'radio,
    .'.$tag.'form-inline .'.$tag.'checkbox {
      display: inline-block;
      margin-top: 0;
      margin-bottom: 0;
      vertical-align: middle;
    }
    .'.$tag.'form-inline .'.$tag.'radio label,
    .'.$tag.'form-inline .'.$tag.'checkbox label {
      padding-left: 0;
    }
    .'.$tag.'form-inline .'.$tag.'radio input[type="radio"],
    .'.$tag.'form-inline .'.$tag.'checkbox input[type="checkbox"] {
      position: relative;
      margin-left: 0;
    }
    .'.$tag.'form-inline .'.$tag.'has-feedback .'.$tag.'form-control-feedback {
      top: 0;
    }
  }
  .'.$tag.'form-horizontal .'.$tag.'radio,
  .'.$tag.'form-horizontal .'.$tag.'checkbox,
  .'.$tag.'form-horizontal .'.$tag.'radio-inline,
  .'.$tag.'form-horizontal .'.$tag.'checkbox-inline {
    padding-top: 7px;
    margin-top: 0;
    margin-bottom: 0;
  }
  .'.$tag.'form-horizontal .'.$tag.'radio,
  .'.$tag.'form-horizontal .'.$tag.'checkbox {
    min-height: 27px;
  }
  .'.$tag.'form-horizontal .'.$tag.'form-group {
    margin-right: -15px;
    margin-left: -15px;
  }
  @media (min-width: 768px) {
    .'.$tag.'form-horizontal .'.$tag.'control-label {
      padding-top: 7px;
      margin-bottom: 0;
      text-align: right;
    }
  }
  .'.$tag.'form-horizontal .'.$tag.'has-feedback .'.$tag.'form-control-feedback {
    right: 15px;
  }
  @media (min-width: 768px) {
    .'.$tag.'form-horizontal .'.$tag.'form-group-lg .'.$tag.'control-label {
      padding-top: 11px;
      font-size: 18px;
    }
  }
  @media (min-width: 768px) {
    .'.$tag.'form-horizontal .'.$tag.'form-group-sm .'.$tag.'control-label {
      padding-top: 6px;
      font-size: 12px;
    }
  }
  .'.$tag.'btn {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
        touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
       -moz-user-select: none;
        -ms-user-select: none;
            user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
  }
  .'.$tag.'btn:focus,
  .'.$tag.'btn:active:focus,
  .'.$tag.'btn.'.$tag.'active:focus,
  .'.$tag.'btn.'.$tag.'focus,
  .'.$tag.'btn:active.'.$tag.'focus,
  .'.$tag.'btn.'.$tag.'active.'.$tag.'focus {
    outline: 5px auto -webkit-focus-ring-color;
    outline-offset: -2px;
  }
  .'.$tag.'btn:hover,
  .'.$tag.'btn:focus,
  .'.$tag.'btn.'.$tag.'focus {
    color: #333;
    text-decoration: none;
  }
  .'.$tag.'btn:active,
  .'.$tag.'btn.'.$tag.'active {
    background-image: none;
    outline: 0;
    -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
            box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
  }
  .'.$tag.'btn.'.$tag.'disabled,
  .'.$tag.'btn[disabled],
  fieldset[disabled] .'.$tag.'btn {
    cursor: not-allowed;
    filter: alpha(opacity=65);
    -webkit-box-shadow: none;
            box-shadow: none;
    opacity: .65;
  }
  a.'.$tag.'btn.'.$tag.'disabled,
  fieldset[disabled] a.'.$tag.'btn {
    pointer-events: none;
  }
  .'.$tag.'btn-default {
    color: #333;
    background-color: #fff;
    border-color: #ccc;
  }
  .'.$tag.'btn-default:focus,
  .'.$tag.'btn-default.'.$tag.'focus {
    color: #333;
    background-color: #e6e6e6;
    border-color: #8c8c8c;
  }
  .'.$tag.'btn-default:hover {
    color: #333;
    background-color: #e6e6e6;
    border-color: #adadad;
  }
  .'.$tag.'btn-default:active,
  .'.$tag.'btn-default.'.$tag.'active,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-default {
    color: #333;
    background-color: #e6e6e6;
    border-color: #adadad;
  }
  .'.$tag.'btn-default:active:hover,
  .'.$tag.'btn-default.'.$tag.'active:hover,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-default:hover,
  .'.$tag.'btn-default:active:focus,
  .'.$tag.'btn-default.'.$tag.'active:focus,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-default:focus,
  .'.$tag.'btn-default:active.'.$tag.'focus,
  .'.$tag.'btn-default.'.$tag.'active.'.$tag.'focus,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-default.'.$tag.'focus {
    color: #333;
    background-color: #d4d4d4;
    border-color: #8c8c8c;
  }
  .'.$tag.'btn-default:active,
  .'.$tag.'btn-default.'.$tag.'active,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-default {
    background-image: none;
  }
  .'.$tag.'btn-default.'.$tag.'disabled:hover,
  .'.$tag.'btn-default[disabled]:hover,
  fieldset[disabled] .'.$tag.'btn-default:hover,
  .'.$tag.'btn-default.'.$tag.'disabled:focus,
  .'.$tag.'btn-default[disabled]:focus,
  fieldset[disabled] .'.$tag.'btn-default:focus,
  .'.$tag.'btn-default.'.$tag.'disabled.'.$tag.'focus,
  .'.$tag.'btn-default[disabled].'.$tag.'focus,
  fieldset[disabled] .'.$tag.'btn-default.'.$tag.'focus {
    background-color: #fff;
    border-color: #ccc;
  }
  .'.$tag.'btn-default .'.$tag.'badge {
    color: #fff;
    background-color: #333;
  }
  .'.$tag.'btn-primary {
    color: #fff;
    background-color: #337ab7;
    border-color: #2e6da4;
  }
  .'.$tag.'btn-primary:focus,
  .'.$tag.'btn-primary.'.$tag.'focus {
    color: #fff;
    background-color: #286090;
    border-color: #122b40;
  }
  .'.$tag.'btn-primary:hover {
    color: #fff;
    background-color: #286090;
    border-color: #204d74;
  }
  .'.$tag.'btn-primary:active,
  .'.$tag.'btn-primary.'.$tag.'active,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-primary {
    color: #fff;
    background-color: #286090;
    border-color: #204d74;
  }
  .'.$tag.'btn-primary:active:hover,
  .'.$tag.'btn-primary.'.$tag.'active:hover,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-primary:hover,
  .'.$tag.'btn-primary:active:focus,
  .'.$tag.'btn-primary.'.$tag.'active:focus,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-primary:focus,
  .'.$tag.'btn-primary:active.'.$tag.'focus,
  .'.$tag.'btn-primary.'.$tag.'active.'.$tag.'focus,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-primary.'.$tag.'focus {
    color: #fff;
    background-color: #204d74;
    border-color: #122b40;
  }
  .'.$tag.'btn-primary:active,
  .'.$tag.'btn-primary.'.$tag.'active,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-primary {
    background-image: none;
  }
  .'.$tag.'btn-primary.'.$tag.'disabled:hover,
  .'.$tag.'btn-primary[disabled]:hover,
  fieldset[disabled] .'.$tag.'btn-primary:hover,
  .'.$tag.'btn-primary.'.$tag.'disabled:focus,
  .'.$tag.'btn-primary[disabled]:focus,
  fieldset[disabled] .'.$tag.'btn-primary:focus,
  .'.$tag.'btn-primary.'.$tag.'disabled.'.$tag.'focus,
  .'.$tag.'btn-primary[disabled].'.$tag.'focus,
  fieldset[disabled] .'.$tag.'btn-primary.'.$tag.'focus {
    background-color: #337ab7;
    border-color: #2e6da4;
  }
  .'.$tag.'btn-primary .'.$tag.'badge {
    color: #337ab7;
    background-color: #fff;
  }
  .'.$tag.'btn-success {
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
  }
  .'.$tag.'btn-success:focus,
  .'.$tag.'btn-success.'.$tag.'focus {
    color: #fff;
    background-color: #449d44;
    border-color: #255625;
  }
  .'.$tag.'btn-success:hover {
    color: #fff;
    background-color: #449d44;
    border-color: #398439;
  }
  .'.$tag.'btn-success:active,
  .'.$tag.'btn-success.'.$tag.'active,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-success {
    color: #fff;
    background-color: #449d44;
    border-color: #398439;
  }
  .'.$tag.'btn-success:active:hover,
  .'.$tag.'btn-success.'.$tag.'active:hover,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-success:hover,
  .'.$tag.'btn-success:active:focus,
  .'.$tag.'btn-success.'.$tag.'active:focus,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-success:focus,
  .'.$tag.'btn-success:active.'.$tag.'focus,
  .'.$tag.'btn-success.'.$tag.'active.'.$tag.'focus,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-success.'.$tag.'focus {
    color: #fff;
    background-color: #398439;
    border-color: #255625;
  }
  .'.$tag.'btn-success:active,
  .'.$tag.'btn-success.'.$tag.'active,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-success {
    background-image: none;
  }
  .'.$tag.'btn-success.'.$tag.'disabled:hover,
  .'.$tag.'btn-success[disabled]:hover,
  fieldset[disabled] .'.$tag.'btn-success:hover,
  .'.$tag.'btn-success.'.$tag.'disabled:focus,
  .'.$tag.'btn-success[disabled]:focus,
  fieldset[disabled] .'.$tag.'btn-success:focus,
  .'.$tag.'btn-success.'.$tag.'disabled.'.$tag.'focus,
  .'.$tag.'btn-success[disabled].'.$tag.'focus,
  fieldset[disabled] .'.$tag.'btn-success.'.$tag.'focus {
    background-color: #5cb85c;
    border-color: #4cae4c;
  }
  .'.$tag.'btn-success .'.$tag.'badge {
    color: #5cb85c;
    background-color: #fff;
  }
  .'.$tag.'btn-info {
    color: #fff;
    background-color: #5bc0de;
    border-color: #46b8da;
  }
  .'.$tag.'btn-info:focus,
  .'.$tag.'btn-info.'.$tag.'focus {
    color: #fff;
    background-color: #31b0d5;
    border-color: #1b6d85;
  }
  .'.$tag.'btn-info:hover {
    color: #fff;
    background-color: #31b0d5;
    border-color: #269abc;
  }
  .'.$tag.'btn-info:active,
  .'.$tag.'btn-info.'.$tag.'active,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-info {
    color: #fff;
    background-color: #31b0d5;
    border-color: #269abc;
  }
  .'.$tag.'btn-info:active:hover,
  .'.$tag.'btn-info.'.$tag.'active:hover,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-info:hover,
  .'.$tag.'btn-info:active:focus,
  .'.$tag.'btn-info.'.$tag.'active:focus,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-info:focus,
  .'.$tag.'btn-info:active.'.$tag.'focus,
  .'.$tag.'btn-info.'.$tag.'active.'.$tag.'focus,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-info.'.$tag.'focus {
    color: #fff;
    background-color: #269abc;
    border-color: #1b6d85;
  }
  .'.$tag.'btn-info:active,
  .'.$tag.'btn-info.'.$tag.'active,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-info {
    background-image: none;
  }
  .'.$tag.'btn-info.'.$tag.'disabled:hover,
  .'.$tag.'btn-info[disabled]:hover,
  fieldset[disabled] .'.$tag.'btn-info:hover,
  .'.$tag.'btn-info.'.$tag.'disabled:focus,
  .'.$tag.'btn-info[disabled]:focus,
  fieldset[disabled] .'.$tag.'btn-info:focus,
  .'.$tag.'btn-info.'.$tag.'disabled.'.$tag.'focus,
  .'.$tag.'btn-info[disabled].'.$tag.'focus,
  fieldset[disabled] .'.$tag.'btn-info.'.$tag.'focus {
    background-color: #5bc0de;
    border-color: #46b8da;
  }
  .'.$tag.'btn-info .'.$tag.'badge {
    color: #5bc0de;
    background-color: #fff;
  }
  .'.$tag.'btn-warning {
    color: #fff;
    background-color: #f0ad4e;
    border-color: #eea236;
  }
  .'.$tag.'btn-warning:focus,
  .'.$tag.'btn-warning.'.$tag.'focus {
    color: #fff;
    background-color: #ec971f;
    border-color: #985f0d;
  }
  .'.$tag.'btn-warning:hover {
    color: #fff;
    background-color: #ec971f;
    border-color: #d58512;
  }
  .'.$tag.'btn-warning:active,
  .'.$tag.'btn-warning.'.$tag.'active,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-warning {
    color: #fff;
    background-color: #ec971f;
    border-color: #d58512;
  }
  .'.$tag.'btn-warning:active:hover,
  .'.$tag.'btn-warning.'.$tag.'active:hover,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-warning:hover,
  .'.$tag.'btn-warning:active:focus,
  .'.$tag.'btn-warning.'.$tag.'active:focus,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-warning:focus,
  .'.$tag.'btn-warning:active.'.$tag.'focus,
  .'.$tag.'btn-warning.'.$tag.'active.'.$tag.'focus,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-warning.'.$tag.'focus {
    color: #fff;
    background-color: #d58512;
    border-color: #985f0d;
  }
  .'.$tag.'btn-warning:active,
  .'.$tag.'btn-warning.'.$tag.'active,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-warning {
    background-image: none;
  }
  .'.$tag.'btn-warning.'.$tag.'disabled:hover,
  .'.$tag.'btn-warning[disabled]:hover,
  fieldset[disabled] .'.$tag.'btn-warning:hover,
  .'.$tag.'btn-warning.'.$tag.'disabled:focus,
  .'.$tag.'btn-warning[disabled]:focus,
  fieldset[disabled] .'.$tag.'btn-warning:focus,
  .'.$tag.'btn-warning.'.$tag.'disabled.'.$tag.'focus,
  .'.$tag.'btn-warning[disabled].'.$tag.'focus,
  fieldset[disabled] .'.$tag.'btn-warning.'.$tag.'focus {
    background-color: #f0ad4e;
    border-color: #eea236;
  }
  .'.$tag.'btn-warning .'.$tag.'badge {
    color: #f0ad4e;
    background-color: #fff;
  }
  .'.$tag.'btn-danger {
    color: #fff;
    background-color: #d9534f;
    border-color: #d43f3a;
  }
  .'.$tag.'btn-danger:focus,
  .'.$tag.'btn-danger.'.$tag.'focus {
    color: #fff;
    background-color: #c9302c;
    border-color: #761c19;
  }
  .'.$tag.'btn-danger:hover {
    color: #fff;
    background-color: #c9302c;
    border-color: #ac2925;
  }
  .'.$tag.'btn-danger:active,
  .'.$tag.'btn-danger.'.$tag.'active,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-danger {
    color: #fff;
    background-color: #c9302c;
    border-color: #ac2925;
  }
  .'.$tag.'btn-danger:active:hover,
  .'.$tag.'btn-danger.'.$tag.'active:hover,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-danger:hover,
  .'.$tag.'btn-danger:active:focus,
  .'.$tag.'btn-danger.'.$tag.'active:focus,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-danger:focus,
  .'.$tag.'btn-danger:active.'.$tag.'focus,
  .'.$tag.'btn-danger.'.$tag.'active.'.$tag.'focus,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-danger.'.$tag.'focus {
    color: #fff;
    background-color: #ac2925;
    border-color: #761c19;
  }
  .'.$tag.'btn-danger:active,
  .'.$tag.'btn-danger.'.$tag.'active,
  .'.$tag.'open > .'.$tag.'dropdown-toggle.'.$tag.'btn-danger {
    background-image: none;
  }
  .'.$tag.'btn-danger.'.$tag.'disabled:hover,
  .'.$tag.'btn-danger[disabled]:hover,
  fieldset[disabled] .'.$tag.'btn-danger:hover,
  .'.$tag.'btn-danger.'.$tag.'disabled:focus,
  .'.$tag.'btn-danger[disabled]:focus,
  fieldset[disabled] .'.$tag.'btn-danger:focus,
  .'.$tag.'btn-danger.'.$tag.'disabled.'.$tag.'focus,
  .'.$tag.'btn-danger[disabled].'.$tag.'focus,
  fieldset[disabled] .'.$tag.'btn-danger.'.$tag.'focus {
    background-color: #d9534f;
    border-color: #d43f3a;
  }
  .'.$tag.'btn-danger .'.$tag.'badge {
    color: #d9534f;
    background-color: #fff;
  }
  .'.$tag.'btn-link {
    font-weight: normal;
    color: #337ab7;
    border-radius: 0;
  }
  .'.$tag.'btn-link,
  .'.$tag.'btn-link:active,
  .'.$tag.'btn-link.'.$tag.'active,
  .'.$tag.'btn-link[disabled],
  fieldset[disabled] .'.$tag.'btn-link {
    background-color: transparent;
    -webkit-box-shadow: none;
            box-shadow: none;
  }
  .'.$tag.'btn-link,
  .'.$tag.'btn-link:hover,
  .'.$tag.'btn-link:focus,
  .'.$tag.'btn-link:active {
    border-color: transparent;
  }
  .'.$tag.'btn-link:hover,
  .'.$tag.'btn-link:focus {
    color: #23527c;
    text-decoration: underline;
    background-color: transparent;
  }
  .'.$tag.'btn-link[disabled]:hover,
  fieldset[disabled] .'.$tag.'btn-link:hover,
  .'.$tag.'btn-link[disabled]:focus,
  fieldset[disabled] .'.$tag.'btn-link:focus {
    color: #777;
    text-decoration: none;
  }
  .'.$tag.'btn-lg,
  .'.$tag.'btn-group-lg > .'.$tag.'btn {
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
  }
  .'.$tag.'btn-sm,
  .'.$tag.'btn-group-sm > .'.$tag.'btn {
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
  }
  .'.$tag.'btn-xs,
  .'.$tag.'btn-group-xs > .'.$tag.'btn {
    padding: 1px 5px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
  }
  .'.$tag.'btn-block {
    display: block;
    width: 100%;
  }
  .'.$tag.'btn-block + .'.$tag.'btn-block {
    margin-top: 5px;
  }
  input[type="submit"].'.$tag.'btn-block,
  input[type="reset"].'.$tag.'btn-block,
  input[type="button"].'.$tag.'btn-block {
    width: 100%;
  }
  .'.$tag.'fade {
    opacity: 0;
    -webkit-transition: opacity .15s linear;
         -o-transition: opacity .15s linear;
            transition: opacity .15s linear;
  }
  .'.$tag.'fade.'.$tag.'in {
    opacity: 1;
  }
  .'.$tag.'collapse {
    display: none;
  }
  .'.$tag.'collapse.'.$tag.'in {
    display: block;
  }
  tr.'.$tag.'collapse.'.$tag.'in {
    display: table-row;
  }
  tbody.'.$tag.'collapse.'.$tag.'in {
    display: table-row-group;
  }
  .'.$tag.'collapsing {
    position: relative;
    height: 0;
    overflow: hidden;
    -webkit-transition-timing-function: ease;
         -o-transition-timing-function: ease;
            transition-timing-function: ease;
    -webkit-transition-duration: .35s;
         -o-transition-duration: .35s;
            transition-duration: .35s;
    -webkit-transition-property: height, visibility;
         -o-transition-property: height, visibility;
            transition-property: height, visibility;
  }
  .'.$tag.'caret {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: 2px;
    vertical-align: middle;
    border-top: 4px dashed;
    border-top: 4px solid \9;
    border-right: 4px solid transparent;
    border-left: 4px solid transparent;
  }
  .'.$tag.'dropup,
  .'.$tag.'dropdown {
    position: relative;
  }
  .'.$tag.'dropdown-toggle:focus {
    outline: 0;
  }
  .'.$tag.'dropdown-menu {
    position: absolute;
    top: 100%;
    left: 0;
    z-index: 1000;
    display: none;
    float: left;
    min-width: 160px;
    padding: 5px 0;
    margin: 2px 0 0;
    font-size: 14px;
    text-align: left;
    list-style: none;
    background-color: #fff;
    -webkit-background-clip: padding-box;
            background-clip: padding-box;
    border: 1px solid #ccc;
    border: 1px solid rgba(0, 0, 0, .15);
    border-radius: 4px;
    -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
  }
  .'.$tag.'dropdown-menu.'.$tag.'pull-right {
    right: 0;
    left: auto;
  }
  .'.$tag.'dropdown-menu .'.$tag.'divider {
    height: 1px;
    margin: 9px 0;
    overflow: hidden;
    background-color: #e5e5e5;
  }
  .'.$tag.'dropdown-menu > li > a {
    display: block;
    padding: 3px 20px;
    clear: both;
    font-weight: normal;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
  }
  .'.$tag.'dropdown-menu > li > a:hover,
  .'.$tag.'dropdown-menu > li > a:focus {
    color: #262626;
    text-decoration: none;
    background-color: #f5f5f5;
  }
  .'.$tag.'dropdown-menu > .'.$tag.'active > a,
  .'.$tag.'dropdown-menu > .'.$tag.'active > a:hover,
  .'.$tag.'dropdown-menu > .'.$tag.'active > a:focus {
    color: #fff;
    text-decoration: none;
    background-color: #337ab7;
    outline: 0;
  }
  .'.$tag.'dropdown-menu > .'.$tag.'disabled > a,
  .'.$tag.'dropdown-menu > .'.$tag.'disabled > a:hover,
  .'.$tag.'dropdown-menu > .'.$tag.'disabled > a:focus {
    color: #777;
  }
  .'.$tag.'dropdown-menu > .'.$tag.'disabled > a:hover,
  .'.$tag.'dropdown-menu > .'.$tag.'disabled > a:focus {
    text-decoration: none;
    cursor: not-allowed;
    background-color: transparent;
    background-image: none;
    filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
  }
  .'.$tag.'open > .'.$tag.'dropdown-menu {
    display: block;
  }
  .'.$tag.'open > a {
    outline: 0;
  }
  .'.$tag.'dropdown-menu-right {
    right: 0;
    left: auto;
  }
  .'.$tag.'dropdown-menu-left {
    right: auto;
    left: 0;
  }
  .'.$tag.'dropdown-header {
    display: block;
    padding: 3px 20px;
    font-size: 12px;
    line-height: 1.42857143;
    color: #777;
    white-space: nowrap;
  }
  .'.$tag.'dropdown-backdrop {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 990;
  }
  .'.$tag.'pull-right > .'.$tag.'dropdown-menu {
    right: 0;
    left: auto;
  }
  .'.$tag.'dropup .'.$tag.'caret,
  .'.$tag.'navbar-fixed-bottom .'.$tag.'dropdown .'.$tag.'caret {
    content: "";
    border-top: 0;
    border-bottom: 4px dashed;
    border-bottom: 4px solid \9;
  }
  .'.$tag.'dropup .'.$tag.'dropdown-menu,
  .'.$tag.'navbar-fixed-bottom .'.$tag.'dropdown .'.$tag.'dropdown-menu {
    top: auto;
    bottom: 100%;
    margin-bottom: 2px;
  }
  @media (min-width: 768px) {
    .'.$tag.'navbar-right .'.$tag.'dropdown-menu {
      right: 0;
      left: auto;
    }
    .'.$tag.'navbar-right .'.$tag.'dropdown-menu-left {
      right: auto;
      left: 0;
    }
  }
  .'.$tag.'btn-group,
  .'.$tag.'btn-group-vertical {
    position: relative;
    display: inline-block;
    vertical-align: middle;
  }
  .'.$tag.'btn-group > .'.$tag.'btn,
  .'.$tag.'btn-group-vertical > .'.$tag.'btn {
    position: relative;
    float: left;
  }
  .'.$tag.'btn-group > .'.$tag.'btn:hover,
  .'.$tag.'btn-group-vertical > .'.$tag.'btn:hover,
  .'.$tag.'btn-group > .'.$tag.'btn:focus,
  .'.$tag.'btn-group-vertical > .'.$tag.'btn:focus,
  .'.$tag.'btn-group > .'.$tag.'btn:active,
  .'.$tag.'btn-group-vertical > .'.$tag.'btn:active,
  .'.$tag.'btn-group > .'.$tag.'btn.'.$tag.'active,
  .'.$tag.'btn-group-vertical > .'.$tag.'btn.'.$tag.'active {
    z-index: 2;
  }
  .'.$tag.'btn-group .'.$tag.'btn + .'.$tag.'btn,
  .'.$tag.'btn-group .'.$tag.'btn + .'.$tag.'btn-group,
  .'.$tag.'btn-group .'.$tag.'btn-group + .'.$tag.'btn,
  .'.$tag.'btn-group .'.$tag.'btn-group + .'.$tag.'btn-group {
    margin-left: -1px;
  }
  .'.$tag.'btn-toolbar {
    margin-left: -5px;
  }
  .'.$tag.'btn-toolbar .'.$tag.'btn,
  .'.$tag.'btn-toolbar .'.$tag.'btn-group,
  .'.$tag.'btn-toolbar .'.$tag.'input-group {
    float: left;
  }
  .'.$tag.'btn-toolbar > .'.$tag.'btn,
  .'.$tag.'btn-toolbar > .'.$tag.'btn-group,
  .'.$tag.'btn-toolbar > .'.$tag.'input-group {
    margin-left: 5px;
  }
  .'.$tag.'btn-group > .'.$tag.'btn:not(:first-child):not(:last-child):not(.'.$tag.'dropdown-toggle) {
    border-radius: 0;
  }
  .'.$tag.'btn-group > .'.$tag.'btn:first-child {
    margin-left: 0;
  }
  .'.$tag.'btn-group > .'.$tag.'btn:first-child:not(:last-child):not(.'.$tag.'dropdown-toggle) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }
  .'.$tag.'btn-group > .'.$tag.'btn:last-child:not(:first-child),
  .'.$tag.'btn-group > .'.$tag.'dropdown-toggle:not(:first-child) {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
  }
  .'.$tag.'btn-group > .'.$tag.'btn-group {
    float: left;
  }
  .'.$tag.'btn-group > .'.$tag.'btn-group:not(:first-child):not(:last-child) > .'.$tag.'btn {
    border-radius: 0;
  }
  .'.$tag.'btn-group > .'.$tag.'btn-group:first-child:not(:last-child) > .'.$tag.'btn:last-child,
  .'.$tag.'btn-group > .'.$tag.'btn-group:first-child:not(:last-child) > .'.$tag.'dropdown-toggle {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }
  .'.$tag.'btn-group > .'.$tag.'btn-group:last-child:not(:first-child) > .'.$tag.'btn:first-child {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
  }
  .'.$tag.'btn-group .'.$tag.'dropdown-toggle:active,
  .'.$tag.'btn-group.'.$tag.'open .'.$tag.'dropdown-toggle {
    outline: 0;
  }
  .'.$tag.'btn-group > .'.$tag.'btn + .'.$tag.'dropdown-toggle {
    padding-right: 8px;
    padding-left: 8px;
  }
  .'.$tag.'btn-group > .'.$tag.'btn-lg + .'.$tag.'dropdown-toggle {
    padding-right: 12px;
    padding-left: 12px;
  }
  .'.$tag.'btn-group.'.$tag.'open .'.$tag.'dropdown-toggle {
    -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
            box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
  }
  .'.$tag.'btn-group.'.$tag.'open .'.$tag.'dropdown-toggle.'.$tag.'btn-link {
    -webkit-box-shadow: none;
            box-shadow: none;
  }
  .'.$tag.'btn .'.$tag.'caret {
    margin-left: 0;
  }
  .'.$tag.'btn-lg .'.$tag.'caret {
    border-width: 5px 5px 0;
    border-bottom-width: 0;
  }
  .'.$tag.'dropup .'.$tag.'btn-lg .'.$tag.'caret {
    border-width: 0 5px 5px;
  }
  .'.$tag.'btn-group-vertical > .'.$tag.'btn,
  .'.$tag.'btn-group-vertical > .'.$tag.'btn-group,
  .'.$tag.'btn-group-vertical > .'.$tag.'btn-group > .'.$tag.'btn {
    display: block;
    float: none;
    width: 100%;
    max-width: 100%;
  }
  .'.$tag.'btn-group-vertical > .'.$tag.'btn-group > .'.$tag.'btn {
    float: none;
  }
  .'.$tag.'btn-group-vertical > .'.$tag.'btn + .'.$tag.'btn,
  .'.$tag.'btn-group-vertical > .'.$tag.'btn + .'.$tag.'btn-group,
  .'.$tag.'btn-group-vertical > .'.$tag.'btn-group + .'.$tag.'btn,
  .'.$tag.'btn-group-vertical > .'.$tag.'btn-group + .'.$tag.'btn-group {
    margin-top: -1px;
    margin-left: 0;
  }
  .'.$tag.'btn-group-vertical > .'.$tag.'btn:not(:first-child):not(:last-child) {
    border-radius: 0;
  }
  .'.$tag.'btn-group-vertical > .'.$tag.'btn:first-child:not(:last-child) {
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
  }
  .'.$tag.'btn-group-vertical > .'.$tag.'btn:last-child:not(:first-child) {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    border-bottom-right-radius: 4px;
    border-bottom-left-radius: 4px;
  }
  .'.$tag.'btn-group-vertical > .'.$tag.'btn-group:not(:first-child):not(:last-child) > .'.$tag.'btn {
    border-radius: 0;
  }
  .'.$tag.'btn-group-vertical > .'.$tag.'btn-group:first-child:not(:last-child) > .'.$tag.'btn:last-child,
  .'.$tag.'btn-group-vertical > .'.$tag.'btn-group:first-child:not(:last-child) > .'.$tag.'dropdown-toggle {
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
  }
  .'.$tag.'btn-group-vertical > .'.$tag.'btn-group:last-child:not(:first-child) > .'.$tag.'btn:first-child {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }
  .'.$tag.'btn-group-justified {
    display: table;
    width: 100%;
    table-layout: fixed;
    border-collapse: separate;
  }
  .'.$tag.'btn-group-justified > .'.$tag.'btn,
  .'.$tag.'btn-group-justified > .'.$tag.'btn-group {
    display: table-cell;
    float: none;
    width: 1%;
  }
  .'.$tag.'btn-group-justified > .'.$tag.'btn-group .'.$tag.'btn {
    width: 100%;
  }
  .'.$tag.'btn-group-justified > .'.$tag.'btn-group .'.$tag.'dropdown-menu {
    left: auto;
  }
  [data-toggle="buttons"] > .'.$tag.'btn input[type="radio"],
  [data-toggle="buttons"] > .'.$tag.'btn-group > .'.$tag.'btn input[type="radio"],
  [data-toggle="buttons"] > .'.$tag.'btn input[type="checkbox"],
  [data-toggle="buttons"] > .'.$tag.'btn-group > .'.$tag.'btn input[type="checkbox"] {
    position: absolute;
    clip: rect(0, 0, 0, 0);
    pointer-events: none;
  }
  .'.$tag.'input-group {
    position: relative;
    display: table;
    border-collapse: separate;
  }
  .'.$tag.'input-group[class*="'.$tag.'col-"] {
    float: none;
    padding-right: 0;
    padding-left: 0;
  }
  .'.$tag.'input-group .'.$tag.'form-control {
    position: relative;
    z-index: 2;
    float: left;
    width: 100%;
    margin-bottom: 0;
  }
  .'.$tag.'input-group .'.$tag.'form-control:focus {
    z-index: 3;
  }
  .'.$tag.'input-group-lg > .'.$tag.'form-control,
  .'.$tag.'input-group-lg > .'.$tag.'input-group-addon,
  .'.$tag.'input-group-lg > .'.$tag.'input-group-btn > .'.$tag.'btn {
    height: 46px;
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
  }
  select.'.$tag.'input-group-lg > .'.$tag.'form-control,
  select.'.$tag.'input-group-lg > .'.$tag.'input-group-addon,
  select.'.$tag.'input-group-lg > .'.$tag.'input-group-btn > .'.$tag.'btn {
    height: 46px;
    line-height: 46px;
  }
  textarea.'.$tag.'input-group-lg > .'.$tag.'form-control,
  textarea.'.$tag.'input-group-lg > .'.$tag.'input-group-addon,
  textarea.'.$tag.'input-group-lg > .'.$tag.'input-group-btn > .'.$tag.'btn,
  select[multiple].'.$tag.'input-group-lg > .'.$tag.'form-control,
  select[multiple].'.$tag.'input-group-lg > .'.$tag.'input-group-addon,
  select[multiple].'.$tag.'input-group-lg > .'.$tag.'input-group-btn > .'.$tag.'btn {
    height: auto;
  }
  .'.$tag.'input-group-sm > .'.$tag.'form-control,
  .'.$tag.'input-group-sm > .'.$tag.'input-group-addon,
  .'.$tag.'input-group-sm > .'.$tag.'input-group-btn > .'.$tag.'btn {
    height: 30px;
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
  }
  select.'.$tag.'input-group-sm > .'.$tag.'form-control,
  select.'.$tag.'input-group-sm > .'.$tag.'input-group-addon,
  select.'.$tag.'input-group-sm > .'.$tag.'input-group-btn > .'.$tag.'btn {
    height: 30px;
    line-height: 30px;
  }
  textarea.'.$tag.'input-group-sm > .'.$tag.'form-control,
  textarea.'.$tag.'input-group-sm > .'.$tag.'input-group-addon,
  textarea.'.$tag.'input-group-sm > .'.$tag.'input-group-btn > .'.$tag.'btn,
  select[multiple].'.$tag.'input-group-sm > .'.$tag.'form-control,
  select[multiple].'.$tag.'input-group-sm > .'.$tag.'input-group-addon,
  select[multiple].'.$tag.'input-group-sm > .'.$tag.'input-group-btn > .'.$tag.'btn {
    height: auto;
  }
  .'.$tag.'input-group-addon,
  .'.$tag.'input-group-btn,
  .'.$tag.'input-group .'.$tag.'form-control {
    display: table-cell;
  }
  .'.$tag.'input-group-addon:not(:first-child):not(:last-child),
  .'.$tag.'input-group-btn:not(:first-child):not(:last-child),
  .'.$tag.'input-group .'.$tag.'form-control:not(:first-child):not(:last-child) {
    border-radius: 0;
  }
  .'.$tag.'input-group-addon,
  .'.$tag.'input-group-btn {
    width: 1%;
    white-space: nowrap;
    vertical-align: middle;
  }
  .'.$tag.'input-group-addon {
    padding: 6px 12px;
    font-size: 14px;
    font-weight: normal;
    line-height: 1;
    color: #555;
    text-align: center;
    background-color: #eee;
    border: 1px solid #ccc;
    border-radius: 4px;
  }
  .'.$tag.'input-group-addon.'.$tag.'input-sm {
    padding: 5px 10px;
    font-size: 12px;
    border-radius: 3px;
  }
  .'.$tag.'input-group-addon.'.$tag.'input-lg {
    padding: 10px 16px;
    font-size: 18px;
    border-radius: 6px;
  }
  .'.$tag.'input-group-addon input[type="radio"],
  .'.$tag.'input-group-addon input[type="checkbox"] {
    margin-top: 0;
  }
  .'.$tag.'input-group .'.$tag.'form-control:first-child,
  .'.$tag.'input-group-addon:first-child,
  .'.$tag.'input-group-btn:first-child > .'.$tag.'btn,
  .'.$tag.'input-group-btn:first-child > .'.$tag.'btn-group > .'.$tag.'btn,
  .'.$tag.'input-group-btn:first-child > .'.$tag.'dropdown-toggle,
  .'.$tag.'input-group-btn:last-child > .'.$tag.'btn:not(:last-child):not(.'.$tag.'dropdown-toggle),
  .'.$tag.'input-group-btn:last-child > .'.$tag.'btn-group:not(:last-child) > .'.$tag.'btn {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }
  .'.$tag.'input-group-addon:first-child {
    border-right: 0;
  }
  .'.$tag.'input-group .'.$tag.'form-control:last-child,
  .'.$tag.'input-group-addon:last-child,
  .'.$tag.'input-group-btn:last-child > .'.$tag.'btn,
  .'.$tag.'input-group-btn:last-child > .'.$tag.'btn-group > .'.$tag.'btn,
  .'.$tag.'input-group-btn:last-child > .'.$tag.'dropdown-toggle,
  .'.$tag.'input-group-btn:first-child > .'.$tag.'btn:not(:first-child),
  .'.$tag.'input-group-btn:first-child > .'.$tag.'btn-group:not(:first-child) > .'.$tag.'btn {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
  }
  .'.$tag.'input-group-addon:last-child {
    border-left: 0;
  }
  .'.$tag.'input-group-btn {
    position: relative;
    font-size: 0;
    white-space: nowrap;
  }
  .'.$tag.'input-group-btn > .'.$tag.'btn {
    position: relative;
  }
  .'.$tag.'input-group-btn > .'.$tag.'btn + .'.$tag.'btn {
    margin-left: -1px;
  }
  .'.$tag.'input-group-btn > .'.$tag.'btn:hover,
  .'.$tag.'input-group-btn > .'.$tag.'btn:focus,
  .'.$tag.'input-group-btn > .'.$tag.'btn:active {
    z-index: 2;
  }
  .'.$tag.'input-group-btn:first-child > .'.$tag.'btn,
  .'.$tag.'input-group-btn:first-child > .'.$tag.'btn-group {
    margin-right: -1px;
  }
  .'.$tag.'input-group-btn:last-child > .'.$tag.'btn,
  .'.$tag.'input-group-btn:last-child > .'.$tag.'btn-group {
    z-index: 2;
    margin-left: -1px;
  }
  .'.$tag.'nav {
    padding-left: 0;
    margin-bottom: 0;
    list-style: none;
  }
  .'.$tag.'nav > li {
    position: relative;
    display: block;
  }
  .'.$tag.'nav > li > a {
    position: relative;
    display: block;
    padding: 10px 15px;
  }
  .'.$tag.'nav > li > a:hover,
  .'.$tag.'nav > li > a:focus {
    text-decoration: none;
    background-color: #eee;
  }
  .'.$tag.'nav > li.'.$tag.'disabled > a {
    color: #777;
  }
  .'.$tag.'nav > li.'.$tag.'disabled > a:hover,
  .'.$tag.'nav > li.'.$tag.'disabled > a:focus {
    color: #777;
    text-decoration: none;
    cursor: not-allowed;
    background-color: transparent;
  }
  .'.$tag.'nav .'.$tag.'open > a,
  .'.$tag.'nav .'.$tag.'open > a:hover,
  .'.$tag.'nav .'.$tag.'open > a:focus {
    background-color: #eee;
    border-color: #337ab7;
  }
  .'.$tag.'nav .'.$tag.'nav-divider {
    height: 1px;
    margin: 9px 0;
    overflow: hidden;
    background-color: #e5e5e5;
  }
  .'.$tag.'nav > li > a > img {
    max-width: none;
  }
  .'.$tag.'nav-tabs {
    border-bottom: 1px solid #ddd;
  }
  .'.$tag.'nav-tabs > li {
    float: left;
    margin-bottom: -1px;
  }
  .'.$tag.'nav-tabs > li > a {
    margin-right: 2px;
    line-height: 1.42857143;
    border: 1px solid transparent;
    border-radius: 4px 4px 0 0;
  }
  .'.$tag.'nav-tabs > li > a:hover {
    border-color: #eee #eee #ddd;
  }
  .'.$tag.'nav-tabs > li.'.$tag.'active > a,
  .'.$tag.'nav-tabs > li.'.$tag.'active > a:hover,
  .'.$tag.'nav-tabs > li.'.$tag.'active > a:focus {
    color: #555;
    cursor: default;
    background-color: #fff;
    border: 1px solid #ddd;
    border-bottom-color: transparent;
  }
  .'.$tag.'nav-tabs.'.$tag.'nav-justified {
    width: 100%;
    border-bottom: 0;
  }
  .'.$tag.'nav-tabs.'.$tag.'nav-justified > li {
    float: none;
  }
  .'.$tag.'nav-tabs.'.$tag.'nav-justified > li > a {
    margin-bottom: 5px;
    text-align: center;
  }
  .'.$tag.'nav-tabs.'.$tag.'nav-justified > .'.$tag.'dropdown .'.$tag.'dropdown-menu {
    top: auto;
    left: auto;
  }
  @media (min-width: 768px) {
    .'.$tag.'nav-tabs.'.$tag.'nav-justified > li {
      display: table-cell;
      width: 1%;
    }
    .'.$tag.'nav-tabs.'.$tag.'nav-justified > li > a {
      margin-bottom: 0;
    }
  }
  .'.$tag.'nav-tabs.'.$tag.'nav-justified > li > a {
    margin-right: 0;
    border-radius: 4px;
  }
  .'.$tag.'nav-tabs.'.$tag.'nav-justified > .'.$tag.'active > a,
  .'.$tag.'nav-tabs.'.$tag.'nav-justified > .'.$tag.'active > a:hover,
  .'.$tag.'nav-tabs.'.$tag.'nav-justified > .'.$tag.'active > a:focus {
    border: 1px solid #ddd;
  }
  @media (min-width: 768px) {
    .'.$tag.'nav-tabs.'.$tag.'nav-justified > li > a {
      border-bottom: 1px solid #ddd;
      border-radius: 4px 4px 0 0;
    }
    .'.$tag.'nav-tabs.'.$tag.'nav-justified > .'.$tag.'active > a,
    .'.$tag.'nav-tabs.'.$tag.'nav-justified > .'.$tag.'active > a:hover,
    .'.$tag.'nav-tabs.'.$tag.'nav-justified > .'.$tag.'active > a:focus {
      border-bottom-color: #fff;
    }
  }
  .'.$tag.'nav-pills > li {
    float: left;
  }
  .'.$tag.'nav-pills > li > a {
    border-radius: 4px;
  }
  .'.$tag.'nav-pills > li + li {
    margin-left: 2px;
  }
  .'.$tag.'nav-pills > li.'.$tag.'active > a,
  .'.$tag.'nav-pills > li.'.$tag.'active > a:hover,
  .'.$tag.'nav-pills > li.'.$tag.'active > a:focus {
    color: #fff;
    background-color: #337ab7;
  }
  .'.$tag.'nav-stacked > li {
    float: none;
  }
  .'.$tag.'nav-stacked > li + li {
    margin-top: 2px;
    margin-left: 0;
  }
  .'.$tag.'nav-justified {
    width: 100%;
  }
  .'.$tag.'nav-justified > li {
    float: none;
  }
  .'.$tag.'nav-justified > li > a {
    margin-bottom: 5px;
    text-align: center;
  }
  .'.$tag.'nav-justified > .'.$tag.'dropdown .'.$tag.'dropdown-menu {
    top: auto;
    left: auto;
  }
  @media (min-width: 768px) {
    .'.$tag.'nav-justified > li {
      display: table-cell;
      width: 1%;
    }
    .'.$tag.'nav-justified > li > a {
      margin-bottom: 0;
    }
  }
  .'.$tag.'nav-tabs-justified {
    border-bottom: 0;
  }
  .'.$tag.'nav-tabs-justified > li > a {
    margin-right: 0;
    border-radius: 4px;
  }
  .'.$tag.'nav-tabs-justified > .'.$tag.'active > a,
  .'.$tag.'nav-tabs-justified > .'.$tag.'active > a:hover,
  .'.$tag.'nav-tabs-justified > .'.$tag.'active > a:focus {
    border: 1px solid #ddd;
  }
  @media (min-width: 768px) {
    .'.$tag.'nav-tabs-justified > li > a {
      border-bottom: 1px solid #ddd;
      border-radius: 4px 4px 0 0;
    }
    .'.$tag.'nav-tabs-justified > .'.$tag.'active > a,
    .'.$tag.'nav-tabs-justified > .'.$tag.'active > a:hover,
    .'.$tag.'nav-tabs-justified > .'.$tag.'active > a:focus {
      border-bottom-color: #fff;
    }
  }
  .'.$tag.'tab-content > .'.$tag.'tab-pane {
    display: none;
  }
  .'.$tag.'tab-content > .'.$tag.'active {
    display: block;
  }
  .'.$tag.'nav-tabs .'.$tag.'dropdown-menu {
    margin-top: -1px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }
  .'.$tag.'navbar {
    position: relative;
    min-height: 50px;
    margin-bottom: 20px;
    border: 1px solid transparent;
  }
  @media (min-width: 768px) {
    .'.$tag.'navbar {
      border-radius: 4px;
    }
  }
  @media (min-width: 768px) {
    .'.$tag.'navbar-header {
      float: left;
    }
  }
  .'.$tag.'navbar-collapse {
    padding-right: 15px;
    padding-left: 15px;
    overflow-x: visible;
    -webkit-overflow-scrolling: touch;
    border-top: 1px solid transparent;
    -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
  }
  .'.$tag.'navbar-collapse.'.$tag.'in {
    overflow-y: auto;
  }
  @media (min-width: 768px) {
    .'.$tag.'navbar-collapse {
      width: auto;
      border-top: 0;
      -webkit-box-shadow: none;
              box-shadow: none;
    }
    .'.$tag.'navbar-collapse.'.$tag.'collapse {
      display: block !important;
      height: auto !important;
      padding-bottom: 0;
      overflow: visible !important;
    }
    .'.$tag.'navbar-collapse.'.$tag.'in {
      overflow-y: visible;
    }
    .'.$tag.'navbar-fixed-top .'.$tag.'navbar-collapse,
    .'.$tag.'navbar-static-top .'.$tag.'navbar-collapse,
    .'.$tag.'navbar-fixed-bottom .'.$tag.'navbar-collapse {
      padding-right: 0;
      padding-left: 0;
    }
  }
  .'.$tag.'navbar-fixed-top .'.$tag.'navbar-collapse,
  .'.$tag.'navbar-fixed-bottom .'.$tag.'navbar-collapse {
    max-height: 340px;
  }
  @media (max-device-width: 480px) and (orientation: landscape) {
    .'.$tag.'navbar-fixed-top .'.$tag.'navbar-collapse,
    .'.$tag.'navbar-fixed-bottom .'.$tag.'navbar-collapse {
      max-height: 200px;
    }
  }
  .'.$tag.'container > .'.$tag.'navbar-header,
  .'.$tag.'container-fluid > .'.$tag.'navbar-header,
  .'.$tag.'container > .'.$tag.'navbar-collapse,
  .'.$tag.'container-fluid > .'.$tag.'navbar-collapse {
    margin-right: -15px;
    margin-left: -15px;
  }
  @media (min-width: 768px) {
    .'.$tag.'container > .'.$tag.'navbar-header,
    .'.$tag.'container-fluid > .'.$tag.'navbar-header,
    .'.$tag.'container > .'.$tag.'navbar-collapse,
    .'.$tag.'container-fluid > .'.$tag.'navbar-collapse {
      margin-right: 0;
      margin-left: 0;
    }
  }
  .'.$tag.'navbar-static-top {
    z-index: 1000;
    border-width: 0 0 1px;
  }
  @media (min-width: 768px) {
    .'.$tag.'navbar-static-top {
      border-radius: 0;
    }
  }
  .'.$tag.'navbar-fixed-top,
  .'.$tag.'navbar-fixed-bottom {
    position: fixed;
    right: 0;
    left: 0;
    z-index: 1030;
  }
  @media (min-width: 768px) {
    .'.$tag.'navbar-fixed-top,
    .'.$tag.'navbar-fixed-bottom {
      border-radius: 0;
    }
  }
  .'.$tag.'navbar-fixed-top {
    top: 0;
    border-width: 0 0 1px;
  }
  .'.$tag.'navbar-fixed-bottom {
    bottom: 0;
    margin-bottom: 0;
    border-width: 1px 0 0;
  }
  .'.$tag.'navbar-brand {
    float: left;
    height: 50px;
    padding: 15px 15px;
    font-size: 18px;
    line-height: 20px;
  }
  .'.$tag.'navbar-brand:hover,
  .'.$tag.'navbar-brand:focus {
    text-decoration: none;
  }
  .'.$tag.'navbar-brand > img {
    display: block;
  }
  @media (min-width: 768px) {
    .'.$tag.'navbar > .'.$tag.'container .'.$tag.'navbar-brand,
    .'.$tag.'navbar > .'.$tag.'container-fluid .'.$tag.'navbar-brand {
      margin-left: -15px;
    }
  }
  .'.$tag.'navbar-toggle {
    position: relative;
    float: right;
    padding: 9px 10px;
    margin-top: 8px;
    margin-right: 15px;
    margin-bottom: 8px;
    background-color: transparent;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
  }
  .'.$tag.'navbar-toggle:focus {
    outline: 0;
  }
  .'.$tag.'navbar-toggle .'.$tag.'icon-bar {
    display: block;
    width: 22px;
    height: 2px;
    border-radius: 1px;
  }
  .'.$tag.'navbar-toggle .'.$tag.'icon-bar + .'.$tag.'icon-bar {
    margin-top: 4px;
  }
  @media (min-width: 768px) {
    .'.$tag.'navbar-toggle {
      display: none;
    }
  }
  .'.$tag.'navbar-nav {
    margin: 7.5px -15px;
  }
  .'.$tag.'navbar-nav > li > a {
    padding-top: 10px;
    padding-bottom: 10px;
    line-height: 20px;
  }
  @media (max-width: 767px) {
    .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu {
      position: static;
      float: none;
      width: auto;
      margin-top: 0;
      background-color: transparent;
      border: 0;
      -webkit-box-shadow: none;
              box-shadow: none;
    }
    .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > li > a,
    .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu .'.$tag.'dropdown-header {
      padding: 5px 15px 5px 25px;
    }
    .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > li > a {
      line-height: 20px;
    }
    .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > li > a:hover,
    .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > li > a:focus {
      background-image: none;
    }
  }
  @media (min-width: 768px) {
    .'.$tag.'navbar-nav {
      float: left;
      margin: 0;
    }
    .'.$tag.'navbar-nav > li {
      float: left;
    }
    .'.$tag.'navbar-nav > li > a {
      padding-top: 15px;
      padding-bottom: 15px;
    }
  }
  .'.$tag.'navbar-form {
    padding: 10px 15px;
    margin-top: 8px;
    margin-right: -15px;
    margin-bottom: 8px;
    margin-left: -15px;
    border-top: 1px solid transparent;
    border-bottom: 1px solid transparent;
    -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1), 0 1px 0 rgba(255, 255, 255, .1);
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1), 0 1px 0 rgba(255, 255, 255, .1);
  }
  @media (min-width: 768px) {
    .'.$tag.'navbar-form .'.$tag.'form-group {
      display: inline-block;
      margin-bottom: 0;
      vertical-align: middle;
    }
    .'.$tag.'navbar-form .'.$tag.'form-control {
      display: inline-block;
      width: auto;
      vertical-align: middle;
    }
    .'.$tag.'navbar-form .'.$tag.'form-control-static {
      display: inline-block;
    }
    .'.$tag.'navbar-form .'.$tag.'input-group {
      display: inline-table;
      vertical-align: middle;
    }
    .'.$tag.'navbar-form .'.$tag.'input-group .'.$tag.'input-group-addon,
    .'.$tag.'navbar-form .'.$tag.'input-group .'.$tag.'input-group-btn,
    .'.$tag.'navbar-form .'.$tag.'input-group .'.$tag.'form-control {
      width: auto;
    }
    .'.$tag.'navbar-form .'.$tag.'input-group > .'.$tag.'form-control {
      width: 100%;
    }
    .'.$tag.'navbar-form .'.$tag.'control-label {
      margin-bottom: 0;
      vertical-align: middle;
    }
    .'.$tag.'navbar-form .'.$tag.'radio,
    .'.$tag.'navbar-form .'.$tag.'checkbox {
      display: inline-block;
      margin-top: 0;
      margin-bottom: 0;
      vertical-align: middle;
    }
    .'.$tag.'navbar-form .'.$tag.'radio label,
    .'.$tag.'navbar-form .'.$tag.'checkbox label {
      padding-left: 0;
    }
    .'.$tag.'navbar-form .'.$tag.'radio input[type="radio"],
    .'.$tag.'navbar-form .'.$tag.'checkbox input[type="checkbox"] {
      position: relative;
      margin-left: 0;
    }
    .'.$tag.'navbar-form .'.$tag.'has-feedback .'.$tag.'form-control-feedback {
      top: 0;
    }
  }
  @media (max-width: 767px) {
    .'.$tag.'navbar-form .'.$tag.'form-group {
      margin-bottom: 5px;
    }
    .'.$tag.'navbar-form .'.$tag.'form-group:last-child {
      margin-bottom: 0;
    }
  }
  @media (min-width: 768px) {
    .'.$tag.'navbar-form {
      width: auto;
      padding-top: 0;
      padding-bottom: 0;
      margin-right: 0;
      margin-left: 0;
      border: 0;
      -webkit-box-shadow: none;
              box-shadow: none;
    }
  }
  .'.$tag.'navbar-nav > li > .'.$tag.'dropdown-menu {
    margin-top: 0;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }
  .'.$tag.'navbar-fixed-bottom .'.$tag.'navbar-nav > li > .'.$tag.'dropdown-menu {
    margin-bottom: 0;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
  }
  .'.$tag.'navbar-btn {
    margin-top: 8px;
    margin-bottom: 8px;
  }
  .'.$tag.'navbar-btn.'.$tag.'btn-sm {
    margin-top: 10px;
    margin-bottom: 10px;
  }
  .'.$tag.'navbar-btn.'.$tag.'btn-xs {
    margin-top: 14px;
    margin-bottom: 14px;
  }
  .'.$tag.'navbar-text {
    margin-top: 15px;
    margin-bottom: 15px;
  }
  @media (min-width: 768px) {
    .'.$tag.'navbar-text {
      float: left;
      margin-right: 15px;
      margin-left: 15px;
    }
  }
  @media (min-width: 768px) {
    .'.$tag.'navbar-left {
      float: left !important;
    }
    .'.$tag.'navbar-right {
      float: right !important;
      margin-right: -15px;
    }
    .'.$tag.'navbar-right ~ .'.$tag.'navbar-right {
      margin-right: 0;
    }
  }
  .'.$tag.'navbar-default {
    background-color: #f8f8f8;
    border-color: #e7e7e7;
  }
  .'.$tag.'navbar-default .'.$tag.'navbar-brand {
    color: #777;
  }
  .'.$tag.'navbar-default .'.$tag.'navbar-brand:hover,
  .'.$tag.'navbar-default .'.$tag.'navbar-brand:focus {
    color: #5e5e5e;
    background-color: transparent;
  }
  .'.$tag.'navbar-default .'.$tag.'navbar-text {
    color: #777;
  }
  .'.$tag.'navbar-default .'.$tag.'navbar-nav > li > a {
    color: #777;
  }
  .'.$tag.'navbar-default .'.$tag.'navbar-nav > li > a:hover,
  .'.$tag.'navbar-default .'.$tag.'navbar-nav > li > a:focus {
    color: #333;
    background-color: transparent;
  }
  .'.$tag.'navbar-default .'.$tag.'navbar-nav > .'.$tag.'active > a,
  .'.$tag.'navbar-default .'.$tag.'navbar-nav > .'.$tag.'active > a:hover,
  .'.$tag.'navbar-default .'.$tag.'navbar-nav > .'.$tag.'active > a:focus {
    color: #555;
    background-color: #e7e7e7;
  }
  .'.$tag.'navbar-default .'.$tag.'navbar-nav > .'.$tag.'disabled > a,
  .'.$tag.'navbar-default .'.$tag.'navbar-nav > .'.$tag.'disabled > a:hover,
  .'.$tag.'navbar-default .'.$tag.'navbar-nav > .'.$tag.'disabled > a:focus {
    color: #ccc;
    background-color: transparent;
  }
  .'.$tag.'navbar-default .'.$tag.'navbar-toggle {
    border-color: #ddd;
  }
  .'.$tag.'navbar-default .'.$tag.'navbar-toggle:hover,
  .'.$tag.'navbar-default .'.$tag.'navbar-toggle:focus {
    background-color: #ddd;
  }
  .'.$tag.'navbar-default .'.$tag.'navbar-toggle .'.$tag.'icon-bar {
    background-color: #888;
  }
  .'.$tag.'navbar-default .'.$tag.'navbar-collapse,
  .'.$tag.'navbar-default .'.$tag.'navbar-form {
    border-color: #e7e7e7;
  }
  .'.$tag.'navbar-default .'.$tag.'navbar-nav > .'.$tag.'open > a,
  .'.$tag.'navbar-default .'.$tag.'navbar-nav > .'.$tag.'open > a:hover,
  .'.$tag.'navbar-default .'.$tag.'navbar-nav > .'.$tag.'open > a:focus {
    color: #555;
    background-color: #e7e7e7;
  }
  @media (max-width: 767px) {
    .'.$tag.'navbar-default .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > li > a {
      color: #777;
    }
    .'.$tag.'navbar-default .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > li > a:hover,
    .'.$tag.'navbar-default .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > li > a:focus {
      color: #333;
      background-color: transparent;
    }
    .'.$tag.'navbar-default .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > .'.$tag.'active > a,
    .'.$tag.'navbar-default .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > .'.$tag.'active > a:hover,
    .'.$tag.'navbar-default .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > .'.$tag.'active > a:focus {
      color: #555;
      background-color: #e7e7e7;
    }
    .'.$tag.'navbar-default .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > .'.$tag.'disabled > a,
    .'.$tag.'navbar-default .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > .'.$tag.'disabled > a:hover,
    .'.$tag.'navbar-default .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > .'.$tag.'disabled > a:focus {
      color: #ccc;
      background-color: transparent;
    }
  }
  .'.$tag.'navbar-default .'.$tag.'navbar-link {
    color: #777;
  }
  .'.$tag.'navbar-default .'.$tag.'navbar-link:hover {
    color: #333;
  }
  .'.$tag.'navbar-default .'.$tag.'btn-link {
    color: #777;
  }
  .'.$tag.'navbar-default .'.$tag.'btn-link:hover,
  .'.$tag.'navbar-default .'.$tag.'btn-link:focus {
    color: #333;
  }
  .'.$tag.'navbar-default .'.$tag.'btn-link[disabled]:hover,
  fieldset[disabled] .'.$tag.'navbar-default .'.$tag.'btn-link:hover,
  .'.$tag.'navbar-default .'.$tag.'btn-link[disabled]:focus,
  fieldset[disabled] .'.$tag.'navbar-default .'.$tag.'btn-link:focus {
    color: #ccc;
  }
  .'.$tag.'navbar-inverse {
    background-color: #222;
    border-color: #080808;
  }
  .'.$tag.'navbar-inverse .'.$tag.'navbar-brand {
    color: #9d9d9d;
  }
  .'.$tag.'navbar-inverse .'.$tag.'navbar-brand:hover,
  .'.$tag.'navbar-inverse .'.$tag.'navbar-brand:focus {
    color: #fff;
    background-color: transparent;
  }
  .'.$tag.'navbar-inverse .'.$tag.'navbar-text {
    color: #9d9d9d;
  }
  .'.$tag.'navbar-inverse .'.$tag.'navbar-nav > li > a {
    color: #9d9d9d;
  }
  .'.$tag.'navbar-inverse .'.$tag.'navbar-nav > li > a:hover,
  .'.$tag.'navbar-inverse .'.$tag.'navbar-nav > li > a:focus {
    color: #fff;
    background-color: transparent;
  }
  .'.$tag.'navbar-inverse .'.$tag.'navbar-nav > .'.$tag.'active > a,
  .'.$tag.'navbar-inverse .'.$tag.'navbar-nav > .'.$tag.'active > a:hover,
  .'.$tag.'navbar-inverse .'.$tag.'navbar-nav > .'.$tag.'active > a:focus {
    color: #fff;
    background-color: #080808;
  }
  .'.$tag.'navbar-inverse .'.$tag.'navbar-nav > .'.$tag.'disabled > a,
  .'.$tag.'navbar-inverse .'.$tag.'navbar-nav > .'.$tag.'disabled > a:hover,
  .'.$tag.'navbar-inverse .'.$tag.'navbar-nav > .'.$tag.'disabled > a:focus {
    color: #444;
    background-color: transparent;
  }
  .'.$tag.'navbar-inverse .'.$tag.'navbar-toggle {
    border-color: #333;
  }
  .'.$tag.'navbar-inverse .'.$tag.'navbar-toggle:hover,
  .'.$tag.'navbar-inverse .'.$tag.'navbar-toggle:focus {
    background-color: #333;
  }
  .'.$tag.'navbar-inverse .'.$tag.'navbar-toggle .'.$tag.'icon-bar {
    background-color: #fff;
  }
  .'.$tag.'navbar-inverse .'.$tag.'navbar-collapse,
  .'.$tag.'navbar-inverse .'.$tag.'navbar-form {
    border-color: #101010;
  }
  .'.$tag.'navbar-inverse .'.$tag.'navbar-nav > .'.$tag.'open > a,
  .'.$tag.'navbar-inverse .'.$tag.'navbar-nav > .'.$tag.'open > a:hover,
  .'.$tag.'navbar-inverse .'.$tag.'navbar-nav > .'.$tag.'open > a:focus {
    color: #fff;
    background-color: #080808;
  }
  @media (max-width: 767px) {
    .'.$tag.'navbar-inverse .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > .'.$tag.'dropdown-header {
      border-color: #080808;
    }
    .'.$tag.'navbar-inverse .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu .'.$tag.'divider {
      background-color: #080808;
    }
    .'.$tag.'navbar-inverse .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > li > a {
      color: #9d9d9d;
    }
    .'.$tag.'navbar-inverse .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > li > a:hover,
    .'.$tag.'navbar-inverse .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > li > a:focus {
      color: #fff;
      background-color: transparent;
    }
    .'.$tag.'navbar-inverse .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > .'.$tag.'active > a,
    .'.$tag.'navbar-inverse .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > .'.$tag.'active > a:hover,
    .'.$tag.'navbar-inverse .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > .'.$tag.'active > a:focus {
      color: #fff;
      background-color: #080808;
    }
    .'.$tag.'navbar-inverse .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > .'.$tag.'disabled > a,
    .'.$tag.'navbar-inverse .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > .'.$tag.'disabled > a:hover,
    .'.$tag.'navbar-inverse .'.$tag.'navbar-nav .'.$tag.'open .'.$tag.'dropdown-menu > .'.$tag.'disabled > a:focus {
      color: #444;
      background-color: transparent;
    }
  }
  .'.$tag.'navbar-inverse .'.$tag.'navbar-link {
    color: #9d9d9d;
  }
  .'.$tag.'navbar-inverse .'.$tag.'navbar-link:hover {
    color: #fff;
  }
  .'.$tag.'navbar-inverse .'.$tag.'btn-link {
    color: #9d9d9d;
  }
  .'.$tag.'navbar-inverse .'.$tag.'btn-link:hover,
  .'.$tag.'navbar-inverse .'.$tag.'btn-link:focus {
    color: #fff;
  }
  .'.$tag.'navbar-inverse .'.$tag.'btn-link[disabled]:hover,
  fieldset[disabled] .'.$tag.'navbar-inverse .'.$tag.'btn-link:hover,
  .'.$tag.'navbar-inverse .'.$tag.'btn-link[disabled]:focus,
  fieldset[disabled] .'.$tag.'navbar-inverse .'.$tag.'btn-link:focus {
    color: #444;
  }
  .'.$tag.'breadcrumb {
    padding: 8px 15px;
    margin-bottom: 20px;
    list-style: none;
    background-color: #f5f5f5;
    border-radius: 4px;
  }
  .'.$tag.'breadcrumb > li {
    display: inline-block;
  }
  .'.$tag.'breadcrumb > li + li:before {
    padding: 0 5px;
    color: #ccc;
    content: "/\00a0";
  }
  .'.$tag.'breadcrumb > .'.$tag.'active {
    color: #777;
  }
  .'.$tag.'pagination {
    display: inline-block;
    padding-left: 0;
    margin: 20px 0;
    border-radius: 4px;
  }
  .'.$tag.'pagination > li {
    display: inline;
  }
  .'.$tag.'pagination > li > a,
  .'.$tag.'pagination > li > span {
    position: relative;
    float: left;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;
  }
  .'.$tag.'pagination > li:first-child > a,
  .'.$tag.'pagination > li:first-child > span {
    margin-left: 0;
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
  }
  .'.$tag.'pagination > li:last-child > a,
  .'.$tag.'pagination > li:last-child > span {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
  }
  .'.$tag.'pagination > li > a:hover,
  .'.$tag.'pagination > li > span:hover,
  .'.$tag.'pagination > li > a:focus,
  .'.$tag.'pagination > li > span:focus {
    z-index: 2;
    color: #23527c;
    background-color: #eee;
    border-color: #ddd;
  }
  .'.$tag.'pagination > .'.$tag.'active > a,
  .'.$tag.'pagination > .'.$tag.'active > span,
  .'.$tag.'pagination > .'.$tag.'active > a:hover,
  .'.$tag.'pagination > .'.$tag.'active > span:hover,
  .'.$tag.'pagination > .'.$tag.'active > a:focus,
  .'.$tag.'pagination > .'.$tag.'active > span:focus {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #337ab7;
    border-color: #337ab7;
  }
  .'.$tag.'pagination > .'.$tag.'disabled > span,
  .'.$tag.'pagination > .'.$tag.'disabled > span:hover,
  .'.$tag.'pagination > .'.$tag.'disabled > span:focus,
  .'.$tag.'pagination > .'.$tag.'disabled > a,
  .'.$tag.'pagination > .'.$tag.'disabled > a:hover,
  .'.$tag.'pagination > .'.$tag.'disabled > a:focus {
    color: #777;
    cursor: not-allowed;
    background-color: #fff;
    border-color: #ddd;
  }
  .'.$tag.'pagination-lg > li > a,
  .'.$tag.'pagination-lg > li > span {
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
  }
  .'.$tag.'pagination-lg > li:first-child > a,
  .'.$tag.'pagination-lg > li:first-child > span {
    border-top-left-radius: 6px;
    border-bottom-left-radius: 6px;
  }
  .'.$tag.'pagination-lg > li:last-child > a,
  .'.$tag.'pagination-lg > li:last-child > span {
    border-top-right-radius: 6px;
    border-bottom-right-radius: 6px;
  }
  .'.$tag.'pagination-sm > li > a,
  .'.$tag.'pagination-sm > li > span {
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
  }
  .'.$tag.'pagination-sm > li:first-child > a,
  .'.$tag.'pagination-sm > li:first-child > span {
    border-top-left-radius: 3px;
    border-bottom-left-radius: 3px;
  }
  .'.$tag.'pagination-sm > li:last-child > a,
  .'.$tag.'pagination-sm > li:last-child > span {
    border-top-right-radius: 3px;
    border-bottom-right-radius: 3px;
  }
  .'.$tag.'pager {
    padding-left: 0;
    margin: 20px 0;
    text-align: center;
    list-style: none;
  }
  .'.$tag.'pager li {
    display: inline;
  }
  .'.$tag.'pager li > a,
  .'.$tag.'pager li > span {
    display: inline-block;
    padding: 5px 14px;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 15px;
  }
  .'.$tag.'pager li > a:hover,
  .'.$tag.'pager li > a:focus {
    text-decoration: none;
    background-color: #eee;
  }
  .'.$tag.'pager .'.$tag.'next > a,
  .'.$tag.'pager .'.$tag.'next > span {
    float: right;
  }
  .'.$tag.'pager .'.$tag.'previous > a,
  .'.$tag.'pager .'.$tag.'previous > span {
    float: left;
  }
  .'.$tag.'pager .'.$tag.'disabled > a,
  .'.$tag.'pager .'.$tag.'disabled > a:hover,
  .'.$tag.'pager .'.$tag.'disabled > a:focus,
  .'.$tag.'pager .'.$tag.'disabled > span {
    color: #777;
    cursor: not-allowed;
    background-color: #fff;
  }
  .'.$tag.'label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: bold;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25em;
  }
  a.'.$tag.'label:hover,
  a.'.$tag.'label:focus {
    color: #fff;
    text-decoration: none;
    cursor: pointer;
  }
  .'.$tag.'label:empty {
    display: none;
  }
  .'.$tag.'btn .'.$tag.'label {
    position: relative;
    top: -1px;
  }
  .'.$tag.'label-default {
    background-color: #777;
  }
  .'.$tag.'label-default[href]:hover,
  .'.$tag.'label-default[href]:focus {
    background-color: #5e5e5e;
  }
  .'.$tag.'label-primary {
    background-color: #337ab7;
  }
  .'.$tag.'label-primary[href]:hover,
  .'.$tag.'label-primary[href]:focus {
    background-color: #286090;
  }
  .'.$tag.'label-success {
    background-color: #5cb85c;
  }
  .'.$tag.'label-success[href]:hover,
  .'.$tag.'label-success[href]:focus {
    background-color: #449d44;
  }
  .'.$tag.'label-info {
    background-color: #5bc0de;
  }
  .'.$tag.'label-info[href]:hover,
  .'.$tag.'label-info[href]:focus {
    background-color: #31b0d5;
  }
  .'.$tag.'label-warning {
    background-color: #f0ad4e;
  }
  .'.$tag.'label-warning[href]:hover,
  .'.$tag.'label-warning[href]:focus {
    background-color: #ec971f;
  }
  .'.$tag.'label-danger {
    background-color: #d9534f;
  }
  .'.$tag.'label-danger[href]:hover,
  .'.$tag.'label-danger[href]:focus {
    background-color: #c9302c;
  }
  .'.$tag.'badge {
    display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 12px;
    font-weight: bold;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    background-color: #777;
    border-radius: 10px;
  }
  .'.$tag.'badge:empty {
    display: none;
  }
  .'.$tag.'btn .'.$tag.'badge {
    position: relative;
    top: -1px;
  }
  .'.$tag.'btn-xs .'.$tag.'badge,
  .'.$tag.'btn-group-xs > .'.$tag.'btn .'.$tag.'badge {
    top: 0;
    padding: 1px 5px;
  }
  a.'.$tag.'badge:hover,
  a.'.$tag.'badge:focus {
    color: #fff;
    text-decoration: none;
    cursor: pointer;
  }
  .'.$tag.'list-group-item.'.$tag.'active > .'.$tag.'badge,
  .'.$tag.'nav-pills > .'.$tag.'active > a > .'.$tag.'badge {
    color: #337ab7;
    background-color: #fff;
  }
  .'.$tag.'list-group-item > .'.$tag.'badge {
    float: right;
  }
  .'.$tag.'list-group-item > .'.$tag.'badge + .'.$tag.'badge {
    margin-right: 5px;
  }
  .'.$tag.'nav-pills > li > a > .'.$tag.'badge {
    margin-left: 3px;
  }
  .'.$tag.'jumbotron {
    padding-top: 30px;
    padding-bottom: 30px;
    margin-bottom: 30px;
    color: inherit;
    background-color: #eee;
  }
  .'.$tag.'jumbotron h1,
  .'.$tag.'jumbotron .'.$tag.'h1 {
    color: inherit;
  }
  .'.$tag.'jumbotron p {
    margin-bottom: 15px;
    font-size: 21px;
    font-weight: 200;
  }
  .'.$tag.'jumbotron > hr {
    border-top-color: #d5d5d5;
  }
  .'.$tag.'container .'.$tag.'jumbotron,
  .'.$tag.'container-fluid .'.$tag.'jumbotron {
    padding-right: 15px;
    padding-left: 15px;
    border-radius: 6px;
  }
  .'.$tag.'jumbotron .'.$tag.'container {
    max-width: 100%;
  }
  @media screen and (min-width: 768px) {
    .'.$tag.'jumbotron {
      padding-top: 48px;
      padding-bottom: 48px;
    }
    .'.$tag.'container .'.$tag.'jumbotron,
    .'.$tag.'container-fluid .'.$tag.'jumbotron {
      padding-right: 60px;
      padding-left: 60px;
    }
    .'.$tag.'jumbotron h1,
    .'.$tag.'jumbotron .'.$tag.'h1 {
      font-size: 63px;
    }
  }
  .'.$tag.'thumbnail {
    display: block;
    padding: 4px;
    margin-bottom: 20px;
    line-height: 1.42857143;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    -webkit-transition: border .2s ease-in-out;
         -o-transition: border .2s ease-in-out;
            transition: border .2s ease-in-out;
  }
  .'.$tag.'thumbnail > img,
  .'.$tag.'thumbnail a > img {
    margin-right: auto;
    margin-left: auto;
  }
  a.'.$tag.'thumbnail:hover,
  a.'.$tag.'thumbnail:focus,
  a.'.$tag.'thumbnail.'.$tag.'active {
    border-color: #337ab7;
  }
  .'.$tag.'thumbnail .'.$tag.'caption {
    padding: 9px;
    color: #333;
  }
  .'.$tag.'alert {
    padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;
  }
  .'.$tag.'alert h4 {
    margin-top: 0;
    color: inherit;
  }
  .'.$tag.'alert .'.$tag.'alert-link {
    font-weight: bold;
  }
  .'.$tag.'alert > p,
  .'.$tag.'alert > ul {
    margin-bottom: 0;
  }
  .'.$tag.'alert > p + p {
    margin-top: 5px;
  }
  .'.$tag.'alert-dismissable,
  .'.$tag.'alert-dismissible {
    padding-right: 35px;
  }
  .'.$tag.'alert-dismissable .'.$tag.'close,
  .'.$tag.'alert-dismissible .'.$tag.'close {
    position: relative;
    top: -2px;
    right: -21px;
    color: inherit;
  }
  .'.$tag.'alert-success {
    color: #3c763d;
    background-color: #dff0d8;
    border-color: #d6e9c6;
  }
  .'.$tag.'alert-success hr {
    border-top-color: #c9e2b3;
  }
  .'.$tag.'alert-success .'.$tag.'alert-link {
    color: #2b542c;
  }
  .'.$tag.'alert-info {
    color: #31708f;
    background-color: #d9edf7;
    border-color: #bce8f1;
  }
  .'.$tag.'alert-info hr {
    border-top-color: #a6e1ec;
  }
  .'.$tag.'alert-info .'.$tag.'alert-link {
    color: #245269;
  }
  .'.$tag.'alert-warning {
    color: #8a6d3b;
    background-color: #fcf8e3;
    border-color: #faebcc;
  }
  .'.$tag.'alert-warning hr {
    border-top-color: #f7e1b5;
  }
  .'.$tag.'alert-warning .'.$tag.'alert-link {
    color: #66512c;
  }
  .'.$tag.'alert-danger {
    color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
  }
  .'.$tag.'alert-danger hr {
    border-top-color: #e4b9c0;
  }
  .'.$tag.'alert-danger .'.$tag.'alert-link {
    color: #843534;
  }
  @-webkit-keyframes progress-bar-stripes {
    from {
      background-position: 40px 0;
    }
    to {
      background-position: 0 0;
    }
  }
  @-o-keyframes progress-bar-stripes {
    from {
      background-position: 40px 0;
    }
    to {
      background-position: 0 0;
    }
  }
  @keyframes progress-bar-stripes {
    from {
      background-position: 40px 0;
    }
    to {
      background-position: 0 0;
    }
  }
  .'.$tag.'progress {
    height: 20px;
    margin-bottom: 20px;
    overflow: hidden;
    background-color: #f5f5f5;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, .1);
            box-shadow: inset 0 1px 2px rgba(0, 0, 0, .1);
  }
  .'.$tag.'progress-bar {
    float: left;
    width: 0;
    height: 100%;
    font-size: 12px;
    line-height: 20px;
    color: #fff;
    text-align: center;
    background-color: #337ab7;
    -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .15);
            box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .15);
    -webkit-transition: width .6s ease;
         -o-transition: width .6s ease;
            transition: width .6s ease;
  }
  .'.$tag.'progress-striped .'.$tag.'progress-bar,
  .'.$tag.'progress-bar-striped {
    background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
    background-image:      -o-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
    background-image:         linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
    -webkit-background-size: 40px 40px;
            background-size: 40px 40px;
  }
  .'.$tag.'progress.'.$tag.'active .'.$tag.'progress-bar,
  .'.$tag.'progress-bar.'.$tag.'active {
    -webkit-animation: progress-bar-stripes 2s linear infinite;
         -o-animation: progress-bar-stripes 2s linear infinite;
            animation: progress-bar-stripes 2s linear infinite;
  }
  .'.$tag.'progress-bar-success {
    background-color: #5cb85c;
  }
  .'.$tag.'progress-striped .'.$tag.'progress-bar-success {
    background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
    background-image:      -o-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
    background-image:         linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  }
  .'.$tag.'progress-bar-info {
    background-color: #5bc0de;
  }
  .'.$tag.'progress-striped .'.$tag.'progress-bar-info {
    background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
    background-image:      -o-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
    background-image:         linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  }
  .'.$tag.'progress-bar-warning {
    background-color: #f0ad4e;
  }
  .'.$tag.'progress-striped .'.$tag.'progress-bar-warning {
    background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
    background-image:      -o-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
    background-image:         linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  }
  .'.$tag.'progress-bar-danger {
    background-color: #d9534f;
  }
  .'.$tag.'progress-striped .'.$tag.'progress-bar-danger {
    background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
    background-image:      -o-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
    background-image:         linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  }
  .'.$tag.'media {
    margin-top: 15px;
  }
  .'.$tag.'media:first-child {
    margin-top: 0;
  }
  .'.$tag.'media,
  .'.$tag.'media-body {
    overflow: hidden;
    zoom: 1;
  }
  .'.$tag.'media-body {
    width: 10000px;
  }
  .'.$tag.'media-object {
    display: block;
  }
  .'.$tag.'media-object.'.$tag.'img-thumbnail {
    max-width: none;
  }
  .'.$tag.'media-right,
  .'.$tag.'media > .'.$tag.'pull-right {
    padding-left: 10px;
  }
  .'.$tag.'media-left,
  .'.$tag.'media > .'.$tag.'pull-left {
    padding-right: 10px;
  }
  .'.$tag.'media-left,
  .'.$tag.'media-right,
  .'.$tag.'media-body {
    display: table-cell;
    vertical-align: top;
  }
  .'.$tag.'media-middle {
    vertical-align: middle;
  }
  .'.$tag.'media-bottom {
    vertical-align: bottom;
  }
  .'.$tag.'media-heading {
    margin-top: 0;
    margin-bottom: 5px;
  }
  .'.$tag.'media-list {
    padding-left: 0;
    list-style: none;
  }
  .'.$tag.'list-group {
    padding-left: 0;
    margin-bottom: 20px;
  }
  .'.$tag.'list-group-item {
    position: relative;
    display: block;
    padding: 10px 15px;
    margin-bottom: -1px;
    background-color: #fff;
    border: 1px solid #ddd;
  }
  .'.$tag.'list-group-item:first-child {
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
  }
  .'.$tag.'list-group-item:last-child {
    margin-bottom: 0;
    border-bottom-right-radius: 4px;
    border-bottom-left-radius: 4px;
  }
  a.'.$tag.'list-group-item,
  button.'.$tag.'list-group-item {
    color: #555;
  }
  a.'.$tag.'list-group-item .'.$tag.'list-group-item-heading,
  button.'.$tag.'list-group-item .'.$tag.'list-group-item-heading {
    color: #333;
  }
  a.'.$tag.'list-group-item:hover,
  button.'.$tag.'list-group-item:hover,
  a.'.$tag.'list-group-item:focus,
  button.'.$tag.'list-group-item:focus {
    color: #555;
    text-decoration: none;
    background-color: #f5f5f5;
  }
  button.'.$tag.'list-group-item {
    width: 100%;
    text-align: left;
  }
  .'.$tag.'list-group-item.'.$tag.'disabled,
  .'.$tag.'list-group-item.'.$tag.'disabled:hover,
  .'.$tag.'list-group-item.'.$tag.'disabled:focus {
    color: #777;
    cursor: not-allowed;
    background-color: #eee;
  }
  .'.$tag.'list-group-item.'.$tag.'disabled .'.$tag.'list-group-item-heading,
  .'.$tag.'list-group-item.'.$tag.'disabled:hover .'.$tag.'list-group-item-heading,
  .'.$tag.'list-group-item.'.$tag.'disabled:focus .'.$tag.'list-group-item-heading {
    color: inherit;
  }
  .'.$tag.'list-group-item.'.$tag.'disabled .'.$tag.'list-group-item-text,
  .'.$tag.'list-group-item.'.$tag.'disabled:hover .'.$tag.'list-group-item-text,
  .'.$tag.'list-group-item.'.$tag.'disabled:focus .'.$tag.'list-group-item-text {
    color: #777;
  }
  .'.$tag.'list-group-item.'.$tag.'active,
  .'.$tag.'list-group-item.'.$tag.'active:hover,
  .'.$tag.'list-group-item.'.$tag.'active:focus {
    z-index: 2;
    color: #fff;
    background-color: #337ab7;
    border-color: #337ab7;
  }
  .'.$tag.'list-group-item.'.$tag.'active .'.$tag.'list-group-item-heading,
  .'.$tag.'list-group-item.'.$tag.'active:hover .'.$tag.'list-group-item-heading,
  .'.$tag.'list-group-item.'.$tag.'active:focus .'.$tag.'list-group-item-heading,
  .'.$tag.'list-group-item.'.$tag.'active .'.$tag.'list-group-item-heading > small,
  .'.$tag.'list-group-item.'.$tag.'active:hover .'.$tag.'list-group-item-heading > small,
  .'.$tag.'list-group-item.'.$tag.'active:focus .'.$tag.'list-group-item-heading > small,
  .'.$tag.'list-group-item.'.$tag.'active .'.$tag.'list-group-item-heading > .'.$tag.'small,
  .'.$tag.'list-group-item.'.$tag.'active:hover .'.$tag.'list-group-item-heading > .'.$tag.'small,
  .'.$tag.'list-group-item.'.$tag.'active:focus .'.$tag.'list-group-item-heading > .'.$tag.'small {
    color: inherit;
  }
  .'.$tag.'list-group-item.'.$tag.'active .'.$tag.'list-group-item-text,
  .'.$tag.'list-group-item.'.$tag.'active:hover .'.$tag.'list-group-item-text,
  .'.$tag.'list-group-item.'.$tag.'active:focus .'.$tag.'list-group-item-text {
    color: #c7ddef;
  }
  .'.$tag.'list-group-item-success {
    color: #3c763d;
    background-color: #dff0d8;
  }
  a.'.$tag.'list-group-item-success,
  button.'.$tag.'list-group-item-success {
    color: #3c763d;
  }
  a.'.$tag.'list-group-item-success .'.$tag.'list-group-item-heading,
  button.'.$tag.'list-group-item-success .'.$tag.'list-group-item-heading {
    color: inherit;
  }
  a.'.$tag.'list-group-item-success:hover,
  button.'.$tag.'list-group-item-success:hover,
  a.'.$tag.'list-group-item-success:focus,
  button.'.$tag.'list-group-item-success:focus {
    color: #3c763d;
    background-color: #d0e9c6;
  }
  a.'.$tag.'list-group-item-success.'.$tag.'active,
  button.'.$tag.'list-group-item-success.'.$tag.'active,
  a.'.$tag.'list-group-item-success.'.$tag.'active:hover,
  button.'.$tag.'list-group-item-success.'.$tag.'active:hover,
  a.'.$tag.'list-group-item-success.'.$tag.'active:focus,
  button.'.$tag.'list-group-item-success.'.$tag.'active:focus {
    color: #fff;
    background-color: #3c763d;
    border-color: #3c763d;
  }
  .'.$tag.'list-group-item-info {
    color: #31708f;
    background-color: #d9edf7;
  }
  a.'.$tag.'list-group-item-info,
  button.'.$tag.'list-group-item-info {
    color: #31708f;
  }
  a.'.$tag.'list-group-item-info .'.$tag.'list-group-item-heading,
  button.'.$tag.'list-group-item-info .'.$tag.'list-group-item-heading {
    color: inherit;
  }
  a.'.$tag.'list-group-item-info:hover,
  button.'.$tag.'list-group-item-info:hover,
  a.'.$tag.'list-group-item-info:focus,
  button.'.$tag.'list-group-item-info:focus {
    color: #31708f;
    background-color: #c4e3f3;
  }
  a.'.$tag.'list-group-item-info.'.$tag.'active,
  button.'.$tag.'list-group-item-info.'.$tag.'active,
  a.'.$tag.'list-group-item-info.'.$tag.'active:hover,
  button.'.$tag.'list-group-item-info.'.$tag.'active:hover,
  a.'.$tag.'list-group-item-info.'.$tag.'active:focus,
  button.'.$tag.'list-group-item-info.'.$tag.'active:focus {
    color: #fff;
    background-color: #31708f;
    border-color: #31708f;
  }
  .'.$tag.'list-group-item-warning {
    color: #8a6d3b;
    background-color: #fcf8e3;
  }
  a.'.$tag.'list-group-item-warning,
  button.'.$tag.'list-group-item-warning {
    color: #8a6d3b;
  }
  a.'.$tag.'list-group-item-warning .'.$tag.'list-group-item-heading,
  button.'.$tag.'list-group-item-warning .'.$tag.'list-group-item-heading {
    color: inherit;
  }
  a.'.$tag.'list-group-item-warning:hover,
  button.'.$tag.'list-group-item-warning:hover,
  a.'.$tag.'list-group-item-warning:focus,
  button.'.$tag.'list-group-item-warning:focus {
    color: #8a6d3b;
    background-color: #faf2cc;
  }
  a.'.$tag.'list-group-item-warning.'.$tag.'active,
  button.'.$tag.'list-group-item-warning.'.$tag.'active,
  a.'.$tag.'list-group-item-warning.'.$tag.'active:hover,
  button.'.$tag.'list-group-item-warning.'.$tag.'active:hover,
  a.'.$tag.'list-group-item-warning.'.$tag.'active:focus,
  button.'.$tag.'list-group-item-warning.'.$tag.'active:focus {
    color: #fff;
    background-color: #8a6d3b;
    border-color: #8a6d3b;
  }
  .'.$tag.'list-group-item-danger {
    color: #a94442;
    background-color: #f2dede;
  }
  a.'.$tag.'list-group-item-danger,
  button.'.$tag.'list-group-item-danger {
    color: #a94442;
  }
  a.'.$tag.'list-group-item-danger .'.$tag.'list-group-item-heading,
  button.'.$tag.'list-group-item-danger .'.$tag.'list-group-item-heading {
    color: inherit;
  }
  a.'.$tag.'list-group-item-danger:hover,
  button.'.$tag.'list-group-item-danger:hover,
  a.'.$tag.'list-group-item-danger:focus,
  button.'.$tag.'list-group-item-danger:focus {
    color: #a94442;
    background-color: #ebcccc;
  }
  a.'.$tag.'list-group-item-danger.'.$tag.'active,
  button.'.$tag.'list-group-item-danger.'.$tag.'active,
  a.'.$tag.'list-group-item-danger.'.$tag.'active:hover,
  button.'.$tag.'list-group-item-danger.'.$tag.'active:hover,
  a.'.$tag.'list-group-item-danger.'.$tag.'active:focus,
  button.'.$tag.'list-group-item-danger.'.$tag.'active:focus {
    color: #fff;
    background-color: #a94442;
    border-color: #a94442;
  }
  .'.$tag.'list-group-item-heading {
    margin-top: 0;
    margin-bottom: 5px;
  }
  .'.$tag.'list-group-item-text {
    margin-bottom: 0;
    line-height: 1.3;
  }
  .'.$tag.'panel {
    margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
  }
  .'.$tag.'panel-body {
    padding: 15px;
  }
  .'.$tag.'panel-heading {
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
  }
  .'.$tag.'panel-heading > .'.$tag.'dropdown .'.$tag.'dropdown-toggle {
    color: inherit;
  }
  .'.$tag.'panel-title {
    margin-top: 0;
    margin-bottom: 0;
    font-size: 16px;
    color: inherit;
  }
  .'.$tag.'panel-title > a,
  .'.$tag.'panel-title > small,
  .'.$tag.'panel-title > .'.$tag.'small,
  .'.$tag.'panel-title > small > a,
  .'.$tag.'panel-title > .'.$tag.'small > a {
    color: inherit;
  }
  .'.$tag.'panel-footer {
    padding: 10px 15px;
    background-color: #f5f5f5;
    border-top: 1px solid #ddd;
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
  }
  .'.$tag.'panel > .'.$tag.'list-group,
  .'.$tag.'panel > .'.$tag.'panel-collapse > .'.$tag.'list-group {
    margin-bottom: 0;
  }
  .'.$tag.'panel > .'.$tag.'list-group .'.$tag.'list-group-item,
  .'.$tag.'panel > .'.$tag.'panel-collapse > .'.$tag.'list-group .'.$tag.'list-group-item {
    border-width: 1px 0;
    border-radius: 0;
  }
  .'.$tag.'panel > .'.$tag.'list-group:first-child .'.$tag.'list-group-item:first-child,
  .'.$tag.'panel > .'.$tag.'panel-collapse > .'.$tag.'list-group:first-child .'.$tag.'list-group-item:first-child {
    border-top: 0;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
  }
  .'.$tag.'panel > .'.$tag.'list-group:last-child .'.$tag.'list-group-item:last-child,
  .'.$tag.'panel > .'.$tag.'panel-collapse > .'.$tag.'list-group:last-child .'.$tag.'list-group-item:last-child {
    border-bottom: 0;
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
  }
  .'.$tag.'panel > .'.$tag.'panel-heading + .'.$tag.'panel-collapse > .'.$tag.'list-group .'.$tag.'list-group-item:first-child {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }
  .'.$tag.'panel-heading + .'.$tag.'list-group .'.$tag.'list-group-item:first-child {
    border-top-width: 0;
  }
  .'.$tag.'list-group + .'.$tag.'panel-footer {
    border-top-width: 0;
  }
  .'.$tag.'panel > .'.$tag.'table,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table,
  .'.$tag.'panel > .'.$tag.'panel-collapse > .'.$tag.'table {
    margin-bottom: 0;
  }
  .'.$tag.'panel > .'.$tag.'table caption,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table caption,
  .'.$tag.'panel > .'.$tag.'panel-collapse > .'.$tag.'table caption {
    padding-right: 15px;
    padding-left: 15px;
  }
  .'.$tag.'panel > .'.$tag.'table:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:first-child > .'.$tag.'table:first-child {
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
  }
  .'.$tag.'panel > .'.$tag.'table:first-child > thead:first-child > tr:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:first-child > .'.$tag.'table:first-child > thead:first-child > tr:first-child,
  .'.$tag.'panel > .'.$tag.'table:first-child > tbody:first-child > tr:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:first-child > .'.$tag.'table:first-child > tbody:first-child > tr:first-child {
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
  }
  .'.$tag.'panel > .'.$tag.'table:first-child > thead:first-child > tr:first-child td:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:first-child > .'.$tag.'table:first-child > thead:first-child > tr:first-child td:first-child,
  .'.$tag.'panel > .'.$tag.'table:first-child > tbody:first-child > tr:first-child td:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:first-child > .'.$tag.'table:first-child > tbody:first-child > tr:first-child td:first-child,
  .'.$tag.'panel > .'.$tag.'table:first-child > thead:first-child > tr:first-child th:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:first-child > .'.$tag.'table:first-child > thead:first-child > tr:first-child th:first-child,
  .'.$tag.'panel > .'.$tag.'table:first-child > tbody:first-child > tr:first-child th:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:first-child > .'.$tag.'table:first-child > tbody:first-child > tr:first-child th:first-child {
    border-top-left-radius: 3px;
  }
  .'.$tag.'panel > .'.$tag.'table:first-child > thead:first-child > tr:first-child td:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:first-child > .'.$tag.'table:first-child > thead:first-child > tr:first-child td:last-child,
  .'.$tag.'panel > .'.$tag.'table:first-child > tbody:first-child > tr:first-child td:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:first-child > .'.$tag.'table:first-child > tbody:first-child > tr:first-child td:last-child,
  .'.$tag.'panel > .'.$tag.'table:first-child > thead:first-child > tr:first-child th:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:first-child > .'.$tag.'table:first-child > thead:first-child > tr:first-child th:last-child,
  .'.$tag.'panel > .'.$tag.'table:first-child > tbody:first-child > tr:first-child th:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:first-child > .'.$tag.'table:first-child > tbody:first-child > tr:first-child th:last-child {
    border-top-right-radius: 3px;
  }
  .'.$tag.'panel > .'.$tag.'table:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:last-child > .'.$tag.'table:last-child {
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
  }
  .'.$tag.'panel > .'.$tag.'table:last-child > tbody:last-child > tr:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:last-child > .'.$tag.'table:last-child > tbody:last-child > tr:last-child,
  .'.$tag.'panel > .'.$tag.'table:last-child > tfoot:last-child > tr:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:last-child > .'.$tag.'table:last-child > tfoot:last-child > tr:last-child {
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
  }
  .'.$tag.'panel > .'.$tag.'table:last-child > tbody:last-child > tr:last-child td:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:last-child > .'.$tag.'table:last-child > tbody:last-child > tr:last-child td:first-child,
  .'.$tag.'panel > .'.$tag.'table:last-child > tfoot:last-child > tr:last-child td:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:last-child > .'.$tag.'table:last-child > tfoot:last-child > tr:last-child td:first-child,
  .'.$tag.'panel > .'.$tag.'table:last-child > tbody:last-child > tr:last-child th:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:last-child > .'.$tag.'table:last-child > tbody:last-child > tr:last-child th:first-child,
  .'.$tag.'panel > .'.$tag.'table:last-child > tfoot:last-child > tr:last-child th:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:last-child > .'.$tag.'table:last-child > tfoot:last-child > tr:last-child th:first-child {
    border-bottom-left-radius: 3px;
  }
  .'.$tag.'panel > .'.$tag.'table:last-child > tbody:last-child > tr:last-child td:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:last-child > .'.$tag.'table:last-child > tbody:last-child > tr:last-child td:last-child,
  .'.$tag.'panel > .'.$tag.'table:last-child > tfoot:last-child > tr:last-child td:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:last-child > .'.$tag.'table:last-child > tfoot:last-child > tr:last-child td:last-child,
  .'.$tag.'panel > .'.$tag.'table:last-child > tbody:last-child > tr:last-child th:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:last-child > .'.$tag.'table:last-child > tbody:last-child > tr:last-child th:last-child,
  .'.$tag.'panel > .'.$tag.'table:last-child > tfoot:last-child > tr:last-child th:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive:last-child > .'.$tag.'table:last-child > tfoot:last-child > tr:last-child th:last-child {
    border-bottom-right-radius: 3px;
  }
  .'.$tag.'panel > .'.$tag.'panel-body + .'.$tag.'table,
  .'.$tag.'panel > .'.$tag.'panel-body + .'.$tag.'table-responsive,
  .'.$tag.'panel > .'.$tag.'table + .'.$tag.'panel-body,
  .'.$tag.'panel > .'.$tag.'table-responsive + .'.$tag.'panel-body {
    border-top: 1px solid #ddd;
  }
  .'.$tag.'panel > .'.$tag.'table > tbody:first-child > tr:first-child th,
  .'.$tag.'panel > .'.$tag.'table > tbody:first-child > tr:first-child td {
    border-top: 0;
  }
  .'.$tag.'panel > .'.$tag.'table-bordered,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered {
    border: 0;
  }
  .'.$tag.'panel > .'.$tag.'table-bordered > thead > tr > th:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > thead > tr > th:first-child,
  .'.$tag.'panel > .'.$tag.'table-bordered > tbody > tr > th:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > tbody > tr > th:first-child,
  .'.$tag.'panel > .'.$tag.'table-bordered > tfoot > tr > th:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > tfoot > tr > th:first-child,
  .'.$tag.'panel > .'.$tag.'table-bordered > thead > tr > td:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > thead > tr > td:first-child,
  .'.$tag.'panel > .'.$tag.'table-bordered > tbody > tr > td:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > tbody > tr > td:first-child,
  .'.$tag.'panel > .'.$tag.'table-bordered > tfoot > tr > td:first-child,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > tfoot > tr > td:first-child {
    border-left: 0;
  }
  .'.$tag.'panel > .'.$tag.'table-bordered > thead > tr > th:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > thead > tr > th:last-child,
  .'.$tag.'panel > .'.$tag.'table-bordered > tbody > tr > th:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > tbody > tr > th:last-child,
  .'.$tag.'panel > .'.$tag.'table-bordered > tfoot > tr > th:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > tfoot > tr > th:last-child,
  .'.$tag.'panel > .'.$tag.'table-bordered > thead > tr > td:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > thead > tr > td:last-child,
  .'.$tag.'panel > .'.$tag.'table-bordered > tbody > tr > td:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > tbody > tr > td:last-child,
  .'.$tag.'panel > .'.$tag.'table-bordered > tfoot > tr > td:last-child,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > tfoot > tr > td:last-child {
    border-right: 0;
  }
  .'.$tag.'panel > .'.$tag.'table-bordered > thead > tr:first-child > td,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > thead > tr:first-child > td,
  .'.$tag.'panel > .'.$tag.'table-bordered > tbody > tr:first-child > td,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > tbody > tr:first-child > td,
  .'.$tag.'panel > .'.$tag.'table-bordered > thead > tr:first-child > th,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > thead > tr:first-child > th,
  .'.$tag.'panel > .'.$tag.'table-bordered > tbody > tr:first-child > th,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > tbody > tr:first-child > th {
    border-bottom: 0;
  }
  .'.$tag.'panel > .'.$tag.'table-bordered > tbody > tr:last-child > td,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > tbody > tr:last-child > td,
  .'.$tag.'panel > .'.$tag.'table-bordered > tfoot > tr:last-child > td,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > tfoot > tr:last-child > td,
  .'.$tag.'panel > .'.$tag.'table-bordered > tbody > tr:last-child > th,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > tbody > tr:last-child > th,
  .'.$tag.'panel > .'.$tag.'table-bordered > tfoot > tr:last-child > th,
  .'.$tag.'panel > .'.$tag.'table-responsive > .'.$tag.'table-bordered > tfoot > tr:last-child > th {
    border-bottom: 0;
  }
  .'.$tag.'panel > .'.$tag.'table-responsive {
    margin-bottom: 0;
    border: 0;
  }
  .'.$tag.'panel-group {
    margin-bottom: 20px;
  }
  .'.$tag.'panel-group .'.$tag.'panel {
    margin-bottom: 0;
    border-radius: 4px;
  }
  .'.$tag.'panel-group .'.$tag.'panel + .'.$tag.'panel {
    margin-top: 5px;
  }
  .'.$tag.'panel-group .'.$tag.'panel-heading {
    border-bottom: 0;
  }
  .'.$tag.'panel-group .'.$tag.'panel-heading + .'.$tag.'panel-collapse > .'.$tag.'panel-body,
  .'.$tag.'panel-group .'.$tag.'panel-heading + .'.$tag.'panel-collapse > .'.$tag.'list-group {
    border-top: 1px solid #ddd;
  }
  .'.$tag.'panel-group .'.$tag.'panel-footer {
    border-top: 0;
  }
  .'.$tag.'panel-group .'.$tag.'panel-footer + .'.$tag.'panel-collapse .'.$tag.'panel-body {
    border-bottom: 1px solid #ddd;
  }
  .'.$tag.'panel-default {
    border-color: #ddd;
  }
  .'.$tag.'panel-default > .'.$tag.'panel-heading {
    color: #333;
    background-color: #f5f5f5;
    border-color: #ddd;
  }
  .'.$tag.'panel-default > .'.$tag.'panel-heading + .'.$tag.'panel-collapse > .'.$tag.'panel-body {
    border-top-color: #ddd;
  }
  .'.$tag.'panel-default > .'.$tag.'panel-heading .'.$tag.'badge {
    color: #f5f5f5;
    background-color: #333;
  }
  .'.$tag.'panel-default > .'.$tag.'panel-footer + .'.$tag.'panel-collapse > .'.$tag.'panel-body {
    border-bottom-color: #ddd;
  }
  .'.$tag.'panel-primary {
    border-color: #337ab7;
  }
  .'.$tag.'panel-primary > .'.$tag.'panel-heading {
    color: #fff;
    background-color: #337ab7;
    border-color: #337ab7;
  }
  .'.$tag.'panel-primary > .'.$tag.'panel-heading + .'.$tag.'panel-collapse > .'.$tag.'panel-body {
    border-top-color: #337ab7;
  }
  .'.$tag.'panel-primary > .'.$tag.'panel-heading .'.$tag.'badge {
    color: #337ab7;
    background-color: #fff;
  }
  .'.$tag.'panel-primary > .'.$tag.'panel-footer + .'.$tag.'panel-collapse > .'.$tag.'panel-body {
    border-bottom-color: #337ab7;
  }
  .'.$tag.'panel-success {
    border-color: #d6e9c6;
  }
  .'.$tag.'panel-success > .'.$tag.'panel-heading {
    color: #3c763d;
    background-color: #dff0d8;
    border-color: #d6e9c6;
  }
  .'.$tag.'panel-success > .'.$tag.'panel-heading + .'.$tag.'panel-collapse > .'.$tag.'panel-body {
    border-top-color: #d6e9c6;
  }
  .'.$tag.'panel-success > .'.$tag.'panel-heading .'.$tag.'badge {
    color: #dff0d8;
    background-color: #3c763d;
  }
  .'.$tag.'panel-success > .'.$tag.'panel-footer + .'.$tag.'panel-collapse > .'.$tag.'panel-body {
    border-bottom-color: #d6e9c6;
  }
  .'.$tag.'panel-info {
    border-color: #bce8f1;
  }
  .'.$tag.'panel-info > .'.$tag.'panel-heading {
    color: #31708f;
    background-color: #d9edf7;
    border-color: #bce8f1;
  }
  .'.$tag.'panel-info > .'.$tag.'panel-heading + .'.$tag.'panel-collapse > .'.$tag.'panel-body {
    border-top-color: #bce8f1;
  }
  .'.$tag.'panel-info > .'.$tag.'panel-heading .'.$tag.'badge {
    color: #d9edf7;
    background-color: #31708f;
  }
  .'.$tag.'panel-info > .'.$tag.'panel-footer + .'.$tag.'panel-collapse > .'.$tag.'panel-body {
    border-bottom-color: #bce8f1;
  }
  .'.$tag.'panel-warning {
    border-color: #faebcc;
  }
  .'.$tag.'panel-warning > .'.$tag.'panel-heading {
    color: #8a6d3b;
    background-color: #fcf8e3;
    border-color: #faebcc;
  }
  .'.$tag.'panel-warning > .'.$tag.'panel-heading + .'.$tag.'panel-collapse > .'.$tag.'panel-body {
    border-top-color: #faebcc;
  }
  .'.$tag.'panel-warning > .'.$tag.'panel-heading .'.$tag.'badge {
    color: #fcf8e3;
    background-color: #8a6d3b;
  }
  .'.$tag.'panel-warning > .'.$tag.'panel-footer + .'.$tag.'panel-collapse > .'.$tag.'panel-body {
    border-bottom-color: #faebcc;
  }
  .'.$tag.'panel-danger {
    border-color: #ebccd1;
  }
  .'.$tag.'panel-danger > .'.$tag.'panel-heading {
    color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
  }
  .'.$tag.'panel-danger > .'.$tag.'panel-heading + .'.$tag.'panel-collapse > .'.$tag.'panel-body {
    border-top-color: #ebccd1;
  }
  .'.$tag.'panel-danger > .'.$tag.'panel-heading .'.$tag.'badge {
    color: #f2dede;
    background-color: #a94442;
  }
  .'.$tag.'panel-danger > .'.$tag.'panel-footer + .'.$tag.'panel-collapse > .'.$tag.'panel-body {
    border-bottom-color: #ebccd1;
  }
  .'.$tag.'embed-responsive {
    position: relative;
    display: block;
    height: 0;
    padding: 0;
    overflow: hidden;
  }
  .'.$tag.'embed-responsive .'.$tag.'embed-responsive-item,
  .'.$tag.'embed-responsive iframe,
  .'.$tag.'embed-responsive embed,
  .'.$tag.'embed-responsive object,
  .'.$tag.'embed-responsive video {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border: 0;
  }
  .'.$tag.'embed-responsive-16by9 {
    padding-bottom: 56.25%;
  }
  .'.$tag.'embed-responsive-4by3 {
    padding-bottom: 75%;
  }
  .'.$tag.'well {
    min-height: 20px;
    padding: 19px;
    margin-bottom: 20px;
    background-color: #f5f5f5;
    border: 1px solid #e3e3e3;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
  }
  .'.$tag.'well blockquote {
    border-color: #ddd;
    border-color: rgba(0, 0, 0, .15);
  }
  .'.$tag.'well-lg {
    padding: 24px;
    border-radius: 6px;
  }
  .'.$tag.'well-sm {
    padding: 9px;
    border-radius: 3px;
  }
  .'.$tag.'close {
    float: right;
    font-size: 21px;
    font-weight: bold;
    line-height: 1;
    color: #000;
    text-shadow: 0 1px 0 #fff;
    filter: alpha(opacity=20);
    opacity: .2;
  }
  .'.$tag.'close:hover,
  .'.$tag.'close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
    filter: alpha(opacity=50);
    opacity: .5;
  }
  button.'.$tag.'close {
    -webkit-appearance: none;
    padding: 0;
    cursor: pointer;
    background: transparent;
    border: 0;
  }
  .'.$tag.'modal-open {
    overflow: hidden;
  }
  .'.$tag.'modal {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1050;
    display: none;
    overflow: hidden;
    -webkit-overflow-scrolling: touch;
    outline: 0;
  }
  .'.$tag.'modal.'.$tag.'fade .'.$tag.'modal-dialog {
    -webkit-transition: -webkit-transform .3s ease-out;
         -o-transition:      -o-transform .3s ease-out;
            transition:         transform .3s ease-out;
    -webkit-transform: translate(0, -25%);
        -ms-transform: translate(0, -25%);
         -o-transform: translate(0, -25%);
            transform: translate(0, -25%);
  }
  .'.$tag.'modal.'.$tag.'in .'.$tag.'modal-dialog {
    -webkit-transform: translate(0, 0);
        -ms-transform: translate(0, 0);
         -o-transform: translate(0, 0);
            transform: translate(0, 0);
  }
  .'.$tag.'modal-open .'.$tag.'modal {
    overflow-x: hidden;
    overflow-y: auto;
  }
  .'.$tag.'modal-dialog {
    position: relative;
    width: auto;
    margin: 10px;
  }
  .'.$tag.'modal-content {
    position: relative;
    background-color: #fff;
    -webkit-background-clip: padding-box;
            background-clip: padding-box;
    border: 1px solid #999;
    border: 1px solid rgba(0, 0, 0, .2);
    border-radius: 6px;
    outline: 0;
    -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, .5);
            box-shadow: 0 3px 9px rgba(0, 0, 0, .5);
  }
  .'.$tag.'modal-backdrop {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1040;
    background-color: #000;
  }
  .'.$tag.'modal-backdrop.'.$tag.'fade {
    filter: alpha(opacity=0);
    opacity: 0;
  }
  .'.$tag.'modal-backdrop.'.$tag.'in {
    filter: alpha(opacity=50);
    opacity: .5;
  }
  .'.$tag.'modal-header {
    padding: 15px;
    border-bottom: 1px solid #e5e5e5;
  }
  .'.$tag.'modal-header .'.$tag.'close {
    margin-top: -2px;
  }
  .'.$tag.'modal-title {
    margin: 0;
    line-height: 1.42857143;
  }
  .'.$tag.'modal-body {
    position: relative;
    padding: 15px;
  }
  .'.$tag.'modal-footer {
    padding: 15px;
    text-align: right;
    border-top: 1px solid #e5e5e5;
  }
  .'.$tag.'modal-footer .'.$tag.'btn + .'.$tag.'btn {
    margin-bottom: 0;
    margin-left: 5px;
  }
  .'.$tag.'modal-footer .'.$tag.'btn-group .'.$tag.'btn + .'.$tag.'btn {
    margin-left: -1px;
  }
  .'.$tag.'modal-footer .'.$tag.'btn-block + .'.$tag.'btn-block {
    margin-left: 0;
  }
  .'.$tag.'modal-scrollbar-measure {
    position: absolute;
    top: -9999px;
    width: 50px;
    height: 50px;
    overflow: scroll;
  }
  @media (min-width: 768px) {
    .'.$tag.'modal-dialog {
      width: 600px;
      margin: 30px auto;
    }
    .'.$tag.'modal-content {
      -webkit-box-shadow: 0 5px 15px rgba(0, 0, 0, .5);
              box-shadow: 0 5px 15px rgba(0, 0, 0, .5);
    }
    .'.$tag.'modal-sm {
      width: 300px;
    }
  }
  @media (min-width: 992px) {
    .'.$tag.'modal-lg {
      width: 900px;
    }
  }
  .'.$tag.'tooltip {
    position: absolute;
    z-index: 1070;
    display: block;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 12px;
    font-style: normal;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: left;
    text-align: start;
    text-decoration: none;
    text-shadow: none;
    text-transform: none;
    letter-spacing: normal;
    word-break: normal;
    word-spacing: normal;
    word-wrap: normal;
    white-space: normal;
    filter: alpha(opacity=0);
    opacity: 0;

    line-break: auto;
  }
  .'.$tag.'tooltip.'.$tag.'in {
    filter: alpha(opacity=90);
    opacity: .9;
  }
  .'.$tag.'tooltip.'.$tag.'top {
    padding: 5px 0;
    margin-top: -3px;
  }
  .'.$tag.'tooltip.'.$tag.'right {
    padding: 0 5px;
    margin-left: 3px;
  }
  .'.$tag.'tooltip.'.$tag.'bottom {
    padding: 5px 0;
    margin-top: 3px;
  }
  .'.$tag.'tooltip.'.$tag.'left {
    padding: 0 5px;
    margin-left: -3px;
  }
  .'.$tag.'tooltip-inner {
    max-width: 200px;
    padding: 3px 8px;
    color: #fff;
    text-align: center;
    background-color: #000;
    border-radius: 4px;
  }
  .'.$tag.'tooltip-arrow {
    position: absolute;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
  }
  .'.$tag.'tooltip.'.$tag.'top .'.$tag.'tooltip-arrow {
    bottom: 0;
    left: 50%;
    margin-left: -5px;
    border-width: 5px 5px 0;
    border-top-color: #000;
  }
  .'.$tag.'tooltip.'.$tag.'top-left .'.$tag.'tooltip-arrow {
    right: 5px;
    bottom: 0;
    margin-bottom: -5px;
    border-width: 5px 5px 0;
    border-top-color: #000;
  }
  .'.$tag.'tooltip.'.$tag.'top-right .'.$tag.'tooltip-arrow {
    bottom: 0;
    left: 5px;
    margin-bottom: -5px;
    border-width: 5px 5px 0;
    border-top-color: #000;
  }
  .'.$tag.'tooltip.'.$tag.'right .'.$tag.'tooltip-arrow {
    top: 50%;
    left: 0;
    margin-top: -5px;
    border-width: 5px 5px 5px 0;
    border-right-color: #000;
  }
  .'.$tag.'tooltip.'.$tag.'left .'.$tag.'tooltip-arrow {
    top: 50%;
    right: 0;
    margin-top: -5px;
    border-width: 5px 0 5px 5px;
    border-left-color: #000;
  }
  .'.$tag.'tooltip.'.$tag.'bottom .'.$tag.'tooltip-arrow {
    top: 0;
    left: 50%;
    margin-left: -5px;
    border-width: 0 5px 5px;
    border-bottom-color: #000;
  }
  .'.$tag.'tooltip.'.$tag.'bottom-left .'.$tag.'tooltip-arrow {
    top: 0;
    right: 5px;
    margin-top: -5px;
    border-width: 0 5px 5px;
    border-bottom-color: #000;
  }
  .'.$tag.'tooltip.'.$tag.'bottom-right .'.$tag.'tooltip-arrow {
    top: 0;
    left: 5px;
    margin-top: -5px;
    border-width: 0 5px 5px;
    border-bottom-color: #000;
  }
  .'.$tag.'popover {
    position: absolute;
    top: 0;
    left: 0;
    z-index: 1060;
    display: none;
    max-width: 276px;
    padding: 1px;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    font-style: normal;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: left;
    text-align: start;
    text-decoration: none;
    text-shadow: none;
    text-transform: none;
    letter-spacing: normal;
    word-break: normal;
    word-spacing: normal;
    word-wrap: normal;
    white-space: normal;
    background-color: #fff;
    -webkit-background-clip: padding-box;
            background-clip: padding-box;
    border: 1px solid #ccc;
    border: 1px solid rgba(0, 0, 0, .2);
    border-radius: 6px;
    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
            box-shadow: 0 5px 10px rgba(0, 0, 0, .2);

    line-break: auto;
  }
  .'.$tag.'popover.'.$tag.'top {
    margin-top: -10px;
  }
  .'.$tag.'popover.'.$tag.'right {
    margin-left: 10px;
  }
  .'.$tag.'popover.'.$tag.'bottom {
    margin-top: 10px;
  }
  .'.$tag.'popover.'.$tag.'left {
    margin-left: -10px;
  }
  .'.$tag.'popover-title {
    padding: 8px 14px;
    margin: 0;
    font-size: 14px;
    background-color: #f7f7f7;
    border-bottom: 1px solid #ebebeb;
    border-radius: 5px 5px 0 0;
  }
  .'.$tag.'popover-content {
    padding: 9px 14px;
  }
  .'.$tag.'popover > .'.$tag.'arrow,
  .'.$tag.'popover > .'.$tag.'arrow:after {
    position: absolute;
    display: block;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
  }
  .'.$tag.'popover > .'.$tag.'arrow {
    border-width: 11px;
  }
  .'.$tag.'popover > .'.$tag.'arrow:after {
    content: "";
    border-width: 10px;
  }
  .'.$tag.'popover.'.$tag.'top > .'.$tag.'arrow {
    bottom: -11px;
    left: 50%;
    margin-left: -11px;
    border-top-color: #999;
    border-top-color: rgba(0, 0, 0, .25);
    border-bottom-width: 0;
  }
  .'.$tag.'popover.'.$tag.'top > .'.$tag.'arrow:after {
    bottom: 1px;
    margin-left: -10px;
    content: " ";
    border-top-color: #fff;
    border-bottom-width: 0;
  }
  .'.$tag.'popover.'.$tag.'right > .'.$tag.'arrow {
    top: 50%;
    left: -11px;
    margin-top: -11px;
    border-right-color: #999;
    border-right-color: rgba(0, 0, 0, .25);
    border-left-width: 0;
  }
  .'.$tag.'popover.'.$tag.'right > .'.$tag.'arrow:after {
    bottom: -10px;
    left: 1px;
    content: " ";
    border-right-color: #fff;
    border-left-width: 0;
  }
  .'.$tag.'popover.'.$tag.'bottom > .'.$tag.'arrow {
    top: -11px;
    left: 50%;
    margin-left: -11px;
    border-top-width: 0;
    border-bottom-color: #999;
    border-bottom-color: rgba(0, 0, 0, .25);
  }
  .'.$tag.'popover.'.$tag.'bottom > .'.$tag.'arrow:after {
    top: 1px;
    margin-left: -10px;
    content: " ";
    border-top-width: 0;
    border-bottom-color: #fff;
  }
  .'.$tag.'popover.'.$tag.'left > .'.$tag.'arrow {
    top: 50%;
    right: -11px;
    margin-top: -11px;
    border-right-width: 0;
    border-left-color: #999;
    border-left-color: rgba(0, 0, 0, .25);
  }
  .'.$tag.'popover.'.$tag.'left > .'.$tag.'arrow:after {
    right: 1px;
    bottom: -10px;
    content: " ";
    border-right-width: 0;
    border-left-color: #fff;
  }
  .'.$tag.'carousel {
    position: relative;
  }
  .'.$tag.'carousel-inner {
    position: relative;
    width: 100%;
    overflow: hidden;
  }
  .'.$tag.'carousel-inner > .'.$tag.'item {
    position: relative;
    display: none;
    -webkit-transition: .6s ease-in-out left;
         -o-transition: .6s ease-in-out left;
            transition: .6s ease-in-out left;
  }
  .'.$tag.'carousel-inner > .'.$tag.'item > img,
  .'.$tag.'carousel-inner > .'.$tag.'item > a > img {
    line-height: 1;
  }
  @media all and (transform-3d), (-webkit-transform-3d) {
    .'.$tag.'carousel-inner > .'.$tag.'item {
      -webkit-transition: -webkit-transform .6s ease-in-out;
           -o-transition:      -o-transform .6s ease-in-out;
              transition:         transform .6s ease-in-out;

      -webkit-backface-visibility: hidden;
              backface-visibility: hidden;
      -webkit-perspective: 1000px;
              perspective: 1000px;
    }
    .'.$tag.'carousel-inner > .'.$tag.'item.'.$tag.'next,
    .'.$tag.'carousel-inner > .'.$tag.'item.'.$tag.'active.'.$tag.'right {
      left: 0;
      -webkit-transform: translate3d(100%, 0, 0);
              transform: translate3d(100%, 0, 0);
    }
    .'.$tag.'carousel-inner > .'.$tag.'item.'.$tag.'prev,
    .'.$tag.'carousel-inner > .'.$tag.'item.'.$tag.'active.'.$tag.'left {
      left: 0;
      -webkit-transform: translate3d(-100%, 0, 0);
              transform: translate3d(-100%, 0, 0);
    }
    .'.$tag.'carousel-inner > .'.$tag.'item.'.$tag.'next.'.$tag.'left,
    .'.$tag.'carousel-inner > .'.$tag.'item.'.$tag.'prev.'.$tag.'right,
    .'.$tag.'carousel-inner > .'.$tag.'item.'.$tag.'active {
      left: 0;
      -webkit-transform: translate3d(0, 0, 0);
              transform: translate3d(0, 0, 0);
    }
  }
  .'.$tag.'carousel-inner > .'.$tag.'active,
  .'.$tag.'carousel-inner > .'.$tag.'next,
  .'.$tag.'carousel-inner > .'.$tag.'prev {
    display: block;
  }
  .'.$tag.'carousel-inner > .'.$tag.'active {
    left: 0;
  }
  .'.$tag.'carousel-inner > .'.$tag.'next,
  .'.$tag.'carousel-inner > .'.$tag.'prev {
    position: absolute;
    top: 0;
    width: 100%;
  }
  .'.$tag.'carousel-inner > .'.$tag.'next {
    left: 100%;
  }
  .'.$tag.'carousel-inner > .'.$tag.'prev {
    left: -100%;
  }
  .'.$tag.'carousel-inner > .'.$tag.'next.'.$tag.'left,
  .'.$tag.'carousel-inner > .'.$tag.'prev.'.$tag.'right {
    left: 0;
  }
  .'.$tag.'carousel-inner > .'.$tag.'active.'.$tag.'left {
    left: -100%;
  }
  .'.$tag.'carousel-inner > .'.$tag.'active.'.$tag.'right {
    left: 100%;
  }
  .'.$tag.'carousel-control {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    width: 15%;
    font-size: 20px;
    color: #fff;
    text-align: center;
    text-shadow: 0 1px 2px rgba(0, 0, 0, .6);
    background-color: rgba(0, 0, 0, 0);
    filter: alpha(opacity=50);
    opacity: .5;
  }
  .'.$tag.'carousel-control.'.$tag.'left {
    background-image: -webkit-linear-gradient(left, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .0001) 100%);
    background-image:      -o-linear-gradient(left, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .0001) 100%);
    background-image: -webkit-gradient(linear, left top, right top, from(rgba(0, 0, 0, .5)), to(rgba(0, 0, 0, .0001)));
    background-image:         linear-gradient(to right, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .0001) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#80000000", endColorstr="#00000000", GradientType=1);
    background-repeat: repeat-x;
  }
  .'.$tag.'carousel-control.'.$tag.'right {
    right: 0;
    left: auto;
    background-image: -webkit-linear-gradient(left, rgba(0, 0, 0, .0001) 0%, rgba(0, 0, 0, .5) 100%);
    background-image:      -o-linear-gradient(left, rgba(0, 0, 0, .0001) 0%, rgba(0, 0, 0, .5) 100%);
    background-image: -webkit-gradient(linear, left top, right top, from(rgba(0, 0, 0, .0001)), to(rgba(0, 0, 0, .5)));
    background-image:         linear-gradient(to right, rgba(0, 0, 0, .0001) 0%, rgba(0, 0, 0, .5) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#00000000", endColorstr="#80000000", GradientType=1);
    background-repeat: repeat-x;
  }
  .'.$tag.'carousel-control:hover,
  .'.$tag.'carousel-control:focus {
    color: #fff;
    text-decoration: none;
    filter: alpha(opacity=90);
    outline: 0;
    opacity: .9;
  }
  .'.$tag.'carousel-control .'.$tag.'icon-prev,
  .'.$tag.'carousel-control .'.$tag.'icon-next,
  .'.$tag.'carousel-control .'.$tag.'glyphicon-chevron-left,
  .'.$tag.'carousel-control .'.$tag.'glyphicon-chevron-right {
    position: absolute;
    top: 50%;
    z-index: 5;
    display: inline-block;
    margin-top: -10px;
  }
  .'.$tag.'carousel-control .'.$tag.'icon-prev,
  .'.$tag.'carousel-control .'.$tag.'glyphicon-chevron-left {
    left: 50%;
    margin-left: -10px;
  }
  .'.$tag.'carousel-control .'.$tag.'icon-next,
  .'.$tag.'carousel-control .'.$tag.'glyphicon-chevron-right {
    right: 50%;
    margin-right: -10px;
  }
  .'.$tag.'carousel-control .'.$tag.'icon-prev,
  .'.$tag.'carousel-control .'.$tag.'icon-next {
    width: 20px;
    height: 20px;
    font-family: serif;
    line-height: 1;
  }
  .'.$tag.'carousel-control .'.$tag.'icon-prev:before {
    content: "\2039";
  }
  .'.$tag.'carousel-control .'.$tag.'icon-next:before {
    content: "\203a";
  }
  .'.$tag.'carousel-indicators {
    position: absolute;
    bottom: 10px;
    left: 50%;
    z-index: 15;
    width: 60%;
    padding-left: 0;
    margin-left: -30%;
    text-align: center;
    list-style: none;
  }
  .'.$tag.'carousel-indicators li {
    display: inline-block;
    width: 10px;
    height: 10px;
    margin: 1px;
    text-indent: -999px;
    cursor: pointer;
    background-color: #000 \9;
    background-color: rgba(0, 0, 0, 0);
    border: 1px solid #fff;
    border-radius: 10px;
  }
  .'.$tag.'carousel-indicators .'.$tag.'active {
    width: 12px;
    height: 12px;
    margin: 0;
    background-color: #fff;
  }
  .'.$tag.'carousel-caption {
    position: absolute;
    right: 15%;
    bottom: 20px;
    left: 15%;
    z-index: 10;
    padding-top: 20px;
    padding-bottom: 20px;
    color: #fff;
    text-align: center;
    text-shadow: 0 1px 2px rgba(0, 0, 0, .6);
  }
  .'.$tag.'carousel-caption .'.$tag.'btn {
    text-shadow: none;
  }
  @media screen and (min-width: 768px) {
    .'.$tag.'carousel-control .'.$tag.'glyphicon-chevron-left,
    .'.$tag.'carousel-control .'.$tag.'glyphicon-chevron-right,
    .'.$tag.'carousel-control .'.$tag.'icon-prev,
    .'.$tag.'carousel-control .'.$tag.'icon-next {
      width: 30px;
      height: 30px;
      margin-top: -10px;
      font-size: 30px;
    }
    .'.$tag.'carousel-control .'.$tag.'glyphicon-chevron-left,
    .'.$tag.'carousel-control .'.$tag.'icon-prev {
      margin-left: -10px;
    }
    .'.$tag.'carousel-control .'.$tag.'glyphicon-chevron-right,
    .'.$tag.'carousel-control .'.$tag.'icon-next {
      margin-right: -10px;
    }
    .'.$tag.'carousel-caption {
      right: 20%;
      left: 20%;
      padding-bottom: 30px;
    }
    .'.$tag.'carousel-indicators {
      bottom: 20px;
    }
  }
  .'.$tag.'clearfix:before,
  .'.$tag.'clearfix:after,
  .'.$tag.'dl-horizontal dd:before,
  .'.$tag.'dl-horizontal dd:after,
  .'.$tag.'container:before,
  .'.$tag.'container:after,
  .'.$tag.'container-fluid:before,
  .'.$tag.'container-fluid:after,
  .'.$tag.'row:before,
  .'.$tag.'row:after,
  .'.$tag.'form-horizontal .'.$tag.'form-group:before,
  .'.$tag.'form-horizontal .'.$tag.'form-group:after,
  .'.$tag.'btn-toolbar:before,
  .'.$tag.'btn-toolbar:after,
  .'.$tag.'btn-group-vertical > .'.$tag.'btn-group:before,
  .'.$tag.'btn-group-vertical > .'.$tag.'btn-group:after,
  .'.$tag.'nav:before,
  .'.$tag.'nav:after,
  .'.$tag.'navbar:before,
  .'.$tag.'navbar:after,
  .'.$tag.'navbar-header:before,
  .'.$tag.'navbar-header:after,
  .'.$tag.'navbar-collapse:before,
  .'.$tag.'navbar-collapse:after,
  .'.$tag.'pager:before,
  .'.$tag.'pager:after,
  .'.$tag.'panel-body:before,
  .'.$tag.'panel-body:after,
  .'.$tag.'modal-header:before,
  .'.$tag.'modal-header:after,
  .'.$tag.'modal-footer:before,
  .'.$tag.'modal-footer:after {
    display: table;
    content: " ";
  }
  .'.$tag.'clearfix:after,
  .'.$tag.'dl-horizontal dd:after,
  .'.$tag.'container:after,
  .'.$tag.'container-fluid:after,
  .'.$tag.'row:after,
  .'.$tag.'form-horizontal .'.$tag.'form-group:after,
  .'.$tag.'btn-toolbar:after,
  .'.$tag.'btn-group-vertical > .'.$tag.'btn-group:after,
  .'.$tag.'nav:after,
  .'.$tag.'navbar:after,
  .'.$tag.'navbar-header:after,
  .'.$tag.'navbar-collapse:after,
  .'.$tag.'pager:after,
  .'.$tag.'panel-body:after,
  .'.$tag.'modal-header:after,
  .'.$tag.'modal-footer:after {
    clear: both;
  }
  .'.$tag.'center-block {
    display: block;
    margin-right: auto;
    margin-left: auto;
  }
  .'.$tag.'pull-right {
    float: right !important;
  }
  .'.$tag.'pull-left {
    float: left !important;
  }
  .'.$tag.'hide {
    display: none !important;
  }
  .'.$tag.'show {
    display: block !important;
  }
  .'.$tag.'invisible {
    visibility: hidden;
  }
  .'.$tag.'text-hide {
    font: 0/0 a;
    color: transparent;
    text-shadow: none;
    background-color: transparent;
    border: 0;
  }
  .'.$tag.'hidden {
    display: none !important;
  }
  .'.$tag.'affix {
    position: fixed;
  }
  @-ms-viewport {
    width: device-width;
  }
  .'.$tag.'visible-xs,
  .'.$tag.'visible-sm,
  .'.$tag.'visible-md,
  .'.$tag.'visible-lg {
    display: none !important;
  }
  .'.$tag.'visible-xs-block,
  .'.$tag.'visible-xs-inline,
  .'.$tag.'visible-xs-inline-block,
  .'.$tag.'visible-sm-block,
  .'.$tag.'visible-sm-inline,
  .'.$tag.'visible-sm-inline-block,
  .'.$tag.'visible-md-block,
  .'.$tag.'visible-md-inline,
  .'.$tag.'visible-md-inline-block,
  .'.$tag.'visible-lg-block,
  .'.$tag.'visible-lg-inline,
  .'.$tag.'visible-lg-inline-block {
    display: none !important;
  }
  @media (max-width: 767px) {
    .'.$tag.'visible-xs {
      display: block !important;
    }
    table.'.$tag.'visible-xs {
      display: table !important;
    }
    tr.'.$tag.'visible-xs {
      display: table-row !important;
    }
    th.'.$tag.'visible-xs,
    td.'.$tag.'visible-xs {
      display: table-cell !important;
    }
  }
  @media (max-width: 767px) {
    .'.$tag.'visible-xs-block {
      display: block !important;
    }
  }
  @media (max-width: 767px) {
    .'.$tag.'visible-xs-inline {
      display: inline !important;
    }
  }
  @media (max-width: 767px) {
    .'.$tag.'visible-xs-inline-block {
      display: inline-block !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .'.$tag.'visible-sm {
      display: block !important;
    }
    table.'.$tag.'visible-sm {
      display: table !important;
    }
    tr.'.$tag.'visible-sm {
      display: table-row !important;
    }
    th.'.$tag.'visible-sm,
    td.'.$tag.'visible-sm {
      display: table-cell !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .'.$tag.'visible-sm-block {
      display: block !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .'.$tag.'visible-sm-inline {
      display: inline !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .'.$tag.'visible-sm-inline-block {
      display: inline-block !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .'.$tag.'visible-md {
      display: block !important;
    }
    table.'.$tag.'visible-md {
      display: table !important;
    }
    tr.'.$tag.'visible-md {
      display: table-row !important;
    }
    th.'.$tag.'visible-md,
    td.'.$tag.'visible-md {
      display: table-cell !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .'.$tag.'visible-md-block {
      display: block !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .'.$tag.'visible-md-inline {
      display: inline !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .'.$tag.'visible-md-inline-block {
      display: inline-block !important;
    }
  }
  @media (min-width: 1200px) {
    .'.$tag.'visible-lg {
      display: block !important;
    }
    table.'.$tag.'visible-lg {
      display: table !important;
    }
    tr.'.$tag.'visible-lg {
      display: table-row !important;
    }
    th.'.$tag.'visible-lg,
    td.'.$tag.'visible-lg {
      display: table-cell !important;
    }
  }
  @media (min-width: 1200px) {
    .'.$tag.'visible-lg-block {
      display: block !important;
    }
  }
  @media (min-width: 1200px) {
    .'.$tag.'visible-lg-inline {
      display: inline !important;
    }
  }
  @media (min-width: 1200px) {
    .'.$tag.'visible-lg-inline-block {
      display: inline-block !important;
    }
  }
  @media (max-width: 767px) {
    .'.$tag.'hidden-xs {
      display: none !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .'.$tag.'hidden-sm {
      display: none !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .'.$tag.'hidden-md {
      display: none !important;
    }
  }
  @media (min-width: 1200px) {
    .'.$tag.'hidden-lg {
      display: none !important;
    }
  }
  .'.$tag.'visible-print {
    display: none !important;
  }
  @media print {
    .'.$tag.'visible-print {
      display: block !important;
    }
    table.'.$tag.'visible-print {
      display: table !important;
    }
    tr.'.$tag.'visible-print {
      display: table-row !important;
    }
    th.'.$tag.'visible-print,
    td.'.$tag.'visible-print {
      display: table-cell !important;
    }
  }
  .'.$tag.'visible-print-block {
    display: none !important;
  }
  @media print {
    .'.$tag.'visible-print-block {
      display: block !important;
    }
  }
  .'.$tag.'visible-print-inline {
    display: none !important;
  }
  @media print {
    .'.$tag.'visible-print-inline {
      display: inline !important;
    }
  }
  .'.$tag.'visible-print-inline-block {
    display: none !important;
  }
  @media print {
    .'.$tag.'visible-print-inline-block {
      display: inline-block !important;
    }
  }
  @media print {
    .'.$tag.'hidden-print {
      display: none !important;
    }
  }
  /*# sourceMappingURL=bootstrap.css.map */';

}