CREATE TABLE IF NOT EXISTS `search_terms` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `type` enum('parent','child') NOT NULL DEFAULT 'parent',
  `parent_id` int(11) DEFAULT NULL,
  `se` varchar(20) NOT NULL,
  `last_robot_access` datetime NOT NULL,
  `last_human_access` datetime NOT NULL,
  `access_count` varchar(255) NOT NULL,
  `term_status` enum('0','1','2','3','4','5','6','7','8','9') NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `slug` (`slug`),
  KEY `term_status` (`term_status`),
  KEY `last_robot_access` (`last_robot_access`,`last_human_access`),
  FULLTEXT KEY `term` (`term`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2263 ;

CREATE TABLE IF NOT EXISTS `term_images` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `term` varchar(255) NOT NULL,
  `parent_term` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `height` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;