<?php


if( ! function_exists('form_is_selected') ) {
function form_is_selected( $checked_value, $matched_value ) {
  if( $checked_value == $matched_value )
    echo 'selected="selected"';
}
}


if( ! function_exists('form_is_checked') ) {
function form_is_checked( $checked_value, $matched_value ) {
  if( $checked_value == $matched_value )
    echo 'checked="checked"';
}
}


/*------------------------------------------------------------------------------
          QUERY CONSTRUCTORS
------------------------------------------------------------------------------*/
if( ! function_exists('get_table_column_names') ) {
function get_table_column_names( $tablename, $return=ARRAY_N ) {
  global $db;

  $tablenames = array();
  $columns    = array();
  $tables = $db->get_results("SHOW COLUMNS FROM {$tablename}");
  foreach ($tables as $table) {
    $tablenames[$table->Field] = '';
    $columns[] = $table->Field;
  }


  if( $return==OBJECT ) 
    return (object)$tablenames;
  
  else if ( $return==ARRAY_A ) 
    return $tablenames;

  else if ( $return==ARRAY_N ) 
   return $columns; 

}
}


/**
 * Construct an INSERT query from an array source
 *
 * @param string $tablename
 * @param array $args = array(
 *  'field1' => 'field1_value',
 *  'field2' => 'field2_value',
 *  'field3' => 'field3_value'
 *  )
 *
 * @return string
 */
if( ! function_exists('construct_query_insert') ) {
function construct_query_insert($tablename, $args) {
  global $db;
  
  $tablename = "`".$tablename."`";

  $fields = array_keys($args);
  
  // escape user input
  foreach ($args as $key => $arg) {
    $args[$key] = $db->escape($arg);
  }
  
  $fields = '`' .implode("`,`", $fields). '`';
  $values = "'".implode("', '", $args)."'";

  $query = "INSERT INTO ". $tablename;
  $query .= " (". $fields.")";
  $query .= " VALUES (". $values.")";

  return $query;

}
}


/**
 * Construct a REPLACE query from an array source
 *
 * @param string $tablename
 * @param array $args = array(
 *  'field1' => 'field1_value',
 *  'field2' => 'field2_value',
 *  'field3' => 'field3_value'
 *  )
 *
 * @return string
 */
if( ! function_exists('construct_query_replace') ) {
function construct_query_replace($tablename, $args) {
    $tablename = "`".$tablename."`";

    $fields = array_keys($args);
    $fields = '`' .implode("`,`", $fields). '`';
    $values = "'".implode("', '", $args)."'";

    $query = "REPLACE ". $tablename;
    $query .= " (". $fields.")";
    $query .= " VALUES (". $values.")";
    return $query;

}
}


/**
 * Construct an UPDATE query from an array source
 *
 * @param string $tablename
 * @param mixed $sets
 * @param string $wheres
 * @return string
 */
if( ! function_exists('construct_query_update') ) {
function construct_query_update($tablename, $sets, $wheres) {

  $tablename = "`".$tablename."`";
  $set_string = set_imploder($sets);
  $query = "UPDATE ". $tablename;
  $query .= " SET ". $set_string;
  $query .= " WHERE ". $wheres;

  return $query;

}
}

/**
 * Construct a DELETE query to be queried
 *
 * @param string $tablename
 * @param string $wheres
 * @return string
 */
if( ! function_exists('construct_query_delete') ) {
function construct_query_delete($tablename, $wheres) {

  $tablename = "`".$tablename."`";
  $query_delete = "DELETE FROM ".$tablename." WHERE ".$wheres;

  return $query_delete;

}
}


/**
 * Construct the WHERE clause in an UPDATE or DELETE query statement
 *
 * @param mixed
 * can be $args = array(
 *  'field1' => 'value1',
 *  'field2' => 'value2',
 *  'field3' => 'value3'
 *  )
 *
 * or only $string
 * @param string $mode Whether OR or AND
 * @param string $operand Whether =, <, >
 *
 * @return string
 */
function where_imploder($args, $mode = "AND", $operand = "=") {

    if (!is_array($args)) {

  return $args;

    } else {

  $wheres = array();
  foreach($args as $key => $value) {
      $value = sanitize_string($value);
      $wheres[] = "`$key` $operand '$value'";
  }
  if($mode == "AND") return implode(" AND ", $wheres);
  elseif($mode == "OR") return implode(" OR ", $wheres);
    }
}

/**
 * Construct the SET clause in an UPDATE query statement
 * @return string
 */
function set_imploder($args) {
  
  global $db;

  if (!is_array($args)) {

  return $args;

  } else {

  $sets = array();
  foreach($args as $key => $value) {
      $sets[] = "`" . $key . "` = '" . $db->escape($value) . "'";
  }
  return implode(",", $sets);

  }

}