<?php 

ob_start("ob_html_compress");
$p = 'go';
include AGCL_TEMPLATE_PATH . 'header.php'; 

?>

<div class="margin-top-50"></div>
<div class="<?php echo CSS_PREFIX; ?>container">

	<div class="<?php echo CSS_PREFIX; ?>row <?php echo CSS_PREFIX; ?>text-center">	
	    <h2>Search For <?php echo $_GET['q'];?></h2> 
	    <hr>

		  <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>text-left">

			<?php $q = preg_replace('/([^a-z0-9]+)/i', ' ', $_GET['q']); ?>
			<?php $kwx = $db->get_results( "SELECT * FROM search_terms where type = 'parent' AND term Like '%".$q."%'" );
			if( $kwx ): ?>
			    <?php foreach ($kwx as $kv) : ?>

		      		<a href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo ucwords($kv->term);?>"><?php echo ucwords($kv->term);?></a>

				<br>
			    <?php endforeach; ?>
			<?php endif; ?>

		  </div>

	</div><!--.row-->

</div><!--.container-->


<?php include AGCL_TEMPLATE_PATH . 'footer.php'; ?>
<?php ob_end_flush(); ?>
