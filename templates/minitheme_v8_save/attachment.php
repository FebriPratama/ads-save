<?php 

	ob_start("ob_html_compress");

	$p = 'attachment';
	$q = preg_replace('/([^a-z0-9]+)/i', ' ', $URL['args'][1]);

	$term  = $q;
	$term .= "\n" . file_get_contents(ABSPATH . 'term.txt');
	file_put_contents(ABSPATH . 'term.txt', $term);  

	include AGCL_TEMPLATE_PATH . 'header.php';
		  
?>
	<div class="margin-top-50"></div>
	<div class="<?php echo CSS_PREFIX; ?>container">
		<div class="<?php echo CSS_PREFIX; ?>row">
			<div class="<?php echo CSS_PREFIX; ?>col-md-12">
				<ol class="<?php echo CSS_PREFIX; ?>breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				  <li>
				  	<a itemprop="url" rel="nofollow" href="<?php echo SITE_URL ?>">
				  		<i itemprop="title">Home</i>
				  	</a>
				  </li>
				  <li itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				  	<a itemprop="url" href="<?php echo _a_url_q( $item->term ); ?>">
				  		<i itemprop="title"><?php echo $item->term; ?></i>
				  	</a>
				  </li>
				  <li class="<?php echo CSS_PREFIX; ?>active" itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				  	<a itemprop="url" href="#">
				  		<i itemprop="title"><?php echo ucwords($q);?></i>
				  	</a>
				  </li>
				</ol>
			</div>
		</div>
	</div>
	<div class="<?php echo CSS_PREFIX; ?>container">
		<div class="<?php echo CSS_PREFIX; ?>row">
			<div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>text-center">
				<h1 itemprop="name"><?php echo ucwords($q);?></h1>
			</div>
		</div>
	    <div class="<?php echo CSS_PREFIX; ?>row">
            <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>col-xs-12 <?php echo CSS_PREFIX; ?>text-center">
					
	            <div class="ads leaderboard-xs leaderboard-md leaderboard-sm leaderboard-lg">
	                <?php if(hasOption($db,'ads728')) echo getOption($db,'ads728')->opt_value; ?>
	            </div>      
				
            </div>
	    </div>
	</div>	

	<div>
		<div class="<?php echo CSS_PREFIX; ?>container">
			<div class="margin-top-40"></div>
			<div class="<?php echo CSS_PREFIX; ?>row">
                <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>col-md-12">

					<?php

					        $title = $img['term'];

					        $width = $img['width'];
					        $height = $img['height'];
					        $imageurl = $img['url'];
					        $thumbnail_url = $img['thumb'];

							?>

							<div class="<?php echo CSS_PREFIX; ?>col-xs-12 <?php echo CSS_PREFIX; ?>col-md-12">

					              <div class="<?php echo CSS_PREFIX; ?>panel <?php echo CSS_PREFIX; ?>panel-default" itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                                    <div class="<?php echo CSS_PREFIX; ?>panel-heading <?php echo CSS_PREFIX; ?>text-center">
                                    	<?php echo suffleTitleFull($title,$imageurl); ?>
                                    </div>
					                <div class="<?php echo CSS_PREFIX; ?>panel-body <?php echo CSS_PREFIX; ?>text-center" style="min-height:100px;padding:0px;">

					                		<img src="<?php echo $imageurl; ?>" class="thumb img-responsive" title="<?php echo $title ?>" style="width:100%;height:auto;" width="<?php echo $width;?>" height="<?php echo $height;?>" alt="<?php echo $title ?>"/> 

					                		<?php

										    		if(array_key_exists('large', $img['childs']) && is_object($img['childs']['large'])){

												        $width = $img['childs']['large']->width;
												        $height = $img['childs']['large']->height;
												        $imageurl = SITE_URL.'imgs/'.$img['childs']['large']->url;
												        $thumbnail_url = $img['childs']['large']->thumb;

											?>		
												
												<a target="_blank" href="<?php echo $imageurl; ?>" title="<?php echo $title ?> Large Version">.</a>		

					        				<?php } ?>

					                			<?php

										    		if(array_key_exists('medium', $img['childs']) && is_object($img['childs']['medium'])){

												        $width = $img['childs']['medium']->width;
												        $height = $img['childs']['medium']->height;
												        $imageurl = SITE_URL.'imgs/'.$img['childs']['medium']->url;
												        $thumbnail_url = $img['childs']['medium']->thumb;

											?>				    
												<a target="_blank" href="<?php echo $imageurl; ?>" title="<?php echo $title ?> Medium Version">.</a>	
					        				<?php } ?>

					                			<?php

											    	if(array_key_exists('thumb', $img['childs']) && is_object($img['childs']['thumb'])){

												        $width = $img['childs']['thumb']->width;
												        $height = $img['childs']['thumb']->height;
												        $imageurl = SITE_URL.'imgs/'.$img['childs']['thumb']->url;
												        $thumbnail_url = $img['childs']['thumb']->thumb;

											?>				
												<a target="_blank" href="<?php echo $imageurl; ?>" title="<?php echo $title ?> Thumb Version">.</a>	
					        				<?php } ?>
		                		
					                </div>

					              </div>

							</div>

						<?php 		
					?>

                </div>

	        </div>
       </div>
	</div>

	<?php if(IS_ADDON_BING){ ?>
		<div class="<?php echo CSS_PREFIX; ?>container">
			<div class="<?php echo CSS_PREFIX; ?>row">
				<div class="<?php echo CSS_PREFIX; ?>col-md-12">
					<h3 itemprop="name">Descriptions of <?php echo ucwords($q);?></h3>
				</div>
				<div class="<?php echo CSS_PREFIX; ?>col-md-12">
					<div class="<?php echo CSS_PREFIX; ?>panel <?php echo CSS_PREFIX; ?>panel-default">
						<div class="<?php echo CSS_PREFIX; ?>panel-body">

							<!--ads-->
							<div class="<?php echo CSS_PREFIX; ?>col-md-12">
								<div class="<?php echo CSS_PREFIX; ?>col-md-4">
						            
						            <div class="ads rec-300">
						                <?php if(hasOption($db,'ads300')) echo getOption($db,'ads300')->opt_value; ?>
						            </div>      

								</div>
								<div class="<?php echo CSS_PREFIX; ?>col-md-8">
									
									<h3>Hit One of the Thumbnails to Get More <?php echo ucwords(suffleCat());?> Ideas.</h3>

						            <?php  

						                $kwx = $db->get_results( "SELECT * FROM search_terms AS r1 JOIN (SELECT (RAND() * (SELECT MAX(id) FROM search_terms)) AS id) AS r2 WHERE r1.id >= r2.id  AND r1.type='parent' LIMIT 9" );

						              ?>

									<div class="<?php echo CSS_PREFIX; ?>col-md-6">

						                <?php $i=0; ?>
						                <?php if( $kwx ) : ?>
						                <?php foreach ($kwx as $kv) : ?>
						                    <?php if($i>1) break; ?>
						                    <?php $img = $db->get_row("select b.* from search_terms as a join term_images as b on b.parent_term = a.ID where a.parent_id = '".$kv->ID."'"); ?>

						                    <?php if(is_object($img)){ ?>

						                    <?php 

						                      $img = array(

						                          'term'  => removeSpecial(ucwords($kv->term)),
						                          'url'  => SITE_URL.'imgs/'.$img->url,       
						                          'height' =>$img->height,
						                          'width' => $img->width

						                          ); 

						                    ?>
						                    <div class="<?php echo CSS_PREFIX; ?>row" style="margin-bottom:10px;">
												
												<div class="<?php echo CSS_PREFIX; ?>col-md-4">
												
													<a href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $kv->term; ?>" rel="nofollow">
								                		
								                		<img src="<?php echo AGCL_TEMPLATE_URL ?>lib/img/loading.png" class="thumb lazyload" title="<?php echo $kv->term; ?>" width="75" height="75" data-src="<?php echo $img['url']; ?>" alt="<?php echo $title ?>"/>  
								                
								                	</a>

							                    </div>
						                    
							                    <div class="<?php echo CSS_PREFIX; ?>col-md-8">
													
													<a href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $kv->term; ?>" rel="nofollow">
								                		
							                    		<h4 class="single-header"><?php echo $img['term']; ?></h4>
							                    	
							                    	</a>

							                    </div>

						                    </div>
						                    
						                    <?php } ?>

						                <?php $i++; ?>
						                <?php endforeach; ?>
						                <?php endif; ?>

									</div>
									<div class="<?php echo CSS_PREFIX; ?>col-md-6">

						                <?php $i=0; ?>
						                <?php if( $kwx ) : ?>
						                <?php foreach ($kwx as $kv) : ?>
						                   
						                    <?php if($i>1 && $i<4){ ?>
						                    <?php $img = $db->get_row("select b.* from search_terms as a join term_images as b on b.parent_term = a.ID where a.parent_id = '".$kv->ID."'"); ?>

						                    <?php if(is_object($img)){ ?>

						                    <?php 

						                      $img = array(

						                          'term'  => removeSpecial(ucwords($kv->term)),
						                          'url'  => SITE_URL.'imgs/'.$img->url,       
						                          'height' =>$img->height,
						                          'width' => $img->width

						                          ); 

						                    ?>
						                    <div class="<?php echo CSS_PREFIX; ?>row" style="margin-bottom:10px;">
												
												<div class="<?php echo CSS_PREFIX; ?>col-md-4">
												
													<a href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $kv->term; ?>" rel="nofollow">
								                		
								                		<img src="<?php echo AGCL_TEMPLATE_URL ?>lib/img/loading.png" class="thumb lazyload" title="<?php echo $kv->term; ?>" width="75" height="75" data-src="<?php echo $img['url']; ?>" alt="<?php echo $title ?>"/>  
								                
								                	</a>

							                    </div>
						                    
							                    <div class="<?php echo CSS_PREFIX; ?>col-md-8">
													
													<a href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $kv->term; ?>" rel="nofollow">
								                		
							                    		<h4 class="single-header"><?php echo $img['term']; ?></h4>
							                    	
							                    	</a>

							                    </div>

						                    </div>
						                    
						                    <?php } ?>
						                   
						                   <?php } ?>

						                <?php $i++; ?>
						                <?php endforeach; ?>
						                <?php endif; ?>

									</div>

								</div>
							</div>
							<div class="<?php echo CSS_PREFIX; ?>col-md-12">
								<blockquote class="blockquote description">

									<p>
										<strong><?php echo ucwords($q);?></strong>
								    	<em><?php echo ucwords($q);?></em> - <strong><?php echo $item->term; ?></strong>
									</p>

							    </blockquote>	
							</div>

						</div>
					</div>
				</div>

			</div>
		</div>	

	<?php } ?>

   	<div class="<?php echo CSS_PREFIX; ?>container">
		<h3 itemprop="name" class="content-title">Pictures gallery of <?php echo ucwords($q);?></h3>

	   	<div class="<?php echo CSS_PREFIX; ?>row">
            <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>col-xs-12 <?php echo CSS_PREFIX; ?>text-center">
					
	            <div class="ads leaderboard-xs leaderboard-md leaderboard-sm leaderboard-lg">
	                <?php if(hasOption($db,'ads728')) echo getOption($db,'ads728')->opt_value; ?>
	            </div>      
				
            </div>
	        <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>col-md-12">

				<?php

				if( $imgs ):
					$g = 0;
					foreach ($imgs as $img) :

				        $title = $img['term'];

				    	if(array_key_exists('thumb', $img['childs']) && is_object($img['childs']['thumb'])){

					        $width = $img['childs']['thumb']->width;
					        $height = $img['childs']['thumb']->height;
					        $imageurl = SITE_URL.'imgs/'.$img['childs']['thumb']->url;
					        $thumbnail_url = $img['childs']['thumb']->thumb;

				    	}else{

					        $width = $img['width'];
					        $height = $img['height'];
					        $imageurl = $img['url'];
					        $thumbnail_url = $img['thumb'];

				    	}

						?>

						<div class="<?php echo CSS_PREFIX; ?>col-xs-12 <?php echo CSS_PREFIX; ?>col-md-3">

				              <div class="<?php echo CSS_PREFIX; ?>panel">

				                <div class="<?php echo CSS_PREFIX; ?>panel-body <?php echo CSS_PREFIX; ?>text-center" style="padding:0px;overflow:hidden;width:100px;height:100px;">

				                	<a href="<?php echo to_attachment($item->term,$title); ?>" title="<?php echo $title ?> Thumb Version" itemprop="associatedMedia" itemid="<?php echo $imageurl; ?>" itemscope itemtype="http://schema.org/ImageObject">
				                		
				                		<img src="<?php echo $imageurl; ?>" class="thumb lazyload" title="<?php echo $title ?> Thumb Version" width="<?php echo $width;?>" height="<?php echo $height;?>" alt="<?php echo $title ?> Thumb Version"/>

				                	</a>

			                		<?php

								        $width = $img['width'];
								        $height = $img['height'];
								        $imageurl = $img['url'];
								        $thumbnail_url = $img['thumb'];

									?>		
										
										<a target="_blank" href="<?php echo $imageurl; ?>" title="<?php echo $title ?>">.</a>		

			                		<?php

								    		if(array_key_exists('large', $img['childs']) && is_object($img['childs']['large'])){

										        $width = $img['childs']['large']->width;
										        $height = $img['childs']['large']->height;
										        $imageurl = SITE_URL.'imgs/'.$img['childs']['large']->url;
										        $thumbnail_url = $img['childs']['large']->thumb;

									?>		
										
										<a target="_blank" href="<?php echo $imageurl; ?>" title="<?php echo $title ?> Large Version">.</a>		

			        				<?php } ?>

			                			<?php

								    		if(array_key_exists('medium', $img['childs']) && is_object($img['childs']['medium'])){

										        $width = $img['childs']['medium']->width;
										        $height = $img['childs']['medium']->height;
										        $imageurl = SITE_URL.'imgs/'.$img['childs']['medium']->url;
										        $thumbnail_url = $img['childs']['medium']->thumb;

									?>				    
										<a target="_blank" href="<?php echo $imageurl; ?>" title="<?php echo $title ?> Medium Version">.</a>	
			        				<?php } ?>

				                </div>

				              </div>

						</div>


					<?php
					
					if($g>12) break;

					$g++;
					endforeach;
				endif;

				?>

	        </div>
	   	</div>

		<div class="<?php echo CSS_PREFIX; ?>clear"></div>

	</div>

<?php include AGCL_TEMPLATE_PATH . 'footer.php'; ?>
<?php ob_end_flush(); ?>
