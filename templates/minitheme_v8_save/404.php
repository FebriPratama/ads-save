<?php 
ob_start("ob_html_compress");
$p = '404';
include AGCL_TEMPLATE_PATH . 'header.php'; ?>

<div class="margin-top-50"></div>
<div class="<?php echo CSS_PREFIX; ?>container">
	<div class="<?php echo CSS_PREFIX; ?>row">
	  <div class="<?php echo CSS_PREFIX; ?>col-sm-12">
	    <h1>Page not found</h1>
	  </div><!--.col-->
	</div><!--.row-->
</div><!--.container-->

<?php include AGCL_TEMPLATE_PATH . 'footer.php'; ?>
<?php ob_end_flush(); ?>
