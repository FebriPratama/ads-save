<?php 

	ob_start("ob_html_compress");

	$item = $db->get_row("SELECT * FROM search_terms WHERE slug = '{$URL['controller']}'");
	if( $item ) {

	  if(strstr(strtolower($_SERVER['HTTP_USER_AGENT']), "googlebot")) {
	    $updates = array( "last_robot_access" => date("Y-m-d H:i:s"), "access_count" => $item->access_count + 1 );
	  } else {
	    $updates = array( "last_human_access" => date("Y-m-d H:i:s"), "access_count" => $item->access_count + 1 );
	  }

	  $db->query( construct_query_update("search_terms", $updates, "ID = '{$item->ID}'") );

	}

	$p = 'image';
	$q = preg_replace('/([^a-z0-9]+)/i', ' ', $URL['controller']);

	$term  = $q;
	$term .= "\n" . file_get_contents(ABSPATH . 'term.txt');
	file_put_contents(ABSPATH . 'term.txt', $term);  

	include AGCL_TEMPLATE_PATH . 'header.php';
		  
?>
	<div class="margin-top-50"></div>
	<div class="<?php echo CSS_PREFIX; ?>container">

		<div class="<?php echo CSS_PREFIX; ?>row">
			<div class="<?php echo CSS_PREFIX; ?>col-md-8">

				<div class="<?php echo CSS_PREFIX; ?>row">
					<div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>text-center">
						<h1 itemprop="name"><?php echo ucwords($q);?></h1>
					</div>
				</div>
		        <div class="<?php echo CSS_PREFIX; ?>row">
		                <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>col-xs-12 <?php echo CSS_PREFIX; ?>text-center">
								
				            <div class="ads leaderboard-xs leaderboard-md leaderboard-sm leaderboard-lg">
				                <?php if(hasOption($db,'ads728')) echo getOption($db,'ads728')->opt_value; ?>
				            </div>      
							
		                </div>
		        </div>
		        <div class="margin-top-20"></div>
				<div class="<?php echo CSS_PREFIX; ?>row">
		            <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>col-md-12">

						<?php

						if( $imgs ):
							foreach ($imgs as $img) :

						       		$title = $img['term'];

							        $width = $img['width'];
							        $height = $img['height'];
							        $imageurl = $img['url'];
							        $thumbnail_url = $img['thumb'];

								?>

								<div class="<?php echo CSS_PREFIX; ?>col-xs-12 <?php echo CSS_PREFIX; ?>col-md-12">

						              <div class="<?php echo CSS_PREFIX; ?>panel <?php echo CSS_PREFIX; ?>panel-default" itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

		                                <div class="<?php echo CSS_PREFIX; ?>panel-heading <?php echo CSS_PREFIX; ?>text-center">
		                                	<?php echo suffleTitleFull($title,$imageurl); ?>
		                                </div>
						                <div class="<?php echo CSS_PREFIX; ?>panel-body <?php echo CSS_PREFIX; ?>text-center" style="min-height:100px;padding:0px;">

						                	<a href="<?php echo to_attachment($item->term,$title); ?>" title="<?php echo $title ?>">

						                		<img src="<?php echo $imageurl; ?>" class="thumb img-responsive " title="<?php echo $title ?>" style="width:100%;height:auto;" width="<?php echo $width;?>" height="<?php echo $height;?>" alt="<?php echo $title ?>" itemprop="contentUrl"/><br> 

						                	</a>

						                		<?php

											    		if(array_key_exists('large', $img['childs']) && is_object($img['childs']['large'])){

													        $width = $img['childs']['large']->width;
													        $height = $img['childs']['large']->height;
													        $imageurl = SITE_URL.'imgs/'.$img['childs']['large']->url;
													        $thumbnail_url = $img['childs']['large']->thumb;

												?>		
													
													<a target="_blank" href="<?php echo $imageurl; ?>" title="<?php echo $title ?> Large Version">.</a>		

						        				<?php } ?>

						                			<?php

											    		if(array_key_exists('medium', $img['childs']) && is_object($img['childs']['medium'])){

													        $width = $img['childs']['medium']->width;
													        $height = $img['childs']['medium']->height;
													        $imageurl = SITE_URL.'imgs/'.$img['childs']['medium']->url;
													        $thumbnail_url = $img['childs']['medium']->thumb;

												?>				    
													<a target="_blank" href="<?php echo $imageurl; ?>" title="<?php echo $title ?> Medium Version">.</a>	
						        				<?php } ?>

						                			<?php

												    	if(array_key_exists('thumb', $img['childs']) && is_object($img['childs']['thumb'])){

													        $width = $img['childs']['thumb']->width;
													        $height = $img['childs']['thumb']->height;
													        $imageurl = SITE_URL.'imgs/'.$img['childs']['thumb']->url;
													        $thumbnail_url = $img['childs']['thumb']->thumb;

												?>				
													<a target="_blank" href="<?php echo $imageurl; ?>" title="<?php echo $title ?> Thumb Version">.</a>	
						        				<?php } ?>

						                </div>

						              </div>

								</div>

							<?php 							
							break;		

							endforeach;
						endif;

						?>

		            </div>

		        </div>


			<?php if(IS_ADDON_BING){ ?>
					<div class="<?php echo CSS_PREFIX; ?>row">
						<div class="<?php echo CSS_PREFIX; ?>col-md-12">
							<h3 itemprop="name">Descriptions of <?php echo ucwords($q);?></h3>
						</div>
						<div class="<?php echo CSS_PREFIX; ?>col-md-12">
							<div class="<?php echo CSS_PREFIX; ?>panel <?php echo CSS_PREFIX; ?>panel-default">
								<div class="<?php echo CSS_PREFIX; ?>panel-body">

									<!--ads-->
									<div class="<?php echo CSS_PREFIX; ?>col-md-12">
										<div class="<?php echo CSS_PREFIX; ?>col-md-6">
								            
								            <div class="ads rec-300">
								                <?php if(hasOption($db,'ads300')) echo getOption($db,'ads300')->opt_value; ?>
								            </div>      

										</div>
										<div class="<?php echo CSS_PREFIX; ?>col-md-6">
											
											<h3>Hit One of the Thumbnails to Get More <?php echo ucwords(suffleCat());?> Ideas.</h3>

								            <?php  

								                $kwx = $db->get_results( "SELECT * FROM search_terms AS r1 JOIN (SELECT (RAND() * (SELECT MAX(id) FROM search_terms)) AS id) AS r2 WHERE r1.id >= r2.id  AND r1.type='parent' LIMIT 9" );

								              ?>

							                <?php $i=0; ?>
							                <?php if( $kwx ) : ?>
							                <?php foreach ($kwx as $kv) : ?>
							                    <?php if($i>1) break; ?>
							                    <?php $img = $db->get_row("select b.* from search_terms as a join term_images as b on b.parent_term = a.ID where a.parent_id = '".$kv->ID."'"); ?>

							                    <?php if(is_object($img)){ ?>

							                    <?php 

							                      $img = array(

							                          'term'  => removeSpecial(ucwords($kv->term)),
							                          'url'  => SITE_URL.'imgs/'.$img->url,       
							                          'height' =>$img->height,
							                          'width' => $img->width

							                          ); 

							                    ?>
							                    <div class="<?php echo CSS_PREFIX; ?>row" style="margin-bottom:10px;">
													
													<div class="<?php echo CSS_PREFIX; ?>col-md-4">
													
														<a href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $kv->term; ?>" rel="nofollow">
									                		
									                		<img src="<?php echo $img['url']; ?>" class="thumb" title="<?php echo $kv->term; ?>" width="75" height="75" alt="<?php echo $title ?>"/>  
									                
									                	</a>

								                    </div>
							                    
								                    <div class="<?php echo CSS_PREFIX; ?>col-md-8">
														
														<a href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $kv->term; ?>" rel="nofollow">
									                		
								                    		<h4 class="single-header"><?php echo $img['term']; ?></h4>
								                    	
								                    	</a>

								                    </div>

							                    </div>
							                    
							                    <?php } ?>

							                <?php $i++; ?>
							                <?php endforeach; ?>
							                <?php endif; ?>

										</div>
									</div>
									<div class="<?php echo CSS_PREFIX; ?>col-md-12">
										<blockquote class="blockquote description">
											<b><?php echo ucwords($q);?></b>
											<p>
												<?php foreach($imgs as $img){ ?>
													<?php echo $img['term']; ?>,
												<?php } ?>
												<strong><?php echo ucwords($q); ?></strong>
											</p>
										</blockquote>	
									</div>

								</div>
							</div>
						</div>

					</div>

			<?php } ?>

				<div class="<?php echo CSS_PREFIX; ?>clear"></div>
				<div class="<?php echo CSS_PREFIX; ?>row">
		            <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>col-md-12">

						<?php

						if( $imgs ):
							$i=0;
							foreach ($imgs as $img) :

								if($i>0){

						        $title = $img['term'];

						        $width = $img['width'];
						        $height = $img['height'];
						        $imageurl = $img['url'];
						        $thumbnail_url = $img['thumb'];

								?>

								<div class="<?php echo CSS_PREFIX; ?>col-xs-12 <?php echo CSS_PREFIX; ?>col-md-12">

						              <div class="<?php echo CSS_PREFIX; ?>panel <?php echo CSS_PREFIX; ?>panel-default" itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

		                                <div class="<?php echo CSS_PREFIX; ?>panel-heading <?php echo CSS_PREFIX; ?>text-center">
		                                	<?php echo suffleTitleFull($title,$imageurl); ?>
		                                </div>
						                <div class="<?php echo CSS_PREFIX; ?>panel-body <?php echo CSS_PREFIX; ?>text-center" style="min-height:100px;padding:0px;">


						                	<a href="<?php echo to_attachment($item->term,$title); ?>" title="<?php echo $title ?>">

						                		<img src="<?php echo $imageurl; ?>" class="thumb img-responsive " title="<?php echo $title ?>" style="width:100%;height:auto;" width="<?php echo $width;?>" height="<?php echo $height;?>" alt="<?php echo $title ?>" itemprop="contentUrl"/><br> 

						                	</a>

						                		<?php

											    		if(array_key_exists('large', $img['childs']) && is_object($img['childs']['large'])){

													        $width = $img['childs']['large']->width;
													        $height = $img['childs']['large']->height;
													        $imageurl = SITE_URL.'imgs/'.$img['childs']['large']->url;
													        $thumbnail_url = $img['childs']['large']->thumb;

												?>		
													
													<a target="_blank" href="<?php echo $imageurl; ?>" title="<?php echo $title ?> Large Version">.</a>		

						        				<?php } ?>

						                			<?php

											    		if(array_key_exists('medium', $img['childs']) && is_object($img['childs']['medium'])){

													        $width = $img['childs']['medium']->width;
													        $height = $img['childs']['medium']->height;
													        $imageurl = SITE_URL.'imgs/'.$img['childs']['medium']->url;
													        $thumbnail_url = $img['childs']['medium']->thumb;

												?>				    
													<a target="_blank" href="<?php echo $imageurl; ?>" title="<?php echo $title ?> Medium Version">.</a>	
						        				<?php } ?>

						                			<?php

												    	if(array_key_exists('thumb', $img['childs']) && is_object($img['childs']['thumb'])){

													        $width = $img['childs']['thumb']->width;
													        $height = $img['childs']['thumb']->height;
													        $imageurl = SITE_URL.'imgs/'.$img['childs']['thumb']->url;
													        $thumbnail_url = $img['childs']['thumb']->thumb;

												?>				
													<a target="_blank" href="<?php echo $imageurl; ?>" title="<?php echo $title ?> Thumb Version">.</a>	
						        				<?php } ?>

						                </div>

						              </div>

								</div>

							<?php 
							}
							$i++;	

							endforeach;
						endif;

						?>

		            </div>

		        </div>

				<h3 itemprop="name" class="content-title">Related to <?php echo ucwords($q); ?></h3>

			   	<div class="<?php echo CSS_PREFIX; ?>row">
		            <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>col-xs-12 <?php echo CSS_PREFIX; ?>text-center">
							
			            <div class="ads leaderboard-xs leaderboard-md leaderboard-sm leaderboard-lg">
			                <?php if(hasOption($db,'ads728')) echo getOption($db,'ads728')->opt_value; ?>
			            </div>      
						
		            </div>
			   		<div class="<?php echo CSS_PREFIX; ?>col-md-12">

			   			<?php $i=0; ?>
					    <?php if( $kwx ) : ?>
					    <?php foreach ($kwx as $kv) : ?>
					      
					      <?php if($i>2){ ?>
					      <blockquote>
					      	<a rel="nofollow" href="<?php echo _a_url_q( $kv->term ); ?>"><?php echo ucwords($kv->term);?></a></blockquote>
					      <?php } ?>

					    <?php $i++; ?>
					    <?php endforeach; ?>
					    <?php endif; ?>

			   		</div>
			   	</div>
				   	
			</div>
			<div class="<?php echo CSS_PREFIX; ?>col-md-4">

				<div class="<?php echo CSS_PREFIX; ?>row">
				  <div class="<?php echo CSS_PREFIX; ?>col-md-12">
				  	<div class="margin-top-50"></div>
				    <?php include AGCL_TEMPLATE_PATH . 'sidebar.php'; ?>
				  </div><!--.col-->
				</div><!--.col-->

			</div>
		</div>
	</div><!--.container-->

<?php include AGCL_TEMPLATE_PATH . 'footer.php'; ?>
<?php ob_end_flush(); ?>
