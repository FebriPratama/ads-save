<?php 

  ob_start("ob_html_compress");
  $p = 'index';
  include AGCL_TEMPLATE_PATH . 'header.php'; 

?>
<div class="margin-top-50"></div>
<div class="<?php echo CSS_PREFIX; ?>container">
  <div class="<?php echo CSS_PREFIX; ?>row">

    <div class="<?php echo CSS_PREFIX; ?>col-md-8">

      <div class="<?php echo CSS_PREFIX; ?>row">
        <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>text-center">
          <h1 itemprop="name">Find your best inspiration below</h1>        
        </div>
      </div>
      <div class="<?php echo CSS_PREFIX; ?>row">
          <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>col-xs-12">

            <div class="ads leaderboard-xs leaderboard-md leaderboard-sm leaderboard-lg">
                <?php if(hasOption($db,'ads728')) echo getOption($db,'ads728')->opt_value; ?>
            </div>                
        
          </div>
      </div>
      <div class="<?php echo CSS_PREFIX; ?>row">

        <?php 

          if( $kwx ): ?>
          
          <?php foreach ($kwx as $kv) : ?>

            <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>col-xs-12">

              <div class="<?php echo CSS_PREFIX; ?>panel <?php echo CSS_PREFIX; ?>panel-default">
                <div class="<?php echo CSS_PREFIX; ?>panel-heading <?php echo CSS_PREFIX; ?>text-center">
                  <?php echo removeSpecial(ucwords($kv->term));?>
                </div>
                <div class="<?php echo CSS_PREFIX; ?>panel-body">

                  <div class="<?php echo CSS_PREFIX; ?>row">

                    <?php $i=1; ?>
                    <?php $datas = $db->get_results( "SELECT * FROM search_terms where parent_id = ".$kv->ID." AND type='child'" ); ?>
                    <?php if(is_array($datas)){ ?>
                      
                      <?php shuffle($datas); ?>

                      <?php foreach($datas as $d){ ?>

                        <?php
                        
                        if($i==9) break;

                        $cache = new Cache();
                        $key = md5('index_post_'.$d->ID);
                        
                        if($cache->isCached($key)){

                          $img = $cache->retrieve($key);

                        }else{

                          $im = $db->get_row("SELECT * FROM term_images where parent_term='".$d->ID."'");
                          if(!is_object($im)) continue;

                          $childImgs = getChildImages($db,$im);
                          $img = array(

                              'term'  => removeSpecial(ucwords($d->term)),
                              'url'  => SITE_URL.'imgs/'.$im->url,                  
                              'childs' => $childImgs,                  
                              'height' =>$im->height,
                              'width' => $im->width,
                              'thumb' => $im->thumb,
                              'type'    => $im->type

                              ); 

                          $cache->setCache($key)->store($key, $img);

                        }


                        ?>

                        <?php if($i===1){ ?>

                          <div class="<?php echo CSS_PREFIX; ?>col-md-12" style="margin-bottom:20px;">
                            <a href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $img['term']; ?>" rel="bookmark" itemid="<?php echo $img['url']; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                              <img src="<?php echo $img['url']; ?>" class="thumb <?php echo CSS_PREFIX; ?>img-responsive" title="<?php echo $img['term']; ?>" style="width:100%;height:auto;" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" alt="<?php echo $img['term']; ?>" itemprop="contentUrl"/> 

                            </a>
                          </div>

                        <?php }else{ ?>

                          <?php if(array_key_exists('medium', $img['childs']) && is_object($img['childs']['medium'])){ ?>

                            <div class="<?php echo CSS_PREFIX; ?>col-md-3 <?php echo CSS_PREFIX; ?>col-sm-4 <?php echo CSS_PREFIX; ?>col-xs-4">
                              <a href="<?php echo to_attachment($kv->term,$d->term); ?>" title="<?php echo $img['term']; ?> Medium Version" rel="bookmark" itemid="<?php echo $img['childs']['medium']->url; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                                <img src="<?php echo SITE_URL.'imgs/'.$img['childs']['medium']->url; ?>" class="thumb <?php echo CSS_PREFIX; ?>img-responsive" title="<?php echo $img['term']; ?> Thumbnail Version" width="<?php echo $img['childs']['medium']->width;?>" height="<?php echo $img['childs']['medium']->height;?>" alt="<?php echo $img['term']; ?> Medium Version" itemprop="contentUrl"/> 

                              </a>
                            </div>

                          <?php } ?>

                        <?php } ?>
                        <?php $i++; ?>
                      <?php } ?>
                    <?php } ?>

                  </div>

                </div>
              </div>

            </div>
            
          <?php endforeach; ?>
        
        <?php endif; ?>

      </div>

      <div class="<?php echo CSS_PREFIX; ?>row">
        <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>text-center">
          
          <?php $total = $db->get_row( "SELECT count(*) as total FROM  search_terms WHERE type='parent'"); ?>
          <?php $totalPage = ceil($total->total/18); ?> 
          <?php $totalPage = $totalPage < 1 ? 1 : $totalPage ?>
          <?php for($i=1;$i<=$totalPage;$i++){

                if(trim($URL['args'][1]) == '' && $i === 1){

                }else if(trim($URL['args'][1]) !== '' && $URL['args'][1] == $i){

                  echo '.&nbsp;&nbsp;';

                }else{

                  $nofol = $i == 1 ? 'rel="nofollow"' : '';
                  $link = $i == 1 ? SITE_URL : SITE_URL . 'page/'. $i . AGCL_URL_SUFFIX;

                  echo '<a href="'.$link.'" '.$nofol.'>.</a>&nbsp;&nbsp;';

                }                  

          } ?>

        </div>
      </div>

    </div><!--.col-->
    
    <div class="<?php echo CSS_PREFIX; ?>col-md-4">
      <div class="margin-top-50"></div>
      <?php include AGCL_TEMPLATE_PATH . 'sidebar.php'; ?>
    </div>

  </div><!--.row-->
  
</div><!--.container-->

<?php include AGCL_TEMPLATE_PATH . 'footer.php'; ?>
<?php ob_end_flush(); ?>

