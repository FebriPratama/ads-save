<?php 

if( isset($_POST['send_message']) ) {

  $emailaddr_sender = str_replace(array('http://', 'https://', '/'), "", $domain);
  
  if( empty($_POST) ) die( 'POST is empty. Go Back.' );
  $nama  = $_POST['name'];
  $email = $_POST['email'];
  $pesan = $_POST['message'];


  //------- validation finished! let's build our message -------//
  $mailbody  = '<html><body>';
  $mailbody .= '<p><b>Nama:</b> ' . $nama . '</p>';
  $mailbody .= '<p><b>E-Mail:</b> ' . $email . '</p>';
  $mailbody .= '<p><b>Pesan:</b> <br/>' . nl2br($pesan) . '</p>';
  $mailbody .= '</body><html>';

  $to       = $contact_email;
  $subject  = "{$nama} mengirim pesan dari " . SITE_URL;
  $message  = $mailbody;
  $headers  = 'From: '. $blogname .' <'. $contact_email .'>' . "\r\n" .
              'Reply-To: ' . $email . "\r\n" .
              'MIME-Version: 1.0' . "\r\n" .
              'Content-type: text/html; charset=UTF-8' . "\r\n" . 
              'X-Mailer: PHP/' . phpversion();
              
  $mail = NULL;
  
  //------- send message if honeypot is empty -------//
  if( strlen($_POST['username']) < 1 ) {
    $mail = @mail($to, $subject, $message, $headers);
    
    if( ! $mail ) {
      
      $redir = SITE_URL . '/info/contact' . AGCL_URL_SUFFIX . '?m=mailnotsent';
      header("Location: $redir");
      die();

    } else {
      
      $redir = SITE_URL . '/info/contact' . AGCL_URL_SUFFIX . '?m=mailsent';
      header("Location: $redir");
      die();

    }
    
  } 
  
  //------- it's a bot. redirecting as successful -------//
  else {

    $redir = SITE_URL . '/info/contact' . AGCL_URL_SUFFIX . '?m=mailsent';
    header("Location: $redir");
    die();

  }

}

$p          = 'info';
$headtitle  = 'Contact';

include AGCL_TEMPLATE_PATH . 'header.php';

?>

<div class="<?php echo CSS_PREFIX; ?>container">
<div class="<?php echo CSS_PREFIX; ?>row">

  <div class="margin-top-50"></div>
  <div class="<?php echo CSS_PREFIX; ?>col-sm-12">

    <?php if( filter_input(INPUT_GET, 'm') == 'mailnotsent' ): ?>
    <div class="<?php echo CSS_PREFIX; ?>alert <?php echo CSS_PREFIX; ?>alert-warning <?php echo CSS_PREFIX; ?>alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>Mail not Sent</strong> Something goes wrong in the server. Please notify administrator via <code><?php echo $contact_email ?></code>
    </div>
    <?php elseif( filter_input(INPUT_GET, 'm') == 'mailsent' ): ?>
    <div class="<?php echo CSS_PREFIX; ?>alert <?php echo CSS_PREFIX; ?>alert-success <?php echo CSS_PREFIX; ?>alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>Mail Sent</strong> Thank you for your feedback
    </div>
    <?php endif; ?>

    <h1>Contact</h1>

    <blockquote>Getting confused of this website? Need to ask some questions? 
        Or want to report some issues? Glad, you came to the right page. 
        Get in touch with us, using the form on this side. Fill up some basic data 
        like name and email address, pick a subject, and fire-up your message. 
        We’ll get to you A.S.A.P.</blockquote>

    <form role="form" method="post" action="">
      <div class="<?php echo CSS_PREFIX; ?>form-group">
         <label for="name">Name</label>
         <input name="name" class="<?php echo CSS_PREFIX; ?>form-control" id="name" type="text" required="required"/>
      </div>

      <div class="<?php echo CSS_PREFIX; ?>form-group">
         <label for="email">E-mail</label>
         <input name="email" class="<?php echo CSS_PREFIX; ?>form-control" id="email" type="email" required="required"/>
      </div>


      <div class="<?php echo CSS_PREFIX; ?>form-group">
        <label for="message">Message</label>
        <textarea name="message" class="<?php echo CSS_PREFIX; ?>form-control" id="message" rows="5" required="required"></textarea>

        <p class="<?php echo CSS_PREFIX; ?>help-block">
          Write your message here
        </p>
      </div>

      <div class="<?php echo CSS_PREFIX; ?>form-group" style="display: none; opacity: 0; height: 0;">
         <input name="username" type="text"/>
      </div>

      <button type="submit" name="send_message" class="<?php echo CSS_PREFIX; ?>btn <?php echo CSS_PREFIX; ?>btn-action">Submit</button>
    </form>  

  </div><!--.col-->

</div><!--.row-->
<?php include AGCL_TEMPLATE_PATH . 'footer.php'; ?>
</div><!--.container-->


