<?php

header("Content-type: text/css");

echo '
html {
  font-family: sans-serif;
  -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
}
body {
  margin: 0;
}
article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
main,
menu,
nav,
section,
summary {
  display: block;
}
audio,
canvas,
progress,
video {
  display: inline-block;
  vertical-align: baseline;
}
audio:not([controls]) {
  display: none;
  height: 0;
}
[hidden],
template {
  display: none;
}
a {
  background-color: transparent;
}
a:active,
a:hover {
  outline: 0;
}
abbr[title] {
  border-bottom: 1px dotted;
}
b,
strong {
  font-weight: bold;
}
dfn {
  font-style: italic;
}
h1 {
  margin: .67em 0;
  font-size: 2em;
}
mark {
  color: #000;
  background: #ff0;
}
small {
  font-size: 80%;
}
sub,
sup {
  position: relative;
  font-size: 75%;
  line-height: 0;
  vertical-align: baseline;
}
sup {
  top: -.5em;
}
sub {
  bottom: -.25em;
}
img {
  border: 0;
}
svg:not(:root) {
  overflow: hidden;
}
figure {
  margin: 1em 40px;
}
hr {
  height: 0;
  -webkit-box-sizing: content-box;
     -moz-box-sizing: content-box;
          box-sizing: content-box;
}
pre {
  overflow: auto;
}
code,
kbd,
pre,
samp {
  font-family: monospace, monospace;
  font-size: 1em;
}
button,
input,
optgroup,
select,
textarea {
  margin: 0;
  font: inherit;
  color: inherit;
}
button {
  overflow: visible;
}
button,
select {
  text-transform: none;
}
button,
html input[type="button"],
input[type="reset"],
input[type="submit"] {
  -webkit-appearance: button;
  cursor: pointer;
}
button[disabled],
html input[disabled] {
  cursor: default;
}
button::-moz-focus-inner,
input::-moz-focus-inner {
  padding: 0;
  border: 0;
}
input {
  line-height: normal;
}
input[type="checkbox"],
input[type="radio"] {
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 0;
}
input[type="number"]::-webkit-inner-spin-button,
input[type="number"]::-webkit-outer-spin-button {
  height: auto;
}
input[type="search"] {
  -webkit-box-sizing: content-box;
     -moz-box-sizing: content-box;
          box-sizing: content-box;
  -webkit-appearance: textfield;
}
input[type="search"]::-webkit-search-cancel-button,
input[type="search"]::-webkit-search-decoration {
  -webkit-appearance: none;
}
fieldset {
  padding: .35em .625em .75em;
  margin: 0 2px;
  border: 1px solid #c0c0c0;
}
legend {
  padding: 0;
  border: 0;
}
textarea {
  overflow: auto;
}
optgroup {
  font-weight: bold;
}
table {
  border-spacing: 0;
  border-collapse: collapse;
}
td,
th {
  padding: 0;
}
/*! Source: https://github.com/h5bp/html5-boilerplate/blob/master/src/css/main.css */
@media print {
  *,
  *:before,
  *:after {
    color: #000 !important;
    text-shadow: none !important;
    background: transparent !important;
    -webkit-box-shadow: none !important;
            box-shadow: none !important;
  }
  a,
  a:visited {
    text-decoration: underline;
  }
  a[href]:after {
    content: " (" attr(href) ")";
  }
  abbr[title]:after {
    content: " (" attr(title) ")";
  }
  a[href^="#"]:after,
  a[href^="javascript:"]:after {
    content: "";
  }
  pre,
  blockquote {
    border: 1px solid #999;

    page-break-inside: avoid;
  }
  thead {
    display: table-header-group;
  }
  tr,
  img {
    page-break-inside: avoid;
  }
  img {
    max-width: 100% !important;
  }
  p,
  h2,
  h3 {
    orphans: 3;
    widows: 3;
  }
  h2,
  h3 {
    page-break-after: avoid;
  }
  .'.CSS_PREFIX.'navbar {
    display: none;
  }
  .'.CSS_PREFIX.'btn > .'.CSS_PREFIX.'caret,
  .'.CSS_PREFIX.'dropup > .'.CSS_PREFIX.'btn > .'.CSS_PREFIX.'caret {
    border-top-color: #000 !important;
  }
  .'.CSS_PREFIX.'label {
    border: 1px solid #000;
  }
  .'.CSS_PREFIX.'table {
    border-collapse: collapse !important;
  }
  .'.CSS_PREFIX.'table td,
  .'.CSS_PREFIX.'table th {
    background-color: #fff !important;
  }
  .'.CSS_PREFIX.'table-bordered th,
  .'.CSS_PREFIX.'table-bordered td {
    border: 1px solid #ddd !important;
  }
}
@font-face {
  font-family: "Glyphicons Halflings";

  src: url("../fonts/glyphicons-halflings-regular.eot");
  src: url("../fonts/glyphicons-halflings-regular.'.CSS_PREFIX.'eot?#iefix") format("embedded-opentype"), url("../fonts/glyphicons-halflings-regular.'.CSS_PREFIX.'woff2") format("woff2"), url("../fonts/glyphicons-halflings-regular.'.CSS_PREFIX.'woff") format("woff"), url("../fonts/glyphicons-halflings-regular.'.CSS_PREFIX.'ttf") format("truetype"), url("../fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular") format("svg");
}
.'.CSS_PREFIX.'glyphicon {
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: "Glyphicons Halflings";
  font-style: normal;
  font-weight: normal;
  line-height: 1;

  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
.'.CSS_PREFIX.'glyphicon-asterisk:before {
  content: "\002a";
}
.'.CSS_PREFIX.'glyphicon-plus:before {
  content: "\002b";
}
.'.CSS_PREFIX.'glyphicon-euro:before,
.'.CSS_PREFIX.'glyphicon-eur:before {
  content: "\20ac";
}
.'.CSS_PREFIX.'glyphicon-minus:before {
  content: "\2212";
}
.'.CSS_PREFIX.'glyphicon-cloud:before {
  content: "\2601";
}
.'.CSS_PREFIX.'glyphicon-envelope:before {
  content: "\2709";
}
.'.CSS_PREFIX.'glyphicon-pencil:before {
  content: "\270f";
}
.'.CSS_PREFIX.'glyphicon-glass:before {
  content: "\e001";
}
.'.CSS_PREFIX.'glyphicon-music:before {
  content: "\e002";
}
.'.CSS_PREFIX.'glyphicon-search:before {
  content: "\e003";
}
.'.CSS_PREFIX.'glyphicon-heart:before {
  content: "\e005";
}
.'.CSS_PREFIX.'glyphicon-star:before {
  content: "\e006";
}
.'.CSS_PREFIX.'glyphicon-star-empty:before {
  content: "\e007";
}
.'.CSS_PREFIX.'glyphicon-user:before {
  content: "\e008";
}
.'.CSS_PREFIX.'glyphicon-film:before {
  content: "\e009";
}
.'.CSS_PREFIX.'glyphicon-th-large:before {
  content: "\e010";
}
.'.CSS_PREFIX.'glyphicon-th:before {
  content: "\e011";
}
.'.CSS_PREFIX.'glyphicon-th-list:before {
  content: "\e012";
}
.'.CSS_PREFIX.'glyphicon-ok:before {
  content: "\e013";
}
.'.CSS_PREFIX.'glyphicon-remove:before {
  content: "\e014";
}
.'.CSS_PREFIX.'glyphicon-zoom-in:before {
  content: "\e015";
}
.'.CSS_PREFIX.'glyphicon-zoom-out:before {
  content: "\e016";
}
.'.CSS_PREFIX.'glyphicon-off:before {
  content: "\e017";
}
.'.CSS_PREFIX.'glyphicon-signal:before {
  content: "\e018";
}
.'.CSS_PREFIX.'glyphicon-cog:before {
  content: "\e019";
}
.'.CSS_PREFIX.'glyphicon-trash:before {
  content: "\e020";
}
.'.CSS_PREFIX.'glyphicon-home:before {
  content: "\e021";
}
.'.CSS_PREFIX.'glyphicon-file:before {
  content: "\e022";
}
.'.CSS_PREFIX.'glyphicon-time:before {
  content: "\e023";
}
.'.CSS_PREFIX.'glyphicon-road:before {
  content: "\e024";
}
.'.CSS_PREFIX.'glyphicon-download-alt:before {
  content: "\e025";
}
.'.CSS_PREFIX.'glyphicon-download:before {
  content: "\e026";
}
.'.CSS_PREFIX.'glyphicon-upload:before {
  content: "\e027";
}
.'.CSS_PREFIX.'glyphicon-inbox:before {
  content: "\e028";
}
.'.CSS_PREFIX.'glyphicon-play-circle:before {
  content: "\e029";
}
.'.CSS_PREFIX.'glyphicon-repeat:before {
  content: "\e030";
}
.'.CSS_PREFIX.'glyphicon-refresh:before {
  content: "\e031";
}
.'.CSS_PREFIX.'glyphicon-list-alt:before {
  content: "\e032";
}
.'.CSS_PREFIX.'glyphicon-lock:before {
  content: "\e033";
}
.'.CSS_PREFIX.'glyphicon-flag:before {
  content: "\e034";
}
.'.CSS_PREFIX.'glyphicon-headphones:before {
  content: "\e035";
}
.'.CSS_PREFIX.'glyphicon-volume-off:before {
  content: "\e036";
}
.'.CSS_PREFIX.'glyphicon-volume-down:before {
  content: "\e037";
}
.'.CSS_PREFIX.'glyphicon-volume-up:before {
  content: "\e038";
}
.'.CSS_PREFIX.'glyphicon-qrcode:before {
  content: "\e039";
}
.'.CSS_PREFIX.'glyphicon-barcode:before {
  content: "\e040";
}
.'.CSS_PREFIX.'glyphicon-tag:before {
  content: "\e041";
}
.'.CSS_PREFIX.'glyphicon-tags:before {
  content: "\e042";
}
.'.CSS_PREFIX.'glyphicon-book:before {
  content: "\e043";
}
.'.CSS_PREFIX.'glyphicon-bookmark:before {
  content: "\e044";
}
.'.CSS_PREFIX.'glyphicon-print:before {
  content: "\e045";
}
.'.CSS_PREFIX.'glyphicon-camera:before {
  content: "\e046";
}
.'.CSS_PREFIX.'glyphicon-font:before {
  content: "\e047";
}
.'.CSS_PREFIX.'glyphicon-bold:before {
  content: "\e048";
}
.'.CSS_PREFIX.'glyphicon-italic:before {
  content: "\e049";
}
.'.CSS_PREFIX.'glyphicon-text-height:before {
  content: "\e050";
}
.'.CSS_PREFIX.'glyphicon-text-width:before {
  content: "\e051";
}
.'.CSS_PREFIX.'glyphicon-align-left:before {
  content: "\e052";
}
.'.CSS_PREFIX.'glyphicon-align-center:before {
  content: "\e053";
}
.'.CSS_PREFIX.'glyphicon-align-right:before {
  content: "\e054";
}
.'.CSS_PREFIX.'glyphicon-align-justify:before {
  content: "\e055";
}
.'.CSS_PREFIX.'glyphicon-list:before {
  content: "\e056";
}
.'.CSS_PREFIX.'glyphicon-indent-left:before {
  content: "\e057";
}
.'.CSS_PREFIX.'glyphicon-indent-right:before {
  content: "\e058";
}
.'.CSS_PREFIX.'glyphicon-facetime-video:before {
  content: "\e059";
}
.'.CSS_PREFIX.'glyphicon-picture:before {
  content: "\e060";
}
.'.CSS_PREFIX.'glyphicon-map-marker:before {
  content: "\e062";
}
.'.CSS_PREFIX.'glyphicon-adjust:before {
  content: "\e063";
}
.'.CSS_PREFIX.'glyphicon-tint:before {
  content: "\e064";
}
.'.CSS_PREFIX.'glyphicon-edit:before {
  content: "\e065";
}
.'.CSS_PREFIX.'glyphicon-share:before {
  content: "\e066";
}
.'.CSS_PREFIX.'glyphicon-check:before {
  content: "\e067";
}
.'.CSS_PREFIX.'glyphicon-move:before {
  content: "\e068";
}
.'.CSS_PREFIX.'glyphicon-step-backward:before {
  content: "\e069";
}
.'.CSS_PREFIX.'glyphicon-fast-backward:before {
  content: "\e070";
}
.'.CSS_PREFIX.'glyphicon-backward:before {
  content: "\e071";
}
.'.CSS_PREFIX.'glyphicon-play:before {
  content: "\e072";
}
.'.CSS_PREFIX.'glyphicon-pause:before {
  content: "\e073";
}
.'.CSS_PREFIX.'glyphicon-stop:before {
  content: "\e074";
}
.'.CSS_PREFIX.'glyphicon-forward:before {
  content: "\e075";
}
.'.CSS_PREFIX.'glyphicon-fast-forward:before {
  content: "\e076";
}
.'.CSS_PREFIX.'glyphicon-step-forward:before {
  content: "\e077";
}
.'.CSS_PREFIX.'glyphicon-eject:before {
  content: "\e078";
}
.'.CSS_PREFIX.'glyphicon-chevron-left:before {
  content: "\e079";
}
.'.CSS_PREFIX.'glyphicon-chevron-right:before {
  content: "\e080";
}
.'.CSS_PREFIX.'glyphicon-plus-sign:before {
  content: "\e081";
}
.'.CSS_PREFIX.'glyphicon-minus-sign:before {
  content: "\e082";
}
.'.CSS_PREFIX.'glyphicon-remove-sign:before {
  content: "\e083";
}
.'.CSS_PREFIX.'glyphicon-ok-sign:before {
  content: "\e084";
}
.'.CSS_PREFIX.'glyphicon-question-sign:before {
  content: "\e085";
}
.'.CSS_PREFIX.'glyphicon-info-sign:before {
  content: "\e086";
}
.'.CSS_PREFIX.'glyphicon-screenshot:before {
  content: "\e087";
}
.'.CSS_PREFIX.'glyphicon-remove-circle:before {
  content: "\e088";
}
.'.CSS_PREFIX.'glyphicon-ok-circle:before {
  content: "\e089";
}
.'.CSS_PREFIX.'glyphicon-ban-circle:before {
  content: "\e090";
}
.'.CSS_PREFIX.'glyphicon-arrow-left:before {
  content: "\e091";
}
.'.CSS_PREFIX.'glyphicon-arrow-right:before {
  content: "\e092";
}
.'.CSS_PREFIX.'glyphicon-arrow-up:before {
  content: "\e093";
}
.'.CSS_PREFIX.'glyphicon-arrow-down:before {
  content: "\e094";
}
.'.CSS_PREFIX.'glyphicon-share-alt:before {
  content: "\e095";
}
.'.CSS_PREFIX.'glyphicon-resize-full:before {
  content: "\e096";
}
.'.CSS_PREFIX.'glyphicon-resize-small:before {
  content: "\e097";
}
.'.CSS_PREFIX.'glyphicon-exclamation-sign:before {
  content: "\e101";
}
.'.CSS_PREFIX.'glyphicon-gift:before {
  content: "\e102";
}
.'.CSS_PREFIX.'glyphicon-leaf:before {
  content: "\e103";
}
.'.CSS_PREFIX.'glyphicon-fire:before {
  content: "\e104";
}
.'.CSS_PREFIX.'glyphicon-eye-open:before {
  content: "\e105";
}
.'.CSS_PREFIX.'glyphicon-eye-close:before {
  content: "\e106";
}
.'.CSS_PREFIX.'glyphicon-warning-sign:before {
  content: "\e107";
}
.'.CSS_PREFIX.'glyphicon-plane:before {
  content: "\e108";
}
.'.CSS_PREFIX.'glyphicon-calendar:before {
  content: "\e109";
}
.'.CSS_PREFIX.'glyphicon-random:before {
  content: "\e110";
}
.'.CSS_PREFIX.'glyphicon-comment:before {
  content: "\e111";
}
.'.CSS_PREFIX.'glyphicon-magnet:before {
  content: "\e112";
}
.'.CSS_PREFIX.'glyphicon-chevron-up:before {
  content: "\e113";
}
.'.CSS_PREFIX.'glyphicon-chevron-down:before {
  content: "\e114";
}
.'.CSS_PREFIX.'glyphicon-retweet:before {
  content: "\e115";
}
.'.CSS_PREFIX.'glyphicon-shopping-cart:before {
  content: "\e116";
}
.'.CSS_PREFIX.'glyphicon-folder-close:before {
  content: "\e117";
}
.'.CSS_PREFIX.'glyphicon-folder-open:before {
  content: "\e118";
}
.'.CSS_PREFIX.'glyphicon-resize-vertical:before {
  content: "\e119";
}
.'.CSS_PREFIX.'glyphicon-resize-horizontal:before {
  content: "\e120";
}
.'.CSS_PREFIX.'glyphicon-hdd:before {
  content: "\e121";
}
.'.CSS_PREFIX.'glyphicon-bullhorn:before {
  content: "\e122";
}
.'.CSS_PREFIX.'glyphicon-bell:before {
  content: "\e123";
}
.'.CSS_PREFIX.'glyphicon-certificate:before {
  content: "\e124";
}
.'.CSS_PREFIX.'glyphicon-thumbs-up:before {
  content: "\e125";
}
.'.CSS_PREFIX.'glyphicon-thumbs-down:before {
  content: "\e126";
}
.'.CSS_PREFIX.'glyphicon-hand-right:before {
  content: "\e127";
}
.'.CSS_PREFIX.'glyphicon-hand-left:before {
  content: "\e128";
}
.'.CSS_PREFIX.'glyphicon-hand-up:before {
  content: "\e129";
}
.'.CSS_PREFIX.'glyphicon-hand-down:before {
  content: "\e130";
}
.'.CSS_PREFIX.'glyphicon-circle-arrow-right:before {
  content: "\e131";
}
.'.CSS_PREFIX.'glyphicon-circle-arrow-left:before {
  content: "\e132";
}
.'.CSS_PREFIX.'glyphicon-circle-arrow-up:before {
  content: "\e133";
}
.'.CSS_PREFIX.'glyphicon-circle-arrow-down:before {
  content: "\e134";
}
.'.CSS_PREFIX.'glyphicon-globe:before {
  content: "\e135";
}
.'.CSS_PREFIX.'glyphicon-wrench:before {
  content: "\e136";
}
.'.CSS_PREFIX.'glyphicon-tasks:before {
  content: "\e137";
}
.'.CSS_PREFIX.'glyphicon-filter:before {
  content: "\e138";
}
.'.CSS_PREFIX.'glyphicon-briefcase:before {
  content: "\e139";
}
.'.CSS_PREFIX.'glyphicon-fullscreen:before {
  content: "\e140";
}
.'.CSS_PREFIX.'glyphicon-dashboard:before {
  content: "\e141";
}
.'.CSS_PREFIX.'glyphicon-paperclip:before {
  content: "\e142";
}
.'.CSS_PREFIX.'glyphicon-heart-empty:before {
  content: "\e143";
}
.'.CSS_PREFIX.'glyphicon-link:before {
  content: "\e144";
}
.'.CSS_PREFIX.'glyphicon-phone:before {
  content: "\e145";
}
.'.CSS_PREFIX.'glyphicon-pushpin:before {
  content: "\e146";
}
.'.CSS_PREFIX.'glyphicon-usd:before {
  content: "\e148";
}
.'.CSS_PREFIX.'glyphicon-gbp:before {
  content: "\e149";
}
.'.CSS_PREFIX.'glyphicon-sort:before {
  content: "\e150";
}
.'.CSS_PREFIX.'glyphicon-sort-by-alphabet:before {
  content: "\e151";
}
.'.CSS_PREFIX.'glyphicon-sort-by-alphabet-alt:before {
  content: "\e152";
}
.'.CSS_PREFIX.'glyphicon-sort-by-order:before {
  content: "\e153";
}
.'.CSS_PREFIX.'glyphicon-sort-by-order-alt:before {
  content: "\e154";
}
.'.CSS_PREFIX.'glyphicon-sort-by-attributes:before {
  content: "\e155";
}
.'.CSS_PREFIX.'glyphicon-sort-by-attributes-alt:before {
  content: "\e156";
}
.'.CSS_PREFIX.'glyphicon-unchecked:before {
  content: "\e157";
}
.'.CSS_PREFIX.'glyphicon-expand:before {
  content: "\e158";
}
.'.CSS_PREFIX.'glyphicon-collapse-down:before {
  content: "\e159";
}
.'.CSS_PREFIX.'glyphicon-collapse-up:before {
  content: "\e160";
}
.'.CSS_PREFIX.'glyphicon-log-in:before {
  content: "\e161";
}
.'.CSS_PREFIX.'glyphicon-flash:before {
  content: "\e162";
}
.'.CSS_PREFIX.'glyphicon-log-out:before {
  content: "\e163";
}
.'.CSS_PREFIX.'glyphicon-new-window:before {
  content: "\e164";
}
.'.CSS_PREFIX.'glyphicon-record:before {
  content: "\e165";
}
.'.CSS_PREFIX.'glyphicon-save:before {
  content: "\e166";
}
.'.CSS_PREFIX.'glyphicon-open:before {
  content: "\e167";
}
.'.CSS_PREFIX.'glyphicon-saved:before {
  content: "\e168";
}
.'.CSS_PREFIX.'glyphicon-import:before {
  content: "\e169";
}
.'.CSS_PREFIX.'glyphicon-export:before {
  content: "\e170";
}
.'.CSS_PREFIX.'glyphicon-send:before {
  content: "\e171";
}
.'.CSS_PREFIX.'glyphicon-floppy-disk:before {
  content: "\e172";
}
.'.CSS_PREFIX.'glyphicon-floppy-saved:before {
  content: "\e173";
}
.'.CSS_PREFIX.'glyphicon-floppy-remove:before {
  content: "\e174";
}
.'.CSS_PREFIX.'glyphicon-floppy-save:before {
  content: "\e175";
}
.'.CSS_PREFIX.'glyphicon-floppy-open:before {
  content: "\e176";
}
.'.CSS_PREFIX.'glyphicon-credit-card:before {
  content: "\e177";
}
.'.CSS_PREFIX.'glyphicon-transfer:before {
  content: "\e178";
}
.'.CSS_PREFIX.'glyphicon-cutlery:before {
  content: "\e179";
}
.'.CSS_PREFIX.'glyphicon-header:before {
  content: "\e180";
}
.'.CSS_PREFIX.'glyphicon-compressed:before {
  content: "\e181";
}
.'.CSS_PREFIX.'glyphicon-earphone:before {
  content: "\e182";
}
.'.CSS_PREFIX.'glyphicon-phone-alt:before {
  content: "\e183";
}
.'.CSS_PREFIX.'glyphicon-tower:before {
  content: "\e184";
}
.'.CSS_PREFIX.'glyphicon-stats:before {
  content: "\e185";
}
.'.CSS_PREFIX.'glyphicon-sd-video:before {
  content: "\e186";
}
.'.CSS_PREFIX.'glyphicon-hd-video:before {
  content: "\e187";
}
.'.CSS_PREFIX.'glyphicon-subtitles:before {
  content: "\e188";
}
.'.CSS_PREFIX.'glyphicon-sound-stereo:before {
  content: "\e189";
}
.'.CSS_PREFIX.'glyphicon-sound-dolby:before {
  content: "\e190";
}
.'.CSS_PREFIX.'glyphicon-sound-5-1:before {
  content: "\e191";
}
.'.CSS_PREFIX.'glyphicon-sound-6-1:before {
  content: "\e192";
}
.'.CSS_PREFIX.'glyphicon-sound-7-1:before {
  content: "\e193";
}
.'.CSS_PREFIX.'glyphicon-copyright-mark:before {
  content: "\e194";
}
.'.CSS_PREFIX.'glyphicon-registration-mark:before {
  content: "\e195";
}
.'.CSS_PREFIX.'glyphicon-cloud-download:before {
  content: "\e197";
}
.'.CSS_PREFIX.'glyphicon-cloud-upload:before {
  content: "\e198";
}
.'.CSS_PREFIX.'glyphicon-tree-conifer:before {
  content: "\e199";
}
.'.CSS_PREFIX.'glyphicon-tree-deciduous:before {
  content: "\e200";
}
.'.CSS_PREFIX.'glyphicon-cd:before {
  content: "\e201";
}
.'.CSS_PREFIX.'glyphicon-save-file:before {
  content: "\e202";
}
.'.CSS_PREFIX.'glyphicon-open-file:before {
  content: "\e203";
}
.'.CSS_PREFIX.'glyphicon-level-up:before {
  content: "\e204";
}
.'.CSS_PREFIX.'glyphicon-copy:before {
  content: "\e205";
}
.'.CSS_PREFIX.'glyphicon-paste:before {
  content: "\e206";
}
.'.CSS_PREFIX.'glyphicon-alert:before {
  content: "\e209";
}
.'.CSS_PREFIX.'glyphicon-equalizer:before {
  content: "\e210";
}
.'.CSS_PREFIX.'glyphicon-king:before {
  content: "\e211";
}
.'.CSS_PREFIX.'glyphicon-queen:before {
  content: "\e212";
}
.'.CSS_PREFIX.'glyphicon-pawn:before {
  content: "\e213";
}
.'.CSS_PREFIX.'glyphicon-bishop:before {
  content: "\e214";
}
.'.CSS_PREFIX.'glyphicon-knight:before {
  content: "\e215";
}
.'.CSS_PREFIX.'glyphicon-baby-formula:before {
  content: "\e216";
}
.'.CSS_PREFIX.'glyphicon-tent:before {
  content: "\26fa";
}
.'.CSS_PREFIX.'glyphicon-blackboard:before {
  content: "\e218";
}
.'.CSS_PREFIX.'glyphicon-bed:before {
  content: "\e219";
}
.'.CSS_PREFIX.'glyphicon-apple:before {
  content: "\f8ff";
}
.'.CSS_PREFIX.'glyphicon-erase:before {
  content: "\e221";
}
.'.CSS_PREFIX.'glyphicon-hourglass:before {
  content: "\231b";
}
.'.CSS_PREFIX.'glyphicon-lamp:before {
  content: "\e223";
}
.'.CSS_PREFIX.'glyphicon-duplicate:before {
  content: "\e224";
}
.'.CSS_PREFIX.'glyphicon-piggy-bank:before {
  content: "\e225";
}
.'.CSS_PREFIX.'glyphicon-scissors:before {
  content: "\e226";
}
.'.CSS_PREFIX.'glyphicon-bitcoin:before {
  content: "\e227";
}
.'.CSS_PREFIX.'glyphicon-btc:before {
  content: "\e227";
}
.'.CSS_PREFIX.'glyphicon-xbt:before {
  content: "\e227";
}
.'.CSS_PREFIX.'glyphicon-yen:before {
  content: "\00a5";
}
.'.CSS_PREFIX.'glyphicon-jpy:before {
  content: "\00a5";
}
.'.CSS_PREFIX.'glyphicon-ruble:before {
  content: "\20bd";
}
.'.CSS_PREFIX.'glyphicon-rub:before {
  content: "\20bd";
}
.'.CSS_PREFIX.'glyphicon-scale:before {
  content: "\e230";
}
.'.CSS_PREFIX.'glyphicon-ice-lolly:before {
  content: "\e231";
}
.'.CSS_PREFIX.'glyphicon-ice-lolly-tasted:before {
  content: "\e232";
}
.'.CSS_PREFIX.'glyphicon-education:before {
  content: "\e233";
}
.'.CSS_PREFIX.'glyphicon-option-horizontal:before {
  content: "\e234";
}
.'.CSS_PREFIX.'glyphicon-option-vertical:before {
  content: "\e235";
}
.'.CSS_PREFIX.'glyphicon-menu-hamburger:before {
  content: "\e236";
}
.'.CSS_PREFIX.'glyphicon-modal-window:before {
  content: "\e237";
}
.'.CSS_PREFIX.'glyphicon-oil:before {
  content: "\e238";
}
.'.CSS_PREFIX.'glyphicon-grain:before {
  content: "\e239";
}
.'.CSS_PREFIX.'glyphicon-sunglasses:before {
  content: "\e240";
}
.'.CSS_PREFIX.'glyphicon-text-size:before {
  content: "\e241";
}
.'.CSS_PREFIX.'glyphicon-text-color:before {
  content: "\e242";
}
.'.CSS_PREFIX.'glyphicon-text-background:before {
  content: "\e243";
}
.'.CSS_PREFIX.'glyphicon-object-align-top:before {
  content: "\e244";
}
.'.CSS_PREFIX.'glyphicon-object-align-bottom:before {
  content: "\e245";
}
.'.CSS_PREFIX.'glyphicon-object-align-horizontal:before {
  content: "\e246";
}
.'.CSS_PREFIX.'glyphicon-object-align-left:before {
  content: "\e247";
}
.'.CSS_PREFIX.'glyphicon-object-align-vertical:before {
  content: "\e248";
}
.'.CSS_PREFIX.'glyphicon-object-align-right:before {
  content: "\e249";
}
.'.CSS_PREFIX.'glyphicon-triangle-right:before {
  content: "\e250";
}
.'.CSS_PREFIX.'glyphicon-triangle-left:before {
  content: "\e251";
}
.'.CSS_PREFIX.'glyphicon-triangle-bottom:before {
  content: "\e252";
}
.'.CSS_PREFIX.'glyphicon-triangle-top:before {
  content: "\e253";
}
.'.CSS_PREFIX.'glyphicon-console:before {
  content: "\e254";
}
.'.CSS_PREFIX.'glyphicon-superscript:before {
  content: "\e255";
}
.'.CSS_PREFIX.'glyphicon-subscript:before {
  content: "\e256";
}
.'.CSS_PREFIX.'glyphicon-menu-left:before {
  content: "\e257";
}
.'.CSS_PREFIX.'glyphicon-menu-right:before {
  content: "\e258";
}
.'.CSS_PREFIX.'glyphicon-menu-down:before {
  content: "\e259";
}
.'.CSS_PREFIX.'glyphicon-menu-up:before {
  content: "\e260";
}
* {
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
}
*:before,
*:after {
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
}
html {
  font-size: 10px;

  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}
body {
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 14px;
  line-height: 1.42857143;
  color: #333;
  background-color: #fff;
}
input,
button,
select,
textarea {
  font-family: inherit;
  font-size: inherit;
  line-height: inherit;
}
a {
  color: #337ab7;
  text-decoration: none;
}
a:hover,
a:focus {
  color: #23527c;
  text-decoration: underline;
}
a:focus {
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px;
}
figure {
  margin: 0;
}
img {
  vertical-align: middle;
}
.'.CSS_PREFIX.'img-responsive,
.'.CSS_PREFIX.'thumbnail > img,
.'.CSS_PREFIX.'thumbnail a > img,
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'item > img,
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'item > a > img {
  display: block;
  max-width: 100%;
  height: auto;
}
.'.CSS_PREFIX.'img-rounded {
  border-radius: 6px;
}
.'.CSS_PREFIX.'img-thumbnail {
  display: inline-block;
  max-width: 100%;
  height: auto;
  padding: 4px;
  line-height: 1.42857143;
  background-color: #fff;
  border: 1px solid #ddd;
  border-radius: 4px;
  -webkit-transition: all .2s ease-in-out;
       -o-transition: all .2s ease-in-out;
          transition: all .2s ease-in-out;
}
.'.CSS_PREFIX.'img-circle {
  border-radius: 50%;
}
hr {
  margin-top: 20px;
  margin-bottom: 20px;
  border: 0;
  border-top: 1px solid #eee;
}
.'.CSS_PREFIX.'sr-only {
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip: rect(0, 0, 0, 0);
  border: 0;
}
.'.CSS_PREFIX.'sr-only-focusable:active,
.'.CSS_PREFIX.'sr-only-focusable:focus {
  position: static;
  width: auto;
  height: auto;
  margin: 0;
  overflow: visible;
  clip: auto;
}
[role="button"] {
  cursor: pointer;
}
h1,
h2,
h3,
h4,
h5,
h6,
.'.CSS_PREFIX.'h1,
.'.CSS_PREFIX.'h2,
.'.CSS_PREFIX.'h3,
.'.CSS_PREFIX.'h4,
.'.CSS_PREFIX.'h5,
.'.CSS_PREFIX.'h6 {
  font-family: inherit;
  font-weight: 500;
  line-height: 1.1;
  color: inherit;
}
h1 small,
h2 small,
h3 small,
h4 small,
h5 small,
h6 small,
.'.CSS_PREFIX.'h1 small,
.'.CSS_PREFIX.'h2 small,
.'.CSS_PREFIX.'h3 small,
.'.CSS_PREFIX.'h4 small,
.'.CSS_PREFIX.'h5 small,
.'.CSS_PREFIX.'h6 small,
h1 .'.CSS_PREFIX.'small,
h2 .'.CSS_PREFIX.'small,
h3 .'.CSS_PREFIX.'small,
h4 .'.CSS_PREFIX.'small,
h5 .'.CSS_PREFIX.'small,
h6 .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'h1 .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'h2 .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'h3 .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'h4 .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'h5 .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'h6 .'.CSS_PREFIX.'small {
  font-weight: normal;
  line-height: 1;
  color: #777;
}
h1,
.'.CSS_PREFIX.'h1,
h2,
.'.CSS_PREFIX.'h2,
h3,
.'.CSS_PREFIX.'h3 {
  margin-top: 20px;
  margin-bottom: 10px;
}
h1 small,
.'.CSS_PREFIX.'h1 small,
h2 small,
.'.CSS_PREFIX.'h2 small,
h3 small,
.'.CSS_PREFIX.'h3 small,
h1 .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'h1 .'.CSS_PREFIX.'small,
h2 .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'h2 .'.CSS_PREFIX.'small,
h3 .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'h3 .'.CSS_PREFIX.'small {
  font-size: 65%;
}
h4,
.'.CSS_PREFIX.'h4,
h5,
.'.CSS_PREFIX.'h5,
h6,
.'.CSS_PREFIX.'h6 {
  margin-top: 10px;
  margin-bottom: 10px;
}
h4 small,
.'.CSS_PREFIX.'h4 small,
h5 small,
.'.CSS_PREFIX.'h5 small,
h6 small,
.'.CSS_PREFIX.'h6 small,
h4 .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'h4 .'.CSS_PREFIX.'small,
h5 .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'h5 .'.CSS_PREFIX.'small,
h6 .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'h6 .'.CSS_PREFIX.'small {
  font-size: 75%;
}
h1,
.'.CSS_PREFIX.'h1 {
  font-size: 36px;
}
h2,
.'.CSS_PREFIX.'h2 {
  font-size: 30px;
}
h3,
.'.CSS_PREFIX.'h3 {
  font-size: 24px;
}
h4,
.'.CSS_PREFIX.'h4 {
  font-size: 18px;
}
h5,
.'.CSS_PREFIX.'h5 {
  font-size: 14px;
}
h6,
.'.CSS_PREFIX.'h6 {
  font-size: 12px;
}
p {
  margin: 0 0 10px;
}
.'.CSS_PREFIX.'lead {
  margin-bottom: 20px;
  font-size: 16px;
  font-weight: 300;
  line-height: 1.4;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'lead {
    font-size: 21px;
  }
}
small,
.'.CSS_PREFIX.'small {
  font-size: 85%;
}
mark,
.'.CSS_PREFIX.'mark {
  padding: .2em;
  background-color: #fcf8e3;
}
.'.CSS_PREFIX.'text-left {
  text-align: left;
}
.'.CSS_PREFIX.'text-right {
  text-align: right;
}
.'.CSS_PREFIX.'text-center {
  text-align: center;
}
.'.CSS_PREFIX.'text-justify {
  text-align: justify;
}
.'.CSS_PREFIX.'text-nowrap {
  white-space: nowrap;
}
.'.CSS_PREFIX.'text-lowercase {
  text-transform: lowercase;
}
.'.CSS_PREFIX.'text-uppercase {
  text-transform: uppercase;
}
.'.CSS_PREFIX.'text-capitalize {
  text-transform: capitalize;
}
.'.CSS_PREFIX.'text-muted {
  color: #777;
}
.'.CSS_PREFIX.'text-primary {
  color: #337ab7;
}
a.'.CSS_PREFIX.'text-primary:hover,
a.'.CSS_PREFIX.'text-primary:focus {
  color: #286090;
}
.'.CSS_PREFIX.'text-success {
  color: #3c763d;
}
a.'.CSS_PREFIX.'text-success:hover,
a.'.CSS_PREFIX.'text-success:focus {
  color: #2b542c;
}
.'.CSS_PREFIX.'text-info {
  color: #31708f;
}
a.'.CSS_PREFIX.'text-info:hover,
a.'.CSS_PREFIX.'text-info:focus {
  color: #245269;
}
.'.CSS_PREFIX.'text-warning {
  color: #8a6d3b;
}
a.'.CSS_PREFIX.'text-warning:hover,
a.'.CSS_PREFIX.'text-warning:focus {
  color: #66512c;
}
.'.CSS_PREFIX.'text-danger {
  color: #a94442;
}
a.'.CSS_PREFIX.'text-danger:hover,
a.'.CSS_PREFIX.'text-danger:focus {
  color: #843534;
}
.'.CSS_PREFIX.'bg-primary {
  color: #fff;
  background-color: #337ab7;
}
a.'.CSS_PREFIX.'bg-primary:hover,
a.'.CSS_PREFIX.'bg-primary:focus {
  background-color: #286090;
}
.'.CSS_PREFIX.'bg-success {
  background-color: #dff0d8;
}
a.'.CSS_PREFIX.'bg-success:hover,
a.'.CSS_PREFIX.'bg-success:focus {
  background-color: #c1e2b3;
}
.'.CSS_PREFIX.'bg-info {
  background-color: #d9edf7;
}
a.'.CSS_PREFIX.'bg-info:hover,
a.'.CSS_PREFIX.'bg-info:focus {
  background-color: #afd9ee;
}
.'.CSS_PREFIX.'bg-warning {
  background-color: #fcf8e3;
}
a.'.CSS_PREFIX.'bg-warning:hover,
a.'.CSS_PREFIX.'bg-warning:focus {
  background-color: #f7ecb5;
}
.'.CSS_PREFIX.'bg-danger {
  background-color: #f2dede;
}
a.'.CSS_PREFIX.'bg-danger:hover,
a.'.CSS_PREFIX.'bg-danger:focus {
  background-color: #e4b9b9;
}
.'.CSS_PREFIX.'page-header {
  padding-bottom: 9px;
  margin: 40px 0 20px;
  border-bottom: 1px solid #eee;
}
ul,
ol {
  margin-top: 0;
  margin-bottom: 10px;
}
ul ul,
ol ul,
ul ol,
ol ol {
  margin-bottom: 0;
}
.'.CSS_PREFIX.'list-unstyled {
  padding-left: 0;
  list-style: none;
}
.'.CSS_PREFIX.'list-inline {
  padding-left: 0;
  margin-left: -5px;
  list-style: none;
}
.'.CSS_PREFIX.'list-inline > li {
  display: inline-block;
  padding-right: 5px;
  padding-left: 5px;
}
dl {
  margin-top: 0;
  margin-bottom: 20px;
}
dt,
dd {
  line-height: 1.42857143;
}
dt {
  font-weight: bold;
}
dd {
  margin-left: 0;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'dl-horizontal dt {
    float: left;
    width: 160px;
    overflow: hidden;
    clear: left;
    text-align: right;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
  .'.CSS_PREFIX.'dl-horizontal dd {
    margin-left: 180px;
  }
}
abbr[title],
abbr[data-original-title] {
  cursor: help;
  border-bottom: 1px dotted #777;
}
.'.CSS_PREFIX.'initialism {
  font-size: 90%;
  text-transform: uppercase;
}
blockquote {
  padding: 10px 20px;
  margin: 0 0 20px;
  font-size: 17.5px;
  border-left: 5px solid #eee;
}
blockquote p:last-child,
blockquote ul:last-child,
blockquote ol:last-child {
  margin-bottom: 0;
}
blockquote footer,
blockquote small,
blockquote .'.CSS_PREFIX.'small {
  display: block;
  font-size: 80%;
  line-height: 1.42857143;
  color: #777;
}
blockquote footer:before,
blockquote small:before,
blockquote .'.CSS_PREFIX.'small:before {
  content: "\2014 \00A0";
}
.'.CSS_PREFIX.'blockquote-reverse,
blockquote.'.CSS_PREFIX.'pull-right {
  padding-right: 15px;
  padding-left: 0;
  text-align: right;
  border-right: 5px solid #eee;
  border-left: 0;
}
.'.CSS_PREFIX.'blockquote-reverse footer:before,
blockquote.'.CSS_PREFIX.'pull-right footer:before,
.'.CSS_PREFIX.'blockquote-reverse small:before,
blockquote.'.CSS_PREFIX.'pull-right small:before,
.'.CSS_PREFIX.'blockquote-reverse .'.CSS_PREFIX.'small:before,
blockquote.'.CSS_PREFIX.'pull-right .'.CSS_PREFIX.'small:before {
  content: "";
}
.'.CSS_PREFIX.'blockquote-reverse footer:after,
blockquote.'.CSS_PREFIX.'pull-right footer:after,
.'.CSS_PREFIX.'blockquote-reverse small:after,
blockquote.'.CSS_PREFIX.'pull-right small:after,
.'.CSS_PREFIX.'blockquote-reverse .'.CSS_PREFIX.'small:after,
blockquote.'.CSS_PREFIX.'pull-right .'.CSS_PREFIX.'small:after {
  content: "\00A0 \2014";
}
address {
  margin-bottom: 20px;
  font-style: normal;
  line-height: 1.42857143;
}
code,
kbd,
pre,
samp {
  font-family: Menlo, Monaco, Consolas, "Courier New", monospace;
}
code {
  padding: 2px 4px;
  font-size: 90%;
  color: #c7254e;
  background-color: #f9f2f4;
  border-radius: 4px;
}
kbd {
  padding: 2px 4px;
  font-size: 90%;
  color: #fff;
  background-color: #333;
  border-radius: 3px;
  -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .25);
          box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .25);
}
kbd kbd {
  padding: 0;
  font-size: 100%;
  font-weight: bold;
  -webkit-box-shadow: none;
          box-shadow: none;
}
pre {
  display: block;
  padding: 9.5px;
  margin: 0 0 10px;
  font-size: 13px;
  line-height: 1.42857143;
  color: #333;
  word-break: break-all;
  word-wrap: break-word;
  background-color: #f5f5f5;
  border: 1px solid #ccc;
  border-radius: 4px;
}
pre code {
  padding: 0;
  font-size: inherit;
  color: inherit;
  white-space: pre-wrap;
  background-color: transparent;
  border-radius: 0;
}
.'.CSS_PREFIX.'pre-scrollable {
  max-height: 340px;
  overflow-y: scroll;
}
.'.CSS_PREFIX.'container {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'container {
    width: 750px;
  }
}
@media (min-width: 992px) {
  .'.CSS_PREFIX.'container {
    width: 970px;
  }
}
@media (min-width: 1200px) {
  .'.CSS_PREFIX.'container {
    width: 1170px;
  }
}
.'.CSS_PREFIX.'container-fluid {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}
.'.CSS_PREFIX.'row {
  margin-right: -15px;
  margin-left: -15px;
}
.'.CSS_PREFIX.'col-xs-1, .'.CSS_PREFIX.'col-sm-1, .'.CSS_PREFIX.'col-md-1, .'.CSS_PREFIX.'col-lg-1, .'.CSS_PREFIX.'col-xs-2, .'.CSS_PREFIX.'col-sm-2, .'.CSS_PREFIX.'col-md-2, .'.CSS_PREFIX.'col-lg-2, .'.CSS_PREFIX.'col-xs-3, .'.CSS_PREFIX.'col-sm-3, .'.CSS_PREFIX.'col-md-3, .'.CSS_PREFIX.'col-lg-3, .'.CSS_PREFIX.'col-xs-4, .'.CSS_PREFIX.'col-sm-4, .'.CSS_PREFIX.'col-md-4, .'.CSS_PREFIX.'col-lg-4, .'.CSS_PREFIX.'col-xs-5, .'.CSS_PREFIX.'col-sm-5, .'.CSS_PREFIX.'col-md-5, .'.CSS_PREFIX.'col-lg-5, .'.CSS_PREFIX.'col-xs-6, .'.CSS_PREFIX.'col-sm-6, .'.CSS_PREFIX.'col-md-6, .'.CSS_PREFIX.'col-lg-6, .'.CSS_PREFIX.'col-xs-7, .'.CSS_PREFIX.'col-sm-7, .'.CSS_PREFIX.'col-md-7, .'.CSS_PREFIX.'col-lg-7, .'.CSS_PREFIX.'col-xs-8, .'.CSS_PREFIX.'col-sm-8, .'.CSS_PREFIX.'col-md-8, .'.CSS_PREFIX.'col-lg-8, .'.CSS_PREFIX.'col-xs-9, .'.CSS_PREFIX.'col-sm-9, .'.CSS_PREFIX.'col-md-9, .'.CSS_PREFIX.'col-lg-9, .'.CSS_PREFIX.'col-xs-10, .'.CSS_PREFIX.'col-sm-10, .'.CSS_PREFIX.'col-md-10, .'.CSS_PREFIX.'col-lg-10, .'.CSS_PREFIX.'col-xs-11, .'.CSS_PREFIX.'col-sm-11, .'.CSS_PREFIX.'col-md-11, .'.CSS_PREFIX.'col-lg-11, .'.CSS_PREFIX.'col-xs-12, .'.CSS_PREFIX.'col-sm-12, .'.CSS_PREFIX.'col-md-12, .'.CSS_PREFIX.'col-lg-12 {
  position: relative;
  min-height: 1px;
  padding-right: 15px;
  padding-left: 15px;
}
.'.CSS_PREFIX.'col-xs-1, .'.CSS_PREFIX.'col-xs-2, .'.CSS_PREFIX.'col-xs-3, .'.CSS_PREFIX.'col-xs-4, .'.CSS_PREFIX.'col-xs-5, .'.CSS_PREFIX.'col-xs-6, .'.CSS_PREFIX.'col-xs-7, .'.CSS_PREFIX.'col-xs-8, .'.CSS_PREFIX.'col-xs-9, .'.CSS_PREFIX.'col-xs-10, .'.CSS_PREFIX.'col-xs-11, .'.CSS_PREFIX.'col-xs-12 {
  float: left;
}
.'.CSS_PREFIX.'col-xs-12 {
  width: 100%;
}
.'.CSS_PREFIX.'col-xs-11 {
  width: 91.66666667%;
}
.'.CSS_PREFIX.'col-xs-10 {
  width: 83.33333333%;
}
.'.CSS_PREFIX.'col-xs-9 {
  width: 75%;
}
.'.CSS_PREFIX.'col-xs-8 {
  width: 66.66666667%;
}
.'.CSS_PREFIX.'col-xs-7 {
  width: 58.33333333%;
}
.'.CSS_PREFIX.'col-xs-6 {
  width: 50%;
}
.'.CSS_PREFIX.'col-xs-5 {
  width: 41.66666667%;
}
.'.CSS_PREFIX.'col-xs-4 {
  width: 33.33333333%;
}
.'.CSS_PREFIX.'col-xs-3 {
  width: 25%;
}
.'.CSS_PREFIX.'col-xs-2 {
  width: 16.66666667%;
}
.'.CSS_PREFIX.'col-xs-1 {
  width: 8.33333333%;
}
.'.CSS_PREFIX.'col-xs-pull-12 {
  right: 100%;
}
.'.CSS_PREFIX.'col-xs-pull-11 {
  right: 91.66666667%;
}
.'.CSS_PREFIX.'col-xs-pull-10 {
  right: 83.33333333%;
}
.'.CSS_PREFIX.'col-xs-pull-9 {
  right: 75%;
}
.'.CSS_PREFIX.'col-xs-pull-8 {
  right: 66.66666667%;
}
.'.CSS_PREFIX.'col-xs-pull-7 {
  right: 58.33333333%;
}
.'.CSS_PREFIX.'col-xs-pull-6 {
  right: 50%;
}
.'.CSS_PREFIX.'col-xs-pull-5 {
  right: 41.66666667%;
}
.'.CSS_PREFIX.'col-xs-pull-4 {
  right: 33.33333333%;
}
.'.CSS_PREFIX.'col-xs-pull-3 {
  right: 25%;
}
.'.CSS_PREFIX.'col-xs-pull-2 {
  right: 16.66666667%;
}
.'.CSS_PREFIX.'col-xs-pull-1 {
  right: 8.33333333%;
}
.'.CSS_PREFIX.'col-xs-pull-0 {
  right: auto;
}
.'.CSS_PREFIX.'col-xs-push-12 {
  left: 100%;
}
.'.CSS_PREFIX.'col-xs-push-11 {
  left: 91.66666667%;
}
.'.CSS_PREFIX.'col-xs-push-10 {
  left: 83.33333333%;
}
.'.CSS_PREFIX.'col-xs-push-9 {
  left: 75%;
}
.'.CSS_PREFIX.'col-xs-push-8 {
  left: 66.66666667%;
}
.'.CSS_PREFIX.'col-xs-push-7 {
  left: 58.33333333%;
}
.'.CSS_PREFIX.'col-xs-push-6 {
  left: 50%;
}
.'.CSS_PREFIX.'col-xs-push-5 {
  left: 41.66666667%;
}
.'.CSS_PREFIX.'col-xs-push-4 {
  left: 33.33333333%;
}
.'.CSS_PREFIX.'col-xs-push-3 {
  left: 25%;
}
.'.CSS_PREFIX.'col-xs-push-2 {
  left: 16.66666667%;
}
.'.CSS_PREFIX.'col-xs-push-1 {
  left: 8.33333333%;
}
.'.CSS_PREFIX.'col-xs-push-0 {
  left: auto;
}
.'.CSS_PREFIX.'col-xs-offset-12 {
  margin-left: 100%;
}
.'.CSS_PREFIX.'col-xs-offset-11 {
  margin-left: 91.66666667%;
}
.'.CSS_PREFIX.'col-xs-offset-10 {
  margin-left: 83.33333333%;
}
.'.CSS_PREFIX.'col-xs-offset-9 {
  margin-left: 75%;
}
.'.CSS_PREFIX.'col-xs-offset-8 {
  margin-left: 66.66666667%;
}
.'.CSS_PREFIX.'col-xs-offset-7 {
  margin-left: 58.33333333%;
}
.'.CSS_PREFIX.'col-xs-offset-6 {
  margin-left: 50%;
}
.'.CSS_PREFIX.'col-xs-offset-5 {
  margin-left: 41.66666667%;
}
.'.CSS_PREFIX.'col-xs-offset-4 {
  margin-left: 33.33333333%;
}
.'.CSS_PREFIX.'col-xs-offset-3 {
  margin-left: 25%;
}
.'.CSS_PREFIX.'col-xs-offset-2 {
  margin-left: 16.66666667%;
}
.'.CSS_PREFIX.'col-xs-offset-1 {
  margin-left: 8.33333333%;
}
.'.CSS_PREFIX.'col-xs-offset-0 {
  margin-left: 0;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'col-sm-1, .'.CSS_PREFIX.'col-sm-2, .'.CSS_PREFIX.'col-sm-3, .'.CSS_PREFIX.'col-sm-4, .'.CSS_PREFIX.'col-sm-5, .'.CSS_PREFIX.'col-sm-6, .'.CSS_PREFIX.'col-sm-7, .'.CSS_PREFIX.'col-sm-8, .'.CSS_PREFIX.'col-sm-9, .'.CSS_PREFIX.'col-sm-10, .'.CSS_PREFIX.'col-sm-11, .'.CSS_PREFIX.'col-sm-12 {
    float: left;
  }
  .'.CSS_PREFIX.'col-sm-12 {
    width: 100%;
  }
  .'.CSS_PREFIX.'col-sm-11 {
    width: 91.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-10 {
    width: 83.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-9 {
    width: 75%;
  }
  .'.CSS_PREFIX.'col-sm-8 {
    width: 66.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-7 {
    width: 58.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-6 {
    width: 50%;
  }
  .'.CSS_PREFIX.'col-sm-5 {
    width: 41.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-4 {
    width: 33.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-3 {
    width: 25%;
  }
  .'.CSS_PREFIX.'col-sm-2 {
    width: 16.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-1 {
    width: 8.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-pull-12 {
    right: 100%;
  }
  .'.CSS_PREFIX.'col-sm-pull-11 {
    right: 91.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-pull-10 {
    right: 83.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-pull-9 {
    right: 75%;
  }
  .'.CSS_PREFIX.'col-sm-pull-8 {
    right: 66.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-pull-7 {
    right: 58.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-pull-6 {
    right: 50%;
  }
  .'.CSS_PREFIX.'col-sm-pull-5 {
    right: 41.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-pull-4 {
    right: 33.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-pull-3 {
    right: 25%;
  }
  .'.CSS_PREFIX.'col-sm-pull-2 {
    right: 16.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-pull-1 {
    right: 8.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-pull-0 {
    right: auto;
  }
  .'.CSS_PREFIX.'col-sm-push-12 {
    left: 100%;
  }
  .'.CSS_PREFIX.'col-sm-push-11 {
    left: 91.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-push-10 {
    left: 83.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-push-9 {
    left: 75%;
  }
  .'.CSS_PREFIX.'col-sm-push-8 {
    left: 66.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-push-7 {
    left: 58.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-push-6 {
    left: 50%;
  }
  .'.CSS_PREFIX.'col-sm-push-5 {
    left: 41.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-push-4 {
    left: 33.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-push-3 {
    left: 25%;
  }
  .'.CSS_PREFIX.'col-sm-push-2 {
    left: 16.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-push-1 {
    left: 8.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-push-0 {
    left: auto;
  }
  .'.CSS_PREFIX.'col-sm-offset-12 {
    margin-left: 100%;
  }
  .'.CSS_PREFIX.'col-sm-offset-11 {
    margin-left: 91.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-offset-10 {
    margin-left: 83.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-offset-9 {
    margin-left: 75%;
  }
  .'.CSS_PREFIX.'col-sm-offset-8 {
    margin-left: 66.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-offset-7 {
    margin-left: 58.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-offset-6 {
    margin-left: 50%;
  }
  .'.CSS_PREFIX.'col-sm-offset-5 {
    margin-left: 41.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-offset-4 {
    margin-left: 33.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-offset-3 {
    margin-left: 25%;
  }
  .'.CSS_PREFIX.'col-sm-offset-2 {
    margin-left: 16.66666667%;
  }
  .'.CSS_PREFIX.'col-sm-offset-1 {
    margin-left: 8.33333333%;
  }
  .'.CSS_PREFIX.'col-sm-offset-0 {
    margin-left: 0;
  }
}
@media (min-width: 992px) {
  .'.CSS_PREFIX.'col-md-1, .'.CSS_PREFIX.'col-md-2, .'.CSS_PREFIX.'col-md-3, .'.CSS_PREFIX.'col-md-4, .'.CSS_PREFIX.'col-md-5, .'.CSS_PREFIX.'col-md-6, .'.CSS_PREFIX.'col-md-7, .'.CSS_PREFIX.'col-md-8, .'.CSS_PREFIX.'col-md-9, .'.CSS_PREFIX.'col-md-10, .'.CSS_PREFIX.'col-md-11, .'.CSS_PREFIX.'col-md-12 {
    float: left;
  }
  .'.CSS_PREFIX.'col-md-12 {
    width: 100%;
  }
  .'.CSS_PREFIX.'col-md-11 {
    width: 91.66666667%;
  }
  .'.CSS_PREFIX.'col-md-10 {
    width: 83.33333333%;
  }
  .'.CSS_PREFIX.'col-md-9 {
    width: 75%;
  }
  .'.CSS_PREFIX.'col-md-8 {
    width: 66.66666667%;
  }
  .'.CSS_PREFIX.'col-md-7 {
    width: 58.33333333%;
  }
  .'.CSS_PREFIX.'col-md-6 {
    width: 50%;
  }
  .'.CSS_PREFIX.'col-md-5 {
    width: 41.66666667%;
  }
  .'.CSS_PREFIX.'col-md-4 {
    width: 33.33333333%;
  }
  .'.CSS_PREFIX.'col-md-3 {
    width: 25%;
  }
  .'.CSS_PREFIX.'col-md-2 {
    width: 16.66666667%;
  }
  .'.CSS_PREFIX.'col-md-1 {
    width: 8.33333333%;
  }
  .'.CSS_PREFIX.'col-md-pull-12 {
    right: 100%;
  }
  .'.CSS_PREFIX.'col-md-pull-11 {
    right: 91.66666667%;
  }
  .'.CSS_PREFIX.'col-md-pull-10 {
    right: 83.33333333%;
  }
  .'.CSS_PREFIX.'col-md-pull-9 {
    right: 75%;
  }
  .'.CSS_PREFIX.'col-md-pull-8 {
    right: 66.66666667%;
  }
  .'.CSS_PREFIX.'col-md-pull-7 {
    right: 58.33333333%;
  }
  .'.CSS_PREFIX.'col-md-pull-6 {
    right: 50%;
  }
  .'.CSS_PREFIX.'col-md-pull-5 {
    right: 41.66666667%;
  }
  .'.CSS_PREFIX.'col-md-pull-4 {
    right: 33.33333333%;
  }
  .'.CSS_PREFIX.'col-md-pull-3 {
    right: 25%;
  }
  .'.CSS_PREFIX.'col-md-pull-2 {
    right: 16.66666667%;
  }
  .'.CSS_PREFIX.'col-md-pull-1 {
    right: 8.33333333%;
  }
  .'.CSS_PREFIX.'col-md-pull-0 {
    right: auto;
  }
  .'.CSS_PREFIX.'col-md-push-12 {
    left: 100%;
  }
  .'.CSS_PREFIX.'col-md-push-11 {
    left: 91.66666667%;
  }
  .'.CSS_PREFIX.'col-md-push-10 {
    left: 83.33333333%;
  }
  .'.CSS_PREFIX.'col-md-push-9 {
    left: 75%;
  }
  .'.CSS_PREFIX.'col-md-push-8 {
    left: 66.66666667%;
  }
  .'.CSS_PREFIX.'col-md-push-7 {
    left: 58.33333333%;
  }
  .'.CSS_PREFIX.'col-md-push-6 {
    left: 50%;
  }
  .'.CSS_PREFIX.'col-md-push-5 {
    left: 41.66666667%;
  }
  .'.CSS_PREFIX.'col-md-push-4 {
    left: 33.33333333%;
  }
  .'.CSS_PREFIX.'col-md-push-3 {
    left: 25%;
  }
  .'.CSS_PREFIX.'col-md-push-2 {
    left: 16.66666667%;
  }
  .'.CSS_PREFIX.'col-md-push-1 {
    left: 8.33333333%;
  }
  .'.CSS_PREFIX.'col-md-push-0 {
    left: auto;
  }
  .'.CSS_PREFIX.'col-md-offset-12 {
    margin-left: 100%;
  }
  .'.CSS_PREFIX.'col-md-offset-11 {
    margin-left: 91.66666667%;
  }
  .'.CSS_PREFIX.'col-md-offset-10 {
    margin-left: 83.33333333%;
  }
  .'.CSS_PREFIX.'col-md-offset-9 {
    margin-left: 75%;
  }
  .'.CSS_PREFIX.'col-md-offset-8 {
    margin-left: 66.66666667%;
  }
  .'.CSS_PREFIX.'col-md-offset-7 {
    margin-left: 58.33333333%;
  }
  .'.CSS_PREFIX.'col-md-offset-6 {
    margin-left: 50%;
  }
  .'.CSS_PREFIX.'col-md-offset-5 {
    margin-left: 41.66666667%;
  }
  .'.CSS_PREFIX.'col-md-offset-4 {
    margin-left: 33.33333333%;
  }
  .'.CSS_PREFIX.'col-md-offset-3 {
    margin-left: 25%;
  }
  .'.CSS_PREFIX.'col-md-offset-2 {
    margin-left: 16.66666667%;
  }
  .'.CSS_PREFIX.'col-md-offset-1 {
    margin-left: 8.33333333%;
  }
  .'.CSS_PREFIX.'col-md-offset-0 {
    margin-left: 0;
  }
}
@media (min-width: 1200px) {
  .'.CSS_PREFIX.'col-lg-1, .'.CSS_PREFIX.'col-lg-2, .'.CSS_PREFIX.'col-lg-3, .'.CSS_PREFIX.'col-lg-4, .'.CSS_PREFIX.'col-lg-5, .'.CSS_PREFIX.'col-lg-6, .'.CSS_PREFIX.'col-lg-7, .'.CSS_PREFIX.'col-lg-8, .'.CSS_PREFIX.'col-lg-9, .'.CSS_PREFIX.'col-lg-10, .'.CSS_PREFIX.'col-lg-11, .'.CSS_PREFIX.'col-lg-12 {
    float: left;
  }
  .'.CSS_PREFIX.'col-lg-12 {
    width: 100%;
  }
  .'.CSS_PREFIX.'col-lg-11 {
    width: 91.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-10 {
    width: 83.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-9 {
    width: 75%;
  }
  .'.CSS_PREFIX.'col-lg-8 {
    width: 66.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-7 {
    width: 58.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-6 {
    width: 50%;
  }
  .'.CSS_PREFIX.'col-lg-5 {
    width: 41.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-4 {
    width: 33.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-3 {
    width: 25%;
  }
  .'.CSS_PREFIX.'col-lg-2 {
    width: 16.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-1 {
    width: 8.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-pull-12 {
    right: 100%;
  }
  .'.CSS_PREFIX.'col-lg-pull-11 {
    right: 91.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-pull-10 {
    right: 83.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-pull-9 {
    right: 75%;
  }
  .'.CSS_PREFIX.'col-lg-pull-8 {
    right: 66.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-pull-7 {
    right: 58.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-pull-6 {
    right: 50%;
  }
  .'.CSS_PREFIX.'col-lg-pull-5 {
    right: 41.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-pull-4 {
    right: 33.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-pull-3 {
    right: 25%;
  }
  .'.CSS_PREFIX.'col-lg-pull-2 {
    right: 16.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-pull-1 {
    right: 8.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-pull-0 {
    right: auto;
  }
  .'.CSS_PREFIX.'col-lg-push-12 {
    left: 100%;
  }
  .'.CSS_PREFIX.'col-lg-push-11 {
    left: 91.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-push-10 {
    left: 83.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-push-9 {
    left: 75%;
  }
  .'.CSS_PREFIX.'col-lg-push-8 {
    left: 66.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-push-7 {
    left: 58.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-push-6 {
    left: 50%;
  }
  .'.CSS_PREFIX.'col-lg-push-5 {
    left: 41.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-push-4 {
    left: 33.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-push-3 {
    left: 25%;
  }
  .'.CSS_PREFIX.'col-lg-push-2 {
    left: 16.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-push-1 {
    left: 8.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-push-0 {
    left: auto;
  }
  .'.CSS_PREFIX.'col-lg-offset-12 {
    margin-left: 100%;
  }
  .'.CSS_PREFIX.'col-lg-offset-11 {
    margin-left: 91.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-offset-10 {
    margin-left: 83.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-offset-9 {
    margin-left: 75%;
  }
  .'.CSS_PREFIX.'col-lg-offset-8 {
    margin-left: 66.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-offset-7 {
    margin-left: 58.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-offset-6 {
    margin-left: 50%;
  }
  .'.CSS_PREFIX.'col-lg-offset-5 {
    margin-left: 41.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-offset-4 {
    margin-left: 33.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-offset-3 {
    margin-left: 25%;
  }
  .'.CSS_PREFIX.'col-lg-offset-2 {
    margin-left: 16.66666667%;
  }
  .'.CSS_PREFIX.'col-lg-offset-1 {
    margin-left: 8.33333333%;
  }
  .'.CSS_PREFIX.'col-lg-offset-0 {
    margin-left: 0;
  }
}
table {
  background-color: transparent;
}
caption {
  padding-top: 8px;
  padding-bottom: 8px;
  color: #777;
  text-align: left;
}
th {
  text-align: left;
}
.'.CSS_PREFIX.'table {
  width: 100%;
  max-width: 100%;
  margin-bottom: 20px;
}
.'.CSS_PREFIX.'table > thead > tr > th,
.'.CSS_PREFIX.'table > tbody > tr > th,
.'.CSS_PREFIX.'table > tfoot > tr > th,
.'.CSS_PREFIX.'table > thead > tr > td,
.'.CSS_PREFIX.'table > tbody > tr > td,
.'.CSS_PREFIX.'table > tfoot > tr > td {
  padding: 8px;
  line-height: 1.42857143;
  vertical-align: top;
  border-top: 1px solid #ddd;
}
.'.CSS_PREFIX.'table > thead > tr > th {
  vertical-align: bottom;
  border-bottom: 2px solid #ddd;
}
.'.CSS_PREFIX.'table > caption + thead > tr:first-child > th,
.'.CSS_PREFIX.'table > colgroup + thead > tr:first-child > th,
.'.CSS_PREFIX.'table > thead:first-child > tr:first-child > th,
.'.CSS_PREFIX.'table > caption + thead > tr:first-child > td,
.'.CSS_PREFIX.'table > colgroup + thead > tr:first-child > td,
.'.CSS_PREFIX.'table > thead:first-child > tr:first-child > td {
  border-top: 0;
}
.'.CSS_PREFIX.'table > tbody + tbody {
  border-top: 2px solid #ddd;
}
.'.CSS_PREFIX.'table .'.CSS_PREFIX.'table {
  background-color: #fff;
}
.'.CSS_PREFIX.'table-condensed > thead > tr > th,
.'.CSS_PREFIX.'table-condensed > tbody > tr > th,
.'.CSS_PREFIX.'table-condensed > tfoot > tr > th,
.'.CSS_PREFIX.'table-condensed > thead > tr > td,
.'.CSS_PREFIX.'table-condensed > tbody > tr > td,
.'.CSS_PREFIX.'table-condensed > tfoot > tr > td {
  padding: 5px;
}
.'.CSS_PREFIX.'table-bordered {
  border: 1px solid #ddd;
}
.'.CSS_PREFIX.'table-bordered > thead > tr > th,
.'.CSS_PREFIX.'table-bordered > tbody > tr > th,
.'.CSS_PREFIX.'table-bordered > tfoot > tr > th,
.'.CSS_PREFIX.'table-bordered > thead > tr > td,
.'.CSS_PREFIX.'table-bordered > tbody > tr > td,
.'.CSS_PREFIX.'table-bordered > tfoot > tr > td {
  border: 1px solid #ddd;
}
.'.CSS_PREFIX.'table-bordered > thead > tr > th,
.'.CSS_PREFIX.'table-bordered > thead > tr > td {
  border-bottom-width: 2px;
}
.'.CSS_PREFIX.'table-striped > tbody > tr:nth-of-type(odd) {
  background-color: #f9f9f9;
}
.'.CSS_PREFIX.'table-hover > tbody > tr:hover {
  background-color: #f5f5f5;
}
table col[class*="'.CSS_PREFIX.'col-"] {
  position: static;
  display: table-column;
  float: none;
}
table td[class*="'.CSS_PREFIX.'col-"],
table th[class*="'.CSS_PREFIX.'col-"] {
  position: static;
  display: table-cell;
  float: none;
}
.'.CSS_PREFIX.'table > thead > tr > td.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'table > tbody > tr > td.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'table > tfoot > tr > td.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'table > thead > tr > th.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'table > tbody > tr > th.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'table > tfoot > tr > th.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'table > thead > tr.'.CSS_PREFIX.'active > td,
.'.CSS_PREFIX.'table > tbody > tr.'.CSS_PREFIX.'active > td,
.'.CSS_PREFIX.'table > tfoot > tr.'.CSS_PREFIX.'active > td,
.'.CSS_PREFIX.'table > thead > tr.'.CSS_PREFIX.'active > th,
.'.CSS_PREFIX.'table > tbody > tr.'.CSS_PREFIX.'active > th,
.'.CSS_PREFIX.'table > tfoot > tr.'.CSS_PREFIX.'active > th {
  background-color: #f5f5f5;
}
.'.CSS_PREFIX.'table-hover > tbody > tr > td.'.CSS_PREFIX.'active:hover,
.'.CSS_PREFIX.'table-hover > tbody > tr > th.'.CSS_PREFIX.'active:hover,
.'.CSS_PREFIX.'table-hover > tbody > tr.'.CSS_PREFIX.'active:hover > td,
.'.CSS_PREFIX.'table-hover > tbody > tr:hover > .'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'table-hover > tbody > tr.'.CSS_PREFIX.'active:hover > th {
  background-color: #e8e8e8;
}
.'.CSS_PREFIX.'table > thead > tr > td.'.CSS_PREFIX.'success,
.'.CSS_PREFIX.'table > tbody > tr > td.'.CSS_PREFIX.'success,
.'.CSS_PREFIX.'table > tfoot > tr > td.'.CSS_PREFIX.'success,
.'.CSS_PREFIX.'table > thead > tr > th.'.CSS_PREFIX.'success,
.'.CSS_PREFIX.'table > tbody > tr > th.'.CSS_PREFIX.'success,
.'.CSS_PREFIX.'table > tfoot > tr > th.'.CSS_PREFIX.'success,
.'.CSS_PREFIX.'table > thead > tr.'.CSS_PREFIX.'success > td,
.'.CSS_PREFIX.'table > tbody > tr.'.CSS_PREFIX.'success > td,
.'.CSS_PREFIX.'table > tfoot > tr.'.CSS_PREFIX.'success > td,
.'.CSS_PREFIX.'table > thead > tr.'.CSS_PREFIX.'success > th,
.'.CSS_PREFIX.'table > tbody > tr.'.CSS_PREFIX.'success > th,
.'.CSS_PREFIX.'table > tfoot > tr.'.CSS_PREFIX.'success > th {
  background-color: #dff0d8;
}
.'.CSS_PREFIX.'table-hover > tbody > tr > td.'.CSS_PREFIX.'success:hover,
.'.CSS_PREFIX.'table-hover > tbody > tr > th.'.CSS_PREFIX.'success:hover,
.'.CSS_PREFIX.'table-hover > tbody > tr.'.CSS_PREFIX.'success:hover > td,
.'.CSS_PREFIX.'table-hover > tbody > tr:hover > .'.CSS_PREFIX.'success,
.'.CSS_PREFIX.'table-hover > tbody > tr.'.CSS_PREFIX.'success:hover > th {
  background-color: #d0e9c6;
}
.'.CSS_PREFIX.'table > thead > tr > td.'.CSS_PREFIX.'info,
.'.CSS_PREFIX.'table > tbody > tr > td.'.CSS_PREFIX.'info,
.'.CSS_PREFIX.'table > tfoot > tr > td.'.CSS_PREFIX.'info,
.'.CSS_PREFIX.'table > thead > tr > th.'.CSS_PREFIX.'info,
.'.CSS_PREFIX.'table > tbody > tr > th.'.CSS_PREFIX.'info,
.'.CSS_PREFIX.'table > tfoot > tr > th.'.CSS_PREFIX.'info,
.'.CSS_PREFIX.'table > thead > tr.'.CSS_PREFIX.'info > td,
.'.CSS_PREFIX.'table > tbody > tr.'.CSS_PREFIX.'info > td,
.'.CSS_PREFIX.'table > tfoot > tr.'.CSS_PREFIX.'info > td,
.'.CSS_PREFIX.'table > thead > tr.'.CSS_PREFIX.'info > th,
.'.CSS_PREFIX.'table > tbody > tr.'.CSS_PREFIX.'info > th,
.'.CSS_PREFIX.'table > tfoot > tr.'.CSS_PREFIX.'info > th {
  background-color: #d9edf7;
}
.'.CSS_PREFIX.'table-hover > tbody > tr > td.'.CSS_PREFIX.'info:hover,
.'.CSS_PREFIX.'table-hover > tbody > tr > th.'.CSS_PREFIX.'info:hover,
.'.CSS_PREFIX.'table-hover > tbody > tr.'.CSS_PREFIX.'info:hover > td,
.'.CSS_PREFIX.'table-hover > tbody > tr:hover > .'.CSS_PREFIX.'info,
.'.CSS_PREFIX.'table-hover > tbody > tr.'.CSS_PREFIX.'info:hover > th {
  background-color: #c4e3f3;
}
.'.CSS_PREFIX.'table > thead > tr > td.'.CSS_PREFIX.'warning,
.'.CSS_PREFIX.'table > tbody > tr > td.'.CSS_PREFIX.'warning,
.'.CSS_PREFIX.'table > tfoot > tr > td.'.CSS_PREFIX.'warning,
.'.CSS_PREFIX.'table > thead > tr > th.'.CSS_PREFIX.'warning,
.'.CSS_PREFIX.'table > tbody > tr > th.'.CSS_PREFIX.'warning,
.'.CSS_PREFIX.'table > tfoot > tr > th.'.CSS_PREFIX.'warning,
.'.CSS_PREFIX.'table > thead > tr.'.CSS_PREFIX.'warning > td,
.'.CSS_PREFIX.'table > tbody > tr.'.CSS_PREFIX.'warning > td,
.'.CSS_PREFIX.'table > tfoot > tr.'.CSS_PREFIX.'warning > td,
.'.CSS_PREFIX.'table > thead > tr.'.CSS_PREFIX.'warning > th,
.'.CSS_PREFIX.'table > tbody > tr.'.CSS_PREFIX.'warning > th,
.'.CSS_PREFIX.'table > tfoot > tr.'.CSS_PREFIX.'warning > th {
  background-color: #fcf8e3;
}
.'.CSS_PREFIX.'table-hover > tbody > tr > td.'.CSS_PREFIX.'warning:hover,
.'.CSS_PREFIX.'table-hover > tbody > tr > th.'.CSS_PREFIX.'warning:hover,
.'.CSS_PREFIX.'table-hover > tbody > tr.'.CSS_PREFIX.'warning:hover > td,
.'.CSS_PREFIX.'table-hover > tbody > tr:hover > .'.CSS_PREFIX.'warning,
.'.CSS_PREFIX.'table-hover > tbody > tr.'.CSS_PREFIX.'warning:hover > th {
  background-color: #faf2cc;
}
.'.CSS_PREFIX.'table > thead > tr > td.'.CSS_PREFIX.'danger,
.'.CSS_PREFIX.'table > tbody > tr > td.'.CSS_PREFIX.'danger,
.'.CSS_PREFIX.'table > tfoot > tr > td.'.CSS_PREFIX.'danger,
.'.CSS_PREFIX.'table > thead > tr > th.'.CSS_PREFIX.'danger,
.'.CSS_PREFIX.'table > tbody > tr > th.'.CSS_PREFIX.'danger,
.'.CSS_PREFIX.'table > tfoot > tr > th.'.CSS_PREFIX.'danger,
.'.CSS_PREFIX.'table > thead > tr.'.CSS_PREFIX.'danger > td,
.'.CSS_PREFIX.'table > tbody > tr.'.CSS_PREFIX.'danger > td,
.'.CSS_PREFIX.'table > tfoot > tr.'.CSS_PREFIX.'danger > td,
.'.CSS_PREFIX.'table > thead > tr.'.CSS_PREFIX.'danger > th,
.'.CSS_PREFIX.'table > tbody > tr.'.CSS_PREFIX.'danger > th,
.'.CSS_PREFIX.'table > tfoot > tr.'.CSS_PREFIX.'danger > th {
  background-color: #f2dede;
}
.'.CSS_PREFIX.'table-hover > tbody > tr > td.'.CSS_PREFIX.'danger:hover,
.'.CSS_PREFIX.'table-hover > tbody > tr > th.'.CSS_PREFIX.'danger:hover,
.'.CSS_PREFIX.'table-hover > tbody > tr.'.CSS_PREFIX.'danger:hover > td,
.'.CSS_PREFIX.'table-hover > tbody > tr:hover > .'.CSS_PREFIX.'danger,
.'.CSS_PREFIX.'table-hover > tbody > tr.'.CSS_PREFIX.'danger:hover > th {
  background-color: #ebcccc;
}
.'.CSS_PREFIX.'table-responsive {
  min-height: .01%;
  overflow-x: auto;
}
@media screen and (max-width: 767px) {
  .'.CSS_PREFIX.'table-responsive {
    width: 100%;
    margin-bottom: 15px;
    overflow-y: hidden;
    -ms-overflow-style: -ms-autohiding-scrollbar;
    border: 1px solid #ddd;
  }
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table {
    margin-bottom: 0;
  }
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table > thead > tr > th,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table > tbody > tr > th,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table > tfoot > tr > th,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table > thead > tr > td,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table > tbody > tr > td,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table > tfoot > tr > td {
    white-space: nowrap;
  }
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered {
    border: 0;
  }
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > thead > tr > th:first-child,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tbody > tr > th:first-child,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tfoot > tr > th:first-child,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > thead > tr > td:first-child,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tbody > tr > td:first-child,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tfoot > tr > td:first-child {
    border-left: 0;
  }
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > thead > tr > th:last-child,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tbody > tr > th:last-child,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tfoot > tr > th:last-child,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > thead > tr > td:last-child,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tbody > tr > td:last-child,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tfoot > tr > td:last-child {
    border-right: 0;
  }
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tbody > tr:last-child > th,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tfoot > tr:last-child > th,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tbody > tr:last-child > td,
  .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tfoot > tr:last-child > td {
    border-bottom: 0;
  }
}
fieldset {
  min-width: 0;
  padding: 0;
  margin: 0;
  border: 0;
}
legend {
  display: block;
  width: 100%;
  padding: 0;
  margin-bottom: 20px;
  font-size: 21px;
  line-height: inherit;
  color: #333;
  border: 0;
  border-bottom: 1px solid #e5e5e5;
}
label {
  display: inline-block;
  max-width: 100%;
  margin-bottom: 5px;
  font-weight: bold;
}
input[type="search"] {
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
}
input[type="radio"],
input[type="checkbox"] {
  margin: 4px 0 0;
  margin-top: 1px \9;
  line-height: normal;
}
input[type="file"] {
  display: block;
}
input[type="range"] {
  display: block;
  width: 100%;
}
select[multiple],
select[size] {
  height: auto;
}
input[type="file"]:focus,
input[type="radio"]:focus,
input[type="checkbox"]:focus {
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px;
}
output {
  display: block;
  padding-top: 7px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
}
.'.CSS_PREFIX.'form-control {
  display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
       -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
.'.CSS_PREFIX.'form-control:focus {
  border-color: #66afe9;
  outline: 0;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
          box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6);
}
.'.CSS_PREFIX.'form-control::-moz-placeholder {
  color: #999;
  opacity: 1;
}
.'.CSS_PREFIX.'form-control:-ms-input-placeholder {
  color: #999;
}
.'.CSS_PREFIX.'form-control::-webkit-input-placeholder {
  color: #999;
}
.'.CSS_PREFIX.'form-control::-ms-expand {
  background-color: transparent;
  border: 0;
}
.'.CSS_PREFIX.'form-control[disabled],
.'.CSS_PREFIX.'form-control[readonly],
fieldset[disabled] .'.CSS_PREFIX.'form-control {
  background-color: #eee;
  opacity: 1;
}
.'.CSS_PREFIX.'form-control[disabled],
fieldset[disabled] .'.CSS_PREFIX.'form-control {
  cursor: not-allowed;
}
textarea.'.CSS_PREFIX.'form-control {
  height: auto;
}
input[type="search"] {
  -webkit-appearance: none;
}
@media screen and (-webkit-min-device-pixel-ratio: 0) {
  input[type="date"].'.CSS_PREFIX.'form-control,
  input[type="time"].'.CSS_PREFIX.'form-control,
  input[type="datetime-local"].'.CSS_PREFIX.'form-control,
  input[type="month"].'.CSS_PREFIX.'form-control {
    line-height: 34px;
  }
  input[type="date"].'.CSS_PREFIX.'input-sm,
  input[type="time"].'.CSS_PREFIX.'input-sm,
  input[type="datetime-local"].'.CSS_PREFIX.'input-sm,
  input[type="month"].'.CSS_PREFIX.'input-sm,
  .'.CSS_PREFIX.'input-group-sm input[type="date"],
  .'.CSS_PREFIX.'input-group-sm input[type="time"],
  .'.CSS_PREFIX.'input-group-sm input[type="datetime-local"],
  .'.CSS_PREFIX.'input-group-sm input[type="month"] {
    line-height: 30px;
  }
  input[type="date"].'.CSS_PREFIX.'input-lg,
  input[type="time"].'.CSS_PREFIX.'input-lg,
  input[type="datetime-local"].'.CSS_PREFIX.'input-lg,
  input[type="month"].'.CSS_PREFIX.'input-lg,
  .'.CSS_PREFIX.'input-group-lg input[type="date"],
  .'.CSS_PREFIX.'input-group-lg input[type="time"],
  .'.CSS_PREFIX.'input-group-lg input[type="datetime-local"],
  .'.CSS_PREFIX.'input-group-lg input[type="month"] {
    line-height: 46px;
  }
}
.'.CSS_PREFIX.'form-group {
  margin-bottom: 15px;
}
.'.CSS_PREFIX.'radio,
.'.CSS_PREFIX.'checkbox {
  position: relative;
  display: block;
  margin-top: 10px;
  margin-bottom: 10px;
}
.'.CSS_PREFIX.'radio label,
.'.CSS_PREFIX.'checkbox label {
  min-height: 20px;
  padding-left: 20px;
  margin-bottom: 0;
  font-weight: normal;
  cursor: pointer;
}
.'.CSS_PREFIX.'radio input[type="radio"],
.'.CSS_PREFIX.'radio-inline input[type="radio"],
.'.CSS_PREFIX.'checkbox input[type="checkbox"],
.'.CSS_PREFIX.'checkbox-inline input[type="checkbox"] {
  position: absolute;
  margin-top: 4px \9;
  margin-left: -20px;
}
.'.CSS_PREFIX.'radio + .'.CSS_PREFIX.'radio,
.'.CSS_PREFIX.'checkbox + .'.CSS_PREFIX.'checkbox {
  margin-top: -5px;
}
.'.CSS_PREFIX.'radio-inline,
.'.CSS_PREFIX.'checkbox-inline {
  position: relative;
  display: inline-block;
  padding-left: 20px;
  margin-bottom: 0;
  font-weight: normal;
  vertical-align: middle;
  cursor: pointer;
}
.'.CSS_PREFIX.'radio-inline + .'.CSS_PREFIX.'radio-inline,
.'.CSS_PREFIX.'checkbox-inline + .'.CSS_PREFIX.'checkbox-inline {
  margin-top: 0;
  margin-left: 10px;
}
input[type="radio"][disabled],
input[type="checkbox"][disabled],
input[type="radio"].'.CSS_PREFIX.'disabled,
input[type="checkbox"].'.CSS_PREFIX.'disabled,
fieldset[disabled] input[type="radio"],
fieldset[disabled] input[type="checkbox"] {
  cursor: not-allowed;
}
.'.CSS_PREFIX.'radio-inline.'.CSS_PREFIX.'disabled,
.'.CSS_PREFIX.'checkbox-inline.'.CSS_PREFIX.'disabled,
fieldset[disabled] .'.CSS_PREFIX.'radio-inline,
fieldset[disabled] .'.CSS_PREFIX.'checkbox-inline {
  cursor: not-allowed;
}
.'.CSS_PREFIX.'radio.'.CSS_PREFIX.'disabled label,
.'.CSS_PREFIX.'checkbox.'.CSS_PREFIX.'disabled label,
fieldset[disabled] .'.CSS_PREFIX.'radio label,
fieldset[disabled] .'.CSS_PREFIX.'checkbox label {
  cursor: not-allowed;
}
.'.CSS_PREFIX.'form-control-static {
  min-height: 34px;
  padding-top: 7px;
  padding-bottom: 7px;
  margin-bottom: 0;
}
.'.CSS_PREFIX.'form-control-static.'.CSS_PREFIX.'input-lg,
.'.CSS_PREFIX.'form-control-static.'.CSS_PREFIX.'input-sm {
  padding-right: 0;
  padding-left: 0;
}
.'.CSS_PREFIX.'input-sm {
  height: 30px;
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
  border-radius: 3px;
}
select.'.CSS_PREFIX.'input-sm {
  height: 30px;
  line-height: 30px;
}
textarea.'.CSS_PREFIX.'input-sm,
select[multiple].'.CSS_PREFIX.'input-sm {
  height: auto;
}
.'.CSS_PREFIX.'form-group-sm .'.CSS_PREFIX.'form-control {
  height: 30px;
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
  border-radius: 3px;
}
.'.CSS_PREFIX.'form-group-sm select.'.CSS_PREFIX.'form-control {
  height: 30px;
  line-height: 30px;
}
.'.CSS_PREFIX.'form-group-sm textarea.'.CSS_PREFIX.'form-control,
.'.CSS_PREFIX.'form-group-sm select[multiple].'.CSS_PREFIX.'form-control {
  height: auto;
}
.'.CSS_PREFIX.'form-group-sm .'.CSS_PREFIX.'form-control-static {
  height: 30px;
  min-height: 32px;
  padding: 6px 10px;
  font-size: 12px;
  line-height: 1.5;
}
.'.CSS_PREFIX.'input-lg {
  height: 46px;
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
  border-radius: 6px;
}
select.'.CSS_PREFIX.'input-lg {
  height: 46px;
  line-height: 46px;
}
textarea.'.CSS_PREFIX.'input-lg,
select[multiple].'.CSS_PREFIX.'input-lg {
  height: auto;
}
.'.CSS_PREFIX.'form-group-lg .'.CSS_PREFIX.'form-control {
  height: 46px;
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
  border-radius: 6px;
}
.'.CSS_PREFIX.'form-group-lg select.'.CSS_PREFIX.'form-control {
  height: 46px;
  line-height: 46px;
}
.'.CSS_PREFIX.'form-group-lg textarea.'.CSS_PREFIX.'form-control,
.'.CSS_PREFIX.'form-group-lg select[multiple].'.CSS_PREFIX.'form-control {
  height: auto;
}
.'.CSS_PREFIX.'form-group-lg .'.CSS_PREFIX.'form-control-static {
  height: 46px;
  min-height: 38px;
  padding: 11px 16px;
  font-size: 18px;
  line-height: 1.3333333;
}
.'.CSS_PREFIX.'has-feedback {
  position: relative;
}
.'.CSS_PREFIX.'has-feedback .'.CSS_PREFIX.'form-control {
  padding-right: 42.5px;
}
.'.CSS_PREFIX.'form-control-feedback {
  position: absolute;
  top: 0;
  right: 0;
  z-index: 2;
  display: block;
  width: 34px;
  height: 34px;
  line-height: 34px;
  text-align: center;
  pointer-events: none;
}
.'.CSS_PREFIX.'input-lg + .'.CSS_PREFIX.'form-control-feedback,
.'.CSS_PREFIX.'input-group-lg + .'.CSS_PREFIX.'form-control-feedback,
.'.CSS_PREFIX.'form-group-lg .'.CSS_PREFIX.'form-control + .'.CSS_PREFIX.'form-control-feedback {
  width: 46px;
  height: 46px;
  line-height: 46px;
}
.'.CSS_PREFIX.'input-sm + .'.CSS_PREFIX.'form-control-feedback,
.'.CSS_PREFIX.'input-group-sm + .'.CSS_PREFIX.'form-control-feedback,
.'.CSS_PREFIX.'form-group-sm .'.CSS_PREFIX.'form-control + .'.CSS_PREFIX.'form-control-feedback {
  width: 30px;
  height: 30px;
  line-height: 30px;
}
.'.CSS_PREFIX.'has-success .'.CSS_PREFIX.'help-block,
.'.CSS_PREFIX.'has-success .'.CSS_PREFIX.'control-label,
.'.CSS_PREFIX.'has-success .'.CSS_PREFIX.'radio,
.'.CSS_PREFIX.'has-success .'.CSS_PREFIX.'checkbox,
.'.CSS_PREFIX.'has-success .'.CSS_PREFIX.'radio-inline,
.'.CSS_PREFIX.'has-success .'.CSS_PREFIX.'checkbox-inline,
.'.CSS_PREFIX.'has-success.'.CSS_PREFIX.'radio label,
.'.CSS_PREFIX.'has-success.'.CSS_PREFIX.'checkbox label,
.'.CSS_PREFIX.'has-success.'.CSS_PREFIX.'radio-inline label,
.'.CSS_PREFIX.'has-success.'.CSS_PREFIX.'checkbox-inline label {
  color: #3c763d;
}
.'.CSS_PREFIX.'has-success .'.CSS_PREFIX.'form-control {
  border-color: #3c763d;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
}
.'.CSS_PREFIX.'has-success .'.CSS_PREFIX.'form-control:focus {
  border-color: #2b542c;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #67b168;
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #67b168;
}
.'.CSS_PREFIX.'has-success .'.CSS_PREFIX.'input-group-addon {
  color: #3c763d;
  background-color: #dff0d8;
  border-color: #3c763d;
}
.'.CSS_PREFIX.'has-success .'.CSS_PREFIX.'form-control-feedback {
  color: #3c763d;
}
.'.CSS_PREFIX.'has-warning .'.CSS_PREFIX.'help-block,
.'.CSS_PREFIX.'has-warning .'.CSS_PREFIX.'control-label,
.'.CSS_PREFIX.'has-warning .'.CSS_PREFIX.'radio,
.'.CSS_PREFIX.'has-warning .'.CSS_PREFIX.'checkbox,
.'.CSS_PREFIX.'has-warning .'.CSS_PREFIX.'radio-inline,
.'.CSS_PREFIX.'has-warning .'.CSS_PREFIX.'checkbox-inline,
.'.CSS_PREFIX.'has-warning.'.CSS_PREFIX.'radio label,
.'.CSS_PREFIX.'has-warning.'.CSS_PREFIX.'checkbox label,
.'.CSS_PREFIX.'has-warning.'.CSS_PREFIX.'radio-inline label,
.'.CSS_PREFIX.'has-warning.'.CSS_PREFIX.'checkbox-inline label {
  color: #8a6d3b;
}
.'.CSS_PREFIX.'has-warning .'.CSS_PREFIX.'form-control {
  border-color: #8a6d3b;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
}
.'.CSS_PREFIX.'has-warning .'.CSS_PREFIX.'form-control:focus {
  border-color: #66512c;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #c0a16b;
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #c0a16b;
}
.'.CSS_PREFIX.'has-warning .'.CSS_PREFIX.'input-group-addon {
  color: #8a6d3b;
  background-color: #fcf8e3;
  border-color: #8a6d3b;
}
.'.CSS_PREFIX.'has-warning .'.CSS_PREFIX.'form-control-feedback {
  color: #8a6d3b;
}
.'.CSS_PREFIX.'has-error .'.CSS_PREFIX.'help-block,
.'.CSS_PREFIX.'has-error .'.CSS_PREFIX.'control-label,
.'.CSS_PREFIX.'has-error .'.CSS_PREFIX.'radio,
.'.CSS_PREFIX.'has-error .'.CSS_PREFIX.'checkbox,
.'.CSS_PREFIX.'has-error .'.CSS_PREFIX.'radio-inline,
.'.CSS_PREFIX.'has-error .'.CSS_PREFIX.'checkbox-inline,
.'.CSS_PREFIX.'has-error.'.CSS_PREFIX.'radio label,
.'.CSS_PREFIX.'has-error.'.CSS_PREFIX.'checkbox label,
.'.CSS_PREFIX.'has-error.'.CSS_PREFIX.'radio-inline label,
.'.CSS_PREFIX.'has-error.'.CSS_PREFIX.'checkbox-inline label {
  color: #a94442;
}
.'.CSS_PREFIX.'has-error .'.CSS_PREFIX.'form-control {
  border-color: #a94442;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
}
.'.CSS_PREFIX.'has-error .'.CSS_PREFIX.'form-control:focus {
  border-color: #843534;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
}
.'.CSS_PREFIX.'has-error .'.CSS_PREFIX.'input-group-addon {
  color: #a94442;
  background-color: #f2dede;
  border-color: #a94442;
}
.'.CSS_PREFIX.'has-error .'.CSS_PREFIX.'form-control-feedback {
  color: #a94442;
}
.'.CSS_PREFIX.'has-feedback label ~ .'.CSS_PREFIX.'form-control-feedback {
  top: 25px;
}
.'.CSS_PREFIX.'has-feedback label.'.CSS_PREFIX.'sr-only ~ .'.CSS_PREFIX.'form-control-feedback {
  top: 0;
}
.'.CSS_PREFIX.'help-block {
  display: block;
  margin-top: 5px;
  margin-bottom: 10px;
  color: #737373;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'form-group {
    display: inline-block;
    margin-bottom: 0;
    vertical-align: middle;
  }
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'form-control {
    display: inline-block;
    width: auto;
    vertical-align: middle;
  }
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'form-control-static {
    display: inline-block;
  }
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'input-group {
    display: inline-table;
    vertical-align: middle;
  }
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'input-group .'.CSS_PREFIX.'input-group-addon,
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'input-group .'.CSS_PREFIX.'input-group-btn,
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'input-group .'.CSS_PREFIX.'form-control {
    width: auto;
  }
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'input-group > .'.CSS_PREFIX.'form-control {
    width: 100%;
  }
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'control-label {
    margin-bottom: 0;
    vertical-align: middle;
  }
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'radio,
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'checkbox {
    display: inline-block;
    margin-top: 0;
    margin-bottom: 0;
    vertical-align: middle;
  }
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'radio label,
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'checkbox label {
    padding-left: 0;
  }
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'radio input[type="radio"],
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'checkbox input[type="checkbox"] {
    position: relative;
    margin-left: 0;
  }
  .'.CSS_PREFIX.'form-inline .'.CSS_PREFIX.'has-feedback .'.CSS_PREFIX.'form-control-feedback {
    top: 0;
  }
}
.'.CSS_PREFIX.'form-horizontal .'.CSS_PREFIX.'radio,
.'.CSS_PREFIX.'form-horizontal .'.CSS_PREFIX.'checkbox,
.'.CSS_PREFIX.'form-horizontal .'.CSS_PREFIX.'radio-inline,
.'.CSS_PREFIX.'form-horizontal .'.CSS_PREFIX.'checkbox-inline {
  padding-top: 7px;
  margin-top: 0;
  margin-bottom: 0;
}
.'.CSS_PREFIX.'form-horizontal .'.CSS_PREFIX.'radio,
.'.CSS_PREFIX.'form-horizontal .'.CSS_PREFIX.'checkbox {
  min-height: 27px;
}
.'.CSS_PREFIX.'form-horizontal .'.CSS_PREFIX.'form-group {
  margin-right: -15px;
  margin-left: -15px;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'form-horizontal .'.CSS_PREFIX.'control-label {
    padding-top: 7px;
    margin-bottom: 0;
    text-align: right;
  }
}
.'.CSS_PREFIX.'form-horizontal .'.CSS_PREFIX.'has-feedback .'.CSS_PREFIX.'form-control-feedback {
  right: 15px;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'form-horizontal .'.CSS_PREFIX.'form-group-lg .'.CSS_PREFIX.'control-label {
    padding-top: 11px;
    font-size: 18px;
  }
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'form-horizontal .'.CSS_PREFIX.'form-group-sm .'.CSS_PREFIX.'control-label {
    padding-top: 6px;
    font-size: 12px;
  }
}
.'.CSS_PREFIX.'btn {
  display: inline-block;
  padding: 6px 12px;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: normal;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  -ms-touch-action: manipulation;
      touch-action: manipulation;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 4px;
}
.'.CSS_PREFIX.'btn:focus,
.'.CSS_PREFIX.'btn:active:focus,
.'.CSS_PREFIX.'btn.'.CSS_PREFIX.'active:focus,
.'.CSS_PREFIX.'btn.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'btn:active.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'btn.'.CSS_PREFIX.'active.'.CSS_PREFIX.'focus {
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px;
}
.'.CSS_PREFIX.'btn:hover,
.'.CSS_PREFIX.'btn:focus,
.'.CSS_PREFIX.'btn.'.CSS_PREFIX.'focus {
  color: #333;
  text-decoration: none;
}
.'.CSS_PREFIX.'btn:active,
.'.CSS_PREFIX.'btn.'.CSS_PREFIX.'active {
  background-image: none;
  outline: 0;
  -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
          box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
}
.'.CSS_PREFIX.'btn.'.CSS_PREFIX.'disabled,
.'.CSS_PREFIX.'btn[disabled],
fieldset[disabled] .'.CSS_PREFIX.'btn {
  cursor: not-allowed;
  filter: alpha(opacity=65);
  -webkit-box-shadow: none;
          box-shadow: none;
  opacity: .65;
}
a.'.CSS_PREFIX.'btn.'.CSS_PREFIX.'disabled,
fieldset[disabled] a.'.CSS_PREFIX.'btn {
  pointer-events: none;
}
.'.CSS_PREFIX.'btn-default {
  color: #333;
  background-color: #fff;
  border-color: #ccc;
}
.'.CSS_PREFIX.'btn-default:focus,
.'.CSS_PREFIX.'btn-default.'.CSS_PREFIX.'focus {
  color: #333;
  background-color: #e6e6e6;
  border-color: #8c8c8c;
}
.'.CSS_PREFIX.'btn-default:hover {
  color: #333;
  background-color: #e6e6e6;
  border-color: #adadad;
}
.'.CSS_PREFIX.'btn-default:active,
.'.CSS_PREFIX.'btn-default.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-default {
  color: #333;
  background-color: #e6e6e6;
  border-color: #adadad;
}
.'.CSS_PREFIX.'btn-default:active:hover,
.'.CSS_PREFIX.'btn-default.'.CSS_PREFIX.'active:hover,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-default:hover,
.'.CSS_PREFIX.'btn-default:active:focus,
.'.CSS_PREFIX.'btn-default.'.CSS_PREFIX.'active:focus,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-default:focus,
.'.CSS_PREFIX.'btn-default:active.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'btn-default.'.CSS_PREFIX.'active.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-default.'.CSS_PREFIX.'focus {
  color: #333;
  background-color: #d4d4d4;
  border-color: #8c8c8c;
}
.'.CSS_PREFIX.'btn-default:active,
.'.CSS_PREFIX.'btn-default.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-default {
  background-image: none;
}
.'.CSS_PREFIX.'btn-default.'.CSS_PREFIX.'disabled:hover,
.'.CSS_PREFIX.'btn-default[disabled]:hover,
fieldset[disabled] .'.CSS_PREFIX.'btn-default:hover,
.'.CSS_PREFIX.'btn-default.'.CSS_PREFIX.'disabled:focus,
.'.CSS_PREFIX.'btn-default[disabled]:focus,
fieldset[disabled] .'.CSS_PREFIX.'btn-default:focus,
.'.CSS_PREFIX.'btn-default.'.CSS_PREFIX.'disabled.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'btn-default[disabled].'.CSS_PREFIX.'focus,
fieldset[disabled] .'.CSS_PREFIX.'btn-default.'.CSS_PREFIX.'focus {
  background-color: #fff;
  border-color: #ccc;
}
.'.CSS_PREFIX.'btn-default .'.CSS_PREFIX.'badge {
  color: #fff;
  background-color: #333;
}
.'.CSS_PREFIX.'btn-primary {
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4;
}
.'.CSS_PREFIX.'btn-primary:focus,
.'.CSS_PREFIX.'btn-primary.'.CSS_PREFIX.'focus {
  color: #fff;
  background-color: #286090;
  border-color: #122b40;
}
.'.CSS_PREFIX.'btn-primary:hover {
  color: #fff;
  background-color: #286090;
  border-color: #204d74;
}
.'.CSS_PREFIX.'btn-primary:active,
.'.CSS_PREFIX.'btn-primary.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-primary {
  color: #fff;
  background-color: #286090;
  border-color: #204d74;
}
.'.CSS_PREFIX.'btn-primary:active:hover,
.'.CSS_PREFIX.'btn-primary.'.CSS_PREFIX.'active:hover,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-primary:hover,
.'.CSS_PREFIX.'btn-primary:active:focus,
.'.CSS_PREFIX.'btn-primary.'.CSS_PREFIX.'active:focus,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-primary:focus,
.'.CSS_PREFIX.'btn-primary:active.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'btn-primary.'.CSS_PREFIX.'active.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-primary.'.CSS_PREFIX.'focus {
  color: #fff;
  background-color: #204d74;
  border-color: #122b40;
}
.'.CSS_PREFIX.'btn-primary:active,
.'.CSS_PREFIX.'btn-primary.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-primary {
  background-image: none;
}
.'.CSS_PREFIX.'btn-primary.'.CSS_PREFIX.'disabled:hover,
.'.CSS_PREFIX.'btn-primary[disabled]:hover,
fieldset[disabled] .'.CSS_PREFIX.'btn-primary:hover,
.'.CSS_PREFIX.'btn-primary.'.CSS_PREFIX.'disabled:focus,
.'.CSS_PREFIX.'btn-primary[disabled]:focus,
fieldset[disabled] .'.CSS_PREFIX.'btn-primary:focus,
.'.CSS_PREFIX.'btn-primary.'.CSS_PREFIX.'disabled.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'btn-primary[disabled].'.CSS_PREFIX.'focus,
fieldset[disabled] .'.CSS_PREFIX.'btn-primary.'.CSS_PREFIX.'focus {
  background-color: #337ab7;
  border-color: #2e6da4;
}
.'.CSS_PREFIX.'btn-primary .'.CSS_PREFIX.'badge {
  color: #337ab7;
  background-color: #fff;
}
.'.CSS_PREFIX.'btn-success {
  color: #fff;
  background-color: #5cb85c;
  border-color: #4cae4c;
}
.'.CSS_PREFIX.'btn-success:focus,
.'.CSS_PREFIX.'btn-success.'.CSS_PREFIX.'focus {
  color: #fff;
  background-color: #449d44;
  border-color: #255625;
}
.'.CSS_PREFIX.'btn-success:hover {
  color: #fff;
  background-color: #449d44;
  border-color: #398439;
}
.'.CSS_PREFIX.'btn-success:active,
.'.CSS_PREFIX.'btn-success.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-success {
  color: #fff;
  background-color: #449d44;
  border-color: #398439;
}
.'.CSS_PREFIX.'btn-success:active:hover,
.'.CSS_PREFIX.'btn-success.'.CSS_PREFIX.'active:hover,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-success:hover,
.'.CSS_PREFIX.'btn-success:active:focus,
.'.CSS_PREFIX.'btn-success.'.CSS_PREFIX.'active:focus,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-success:focus,
.'.CSS_PREFIX.'btn-success:active.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'btn-success.'.CSS_PREFIX.'active.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-success.'.CSS_PREFIX.'focus {
  color: #fff;
  background-color: #398439;
  border-color: #255625;
}
.'.CSS_PREFIX.'btn-success:active,
.'.CSS_PREFIX.'btn-success.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-success {
  background-image: none;
}
.'.CSS_PREFIX.'btn-success.'.CSS_PREFIX.'disabled:hover,
.'.CSS_PREFIX.'btn-success[disabled]:hover,
fieldset[disabled] .'.CSS_PREFIX.'btn-success:hover,
.'.CSS_PREFIX.'btn-success.'.CSS_PREFIX.'disabled:focus,
.'.CSS_PREFIX.'btn-success[disabled]:focus,
fieldset[disabled] .'.CSS_PREFIX.'btn-success:focus,
.'.CSS_PREFIX.'btn-success.'.CSS_PREFIX.'disabled.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'btn-success[disabled].'.CSS_PREFIX.'focus,
fieldset[disabled] .'.CSS_PREFIX.'btn-success.'.CSS_PREFIX.'focus {
  background-color: #5cb85c;
  border-color: #4cae4c;
}
.'.CSS_PREFIX.'btn-success .'.CSS_PREFIX.'badge {
  color: #5cb85c;
  background-color: #fff;
}
.'.CSS_PREFIX.'btn-info {
  color: #fff;
  background-color: #5bc0de;
  border-color: #46b8da;
}
.'.CSS_PREFIX.'btn-info:focus,
.'.CSS_PREFIX.'btn-info.'.CSS_PREFIX.'focus {
  color: #fff;
  background-color: #31b0d5;
  border-color: #1b6d85;
}
.'.CSS_PREFIX.'btn-info:hover {
  color: #fff;
  background-color: #31b0d5;
  border-color: #269abc;
}
.'.CSS_PREFIX.'btn-info:active,
.'.CSS_PREFIX.'btn-info.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-info {
  color: #fff;
  background-color: #31b0d5;
  border-color: #269abc;
}
.'.CSS_PREFIX.'btn-info:active:hover,
.'.CSS_PREFIX.'btn-info.'.CSS_PREFIX.'active:hover,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-info:hover,
.'.CSS_PREFIX.'btn-info:active:focus,
.'.CSS_PREFIX.'btn-info.'.CSS_PREFIX.'active:focus,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-info:focus,
.'.CSS_PREFIX.'btn-info:active.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'btn-info.'.CSS_PREFIX.'active.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-info.'.CSS_PREFIX.'focus {
  color: #fff;
  background-color: #269abc;
  border-color: #1b6d85;
}
.'.CSS_PREFIX.'btn-info:active,
.'.CSS_PREFIX.'btn-info.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-info {
  background-image: none;
}
.'.CSS_PREFIX.'btn-info.'.CSS_PREFIX.'disabled:hover,
.'.CSS_PREFIX.'btn-info[disabled]:hover,
fieldset[disabled] .'.CSS_PREFIX.'btn-info:hover,
.'.CSS_PREFIX.'btn-info.'.CSS_PREFIX.'disabled:focus,
.'.CSS_PREFIX.'btn-info[disabled]:focus,
fieldset[disabled] .'.CSS_PREFIX.'btn-info:focus,
.'.CSS_PREFIX.'btn-info.'.CSS_PREFIX.'disabled.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'btn-info[disabled].'.CSS_PREFIX.'focus,
fieldset[disabled] .'.CSS_PREFIX.'btn-info.'.CSS_PREFIX.'focus {
  background-color: #5bc0de;
  border-color: #46b8da;
}
.'.CSS_PREFIX.'btn-info .'.CSS_PREFIX.'badge {
  color: #5bc0de;
  background-color: #fff;
}
.'.CSS_PREFIX.'btn-warning {
  color: #fff;
  background-color: #f0ad4e;
  border-color: #eea236;
}
.'.CSS_PREFIX.'btn-warning:focus,
.'.CSS_PREFIX.'btn-warning.'.CSS_PREFIX.'focus {
  color: #fff;
  background-color: #ec971f;
  border-color: #985f0d;
}
.'.CSS_PREFIX.'btn-warning:hover {
  color: #fff;
  background-color: #ec971f;
  border-color: #d58512;
}
.'.CSS_PREFIX.'btn-warning:active,
.'.CSS_PREFIX.'btn-warning.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-warning {
  color: #fff;
  background-color: #ec971f;
  border-color: #d58512;
}
.'.CSS_PREFIX.'btn-warning:active:hover,
.'.CSS_PREFIX.'btn-warning.'.CSS_PREFIX.'active:hover,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-warning:hover,
.'.CSS_PREFIX.'btn-warning:active:focus,
.'.CSS_PREFIX.'btn-warning.'.CSS_PREFIX.'active:focus,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-warning:focus,
.'.CSS_PREFIX.'btn-warning:active.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'btn-warning.'.CSS_PREFIX.'active.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-warning.'.CSS_PREFIX.'focus {
  color: #fff;
  background-color: #d58512;
  border-color: #985f0d;
}
.'.CSS_PREFIX.'btn-warning:active,
.'.CSS_PREFIX.'btn-warning.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-warning {
  background-image: none;
}
.'.CSS_PREFIX.'btn-warning.'.CSS_PREFIX.'disabled:hover,
.'.CSS_PREFIX.'btn-warning[disabled]:hover,
fieldset[disabled] .'.CSS_PREFIX.'btn-warning:hover,
.'.CSS_PREFIX.'btn-warning.'.CSS_PREFIX.'disabled:focus,
.'.CSS_PREFIX.'btn-warning[disabled]:focus,
fieldset[disabled] .'.CSS_PREFIX.'btn-warning:focus,
.'.CSS_PREFIX.'btn-warning.'.CSS_PREFIX.'disabled.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'btn-warning[disabled].'.CSS_PREFIX.'focus,
fieldset[disabled] .'.CSS_PREFIX.'btn-warning.'.CSS_PREFIX.'focus {
  background-color: #f0ad4e;
  border-color: #eea236;
}
.'.CSS_PREFIX.'btn-warning .'.CSS_PREFIX.'badge {
  color: #f0ad4e;
  background-color: #fff;
}
.'.CSS_PREFIX.'btn-danger {
  color: #fff;
  background-color: #d9534f;
  border-color: #d43f3a;
}
.'.CSS_PREFIX.'btn-danger:focus,
.'.CSS_PREFIX.'btn-danger.'.CSS_PREFIX.'focus {
  color: #fff;
  background-color: #c9302c;
  border-color: #761c19;
}
.'.CSS_PREFIX.'btn-danger:hover {
  color: #fff;
  background-color: #c9302c;
  border-color: #ac2925;
}
.'.CSS_PREFIX.'btn-danger:active,
.'.CSS_PREFIX.'btn-danger.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-danger {
  color: #fff;
  background-color: #c9302c;
  border-color: #ac2925;
}
.'.CSS_PREFIX.'btn-danger:active:hover,
.'.CSS_PREFIX.'btn-danger.'.CSS_PREFIX.'active:hover,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-danger:hover,
.'.CSS_PREFIX.'btn-danger:active:focus,
.'.CSS_PREFIX.'btn-danger.'.CSS_PREFIX.'active:focus,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-danger:focus,
.'.CSS_PREFIX.'btn-danger:active.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'btn-danger.'.CSS_PREFIX.'active.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-danger.'.CSS_PREFIX.'focus {
  color: #fff;
  background-color: #ac2925;
  border-color: #761c19;
}
.'.CSS_PREFIX.'btn-danger:active,
.'.CSS_PREFIX.'btn-danger.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-danger {
  background-image: none;
}
.'.CSS_PREFIX.'btn-danger.'.CSS_PREFIX.'disabled:hover,
.'.CSS_PREFIX.'btn-danger[disabled]:hover,
fieldset[disabled] .'.CSS_PREFIX.'btn-danger:hover,
.'.CSS_PREFIX.'btn-danger.'.CSS_PREFIX.'disabled:focus,
.'.CSS_PREFIX.'btn-danger[disabled]:focus,
fieldset[disabled] .'.CSS_PREFIX.'btn-danger:focus,
.'.CSS_PREFIX.'btn-danger.'.CSS_PREFIX.'disabled.'.CSS_PREFIX.'focus,
.'.CSS_PREFIX.'btn-danger[disabled].'.CSS_PREFIX.'focus,
fieldset[disabled] .'.CSS_PREFIX.'btn-danger.'.CSS_PREFIX.'focus {
  background-color: #d9534f;
  border-color: #d43f3a;
}
.'.CSS_PREFIX.'btn-danger .'.CSS_PREFIX.'badge {
  color: #d9534f;
  background-color: #fff;
}
.'.CSS_PREFIX.'btn-link {
  font-weight: normal;
  color: #337ab7;
  border-radius: 0;
}
.'.CSS_PREFIX.'btn-link,
.'.CSS_PREFIX.'btn-link:active,
.'.CSS_PREFIX.'btn-link.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'btn-link[disabled],
fieldset[disabled] .'.CSS_PREFIX.'btn-link {
  background-color: transparent;
  -webkit-box-shadow: none;
          box-shadow: none;
}
.'.CSS_PREFIX.'btn-link,
.'.CSS_PREFIX.'btn-link:hover,
.'.CSS_PREFIX.'btn-link:focus,
.'.CSS_PREFIX.'btn-link:active {
  border-color: transparent;
}
.'.CSS_PREFIX.'btn-link:hover,
.'.CSS_PREFIX.'btn-link:focus {
  color: #23527c;
  text-decoration: underline;
  background-color: transparent;
}
.'.CSS_PREFIX.'btn-link[disabled]:hover,
fieldset[disabled] .'.CSS_PREFIX.'btn-link:hover,
.'.CSS_PREFIX.'btn-link[disabled]:focus,
fieldset[disabled] .'.CSS_PREFIX.'btn-link:focus {
  color: #777;
  text-decoration: none;
}
.'.CSS_PREFIX.'btn-lg,
.'.CSS_PREFIX.'btn-group-lg > .'.CSS_PREFIX.'btn {
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
  border-radius: 6px;
}
.'.CSS_PREFIX.'btn-sm,
.'.CSS_PREFIX.'btn-group-sm > .'.CSS_PREFIX.'btn {
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
  border-radius: 3px;
}
.'.CSS_PREFIX.'btn-xs,
.'.CSS_PREFIX.'btn-group-xs > .'.CSS_PREFIX.'btn {
  padding: 1px 5px;
  font-size: 12px;
  line-height: 1.5;
  border-radius: 3px;
}
.'.CSS_PREFIX.'btn-block {
  display: block;
  width: 100%;
}
.'.CSS_PREFIX.'btn-block + .'.CSS_PREFIX.'btn-block {
  margin-top: 5px;
}
input[type="submit"].'.CSS_PREFIX.'btn-block,
input[type="reset"].'.CSS_PREFIX.'btn-block,
input[type="button"].'.CSS_PREFIX.'btn-block {
  width: 100%;
}
.'.CSS_PREFIX.'fade {
  opacity: 0;
  -webkit-transition: opacity .15s linear;
       -o-transition: opacity .15s linear;
          transition: opacity .15s linear;
}
.'.CSS_PREFIX.'fade.'.CSS_PREFIX.'in {
  opacity: 1;
}
.'.CSS_PREFIX.'collapse {
  display: none;
}
.'.CSS_PREFIX.'collapse.'.CSS_PREFIX.'in {
  display: block;
}
tr.'.CSS_PREFIX.'collapse.'.CSS_PREFIX.'in {
  display: table-row;
}
tbody.'.CSS_PREFIX.'collapse.'.CSS_PREFIX.'in {
  display: table-row-group;
}
.'.CSS_PREFIX.'collapsing {
  position: relative;
  height: 0;
  overflow: hidden;
  -webkit-transition-timing-function: ease;
       -o-transition-timing-function: ease;
          transition-timing-function: ease;
  -webkit-transition-duration: .35s;
       -o-transition-duration: .35s;
          transition-duration: .35s;
  -webkit-transition-property: height, visibility;
       -o-transition-property: height, visibility;
          transition-property: height, visibility;
}
.'.CSS_PREFIX.'caret {
  display: inline-block;
  width: 0;
  height: 0;
  margin-left: 2px;
  vertical-align: middle;
  border-top: 4px dashed;
  border-top: 4px solid \9;
  border-right: 4px solid transparent;
  border-left: 4px solid transparent;
}
.'.CSS_PREFIX.'dropup,
.'.CSS_PREFIX.'dropdown {
  position: relative;
}
.'.CSS_PREFIX.'dropdown-toggle:focus {
  outline: 0;
}
.'.CSS_PREFIX.'dropdown-menu {
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 1000;
  display: none;
  float: left;
  min-width: 160px;
  padding: 5px 0;
  margin: 2px 0 0;
  font-size: 14px;
  text-align: left;
  list-style: none;
  background-color: #fff;
  -webkit-background-clip: padding-box;
          background-clip: padding-box;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, .15);
  border-radius: 4px;
  -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
          box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
}
.'.CSS_PREFIX.'dropdown-menu.'.CSS_PREFIX.'pull-right {
  right: 0;
  left: auto;
}
.'.CSS_PREFIX.'dropdown-menu .'.CSS_PREFIX.'divider {
  height: 1px;
  margin: 9px 0;
  overflow: hidden;
  background-color: #e5e5e5;
}
.'.CSS_PREFIX.'dropdown-menu > li > a {
  display: block;
  padding: 3px 20px;
  clear: both;
  font-weight: normal;
  line-height: 1.42857143;
  color: #333;
  white-space: nowrap;
}
.'.CSS_PREFIX.'dropdown-menu > li > a:hover,
.'.CSS_PREFIX.'dropdown-menu > li > a:focus {
  color: #262626;
  text-decoration: none;
  background-color: #f5f5f5;
}
.'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'active > a,
.'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'active > a:hover,
.'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'active > a:focus {
  color: #fff;
  text-decoration: none;
  background-color: #337ab7;
  outline: 0;
}
.'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'disabled > a,
.'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'disabled > a:hover,
.'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'disabled > a:focus {
  color: #777;
}
.'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'disabled > a:hover,
.'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'disabled > a:focus {
  text-decoration: none;
  cursor: not-allowed;
  background-color: transparent;
  background-image: none;
  filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
}
.'.CSS_PREFIX.'open > .'.CSS_PREFIX.'dropdown-menu {
  display: block;
}
.'.CSS_PREFIX.'open > a {
  outline: 0;
}
.'.CSS_PREFIX.'dropdown-menu-right {
  right: 0;
  left: auto;
}
.'.CSS_PREFIX.'dropdown-menu-left {
  right: auto;
  left: 0;
}
.'.CSS_PREFIX.'dropdown-header {
  display: block;
  padding: 3px 20px;
  font-size: 12px;
  line-height: 1.42857143;
  color: #777;
  white-space: nowrap;
}
.'.CSS_PREFIX.'dropdown-backdrop {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 990;
}
.'.CSS_PREFIX.'pull-right > .'.CSS_PREFIX.'dropdown-menu {
  right: 0;
  left: auto;
}
.'.CSS_PREFIX.'dropup .'.CSS_PREFIX.'caret,
.'.CSS_PREFIX.'navbar-fixed-bottom .'.CSS_PREFIX.'dropdown .'.CSS_PREFIX.'caret {
  content: "";
  border-top: 0;
  border-bottom: 4px dashed;
  border-bottom: 4px solid \9;
}
.'.CSS_PREFIX.'dropup .'.CSS_PREFIX.'dropdown-menu,
.'.CSS_PREFIX.'navbar-fixed-bottom .'.CSS_PREFIX.'dropdown .'.CSS_PREFIX.'dropdown-menu {
  top: auto;
  bottom: 100%;
  margin-bottom: 2px;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'navbar-right .'.CSS_PREFIX.'dropdown-menu {
    right: 0;
    left: auto;
  }
  .'.CSS_PREFIX.'navbar-right .'.CSS_PREFIX.'dropdown-menu-left {
    right: auto;
    left: 0;
  }
}
.'.CSS_PREFIX.'btn-group,
.'.CSS_PREFIX.'btn-group-vertical {
  position: relative;
  display: inline-block;
  vertical-align: middle;
}
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn {
  position: relative;
  float: left;
}
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn:hover,
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn:hover,
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn:focus,
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn:focus,
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn:active,
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn:active,
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn.'.CSS_PREFIX.'active {
  z-index: 2;
}
.'.CSS_PREFIX.'btn-group .'.CSS_PREFIX.'btn + .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'btn-group .'.CSS_PREFIX.'btn + .'.CSS_PREFIX.'btn-group,
.'.CSS_PREFIX.'btn-group .'.CSS_PREFIX.'btn-group + .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'btn-group .'.CSS_PREFIX.'btn-group + .'.CSS_PREFIX.'btn-group {
  margin-left: -1px;
}
.'.CSS_PREFIX.'btn-toolbar {
  margin-left: -5px;
}
.'.CSS_PREFIX.'btn-toolbar .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'btn-toolbar .'.CSS_PREFIX.'btn-group,
.'.CSS_PREFIX.'btn-toolbar .'.CSS_PREFIX.'input-group {
  float: left;
}
.'.CSS_PREFIX.'btn-toolbar > .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'btn-toolbar > .'.CSS_PREFIX.'btn-group,
.'.CSS_PREFIX.'btn-toolbar > .'.CSS_PREFIX.'input-group {
  margin-left: 5px;
}
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn:not(:first-child):not(:last-child):not(.'.CSS_PREFIX.'dropdown-toggle) {
  border-radius: 0;
}
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn:first-child {
  margin-left: 0;
}
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn:first-child:not(:last-child):not(.'.CSS_PREFIX.'dropdown-toggle) {
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
}
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn:last-child:not(:first-child),
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'dropdown-toggle:not(:first-child) {
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
}
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn-group {
  float: left;
}
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn-group:not(:first-child):not(:last-child) > .'.CSS_PREFIX.'btn {
  border-radius: 0;
}
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn-group:first-child:not(:last-child) > .'.CSS_PREFIX.'btn:last-child,
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn-group:first-child:not(:last-child) > .'.CSS_PREFIX.'dropdown-toggle {
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
}
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn-group:last-child:not(:first-child) > .'.CSS_PREFIX.'btn:first-child {
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
}
.'.CSS_PREFIX.'btn-group .'.CSS_PREFIX.'dropdown-toggle:active,
.'.CSS_PREFIX.'btn-group.'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-toggle {
  outline: 0;
}
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn + .'.CSS_PREFIX.'dropdown-toggle {
  padding-right: 8px;
  padding-left: 8px;
}
.'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn-lg + .'.CSS_PREFIX.'dropdown-toggle {
  padding-right: 12px;
  padding-left: 12px;
}
.'.CSS_PREFIX.'btn-group.'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-toggle {
  -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
          box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
}
.'.CSS_PREFIX.'btn-group.'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-toggle.'.CSS_PREFIX.'btn-link {
  -webkit-box-shadow: none;
          box-shadow: none;
}
.'.CSS_PREFIX.'btn .'.CSS_PREFIX.'caret {
  margin-left: 0;
}
.'.CSS_PREFIX.'btn-lg .'.CSS_PREFIX.'caret {
  border-width: 5px 5px 0;
  border-bottom-width: 0;
}
.'.CSS_PREFIX.'dropup .'.CSS_PREFIX.'btn-lg .'.CSS_PREFIX.'caret {
  border-width: 0 5px 5px;
}
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn-group,
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn {
  display: block;
  float: none;
  width: 100%;
  max-width: 100%;
}
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn {
  float: none;
}
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn + .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn + .'.CSS_PREFIX.'btn-group,
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn-group + .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn-group + .'.CSS_PREFIX.'btn-group {
  margin-top: -1px;
  margin-left: 0;
}
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn:not(:first-child):not(:last-child) {
  border-radius: 0;
}
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn:first-child:not(:last-child) {
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn:last-child:not(:first-child) {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
}
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn-group:not(:first-child):not(:last-child) > .'.CSS_PREFIX.'btn {
  border-radius: 0;
}
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn-group:first-child:not(:last-child) > .'.CSS_PREFIX.'btn:last-child,
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn-group:first-child:not(:last-child) > .'.CSS_PREFIX.'dropdown-toggle {
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn-group:last-child:not(:first-child) > .'.CSS_PREFIX.'btn:first-child {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
.'.CSS_PREFIX.'btn-group-justified {
  display: table;
  width: 100%;
  table-layout: fixed;
  border-collapse: separate;
}
.'.CSS_PREFIX.'btn-group-justified > .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'btn-group-justified > .'.CSS_PREFIX.'btn-group {
  display: table-cell;
  float: none;
  width: 1%;
}
.'.CSS_PREFIX.'btn-group-justified > .'.CSS_PREFIX.'btn-group .'.CSS_PREFIX.'btn {
  width: 100%;
}
.'.CSS_PREFIX.'btn-group-justified > .'.CSS_PREFIX.'btn-group .'.CSS_PREFIX.'dropdown-menu {
  left: auto;
}
[data-toggle="buttons"] > .'.CSS_PREFIX.'btn input[type="radio"],
[data-toggle="buttons"] > .'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn input[type="radio"],
[data-toggle="buttons"] > .'.CSS_PREFIX.'btn input[type="checkbox"],
[data-toggle="buttons"] > .'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn input[type="checkbox"] {
  position: absolute;
  clip: rect(0, 0, 0, 0);
  pointer-events: none;
}
.'.CSS_PREFIX.'input-group {
  position: relative;
  display: table;
  border-collapse: separate;
}
.'.CSS_PREFIX.'input-group[class*="'.CSS_PREFIX.'col-"] {
  float: none;
  padding-right: 0;
  padding-left: 0;
}
.'.CSS_PREFIX.'input-group .'.CSS_PREFIX.'form-control {
  position: relative;
  z-index: 2;
  float: left;
  width: 100%;
  margin-bottom: 0;
}
.'.CSS_PREFIX.'input-group .'.CSS_PREFIX.'form-control:focus {
  z-index: 3;
}
.'.CSS_PREFIX.'input-group-lg > .'.CSS_PREFIX.'form-control,
.'.CSS_PREFIX.'input-group-lg > .'.CSS_PREFIX.'input-group-addon,
.'.CSS_PREFIX.'input-group-lg > .'.CSS_PREFIX.'input-group-btn > .'.CSS_PREFIX.'btn {
  height: 46px;
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
  border-radius: 6px;
}
select.'.CSS_PREFIX.'input-group-lg > .'.CSS_PREFIX.'form-control,
select.'.CSS_PREFIX.'input-group-lg > .'.CSS_PREFIX.'input-group-addon,
select.'.CSS_PREFIX.'input-group-lg > .'.CSS_PREFIX.'input-group-btn > .'.CSS_PREFIX.'btn {
  height: 46px;
  line-height: 46px;
}
textarea.'.CSS_PREFIX.'input-group-lg > .'.CSS_PREFIX.'form-control,
textarea.'.CSS_PREFIX.'input-group-lg > .'.CSS_PREFIX.'input-group-addon,
textarea.'.CSS_PREFIX.'input-group-lg > .'.CSS_PREFIX.'input-group-btn > .'.CSS_PREFIX.'btn,
select[multiple].'.CSS_PREFIX.'input-group-lg > .'.CSS_PREFIX.'form-control,
select[multiple].'.CSS_PREFIX.'input-group-lg > .'.CSS_PREFIX.'input-group-addon,
select[multiple].'.CSS_PREFIX.'input-group-lg > .'.CSS_PREFIX.'input-group-btn > .'.CSS_PREFIX.'btn {
  height: auto;
}
.'.CSS_PREFIX.'input-group-sm > .'.CSS_PREFIX.'form-control,
.'.CSS_PREFIX.'input-group-sm > .'.CSS_PREFIX.'input-group-addon,
.'.CSS_PREFIX.'input-group-sm > .'.CSS_PREFIX.'input-group-btn > .'.CSS_PREFIX.'btn {
  height: 30px;
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
  border-radius: 3px;
}
select.'.CSS_PREFIX.'input-group-sm > .'.CSS_PREFIX.'form-control,
select.'.CSS_PREFIX.'input-group-sm > .'.CSS_PREFIX.'input-group-addon,
select.'.CSS_PREFIX.'input-group-sm > .'.CSS_PREFIX.'input-group-btn > .'.CSS_PREFIX.'btn {
  height: 30px;
  line-height: 30px;
}
textarea.'.CSS_PREFIX.'input-group-sm > .'.CSS_PREFIX.'form-control,
textarea.'.CSS_PREFIX.'input-group-sm > .'.CSS_PREFIX.'input-group-addon,
textarea.'.CSS_PREFIX.'input-group-sm > .'.CSS_PREFIX.'input-group-btn > .'.CSS_PREFIX.'btn,
select[multiple].'.CSS_PREFIX.'input-group-sm > .'.CSS_PREFIX.'form-control,
select[multiple].'.CSS_PREFIX.'input-group-sm > .'.CSS_PREFIX.'input-group-addon,
select[multiple].'.CSS_PREFIX.'input-group-sm > .'.CSS_PREFIX.'input-group-btn > .'.CSS_PREFIX.'btn {
  height: auto;
}
.'.CSS_PREFIX.'input-group-addon,
.'.CSS_PREFIX.'input-group-btn,
.'.CSS_PREFIX.'input-group .'.CSS_PREFIX.'form-control {
  display: table-cell;
}
.'.CSS_PREFIX.'input-group-addon:not(:first-child):not(:last-child),
.'.CSS_PREFIX.'input-group-btn:not(:first-child):not(:last-child),
.'.CSS_PREFIX.'input-group .'.CSS_PREFIX.'form-control:not(:first-child):not(:last-child) {
  border-radius: 0;
}
.'.CSS_PREFIX.'input-group-addon,
.'.CSS_PREFIX.'input-group-btn {
  width: 1%;
  white-space: nowrap;
  vertical-align: middle;
}
.'.CSS_PREFIX.'input-group-addon {
  padding: 6px 12px;
  font-size: 14px;
  font-weight: normal;
  line-height: 1;
  color: #555;
  text-align: center;
  background-color: #eee;
  border: 1px solid #ccc;
  border-radius: 4px;
}
.'.CSS_PREFIX.'input-group-addon.'.CSS_PREFIX.'input-sm {
  padding: 5px 10px;
  font-size: 12px;
  border-radius: 3px;
}
.'.CSS_PREFIX.'input-group-addon.'.CSS_PREFIX.'input-lg {
  padding: 10px 16px;
  font-size: 18px;
  border-radius: 6px;
}
.'.CSS_PREFIX.'input-group-addon input[type="radio"],
.'.CSS_PREFIX.'input-group-addon input[type="checkbox"] {
  margin-top: 0;
}
.'.CSS_PREFIX.'input-group .'.CSS_PREFIX.'form-control:first-child,
.'.CSS_PREFIX.'input-group-addon:first-child,
.'.CSS_PREFIX.'input-group-btn:first-child > .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'input-group-btn:first-child > .'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'input-group-btn:first-child > .'.CSS_PREFIX.'dropdown-toggle,
.'.CSS_PREFIX.'input-group-btn:last-child > .'.CSS_PREFIX.'btn:not(:last-child):not(.'.CSS_PREFIX.'dropdown-toggle),
.'.CSS_PREFIX.'input-group-btn:last-child > .'.CSS_PREFIX.'btn-group:not(:last-child) > .'.CSS_PREFIX.'btn {
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
}
.'.CSS_PREFIX.'input-group-addon:first-child {
  border-right: 0;
}
.'.CSS_PREFIX.'input-group .'.CSS_PREFIX.'form-control:last-child,
.'.CSS_PREFIX.'input-group-addon:last-child,
.'.CSS_PREFIX.'input-group-btn:last-child > .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'input-group-btn:last-child > .'.CSS_PREFIX.'btn-group > .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'input-group-btn:last-child > .'.CSS_PREFIX.'dropdown-toggle,
.'.CSS_PREFIX.'input-group-btn:first-child > .'.CSS_PREFIX.'btn:not(:first-child),
.'.CSS_PREFIX.'input-group-btn:first-child > .'.CSS_PREFIX.'btn-group:not(:first-child) > .'.CSS_PREFIX.'btn {
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
}
.'.CSS_PREFIX.'input-group-addon:last-child {
  border-left: 0;
}
.'.CSS_PREFIX.'input-group-btn {
  position: relative;
  font-size: 0;
  white-space: nowrap;
}
.'.CSS_PREFIX.'input-group-btn > .'.CSS_PREFIX.'btn {
  position: relative;
}
.'.CSS_PREFIX.'input-group-btn > .'.CSS_PREFIX.'btn + .'.CSS_PREFIX.'btn {
  margin-left: -1px;
}
.'.CSS_PREFIX.'input-group-btn > .'.CSS_PREFIX.'btn:hover,
.'.CSS_PREFIX.'input-group-btn > .'.CSS_PREFIX.'btn:focus,
.'.CSS_PREFIX.'input-group-btn > .'.CSS_PREFIX.'btn:active {
  z-index: 2;
}
.'.CSS_PREFIX.'input-group-btn:first-child > .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'input-group-btn:first-child > .'.CSS_PREFIX.'btn-group {
  margin-right: -1px;
}
.'.CSS_PREFIX.'input-group-btn:last-child > .'.CSS_PREFIX.'btn,
.'.CSS_PREFIX.'input-group-btn:last-child > .'.CSS_PREFIX.'btn-group {
  z-index: 2;
  margin-left: -1px;
}
.'.CSS_PREFIX.'nav {
  padding-left: 0;
  margin-bottom: 0;
  list-style: none;
}
.'.CSS_PREFIX.'nav > li {
  position: relative;
  display: block;
}
.'.CSS_PREFIX.'nav > li > a {
  position: relative;
  display: block;
  padding: 10px 15px;
}
.'.CSS_PREFIX.'nav > li > a:hover,
.'.CSS_PREFIX.'nav > li > a:focus {
  text-decoration: none;
  background-color: #eee;
}
.'.CSS_PREFIX.'nav > li.'.CSS_PREFIX.'disabled > a {
  color: #777;
}
.'.CSS_PREFIX.'nav > li.'.CSS_PREFIX.'disabled > a:hover,
.'.CSS_PREFIX.'nav > li.'.CSS_PREFIX.'disabled > a:focus {
  color: #777;
  text-decoration: none;
  cursor: not-allowed;
  background-color: transparent;
}
.'.CSS_PREFIX.'nav .'.CSS_PREFIX.'open > a,
.'.CSS_PREFIX.'nav .'.CSS_PREFIX.'open > a:hover,
.'.CSS_PREFIX.'nav .'.CSS_PREFIX.'open > a:focus {
  background-color: #eee;
  border-color: #337ab7;
}
.'.CSS_PREFIX.'nav .'.CSS_PREFIX.'nav-divider {
  height: 1px;
  margin: 9px 0;
  overflow: hidden;
  background-color: #e5e5e5;
}
.'.CSS_PREFIX.'nav > li > a > img {
  max-width: none;
}
.'.CSS_PREFIX.'nav-tabs {
  border-bottom: 1px solid #ddd;
}
.'.CSS_PREFIX.'nav-tabs > li {
  float: left;
  margin-bottom: -1px;
}
.'.CSS_PREFIX.'nav-tabs > li > a {
  margin-right: 2px;
  line-height: 1.42857143;
  border: 1px solid transparent;
  border-radius: 4px 4px 0 0;
}
.'.CSS_PREFIX.'nav-tabs > li > a:hover {
  border-color: #eee #eee #ddd;
}
.'.CSS_PREFIX.'nav-tabs > li.'.CSS_PREFIX.'active > a,
.'.CSS_PREFIX.'nav-tabs > li.'.CSS_PREFIX.'active > a:hover,
.'.CSS_PREFIX.'nav-tabs > li.'.CSS_PREFIX.'active > a:focus {
  color: #555;
  cursor: default;
  background-color: #fff;
  border: 1px solid #ddd;
  border-bottom-color: transparent;
}
.'.CSS_PREFIX.'nav-tabs.'.CSS_PREFIX.'nav-justified {
  width: 100%;
  border-bottom: 0;
}
.'.CSS_PREFIX.'nav-tabs.'.CSS_PREFIX.'nav-justified > li {
  float: none;
}
.'.CSS_PREFIX.'nav-tabs.'.CSS_PREFIX.'nav-justified > li > a {
  margin-bottom: 5px;
  text-align: center;
}
.'.CSS_PREFIX.'nav-tabs.'.CSS_PREFIX.'nav-justified > .'.CSS_PREFIX.'dropdown .'.CSS_PREFIX.'dropdown-menu {
  top: auto;
  left: auto;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'nav-tabs.'.CSS_PREFIX.'nav-justified > li {
    display: table-cell;
    width: 1%;
  }
  .'.CSS_PREFIX.'nav-tabs.'.CSS_PREFIX.'nav-justified > li > a {
    margin-bottom: 0;
  }
}
.'.CSS_PREFIX.'nav-tabs.'.CSS_PREFIX.'nav-justified > li > a {
  margin-right: 0;
  border-radius: 4px;
}
.'.CSS_PREFIX.'nav-tabs.'.CSS_PREFIX.'nav-justified > .'.CSS_PREFIX.'active > a,
.'.CSS_PREFIX.'nav-tabs.'.CSS_PREFIX.'nav-justified > .'.CSS_PREFIX.'active > a:hover,
.'.CSS_PREFIX.'nav-tabs.'.CSS_PREFIX.'nav-justified > .'.CSS_PREFIX.'active > a:focus {
  border: 1px solid #ddd;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'nav-tabs.'.CSS_PREFIX.'nav-justified > li > a {
    border-bottom: 1px solid #ddd;
    border-radius: 4px 4px 0 0;
  }
  .'.CSS_PREFIX.'nav-tabs.'.CSS_PREFIX.'nav-justified > .'.CSS_PREFIX.'active > a,
  .'.CSS_PREFIX.'nav-tabs.'.CSS_PREFIX.'nav-justified > .'.CSS_PREFIX.'active > a:hover,
  .'.CSS_PREFIX.'nav-tabs.'.CSS_PREFIX.'nav-justified > .'.CSS_PREFIX.'active > a:focus {
    border-bottom-color: #fff;
  }
}
.'.CSS_PREFIX.'nav-pills > li {
  float: left;
}
.'.CSS_PREFIX.'nav-pills > li > a {
  border-radius: 4px;
}
.'.CSS_PREFIX.'nav-pills > li + li {
  margin-left: 2px;
}
.'.CSS_PREFIX.'nav-pills > li.'.CSS_PREFIX.'active > a,
.'.CSS_PREFIX.'nav-pills > li.'.CSS_PREFIX.'active > a:hover,
.'.CSS_PREFIX.'nav-pills > li.'.CSS_PREFIX.'active > a:focus {
  color: #fff;
  background-color: #337ab7;
}
.'.CSS_PREFIX.'nav-stacked > li {
  float: none;
}
.'.CSS_PREFIX.'nav-stacked > li + li {
  margin-top: 2px;
  margin-left: 0;
}
.'.CSS_PREFIX.'nav-justified {
  width: 100%;
}
.'.CSS_PREFIX.'nav-justified > li {
  float: none;
}
.'.CSS_PREFIX.'nav-justified > li > a {
  margin-bottom: 5px;
  text-align: center;
}
.'.CSS_PREFIX.'nav-justified > .'.CSS_PREFIX.'dropdown .'.CSS_PREFIX.'dropdown-menu {
  top: auto;
  left: auto;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'nav-justified > li {
    display: table-cell;
    width: 1%;
  }
  .'.CSS_PREFIX.'nav-justified > li > a {
    margin-bottom: 0;
  }
}
.'.CSS_PREFIX.'nav-tabs-justified {
  border-bottom: 0;
}
.'.CSS_PREFIX.'nav-tabs-justified > li > a {
  margin-right: 0;
  border-radius: 4px;
}
.'.CSS_PREFIX.'nav-tabs-justified > .'.CSS_PREFIX.'active > a,
.'.CSS_PREFIX.'nav-tabs-justified > .'.CSS_PREFIX.'active > a:hover,
.'.CSS_PREFIX.'nav-tabs-justified > .'.CSS_PREFIX.'active > a:focus {
  border: 1px solid #ddd;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'nav-tabs-justified > li > a {
    border-bottom: 1px solid #ddd;
    border-radius: 4px 4px 0 0;
  }
  .'.CSS_PREFIX.'nav-tabs-justified > .'.CSS_PREFIX.'active > a,
  .'.CSS_PREFIX.'nav-tabs-justified > .'.CSS_PREFIX.'active > a:hover,
  .'.CSS_PREFIX.'nav-tabs-justified > .'.CSS_PREFIX.'active > a:focus {
    border-bottom-color: #fff;
  }
}
.'.CSS_PREFIX.'tab-content > .'.CSS_PREFIX.'tab-pane {
  display: none;
}
.'.CSS_PREFIX.'tab-content > .'.CSS_PREFIX.'active {
  display: block;
}
.'.CSS_PREFIX.'nav-tabs .'.CSS_PREFIX.'dropdown-menu {
  margin-top: -1px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
.'.CSS_PREFIX.'navbar {
  position: relative;
  min-height: 50px;
  margin-bottom: 20px;
  border: 1px solid transparent;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'navbar {
    border-radius: 4px;
  }
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'navbar-header {
    float: left;
  }
}
.'.CSS_PREFIX.'navbar-collapse {
  padding-right: 15px;
  padding-left: 15px;
  overflow-x: visible;
  -webkit-overflow-scrolling: touch;
  border-top: 1px solid transparent;
  -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
          box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
}
.'.CSS_PREFIX.'navbar-collapse.'.CSS_PREFIX.'in {
  overflow-y: auto;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'navbar-collapse {
    width: auto;
    border-top: 0;
    -webkit-box-shadow: none;
            box-shadow: none;
  }
  .'.CSS_PREFIX.'navbar-collapse.'.CSS_PREFIX.'collapse {
    display: block !important;
    height: auto !important;
    padding-bottom: 0;
    overflow: visible !important;
  }
  .'.CSS_PREFIX.'navbar-collapse.'.CSS_PREFIX.'in {
    overflow-y: visible;
  }
  .'.CSS_PREFIX.'navbar-fixed-top .'.CSS_PREFIX.'navbar-collapse,
  .'.CSS_PREFIX.'navbar-static-top .'.CSS_PREFIX.'navbar-collapse,
  .'.CSS_PREFIX.'navbar-fixed-bottom .'.CSS_PREFIX.'navbar-collapse {
    padding-right: 0;
    padding-left: 0;
  }
}
.'.CSS_PREFIX.'navbar-fixed-top .'.CSS_PREFIX.'navbar-collapse,
.'.CSS_PREFIX.'navbar-fixed-bottom .'.CSS_PREFIX.'navbar-collapse {
  max-height: 340px;
}
@media (max-device-width: 480px) and (orientation: landscape) {
  .'.CSS_PREFIX.'navbar-fixed-top .'.CSS_PREFIX.'navbar-collapse,
  .'.CSS_PREFIX.'navbar-fixed-bottom .'.CSS_PREFIX.'navbar-collapse {
    max-height: 200px;
  }
}
.'.CSS_PREFIX.'container > .'.CSS_PREFIX.'navbar-header,
.'.CSS_PREFIX.'container-fluid > .'.CSS_PREFIX.'navbar-header,
.'.CSS_PREFIX.'container > .'.CSS_PREFIX.'navbar-collapse,
.'.CSS_PREFIX.'container-fluid > .'.CSS_PREFIX.'navbar-collapse {
  margin-right: -15px;
  margin-left: -15px;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'container > .'.CSS_PREFIX.'navbar-header,
  .'.CSS_PREFIX.'container-fluid > .'.CSS_PREFIX.'navbar-header,
  .'.CSS_PREFIX.'container > .'.CSS_PREFIX.'navbar-collapse,
  .'.CSS_PREFIX.'container-fluid > .'.CSS_PREFIX.'navbar-collapse {
    margin-right: 0;
    margin-left: 0;
  }
}
.'.CSS_PREFIX.'navbar-static-top {
  z-index: 1000;
  border-width: 0 0 1px;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'navbar-static-top {
    border-radius: 0;
  }
}
.'.CSS_PREFIX.'navbar-fixed-top,
.'.CSS_PREFIX.'navbar-fixed-bottom {
  position: fixed;
  right: 0;
  left: 0;
  z-index: 1030;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'navbar-fixed-top,
  .'.CSS_PREFIX.'navbar-fixed-bottom {
    border-radius: 0;
  }
}
.'.CSS_PREFIX.'navbar-fixed-top {
  top: 0;
  border-width: 0 0 1px;
}
.'.CSS_PREFIX.'navbar-fixed-bottom {
  bottom: 0;
  margin-bottom: 0;
  border-width: 1px 0 0;
}
.'.CSS_PREFIX.'navbar-brand {
  float: left;
  height: 50px;
  padding: 15px 15px;
  font-size: 18px;
  line-height: 20px;
}
.'.CSS_PREFIX.'navbar-brand:hover,
.'.CSS_PREFIX.'navbar-brand:focus {
  text-decoration: none;
}
.'.CSS_PREFIX.'navbar-brand > img {
  display: block;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'navbar > .'.CSS_PREFIX.'container .'.CSS_PREFIX.'navbar-brand,
  .'.CSS_PREFIX.'navbar > .'.CSS_PREFIX.'container-fluid .'.CSS_PREFIX.'navbar-brand {
    margin-left: -15px;
  }
}
.'.CSS_PREFIX.'navbar-toggle {
  position: relative;
  float: right;
  padding: 9px 10px;
  margin-top: 8px;
  margin-right: 15px;
  margin-bottom: 8px;
  background-color: transparent;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 4px;
}
.'.CSS_PREFIX.'navbar-toggle:focus {
  outline: 0;
}
.'.CSS_PREFIX.'navbar-toggle .'.CSS_PREFIX.'icon-bar {
  display: block;
  width: 22px;
  height: 2px;
  border-radius: 1px;
}
.'.CSS_PREFIX.'navbar-toggle .'.CSS_PREFIX.'icon-bar + .'.CSS_PREFIX.'icon-bar {
  margin-top: 4px;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'navbar-toggle {
    display: none;
  }
}
.'.CSS_PREFIX.'navbar-nav {
  margin: 7.5px -15px;
}
.'.CSS_PREFIX.'navbar-nav > li > a {
  padding-top: 10px;
  padding-bottom: 10px;
  line-height: 20px;
}
@media (max-width: 767px) {
  .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu {
    position: static;
    float: none;
    width: auto;
    margin-top: 0;
    background-color: transparent;
    border: 0;
    -webkit-box-shadow: none;
            box-shadow: none;
  }
  .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > li > a,
  .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu .'.CSS_PREFIX.'dropdown-header {
    padding: 5px 15px 5px 25px;
  }
  .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > li > a {
    line-height: 20px;
  }
  .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > li > a:hover,
  .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > li > a:focus {
    background-image: none;
  }
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'navbar-nav {
    float: left;
    margin: 0;
  }
  .'.CSS_PREFIX.'navbar-nav > li {
    float: left;
  }
  .'.CSS_PREFIX.'navbar-nav > li > a {
    padding-top: 15px;
    padding-bottom: 15px;
  }
}
.'.CSS_PREFIX.'navbar-form {
  padding: 10px 15px;
  margin-top: 8px;
  margin-right: -15px;
  margin-bottom: 8px;
  margin-left: -15px;
  border-top: 1px solid transparent;
  border-bottom: 1px solid transparent;
  -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1), 0 1px 0 rgba(255, 255, 255, .1);
          box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1), 0 1px 0 rgba(255, 255, 255, .1);
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'form-group {
    display: inline-block;
    margin-bottom: 0;
    vertical-align: middle;
  }
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'form-control {
    display: inline-block;
    width: auto;
    vertical-align: middle;
  }
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'form-control-static {
    display: inline-block;
  }
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'input-group {
    display: inline-table;
    vertical-align: middle;
  }
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'input-group .'.CSS_PREFIX.'input-group-addon,
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'input-group .'.CSS_PREFIX.'input-group-btn,
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'input-group .'.CSS_PREFIX.'form-control {
    width: auto;
  }
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'input-group > .'.CSS_PREFIX.'form-control {
    width: 100%;
  }
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'control-label {
    margin-bottom: 0;
    vertical-align: middle;
  }
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'radio,
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'checkbox {
    display: inline-block;
    margin-top: 0;
    margin-bottom: 0;
    vertical-align: middle;
  }
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'radio label,
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'checkbox label {
    padding-left: 0;
  }
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'radio input[type="radio"],
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'checkbox input[type="checkbox"] {
    position: relative;
    margin-left: 0;
  }
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'has-feedback .'.CSS_PREFIX.'form-control-feedback {
    top: 0;
  }
}
@media (max-width: 767px) {
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'form-group {
    margin-bottom: 5px;
  }
  .'.CSS_PREFIX.'navbar-form .'.CSS_PREFIX.'form-group:last-child {
    margin-bottom: 0;
  }
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'navbar-form {
    width: auto;
    padding-top: 0;
    padding-bottom: 0;
    margin-right: 0;
    margin-left: 0;
    border: 0;
    -webkit-box-shadow: none;
            box-shadow: none;
  }
}
.'.CSS_PREFIX.'navbar-nav > li > .'.CSS_PREFIX.'dropdown-menu {
  margin-top: 0;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
.'.CSS_PREFIX.'navbar-fixed-bottom .'.CSS_PREFIX.'navbar-nav > li > .'.CSS_PREFIX.'dropdown-menu {
  margin-bottom: 0;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.'.CSS_PREFIX.'navbar-btn {
  margin-top: 8px;
  margin-bottom: 8px;
}
.'.CSS_PREFIX.'navbar-btn.'.CSS_PREFIX.'btn-sm {
  margin-top: 10px;
  margin-bottom: 10px;
}
.'.CSS_PREFIX.'navbar-btn.'.CSS_PREFIX.'btn-xs {
  margin-top: 14px;
  margin-bottom: 14px;
}
.'.CSS_PREFIX.'navbar-text {
  margin-top: 15px;
  margin-bottom: 15px;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'navbar-text {
    float: left;
    margin-right: 15px;
    margin-left: 15px;
  }
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'navbar-left {
    float: left !important;
  }
  .'.CSS_PREFIX.'navbar-right {
    float: right !important;
    margin-right: -15px;
  }
  .'.CSS_PREFIX.'navbar-right ~ .'.CSS_PREFIX.'navbar-right {
    margin-right: 0;
  }
}
.'.CSS_PREFIX.'navbar-default {
  background-color: #f8f8f8;
  border-color: #e7e7e7;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-brand {
  color: #777;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-brand:hover,
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-brand:focus {
  color: #5e5e5e;
  background-color: transparent;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-text {
  color: #777;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav > li > a {
  color: #777;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav > li > a:hover,
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav > li > a:focus {
  color: #333;
  background-color: transparent;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'active > a,
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'active > a:hover,
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'active > a:focus {
  color: #555;
  background-color: #e7e7e7;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'disabled > a,
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'disabled > a:hover,
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'disabled > a:focus {
  color: #ccc;
  background-color: transparent;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-toggle {
  border-color: #ddd;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-toggle:hover,
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-toggle:focus {
  background-color: #ddd;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-toggle .'.CSS_PREFIX.'icon-bar {
  background-color: #888;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-collapse,
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-form {
  border-color: #e7e7e7;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'open > a,
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'open > a:hover,
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'open > a:focus {
  color: #555;
  background-color: #e7e7e7;
}
@media (max-width: 767px) {
  .'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > li > a {
    color: #777;
  }
  .'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > li > a:hover,
  .'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > li > a:focus {
    color: #333;
    background-color: transparent;
  }
  .'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'active > a,
  .'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'active > a:hover,
  .'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'active > a:focus {
    color: #555;
    background-color: #e7e7e7;
  }
  .'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'disabled > a,
  .'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'disabled > a:hover,
  .'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'disabled > a:focus {
    color: #ccc;
    background-color: transparent;
  }
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-link {
  color: #777;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'navbar-link:hover {
  color: #333;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'btn-link {
  color: #777;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'btn-link:hover,
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'btn-link:focus {
  color: #333;
}
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'btn-link[disabled]:hover,
fieldset[disabled] .'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'btn-link:hover,
.'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'btn-link[disabled]:focus,
fieldset[disabled] .'.CSS_PREFIX.'navbar-default .'.CSS_PREFIX.'btn-link:focus {
  color: #ccc;
}
.'.CSS_PREFIX.'navbar-inverse {
  background-color: #222;
  border-color: #080808;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-brand {
  color: #9d9d9d;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-brand:hover,
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-brand:focus {
  color: #fff;
  background-color: transparent;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-text {
  color: #9d9d9d;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav > li > a {
  color: #9d9d9d;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav > li > a:hover,
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav > li > a:focus {
  color: #fff;
  background-color: transparent;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'active > a,
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'active > a:hover,
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'active > a:focus {
  color: #fff;
  background-color: #080808;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'disabled > a,
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'disabled > a:hover,
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'disabled > a:focus {
  color: #444;
  background-color: transparent;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-toggle {
  border-color: #333;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-toggle:hover,
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-toggle:focus {
  background-color: #333;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-toggle .'.CSS_PREFIX.'icon-bar {
  background-color: #fff;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-collapse,
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-form {
  border-color: #101010;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'open > a,
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'open > a:hover,
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav > .'.CSS_PREFIX.'open > a:focus {
  color: #fff;
  background-color: #080808;
}
@media (max-width: 767px) {
  .'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'dropdown-header {
    border-color: #080808;
  }
  .'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu .'.CSS_PREFIX.'divider {
    background-color: #080808;
  }
  .'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > li > a {
    color: #9d9d9d;
  }
  .'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > li > a:hover,
  .'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > li > a:focus {
    color: #fff;
    background-color: transparent;
  }
  .'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'active > a,
  .'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'active > a:hover,
  .'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'active > a:focus {
    color: #fff;
    background-color: #080808;
  }
  .'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'disabled > a,
  .'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'disabled > a:hover,
  .'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-nav .'.CSS_PREFIX.'open .'.CSS_PREFIX.'dropdown-menu > .'.CSS_PREFIX.'disabled > a:focus {
    color: #444;
    background-color: transparent;
  }
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-link {
  color: #9d9d9d;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'navbar-link:hover {
  color: #fff;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'btn-link {
  color: #9d9d9d;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'btn-link:hover,
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'btn-link:focus {
  color: #fff;
}
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'btn-link[disabled]:hover,
fieldset[disabled] .'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'btn-link:hover,
.'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'btn-link[disabled]:focus,
fieldset[disabled] .'.CSS_PREFIX.'navbar-inverse .'.CSS_PREFIX.'btn-link:focus {
  color: #444;
}
.'.CSS_PREFIX.'breadcrumb {
  padding: 8px 15px;
  margin-bottom: 20px;
  list-style: none;
  background-color: #f5f5f5;
  border-radius: 4px;
}
.'.CSS_PREFIX.'breadcrumb > li {
  display: inline-block;
}
.'.CSS_PREFIX.'breadcrumb > li + li:before {
  padding: 0 5px;
  color: #ccc;
  content: "/\00a0";
}
.'.CSS_PREFIX.'breadcrumb > .'.CSS_PREFIX.'active {
  color: #777;
}
.'.CSS_PREFIX.'pagination {
  display: inline-block;
  padding-left: 0;
  margin: 20px 0;
  border-radius: 4px;
}
.'.CSS_PREFIX.'pagination > li {
  display: inline;
}
.'.CSS_PREFIX.'pagination > li > a,
.'.CSS_PREFIX.'pagination > li > span {
  position: relative;
  float: left;
  padding: 6px 12px;
  margin-left: -1px;
  line-height: 1.42857143;
  color: #337ab7;
  text-decoration: none;
  background-color: #fff;
  border: 1px solid #ddd;
}
.'.CSS_PREFIX.'pagination > li:first-child > a,
.'.CSS_PREFIX.'pagination > li:first-child > span {
  margin-left: 0;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.'.CSS_PREFIX.'pagination > li:last-child > a,
.'.CSS_PREFIX.'pagination > li:last-child > span {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
}
.'.CSS_PREFIX.'pagination > li > a:hover,
.'.CSS_PREFIX.'pagination > li > span:hover,
.'.CSS_PREFIX.'pagination > li > a:focus,
.'.CSS_PREFIX.'pagination > li > span:focus {
  z-index: 2;
  color: #23527c;
  background-color: #eee;
  border-color: #ddd;
}
.'.CSS_PREFIX.'pagination > .'.CSS_PREFIX.'active > a,
.'.CSS_PREFIX.'pagination > .'.CSS_PREFIX.'active > span,
.'.CSS_PREFIX.'pagination > .'.CSS_PREFIX.'active > a:hover,
.'.CSS_PREFIX.'pagination > .'.CSS_PREFIX.'active > span:hover,
.'.CSS_PREFIX.'pagination > .'.CSS_PREFIX.'active > a:focus,
.'.CSS_PREFIX.'pagination > .'.CSS_PREFIX.'active > span:focus {
  z-index: 3;
  color: #fff;
  cursor: default;
  background-color: #337ab7;
  border-color: #337ab7;
}
.'.CSS_PREFIX.'pagination > .'.CSS_PREFIX.'disabled > span,
.'.CSS_PREFIX.'pagination > .'.CSS_PREFIX.'disabled > span:hover,
.'.CSS_PREFIX.'pagination > .'.CSS_PREFIX.'disabled > span:focus,
.'.CSS_PREFIX.'pagination > .'.CSS_PREFIX.'disabled > a,
.'.CSS_PREFIX.'pagination > .'.CSS_PREFIX.'disabled > a:hover,
.'.CSS_PREFIX.'pagination > .'.CSS_PREFIX.'disabled > a:focus {
  color: #777;
  cursor: not-allowed;
  background-color: #fff;
  border-color: #ddd;
}
.'.CSS_PREFIX.'pagination-lg > li > a,
.'.CSS_PREFIX.'pagination-lg > li > span {
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
}
.'.CSS_PREFIX.'pagination-lg > li:first-child > a,
.'.CSS_PREFIX.'pagination-lg > li:first-child > span {
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
}
.'.CSS_PREFIX.'pagination-lg > li:last-child > a,
.'.CSS_PREFIX.'pagination-lg > li:last-child > span {
  border-top-right-radius: 6px;
  border-bottom-right-radius: 6px;
}
.'.CSS_PREFIX.'pagination-sm > li > a,
.'.CSS_PREFIX.'pagination-sm > li > span {
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
}
.'.CSS_PREFIX.'pagination-sm > li:first-child > a,
.'.CSS_PREFIX.'pagination-sm > li:first-child > span {
  border-top-left-radius: 3px;
  border-bottom-left-radius: 3px;
}
.'.CSS_PREFIX.'pagination-sm > li:last-child > a,
.'.CSS_PREFIX.'pagination-sm > li:last-child > span {
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px;
}
.'.CSS_PREFIX.'pager {
  padding-left: 0;
  margin: 20px 0;
  text-align: center;
  list-style: none;
}
.'.CSS_PREFIX.'pager li {
  display: inline;
}
.'.CSS_PREFIX.'pager li > a,
.'.CSS_PREFIX.'pager li > span {
  display: inline-block;
  padding: 5px 14px;
  background-color: #fff;
  border: 1px solid #ddd;
  border-radius: 15px;
}
.'.CSS_PREFIX.'pager li > a:hover,
.'.CSS_PREFIX.'pager li > a:focus {
  text-decoration: none;
  background-color: #eee;
}
.'.CSS_PREFIX.'pager .'.CSS_PREFIX.'next > a,
.'.CSS_PREFIX.'pager .'.CSS_PREFIX.'next > span {
  float: right;
}
.'.CSS_PREFIX.'pager .'.CSS_PREFIX.'previous > a,
.'.CSS_PREFIX.'pager .'.CSS_PREFIX.'previous > span {
  float: left;
}
.'.CSS_PREFIX.'pager .'.CSS_PREFIX.'disabled > a,
.'.CSS_PREFIX.'pager .'.CSS_PREFIX.'disabled > a:hover,
.'.CSS_PREFIX.'pager .'.CSS_PREFIX.'disabled > a:focus,
.'.CSS_PREFIX.'pager .'.CSS_PREFIX.'disabled > span {
  color: #777;
  cursor: not-allowed;
  background-color: #fff;
}
.'.CSS_PREFIX.'label {
  display: inline;
  padding: .2em .6em .3em;
  font-size: 75%;
  font-weight: bold;
  line-height: 1;
  color: #fff;
  text-align: center;
  white-space: nowrap;
  vertical-align: baseline;
  border-radius: .25em;
}
a.'.CSS_PREFIX.'label:hover,
a.'.CSS_PREFIX.'label:focus {
  color: #fff;
  text-decoration: none;
  cursor: pointer;
}
.'.CSS_PREFIX.'label:empty {
  display: none;
}
.'.CSS_PREFIX.'btn .'.CSS_PREFIX.'label {
  position: relative;
  top: -1px;
}
.'.CSS_PREFIX.'label-default {
  background-color: #777;
}
.'.CSS_PREFIX.'label-default[href]:hover,
.'.CSS_PREFIX.'label-default[href]:focus {
  background-color: #5e5e5e;
}
.'.CSS_PREFIX.'label-primary {
  background-color: #337ab7;
}
.'.CSS_PREFIX.'label-primary[href]:hover,
.'.CSS_PREFIX.'label-primary[href]:focus {
  background-color: #286090;
}
.'.CSS_PREFIX.'label-success {
  background-color: #5cb85c;
}
.'.CSS_PREFIX.'label-success[href]:hover,
.'.CSS_PREFIX.'label-success[href]:focus {
  background-color: #449d44;
}
.'.CSS_PREFIX.'label-info {
  background-color: #5bc0de;
}
.'.CSS_PREFIX.'label-info[href]:hover,
.'.CSS_PREFIX.'label-info[href]:focus {
  background-color: #31b0d5;
}
.'.CSS_PREFIX.'label-warning {
  background-color: #f0ad4e;
}
.'.CSS_PREFIX.'label-warning[href]:hover,
.'.CSS_PREFIX.'label-warning[href]:focus {
  background-color: #ec971f;
}
.'.CSS_PREFIX.'label-danger {
  background-color: #d9534f;
}
.'.CSS_PREFIX.'label-danger[href]:hover,
.'.CSS_PREFIX.'label-danger[href]:focus {
  background-color: #c9302c;
}
.'.CSS_PREFIX.'badge {
  display: inline-block;
  min-width: 10px;
  padding: 3px 7px;
  font-size: 12px;
  font-weight: bold;
  line-height: 1;
  color: #fff;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  background-color: #777;
  border-radius: 10px;
}
.'.CSS_PREFIX.'badge:empty {
  display: none;
}
.'.CSS_PREFIX.'btn .'.CSS_PREFIX.'badge {
  position: relative;
  top: -1px;
}
.'.CSS_PREFIX.'btn-xs .'.CSS_PREFIX.'badge,
.'.CSS_PREFIX.'btn-group-xs > .'.CSS_PREFIX.'btn .'.CSS_PREFIX.'badge {
  top: 0;
  padding: 1px 5px;
}
a.'.CSS_PREFIX.'badge:hover,
a.'.CSS_PREFIX.'badge:focus {
  color: #fff;
  text-decoration: none;
  cursor: pointer;
}
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active > .'.CSS_PREFIX.'badge,
.'.CSS_PREFIX.'nav-pills > .'.CSS_PREFIX.'active > a > .'.CSS_PREFIX.'badge {
  color: #337ab7;
  background-color: #fff;
}
.'.CSS_PREFIX.'list-group-item > .'.CSS_PREFIX.'badge {
  float: right;
}
.'.CSS_PREFIX.'list-group-item > .'.CSS_PREFIX.'badge + .'.CSS_PREFIX.'badge {
  margin-right: 5px;
}
.'.CSS_PREFIX.'nav-pills > li > a > .'.CSS_PREFIX.'badge {
  margin-left: 3px;
}
.'.CSS_PREFIX.'jumbotron {
  padding-top: 30px;
  padding-bottom: 30px;
  margin-bottom: 30px;
  color: inherit;
  background-color: #eee;
}
.'.CSS_PREFIX.'jumbotron h1,
.'.CSS_PREFIX.'jumbotron .'.CSS_PREFIX.'h1 {
  color: inherit;
}
.'.CSS_PREFIX.'jumbotron p {
  margin-bottom: 15px;
  font-size: 21px;
  font-weight: 200;
}
.'.CSS_PREFIX.'jumbotron > hr {
  border-top-color: #d5d5d5;
}
.'.CSS_PREFIX.'container .'.CSS_PREFIX.'jumbotron,
.'.CSS_PREFIX.'container-fluid .'.CSS_PREFIX.'jumbotron {
  padding-right: 15px;
  padding-left: 15px;
  border-radius: 6px;
}
.'.CSS_PREFIX.'jumbotron .'.CSS_PREFIX.'container {
  max-width: 100%;
}
@media screen and (min-width: 768px) {
  .'.CSS_PREFIX.'jumbotron {
    padding-top: 48px;
    padding-bottom: 48px;
  }
  .'.CSS_PREFIX.'container .'.CSS_PREFIX.'jumbotron,
  .'.CSS_PREFIX.'container-fluid .'.CSS_PREFIX.'jumbotron {
    padding-right: 60px;
    padding-left: 60px;
  }
  .'.CSS_PREFIX.'jumbotron h1,
  .'.CSS_PREFIX.'jumbotron .'.CSS_PREFIX.'h1 {
    font-size: 63px;
  }
}
.'.CSS_PREFIX.'thumbnail {
  display: block;
  padding: 4px;
  margin-bottom: 20px;
  line-height: 1.42857143;
  background-color: #fff;
  border: 1px solid #ddd;
  border-radius: 4px;
  -webkit-transition: border .2s ease-in-out;
       -o-transition: border .2s ease-in-out;
          transition: border .2s ease-in-out;
}
.'.CSS_PREFIX.'thumbnail > img,
.'.CSS_PREFIX.'thumbnail a > img {
  margin-right: auto;
  margin-left: auto;
}
a.'.CSS_PREFIX.'thumbnail:hover,
a.'.CSS_PREFIX.'thumbnail:focus,
a.'.CSS_PREFIX.'thumbnail.'.CSS_PREFIX.'active {
  border-color: #337ab7;
}
.'.CSS_PREFIX.'thumbnail .'.CSS_PREFIX.'caption {
  padding: 9px;
  color: #333;
}
.'.CSS_PREFIX.'alert {
  padding: 15px;
  margin-bottom: 20px;
  border: 1px solid transparent;
  border-radius: 4px;
}
.'.CSS_PREFIX.'alert h4 {
  margin-top: 0;
  color: inherit;
}
.'.CSS_PREFIX.'alert .'.CSS_PREFIX.'alert-link {
  font-weight: bold;
}
.'.CSS_PREFIX.'alert > p,
.'.CSS_PREFIX.'alert > ul {
  margin-bottom: 0;
}
.'.CSS_PREFIX.'alert > p + p {
  margin-top: 5px;
}
.'.CSS_PREFIX.'alert-dismissable,
.'.CSS_PREFIX.'alert-dismissible {
  padding-right: 35px;
}
.'.CSS_PREFIX.'alert-dismissable .'.CSS_PREFIX.'close,
.'.CSS_PREFIX.'alert-dismissible .'.CSS_PREFIX.'close {
  position: relative;
  top: -2px;
  right: -21px;
  color: inherit;
}
.'.CSS_PREFIX.'alert-success {
  color: #3c763d;
  background-color: #dff0d8;
  border-color: #d6e9c6;
}
.'.CSS_PREFIX.'alert-success hr {
  border-top-color: #c9e2b3;
}
.'.CSS_PREFIX.'alert-success .'.CSS_PREFIX.'alert-link {
  color: #2b542c;
}
.'.CSS_PREFIX.'alert-info {
  color: #31708f;
  background-color: #d9edf7;
  border-color: #bce8f1;
}
.'.CSS_PREFIX.'alert-info hr {
  border-top-color: #a6e1ec;
}
.'.CSS_PREFIX.'alert-info .'.CSS_PREFIX.'alert-link {
  color: #245269;
}
.'.CSS_PREFIX.'alert-warning {
  color: #8a6d3b;
  background-color: #fcf8e3;
  border-color: #faebcc;
}
.'.CSS_PREFIX.'alert-warning hr {
  border-top-color: #f7e1b5;
}
.'.CSS_PREFIX.'alert-warning .'.CSS_PREFIX.'alert-link {
  color: #66512c;
}
.'.CSS_PREFIX.'alert-danger {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
}
.'.CSS_PREFIX.'alert-danger hr {
  border-top-color: #e4b9c0;
}
.'.CSS_PREFIX.'alert-danger .'.CSS_PREFIX.'alert-link {
  color: #843534;
}
@-webkit-keyframes progress-bar-stripes {
  from {
    background-position: 40px 0;
  }
  to {
    background-position: 0 0;
  }
}
@-o-keyframes progress-bar-stripes {
  from {
    background-position: 40px 0;
  }
  to {
    background-position: 0 0;
  }
}
@keyframes progress-bar-stripes {
  from {
    background-position: 40px 0;
  }
  to {
    background-position: 0 0;
  }
}
.'.CSS_PREFIX.'progress {
  height: 20px;
  margin-bottom: 20px;
  overflow: hidden;
  background-color: #f5f5f5;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, .1);
          box-shadow: inset 0 1px 2px rgba(0, 0, 0, .1);
}
.'.CSS_PREFIX.'progress-bar {
  float: left;
  width: 0;
  height: 100%;
  font-size: 12px;
  line-height: 20px;
  color: #fff;
  text-align: center;
  background-color: #337ab7;
  -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .15);
          box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .15);
  -webkit-transition: width .6s ease;
       -o-transition: width .6s ease;
          transition: width .6s ease;
}
.'.CSS_PREFIX.'progress-striped .'.CSS_PREFIX.'progress-bar,
.'.CSS_PREFIX.'progress-bar-striped {
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  background-image:      -o-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  background-image:         linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  -webkit-background-size: 40px 40px;
          background-size: 40px 40px;
}
.'.CSS_PREFIX.'progress.'.CSS_PREFIX.'active .'.CSS_PREFIX.'progress-bar,
.'.CSS_PREFIX.'progress-bar.'.CSS_PREFIX.'active {
  -webkit-animation: progress-bar-stripes 2s linear infinite;
       -o-animation: progress-bar-stripes 2s linear infinite;
          animation: progress-bar-stripes 2s linear infinite;
}
.'.CSS_PREFIX.'progress-bar-success {
  background-color: #5cb85c;
}
.'.CSS_PREFIX.'progress-striped .'.CSS_PREFIX.'progress-bar-success {
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  background-image:      -o-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  background-image:         linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
}
.'.CSS_PREFIX.'progress-bar-info {
  background-color: #5bc0de;
}
.'.CSS_PREFIX.'progress-striped .'.CSS_PREFIX.'progress-bar-info {
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  background-image:      -o-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  background-image:         linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
}
.'.CSS_PREFIX.'progress-bar-warning {
  background-color: #f0ad4e;
}
.'.CSS_PREFIX.'progress-striped .'.CSS_PREFIX.'progress-bar-warning {
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  background-image:      -o-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  background-image:         linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
}
.'.CSS_PREFIX.'progress-bar-danger {
  background-color: #d9534f;
}
.'.CSS_PREFIX.'progress-striped .'.CSS_PREFIX.'progress-bar-danger {
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  background-image:      -o-linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
  background-image:         linear-gradient(45deg, rgba(255, 255, 255, .15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .15) 50%, rgba(255, 255, 255, .15) 75%, transparent 75%, transparent);
}
.'.CSS_PREFIX.'media {
  margin-top: 15px;
}
.'.CSS_PREFIX.'media:first-child {
  margin-top: 0;
}
.'.CSS_PREFIX.'media,
.'.CSS_PREFIX.'media-body {
  overflow: hidden;
  zoom: 1;
}
.'.CSS_PREFIX.'media-body {
  width: 10000px;
}
.'.CSS_PREFIX.'media-object {
  display: block;
}
.'.CSS_PREFIX.'media-object.'.CSS_PREFIX.'img-thumbnail {
  max-width: none;
}
.'.CSS_PREFIX.'media-right,
.'.CSS_PREFIX.'media > .'.CSS_PREFIX.'pull-right {
  padding-left: 10px;
}
.'.CSS_PREFIX.'media-left,
.'.CSS_PREFIX.'media > .'.CSS_PREFIX.'pull-left {
  padding-right: 10px;
}
.'.CSS_PREFIX.'media-left,
.'.CSS_PREFIX.'media-right,
.'.CSS_PREFIX.'media-body {
  display: table-cell;
  vertical-align: top;
}
.'.CSS_PREFIX.'media-middle {
  vertical-align: middle;
}
.'.CSS_PREFIX.'media-bottom {
  vertical-align: bottom;
}
.'.CSS_PREFIX.'media-heading {
  margin-top: 0;
  margin-bottom: 5px;
}
.'.CSS_PREFIX.'media-list {
  padding-left: 0;
  list-style: none;
}
.'.CSS_PREFIX.'list-group {
  padding-left: 0;
  margin-bottom: 20px;
}
.'.CSS_PREFIX.'list-group-item {
  position: relative;
  display: block;
  padding: 10px 15px;
  margin-bottom: -1px;
  background-color: #fff;
  border: 1px solid #ddd;
}
.'.CSS_PREFIX.'list-group-item:first-child {
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
}
.'.CSS_PREFIX.'list-group-item:last-child {
  margin-bottom: 0;
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
}
a.'.CSS_PREFIX.'list-group-item,
button.'.CSS_PREFIX.'list-group-item {
  color: #555;
}
a.'.CSS_PREFIX.'list-group-item .'.CSS_PREFIX.'list-group-item-heading,
button.'.CSS_PREFIX.'list-group-item .'.CSS_PREFIX.'list-group-item-heading {
  color: #333;
}
a.'.CSS_PREFIX.'list-group-item:hover,
button.'.CSS_PREFIX.'list-group-item:hover,
a.'.CSS_PREFIX.'list-group-item:focus,
button.'.CSS_PREFIX.'list-group-item:focus {
  color: #555;
  text-decoration: none;
  background-color: #f5f5f5;
}
button.'.CSS_PREFIX.'list-group-item {
  width: 100%;
  text-align: left;
}
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'disabled,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'disabled:hover,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'disabled:focus {
  color: #777;
  cursor: not-allowed;
  background-color: #eee;
}
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'disabled .'.CSS_PREFIX.'list-group-item-heading,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'disabled:hover .'.CSS_PREFIX.'list-group-item-heading,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'disabled:focus .'.CSS_PREFIX.'list-group-item-heading {
  color: inherit;
}
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'disabled .'.CSS_PREFIX.'list-group-item-text,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'disabled:hover .'.CSS_PREFIX.'list-group-item-text,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'disabled:focus .'.CSS_PREFIX.'list-group-item-text {
  color: #777;
}
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active:hover,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active:focus {
  z-index: 2;
  color: #fff;
  background-color: #337ab7;
  border-color: #337ab7;
}
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active .'.CSS_PREFIX.'list-group-item-heading,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active:hover .'.CSS_PREFIX.'list-group-item-heading,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active:focus .'.CSS_PREFIX.'list-group-item-heading,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active .'.CSS_PREFIX.'list-group-item-heading > small,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active:hover .'.CSS_PREFIX.'list-group-item-heading > small,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active:focus .'.CSS_PREFIX.'list-group-item-heading > small,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active .'.CSS_PREFIX.'list-group-item-heading > .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active:hover .'.CSS_PREFIX.'list-group-item-heading > .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active:focus .'.CSS_PREFIX.'list-group-item-heading > .'.CSS_PREFIX.'small {
  color: inherit;
}
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active .'.CSS_PREFIX.'list-group-item-text,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active:hover .'.CSS_PREFIX.'list-group-item-text,
.'.CSS_PREFIX.'list-group-item.'.CSS_PREFIX.'active:focus .'.CSS_PREFIX.'list-group-item-text {
  color: #c7ddef;
}
.'.CSS_PREFIX.'list-group-item-success {
  color: #3c763d;
  background-color: #dff0d8;
}
a.'.CSS_PREFIX.'list-group-item-success,
button.'.CSS_PREFIX.'list-group-item-success {
  color: #3c763d;
}
a.'.CSS_PREFIX.'list-group-item-success .'.CSS_PREFIX.'list-group-item-heading,
button.'.CSS_PREFIX.'list-group-item-success .'.CSS_PREFIX.'list-group-item-heading {
  color: inherit;
}
a.'.CSS_PREFIX.'list-group-item-success:hover,
button.'.CSS_PREFIX.'list-group-item-success:hover,
a.'.CSS_PREFIX.'list-group-item-success:focus,
button.'.CSS_PREFIX.'list-group-item-success:focus {
  color: #3c763d;
  background-color: #d0e9c6;
}
a.'.CSS_PREFIX.'list-group-item-success.'.CSS_PREFIX.'active,
button.'.CSS_PREFIX.'list-group-item-success.'.CSS_PREFIX.'active,
a.'.CSS_PREFIX.'list-group-item-success.'.CSS_PREFIX.'active:hover,
button.'.CSS_PREFIX.'list-group-item-success.'.CSS_PREFIX.'active:hover,
a.'.CSS_PREFIX.'list-group-item-success.'.CSS_PREFIX.'active:focus,
button.'.CSS_PREFIX.'list-group-item-success.'.CSS_PREFIX.'active:focus {
  color: #fff;
  background-color: #3c763d;
  border-color: #3c763d;
}
.'.CSS_PREFIX.'list-group-item-info {
  color: #31708f;
  background-color: #d9edf7;
}
a.'.CSS_PREFIX.'list-group-item-info,
button.'.CSS_PREFIX.'list-group-item-info {
  color: #31708f;
}
a.'.CSS_PREFIX.'list-group-item-info .'.CSS_PREFIX.'list-group-item-heading,
button.'.CSS_PREFIX.'list-group-item-info .'.CSS_PREFIX.'list-group-item-heading {
  color: inherit;
}
a.'.CSS_PREFIX.'list-group-item-info:hover,
button.'.CSS_PREFIX.'list-group-item-info:hover,
a.'.CSS_PREFIX.'list-group-item-info:focus,
button.'.CSS_PREFIX.'list-group-item-info:focus {
  color: #31708f;
  background-color: #c4e3f3;
}
a.'.CSS_PREFIX.'list-group-item-info.'.CSS_PREFIX.'active,
button.'.CSS_PREFIX.'list-group-item-info.'.CSS_PREFIX.'active,
a.'.CSS_PREFIX.'list-group-item-info.'.CSS_PREFIX.'active:hover,
button.'.CSS_PREFIX.'list-group-item-info.'.CSS_PREFIX.'active:hover,
a.'.CSS_PREFIX.'list-group-item-info.'.CSS_PREFIX.'active:focus,
button.'.CSS_PREFIX.'list-group-item-info.'.CSS_PREFIX.'active:focus {
  color: #fff;
  background-color: #31708f;
  border-color: #31708f;
}
.'.CSS_PREFIX.'list-group-item-warning {
  color: #8a6d3b;
  background-color: #fcf8e3;
}
a.'.CSS_PREFIX.'list-group-item-warning,
button.'.CSS_PREFIX.'list-group-item-warning {
  color: #8a6d3b;
}
a.'.CSS_PREFIX.'list-group-item-warning .'.CSS_PREFIX.'list-group-item-heading,
button.'.CSS_PREFIX.'list-group-item-warning .'.CSS_PREFIX.'list-group-item-heading {
  color: inherit;
}
a.'.CSS_PREFIX.'list-group-item-warning:hover,
button.'.CSS_PREFIX.'list-group-item-warning:hover,
a.'.CSS_PREFIX.'list-group-item-warning:focus,
button.'.CSS_PREFIX.'list-group-item-warning:focus {
  color: #8a6d3b;
  background-color: #faf2cc;
}
a.'.CSS_PREFIX.'list-group-item-warning.'.CSS_PREFIX.'active,
button.'.CSS_PREFIX.'list-group-item-warning.'.CSS_PREFIX.'active,
a.'.CSS_PREFIX.'list-group-item-warning.'.CSS_PREFIX.'active:hover,
button.'.CSS_PREFIX.'list-group-item-warning.'.CSS_PREFIX.'active:hover,
a.'.CSS_PREFIX.'list-group-item-warning.'.CSS_PREFIX.'active:focus,
button.'.CSS_PREFIX.'list-group-item-warning.'.CSS_PREFIX.'active:focus {
  color: #fff;
  background-color: #8a6d3b;
  border-color: #8a6d3b;
}
.'.CSS_PREFIX.'list-group-item-danger {
  color: #a94442;
  background-color: #f2dede;
}
a.'.CSS_PREFIX.'list-group-item-danger,
button.'.CSS_PREFIX.'list-group-item-danger {
  color: #a94442;
}
a.'.CSS_PREFIX.'list-group-item-danger .'.CSS_PREFIX.'list-group-item-heading,
button.'.CSS_PREFIX.'list-group-item-danger .'.CSS_PREFIX.'list-group-item-heading {
  color: inherit;
}
a.'.CSS_PREFIX.'list-group-item-danger:hover,
button.'.CSS_PREFIX.'list-group-item-danger:hover,
a.'.CSS_PREFIX.'list-group-item-danger:focus,
button.'.CSS_PREFIX.'list-group-item-danger:focus {
  color: #a94442;
  background-color: #ebcccc;
}
a.'.CSS_PREFIX.'list-group-item-danger.'.CSS_PREFIX.'active,
button.'.CSS_PREFIX.'list-group-item-danger.'.CSS_PREFIX.'active,
a.'.CSS_PREFIX.'list-group-item-danger.'.CSS_PREFIX.'active:hover,
button.'.CSS_PREFIX.'list-group-item-danger.'.CSS_PREFIX.'active:hover,
a.'.CSS_PREFIX.'list-group-item-danger.'.CSS_PREFIX.'active:focus,
button.'.CSS_PREFIX.'list-group-item-danger.'.CSS_PREFIX.'active:focus {
  color: #fff;
  background-color: #a94442;
  border-color: #a94442;
}
.'.CSS_PREFIX.'list-group-item-heading {
  margin-top: 0;
  margin-bottom: 5px;
}
.'.CSS_PREFIX.'list-group-item-text {
  margin-bottom: 0;
  line-height: 1.3;
}
.'.CSS_PREFIX.'panel {
  margin-bottom: 20px;
  background-color: #fff;
  border: 1px solid transparent;
  border-radius: 4px;
  -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
          box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
}
.'.CSS_PREFIX.'panel-body {
  padding: 15px;
}
.'.CSS_PREFIX.'panel-heading {
  padding: 10px 15px;
  border-bottom: 1px solid transparent;
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
}
.'.CSS_PREFIX.'panel-heading > .'.CSS_PREFIX.'dropdown .'.CSS_PREFIX.'dropdown-toggle {
  color: inherit;
}
.'.CSS_PREFIX.'panel-title {
  margin-top: 0;
  margin-bottom: 0;
  font-size: 16px;
  color: inherit;
}
.'.CSS_PREFIX.'panel-title > a,
.'.CSS_PREFIX.'panel-title > small,
.'.CSS_PREFIX.'panel-title > .'.CSS_PREFIX.'small,
.'.CSS_PREFIX.'panel-title > small > a,
.'.CSS_PREFIX.'panel-title > .'.CSS_PREFIX.'small > a {
  color: inherit;
}
.'.CSS_PREFIX.'panel-footer {
  padding: 10px 15px;
  background-color: #f5f5f5;
  border-top: 1px solid #ddd;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'list-group,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'list-group {
  margin-bottom: 0;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'list-group .'.CSS_PREFIX.'list-group-item,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'list-group .'.CSS_PREFIX.'list-group-item {
  border-width: 1px 0;
  border-radius: 0;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'list-group:first-child .'.CSS_PREFIX.'list-group-item:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'list-group:first-child .'.CSS_PREFIX.'list-group-item:first-child {
  border-top: 0;
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'list-group:last-child .'.CSS_PREFIX.'list-group-item:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'list-group:last-child .'.CSS_PREFIX.'list-group-item:last-child {
  border-bottom: 0;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'panel-heading + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'list-group .'.CSS_PREFIX.'list-group-item:first-child {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
.'.CSS_PREFIX.'panel-heading + .'.CSS_PREFIX.'list-group .'.CSS_PREFIX.'list-group-item:first-child {
  border-top-width: 0;
}
.'.CSS_PREFIX.'list-group + .'.CSS_PREFIX.'panel-footer {
  border-top-width: 0;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'table {
  margin-bottom: 0;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table caption,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table caption,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'table caption {
  padding-right: 15px;
  padding-left: 15px;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:first-child > .'.CSS_PREFIX.'table:first-child {
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:first-child > thead:first-child > tr:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:first-child > .'.CSS_PREFIX.'table:first-child > thead:first-child > tr:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:first-child > tbody:first-child > tr:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:first-child > .'.CSS_PREFIX.'table:first-child > tbody:first-child > tr:first-child {
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:first-child > thead:first-child > tr:first-child td:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:first-child > .'.CSS_PREFIX.'table:first-child > thead:first-child > tr:first-child td:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:first-child > tbody:first-child > tr:first-child td:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:first-child > .'.CSS_PREFIX.'table:first-child > tbody:first-child > tr:first-child td:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:first-child > thead:first-child > tr:first-child th:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:first-child > .'.CSS_PREFIX.'table:first-child > thead:first-child > tr:first-child th:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:first-child > tbody:first-child > tr:first-child th:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:first-child > .'.CSS_PREFIX.'table:first-child > tbody:first-child > tr:first-child th:first-child {
  border-top-left-radius: 3px;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:first-child > thead:first-child > tr:first-child td:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:first-child > .'.CSS_PREFIX.'table:first-child > thead:first-child > tr:first-child td:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:first-child > tbody:first-child > tr:first-child td:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:first-child > .'.CSS_PREFIX.'table:first-child > tbody:first-child > tr:first-child td:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:first-child > thead:first-child > tr:first-child th:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:first-child > .'.CSS_PREFIX.'table:first-child > thead:first-child > tr:first-child th:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:first-child > tbody:first-child > tr:first-child th:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:first-child > .'.CSS_PREFIX.'table:first-child > tbody:first-child > tr:first-child th:last-child {
  border-top-right-radius: 3px;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:last-child > .'.CSS_PREFIX.'table:last-child {
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:last-child > tbody:last-child > tr:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:last-child > .'.CSS_PREFIX.'table:last-child > tbody:last-child > tr:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:last-child > tfoot:last-child > tr:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:last-child > .'.CSS_PREFIX.'table:last-child > tfoot:last-child > tr:last-child {
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:last-child > tbody:last-child > tr:last-child td:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:last-child > .'.CSS_PREFIX.'table:last-child > tbody:last-child > tr:last-child td:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:last-child > tfoot:last-child > tr:last-child td:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:last-child > .'.CSS_PREFIX.'table:last-child > tfoot:last-child > tr:last-child td:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:last-child > tbody:last-child > tr:last-child th:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:last-child > .'.CSS_PREFIX.'table:last-child > tbody:last-child > tr:last-child th:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:last-child > tfoot:last-child > tr:last-child th:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:last-child > .'.CSS_PREFIX.'table:last-child > tfoot:last-child > tr:last-child th:first-child {
  border-bottom-left-radius: 3px;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:last-child > tbody:last-child > tr:last-child td:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:last-child > .'.CSS_PREFIX.'table:last-child > tbody:last-child > tr:last-child td:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:last-child > tfoot:last-child > tr:last-child td:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:last-child > .'.CSS_PREFIX.'table:last-child > tfoot:last-child > tr:last-child td:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:last-child > tbody:last-child > tr:last-child th:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:last-child > .'.CSS_PREFIX.'table:last-child > tbody:last-child > tr:last-child th:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table:last-child > tfoot:last-child > tr:last-child th:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive:last-child > .'.CSS_PREFIX.'table:last-child > tfoot:last-child > tr:last-child th:last-child {
  border-bottom-right-radius: 3px;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'panel-body + .'.CSS_PREFIX.'table,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'panel-body + .'.CSS_PREFIX.'table-responsive,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table + .'.CSS_PREFIX.'panel-body,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive + .'.CSS_PREFIX.'panel-body {
  border-top: 1px solid #ddd;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table > tbody:first-child > tr:first-child th,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table > tbody:first-child > tr:first-child td {
  border-top: 0;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered {
  border: 0;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > thead > tr > th:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > thead > tr > th:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > tbody > tr > th:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tbody > tr > th:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > tfoot > tr > th:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tfoot > tr > th:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > thead > tr > td:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > thead > tr > td:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > tbody > tr > td:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tbody > tr > td:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > tfoot > tr > td:first-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tfoot > tr > td:first-child {
  border-left: 0;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > thead > tr > th:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > thead > tr > th:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > tbody > tr > th:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tbody > tr > th:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > tfoot > tr > th:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tfoot > tr > th:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > thead > tr > td:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > thead > tr > td:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > tbody > tr > td:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tbody > tr > td:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > tfoot > tr > td:last-child,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tfoot > tr > td:last-child {
  border-right: 0;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > thead > tr:first-child > td,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > thead > tr:first-child > td,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > tbody > tr:first-child > td,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tbody > tr:first-child > td,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > thead > tr:first-child > th,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > thead > tr:first-child > th,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > tbody > tr:first-child > th,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tbody > tr:first-child > th {
  border-bottom: 0;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > tbody > tr:last-child > td,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tbody > tr:last-child > td,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > tfoot > tr:last-child > td,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tfoot > tr:last-child > td,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > tbody > tr:last-child > th,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tbody > tr:last-child > th,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-bordered > tfoot > tr:last-child > th,
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive > .'.CSS_PREFIX.'table-bordered > tfoot > tr:last-child > th {
  border-bottom: 0;
}
.'.CSS_PREFIX.'panel > .'.CSS_PREFIX.'table-responsive {
  margin-bottom: 0;
  border: 0;
}
.'.CSS_PREFIX.'panel-group {
  margin-bottom: 20px;
}
.'.CSS_PREFIX.'panel-group .'.CSS_PREFIX.'panel {
  margin-bottom: 0;
  border-radius: 4px;
}
.'.CSS_PREFIX.'panel-group .'.CSS_PREFIX.'panel + .'.CSS_PREFIX.'panel {
  margin-top: 5px;
}
.'.CSS_PREFIX.'panel-group .'.CSS_PREFIX.'panel-heading {
  border-bottom: 0;
}
.'.CSS_PREFIX.'panel-group .'.CSS_PREFIX.'panel-heading + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'panel-body,
.'.CSS_PREFIX.'panel-group .'.CSS_PREFIX.'panel-heading + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'list-group {
  border-top: 1px solid #ddd;
}
.'.CSS_PREFIX.'panel-group .'.CSS_PREFIX.'panel-footer {
  border-top: 0;
}
.'.CSS_PREFIX.'panel-group .'.CSS_PREFIX.'panel-footer + .'.CSS_PREFIX.'panel-collapse .'.CSS_PREFIX.'panel-body {
  border-bottom: 1px solid #ddd;
}
.'.CSS_PREFIX.'panel-default {
  border-color: #ddd;
}
.'.CSS_PREFIX.'panel-default > .'.CSS_PREFIX.'panel-heading {
  color: #333;
  background-color: #f5f5f5;
  border-color: #ddd;
}
.'.CSS_PREFIX.'panel-default > .'.CSS_PREFIX.'panel-heading + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'panel-body {
  border-top-color: #ddd;
}
.'.CSS_PREFIX.'panel-default > .'.CSS_PREFIX.'panel-heading .'.CSS_PREFIX.'badge {
  color: #f5f5f5;
  background-color: #333;
}
.'.CSS_PREFIX.'panel-default > .'.CSS_PREFIX.'panel-footer + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'panel-body {
  border-bottom-color: #ddd;
}
.'.CSS_PREFIX.'panel-primary {
  border-color: #337ab7;
}
.'.CSS_PREFIX.'panel-primary > .'.CSS_PREFIX.'panel-heading {
  color: #fff;
  background-color: #337ab7;
  border-color: #337ab7;
}
.'.CSS_PREFIX.'panel-primary > .'.CSS_PREFIX.'panel-heading + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'panel-body {
  border-top-color: #337ab7;
}
.'.CSS_PREFIX.'panel-primary > .'.CSS_PREFIX.'panel-heading .'.CSS_PREFIX.'badge {
  color: #337ab7;
  background-color: #fff;
}
.'.CSS_PREFIX.'panel-primary > .'.CSS_PREFIX.'panel-footer + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'panel-body {
  border-bottom-color: #337ab7;
}
.'.CSS_PREFIX.'panel-success {
  border-color: #d6e9c6;
}
.'.CSS_PREFIX.'panel-success > .'.CSS_PREFIX.'panel-heading {
  color: #3c763d;
  background-color: #dff0d8;
  border-color: #d6e9c6;
}
.'.CSS_PREFIX.'panel-success > .'.CSS_PREFIX.'panel-heading + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'panel-body {
  border-top-color: #d6e9c6;
}
.'.CSS_PREFIX.'panel-success > .'.CSS_PREFIX.'panel-heading .'.CSS_PREFIX.'badge {
  color: #dff0d8;
  background-color: #3c763d;
}
.'.CSS_PREFIX.'panel-success > .'.CSS_PREFIX.'panel-footer + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'panel-body {
  border-bottom-color: #d6e9c6;
}
.'.CSS_PREFIX.'panel-info {
  border-color: #bce8f1;
}
.'.CSS_PREFIX.'panel-info > .'.CSS_PREFIX.'panel-heading {
  color: #31708f;
  background-color: #d9edf7;
  border-color: #bce8f1;
}
.'.CSS_PREFIX.'panel-info > .'.CSS_PREFIX.'panel-heading + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'panel-body {
  border-top-color: #bce8f1;
}
.'.CSS_PREFIX.'panel-info > .'.CSS_PREFIX.'panel-heading .'.CSS_PREFIX.'badge {
  color: #d9edf7;
  background-color: #31708f;
}
.'.CSS_PREFIX.'panel-info > .'.CSS_PREFIX.'panel-footer + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'panel-body {
  border-bottom-color: #bce8f1;
}
.'.CSS_PREFIX.'panel-warning {
  border-color: #faebcc;
}
.'.CSS_PREFIX.'panel-warning > .'.CSS_PREFIX.'panel-heading {
  color: #8a6d3b;
  background-color: #fcf8e3;
  border-color: #faebcc;
}
.'.CSS_PREFIX.'panel-warning > .'.CSS_PREFIX.'panel-heading + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'panel-body {
  border-top-color: #faebcc;
}
.'.CSS_PREFIX.'panel-warning > .'.CSS_PREFIX.'panel-heading .'.CSS_PREFIX.'badge {
  color: #fcf8e3;
  background-color: #8a6d3b;
}
.'.CSS_PREFIX.'panel-warning > .'.CSS_PREFIX.'panel-footer + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'panel-body {
  border-bottom-color: #faebcc;
}
.'.CSS_PREFIX.'panel-danger {
  border-color: #ebccd1;
}
.'.CSS_PREFIX.'panel-danger > .'.CSS_PREFIX.'panel-heading {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
}
.'.CSS_PREFIX.'panel-danger > .'.CSS_PREFIX.'panel-heading + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'panel-body {
  border-top-color: #ebccd1;
}
.'.CSS_PREFIX.'panel-danger > .'.CSS_PREFIX.'panel-heading .'.CSS_PREFIX.'badge {
  color: #f2dede;
  background-color: #a94442;
}
.'.CSS_PREFIX.'panel-danger > .'.CSS_PREFIX.'panel-footer + .'.CSS_PREFIX.'panel-collapse > .'.CSS_PREFIX.'panel-body {
  border-bottom-color: #ebccd1;
}
.'.CSS_PREFIX.'embed-responsive {
  position: relative;
  display: block;
  height: 0;
  padding: 0;
  overflow: hidden;
}
.'.CSS_PREFIX.'embed-responsive .'.CSS_PREFIX.'embed-responsive-item,
.'.CSS_PREFIX.'embed-responsive iframe,
.'.CSS_PREFIX.'embed-responsive embed,
.'.CSS_PREFIX.'embed-responsive object,
.'.CSS_PREFIX.'embed-responsive video {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border: 0;
}
.'.CSS_PREFIX.'embed-responsive-16by9 {
  padding-bottom: 56.25%;
}
.'.CSS_PREFIX.'embed-responsive-4by3 {
  padding-bottom: 75%;
}
.'.CSS_PREFIX.'well {
  min-height: 20px;
  padding: 19px;
  margin-bottom: 20px;
  background-color: #f5f5f5;
  border: 1px solid #e3e3e3;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
}
.'.CSS_PREFIX.'well blockquote {
  border-color: #ddd;
  border-color: rgba(0, 0, 0, .15);
}
.'.CSS_PREFIX.'well-lg {
  padding: 24px;
  border-radius: 6px;
}
.'.CSS_PREFIX.'well-sm {
  padding: 9px;
  border-radius: 3px;
}
.'.CSS_PREFIX.'close {
  float: right;
  font-size: 21px;
  font-weight: bold;
  line-height: 1;
  color: #000;
  text-shadow: 0 1px 0 #fff;
  filter: alpha(opacity=20);
  opacity: .2;
}
.'.CSS_PREFIX.'close:hover,
.'.CSS_PREFIX.'close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
  filter: alpha(opacity=50);
  opacity: .5;
}
button.'.CSS_PREFIX.'close {
  -webkit-appearance: none;
  padding: 0;
  cursor: pointer;
  background: transparent;
  border: 0;
}
.'.CSS_PREFIX.'modal-open {
  overflow: hidden;
}
.'.CSS_PREFIX.'modal {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1050;
  display: none;
  overflow: hidden;
  -webkit-overflow-scrolling: touch;
  outline: 0;
}
.'.CSS_PREFIX.'modal.'.CSS_PREFIX.'fade .'.CSS_PREFIX.'modal-dialog {
  -webkit-transition: -webkit-transform .3s ease-out;
       -o-transition:      -o-transform .3s ease-out;
          transition:         transform .3s ease-out;
  -webkit-transform: translate(0, -25%);
      -ms-transform: translate(0, -25%);
       -o-transform: translate(0, -25%);
          transform: translate(0, -25%);
}
.'.CSS_PREFIX.'modal.'.CSS_PREFIX.'in .'.CSS_PREFIX.'modal-dialog {
  -webkit-transform: translate(0, 0);
      -ms-transform: translate(0, 0);
       -o-transform: translate(0, 0);
          transform: translate(0, 0);
}
.'.CSS_PREFIX.'modal-open .'.CSS_PREFIX.'modal {
  overflow-x: hidden;
  overflow-y: auto;
}
.'.CSS_PREFIX.'modal-dialog {
  position: relative;
  width: auto;
  margin: 10px;
}
.'.CSS_PREFIX.'modal-content {
  position: relative;
  background-color: #fff;
  -webkit-background-clip: padding-box;
          background-clip: padding-box;
  border: 1px solid #999;
  border: 1px solid rgba(0, 0, 0, .2);
  border-radius: 6px;
  outline: 0;
  -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, .5);
          box-shadow: 0 3px 9px rgba(0, 0, 0, .5);
}
.'.CSS_PREFIX.'modal-backdrop {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1040;
  background-color: #000;
}
.'.CSS_PREFIX.'modal-backdrop.'.CSS_PREFIX.'fade {
  filter: alpha(opacity=0);
  opacity: 0;
}
.'.CSS_PREFIX.'modal-backdrop.'.CSS_PREFIX.'in {
  filter: alpha(opacity=50);
  opacity: .5;
}
.'.CSS_PREFIX.'modal-header {
  padding: 15px;
  border-bottom: 1px solid #e5e5e5;
}
.'.CSS_PREFIX.'modal-header .'.CSS_PREFIX.'close {
  margin-top: -2px;
}
.'.CSS_PREFIX.'modal-title {
  margin: 0;
  line-height: 1.42857143;
}
.'.CSS_PREFIX.'modal-body {
  position: relative;
  padding: 15px;
}
.'.CSS_PREFIX.'modal-footer {
  padding: 15px;
  text-align: right;
  border-top: 1px solid #e5e5e5;
}
.'.CSS_PREFIX.'modal-footer .'.CSS_PREFIX.'btn + .'.CSS_PREFIX.'btn {
  margin-bottom: 0;
  margin-left: 5px;
}
.'.CSS_PREFIX.'modal-footer .'.CSS_PREFIX.'btn-group .'.CSS_PREFIX.'btn + .'.CSS_PREFIX.'btn {
  margin-left: -1px;
}
.'.CSS_PREFIX.'modal-footer .'.CSS_PREFIX.'btn-block + .'.CSS_PREFIX.'btn-block {
  margin-left: 0;
}
.'.CSS_PREFIX.'modal-scrollbar-measure {
  position: absolute;
  top: -9999px;
  width: 50px;
  height: 50px;
  overflow: scroll;
}
@media (min-width: 768px) {
  .'.CSS_PREFIX.'modal-dialog {
    width: 600px;
    margin: 30px auto;
  }
  .'.CSS_PREFIX.'modal-content {
    -webkit-box-shadow: 0 5px 15px rgba(0, 0, 0, .5);
            box-shadow: 0 5px 15px rgba(0, 0, 0, .5);
  }
  .'.CSS_PREFIX.'modal-sm {
    width: 300px;
  }
}
@media (min-width: 992px) {
  .'.CSS_PREFIX.'modal-lg {
    width: 900px;
  }
}
.'.CSS_PREFIX.'tooltip {
  position: absolute;
  z-index: 1070;
  display: block;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 12px;
  font-style: normal;
  font-weight: normal;
  line-height: 1.42857143;
  text-align: left;
  text-align: start;
  text-decoration: none;
  text-shadow: none;
  text-transform: none;
  letter-spacing: normal;
  word-break: normal;
  word-spacing: normal;
  word-wrap: normal;
  white-space: normal;
  filter: alpha(opacity=0);
  opacity: 0;

  line-break: auto;
}
.'.CSS_PREFIX.'tooltip.'.CSS_PREFIX.'in {
  filter: alpha(opacity=90);
  opacity: .9;
}
.'.CSS_PREFIX.'tooltip.'.CSS_PREFIX.'top {
  padding: 5px 0;
  margin-top: -3px;
}
.'.CSS_PREFIX.'tooltip.'.CSS_PREFIX.'right {
  padding: 0 5px;
  margin-left: 3px;
}
.'.CSS_PREFIX.'tooltip.'.CSS_PREFIX.'bottom {
  padding: 5px 0;
  margin-top: 3px;
}
.'.CSS_PREFIX.'tooltip.'.CSS_PREFIX.'left {
  padding: 0 5px;
  margin-left: -3px;
}
.'.CSS_PREFIX.'tooltip-inner {
  max-width: 200px;
  padding: 3px 8px;
  color: #fff;
  text-align: center;
  background-color: #000;
  border-radius: 4px;
}
.'.CSS_PREFIX.'tooltip-arrow {
  position: absolute;
  width: 0;
  height: 0;
  border-color: transparent;
  border-style: solid;
}
.'.CSS_PREFIX.'tooltip.'.CSS_PREFIX.'top .'.CSS_PREFIX.'tooltip-arrow {
  bottom: 0;
  left: 50%;
  margin-left: -5px;
  border-width: 5px 5px 0;
  border-top-color: #000;
}
.'.CSS_PREFIX.'tooltip.'.CSS_PREFIX.'top-left .'.CSS_PREFIX.'tooltip-arrow {
  right: 5px;
  bottom: 0;
  margin-bottom: -5px;
  border-width: 5px 5px 0;
  border-top-color: #000;
}
.'.CSS_PREFIX.'tooltip.'.CSS_PREFIX.'top-right .'.CSS_PREFIX.'tooltip-arrow {
  bottom: 0;
  left: 5px;
  margin-bottom: -5px;
  border-width: 5px 5px 0;
  border-top-color: #000;
}
.'.CSS_PREFIX.'tooltip.'.CSS_PREFIX.'right .'.CSS_PREFIX.'tooltip-arrow {
  top: 50%;
  left: 0;
  margin-top: -5px;
  border-width: 5px 5px 5px 0;
  border-right-color: #000;
}
.'.CSS_PREFIX.'tooltip.'.CSS_PREFIX.'left .'.CSS_PREFIX.'tooltip-arrow {
  top: 50%;
  right: 0;
  margin-top: -5px;
  border-width: 5px 0 5px 5px;
  border-left-color: #000;
}
.'.CSS_PREFIX.'tooltip.'.CSS_PREFIX.'bottom .'.CSS_PREFIX.'tooltip-arrow {
  top: 0;
  left: 50%;
  margin-left: -5px;
  border-width: 0 5px 5px;
  border-bottom-color: #000;
}
.'.CSS_PREFIX.'tooltip.'.CSS_PREFIX.'bottom-left .'.CSS_PREFIX.'tooltip-arrow {
  top: 0;
  right: 5px;
  margin-top: -5px;
  border-width: 0 5px 5px;
  border-bottom-color: #000;
}
.'.CSS_PREFIX.'tooltip.'.CSS_PREFIX.'bottom-right .'.CSS_PREFIX.'tooltip-arrow {
  top: 0;
  left: 5px;
  margin-top: -5px;
  border-width: 0 5px 5px;
  border-bottom-color: #000;
}
.'.CSS_PREFIX.'popover {
  position: absolute;
  top: 0;
  left: 0;
  z-index: 1060;
  display: none;
  max-width: 276px;
  padding: 1px;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 14px;
  font-style: normal;
  font-weight: normal;
  line-height: 1.42857143;
  text-align: left;
  text-align: start;
  text-decoration: none;
  text-shadow: none;
  text-transform: none;
  letter-spacing: normal;
  word-break: normal;
  word-spacing: normal;
  word-wrap: normal;
  white-space: normal;
  background-color: #fff;
  -webkit-background-clip: padding-box;
          background-clip: padding-box;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, .2);
  border-radius: 6px;
  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
          box-shadow: 0 5px 10px rgba(0, 0, 0, .2);

  line-break: auto;
}
.'.CSS_PREFIX.'popover.'.CSS_PREFIX.'top {
  margin-top: -10px;
}
.'.CSS_PREFIX.'popover.'.CSS_PREFIX.'right {
  margin-left: 10px;
}
.'.CSS_PREFIX.'popover.'.CSS_PREFIX.'bottom {
  margin-top: 10px;
}
.'.CSS_PREFIX.'popover.'.CSS_PREFIX.'left {
  margin-left: -10px;
}
.'.CSS_PREFIX.'popover-title {
  padding: 8px 14px;
  margin: 0;
  font-size: 14px;
  background-color: #f7f7f7;
  border-bottom: 1px solid #ebebeb;
  border-radius: 5px 5px 0 0;
}
.'.CSS_PREFIX.'popover-content {
  padding: 9px 14px;
}
.'.CSS_PREFIX.'popover > .'.CSS_PREFIX.'arrow,
.'.CSS_PREFIX.'popover > .'.CSS_PREFIX.'arrow:after {
  position: absolute;
  display: block;
  width: 0;
  height: 0;
  border-color: transparent;
  border-style: solid;
}
.'.CSS_PREFIX.'popover > .'.CSS_PREFIX.'arrow {
  border-width: 11px;
}
.'.CSS_PREFIX.'popover > .'.CSS_PREFIX.'arrow:after {
  content: "";
  border-width: 10px;
}
.'.CSS_PREFIX.'popover.'.CSS_PREFIX.'top > .'.CSS_PREFIX.'arrow {
  bottom: -11px;
  left: 50%;
  margin-left: -11px;
  border-top-color: #999;
  border-top-color: rgba(0, 0, 0, .25);
  border-bottom-width: 0;
}
.'.CSS_PREFIX.'popover.'.CSS_PREFIX.'top > .'.CSS_PREFIX.'arrow:after {
  bottom: 1px;
  margin-left: -10px;
  content: " ";
  border-top-color: #fff;
  border-bottom-width: 0;
}
.'.CSS_PREFIX.'popover.'.CSS_PREFIX.'right > .'.CSS_PREFIX.'arrow {
  top: 50%;
  left: -11px;
  margin-top: -11px;
  border-right-color: #999;
  border-right-color: rgba(0, 0, 0, .25);
  border-left-width: 0;
}
.'.CSS_PREFIX.'popover.'.CSS_PREFIX.'right > .'.CSS_PREFIX.'arrow:after {
  bottom: -10px;
  left: 1px;
  content: " ";
  border-right-color: #fff;
  border-left-width: 0;
}
.'.CSS_PREFIX.'popover.'.CSS_PREFIX.'bottom > .'.CSS_PREFIX.'arrow {
  top: -11px;
  left: 50%;
  margin-left: -11px;
  border-top-width: 0;
  border-bottom-color: #999;
  border-bottom-color: rgba(0, 0, 0, .25);
}
.'.CSS_PREFIX.'popover.'.CSS_PREFIX.'bottom > .'.CSS_PREFIX.'arrow:after {
  top: 1px;
  margin-left: -10px;
  content: " ";
  border-top-width: 0;
  border-bottom-color: #fff;
}
.'.CSS_PREFIX.'popover.'.CSS_PREFIX.'left > .'.CSS_PREFIX.'arrow {
  top: 50%;
  right: -11px;
  margin-top: -11px;
  border-right-width: 0;
  border-left-color: #999;
  border-left-color: rgba(0, 0, 0, .25);
}
.'.CSS_PREFIX.'popover.'.CSS_PREFIX.'left > .'.CSS_PREFIX.'arrow:after {
  right: 1px;
  bottom: -10px;
  content: " ";
  border-right-width: 0;
  border-left-color: #fff;
}
.'.CSS_PREFIX.'carousel {
  position: relative;
}
.'.CSS_PREFIX.'carousel-inner {
  position: relative;
  width: 100%;
  overflow: hidden;
}
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'item {
  position: relative;
  display: none;
  -webkit-transition: .6s ease-in-out left;
       -o-transition: .6s ease-in-out left;
          transition: .6s ease-in-out left;
}
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'item > img,
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'item > a > img {
  line-height: 1;
}
@media all and (transform-3d), (-webkit-transform-3d) {
  .'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'item {
    -webkit-transition: -webkit-transform .6s ease-in-out;
         -o-transition:      -o-transform .6s ease-in-out;
            transition:         transform .6s ease-in-out;

    -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
    -webkit-perspective: 1000px;
            perspective: 1000px;
  }
  .'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'item.'.CSS_PREFIX.'next,
  .'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'item.'.CSS_PREFIX.'active.'.CSS_PREFIX.'right {
    left: 0;
    -webkit-transform: translate3d(100%, 0, 0);
            transform: translate3d(100%, 0, 0);
  }
  .'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'item.'.CSS_PREFIX.'prev,
  .'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'item.'.CSS_PREFIX.'active.'.CSS_PREFIX.'left {
    left: 0;
    -webkit-transform: translate3d(-100%, 0, 0);
            transform: translate3d(-100%, 0, 0);
  }
  .'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'item.'.CSS_PREFIX.'next.'.CSS_PREFIX.'left,
  .'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'item.'.CSS_PREFIX.'prev.'.CSS_PREFIX.'right,
  .'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'item.'.CSS_PREFIX.'active {
    left: 0;
    -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
  }
}
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'active,
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'next,
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'prev {
  display: block;
}
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'active {
  left: 0;
}
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'next,
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'prev {
  position: absolute;
  top: 0;
  width: 100%;
}
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'next {
  left: 100%;
}
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'prev {
  left: -100%;
}
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'next.'.CSS_PREFIX.'left,
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'prev.'.CSS_PREFIX.'right {
  left: 0;
}
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'active.'.CSS_PREFIX.'left {
  left: -100%;
}
.'.CSS_PREFIX.'carousel-inner > .'.CSS_PREFIX.'active.'.CSS_PREFIX.'right {
  left: 100%;
}
.'.CSS_PREFIX.'carousel-control {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  width: 15%;
  font-size: 20px;
  color: #fff;
  text-align: center;
  text-shadow: 0 1px 2px rgba(0, 0, 0, .6);
  background-color: rgba(0, 0, 0, 0);
  filter: alpha(opacity=50);
  opacity: .5;
}
.'.CSS_PREFIX.'carousel-control.'.CSS_PREFIX.'left {
  background-image: -webkit-linear-gradient(left, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .0001) 100%);
  background-image:      -o-linear-gradient(left, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .0001) 100%);
  background-image: -webkit-gradient(linear, left top, right top, from(rgba(0, 0, 0, .5)), to(rgba(0, 0, 0, .0001)));
  background-image:         linear-gradient(to right, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .0001) 100%);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#80000000", endColorstr="#00000000", GradientType=1);
  background-repeat: repeat-x;
}
.'.CSS_PREFIX.'carousel-control.'.CSS_PREFIX.'right {
  right: 0;
  left: auto;
  background-image: -webkit-linear-gradient(left, rgba(0, 0, 0, .0001) 0%, rgba(0, 0, 0, .5) 100%);
  background-image:      -o-linear-gradient(left, rgba(0, 0, 0, .0001) 0%, rgba(0, 0, 0, .5) 100%);
  background-image: -webkit-gradient(linear, left top, right top, from(rgba(0, 0, 0, .0001)), to(rgba(0, 0, 0, .5)));
  background-image:         linear-gradient(to right, rgba(0, 0, 0, .0001) 0%, rgba(0, 0, 0, .5) 100%);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#00000000", endColorstr="#80000000", GradientType=1);
  background-repeat: repeat-x;
}
.'.CSS_PREFIX.'carousel-control:hover,
.'.CSS_PREFIX.'carousel-control:focus {
  color: #fff;
  text-decoration: none;
  filter: alpha(opacity=90);
  outline: 0;
  opacity: .9;
}
.'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'icon-prev,
.'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'icon-next,
.'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'glyphicon-chevron-left,
.'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'glyphicon-chevron-right {
  position: absolute;
  top: 50%;
  z-index: 5;
  display: inline-block;
  margin-top: -10px;
}
.'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'icon-prev,
.'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'glyphicon-chevron-left {
  left: 50%;
  margin-left: -10px;
}
.'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'icon-next,
.'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'glyphicon-chevron-right {
  right: 50%;
  margin-right: -10px;
}
.'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'icon-prev,
.'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'icon-next {
  width: 20px;
  height: 20px;
  font-family: serif;
  line-height: 1;
}
.'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'icon-prev:before {
  content: "\2039";
}
.'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'icon-next:before {
  content: "\203a";
}
.'.CSS_PREFIX.'carousel-indicators {
  position: absolute;
  bottom: 10px;
  left: 50%;
  z-index: 15;
  width: 60%;
  padding-left: 0;
  margin-left: -30%;
  text-align: center;
  list-style: none;
}
.'.CSS_PREFIX.'carousel-indicators li {
  display: inline-block;
  width: 10px;
  height: 10px;
  margin: 1px;
  text-indent: -999px;
  cursor: pointer;
  background-color: #000 \9;
  background-color: rgba(0, 0, 0, 0);
  border: 1px solid #fff;
  border-radius: 10px;
}
.'.CSS_PREFIX.'carousel-indicators .'.CSS_PREFIX.'active {
  width: 12px;
  height: 12px;
  margin: 0;
  background-color: #fff;
}
.'.CSS_PREFIX.'carousel-caption {
  position: absolute;
  right: 15%;
  bottom: 20px;
  left: 15%;
  z-index: 10;
  padding-top: 20px;
  padding-bottom: 20px;
  color: #fff;
  text-align: center;
  text-shadow: 0 1px 2px rgba(0, 0, 0, .6);
}
.'.CSS_PREFIX.'carousel-caption .'.CSS_PREFIX.'btn {
  text-shadow: none;
}
@media screen and (min-width: 768px) {
  .'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'glyphicon-chevron-left,
  .'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'glyphicon-chevron-right,
  .'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'icon-prev,
  .'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'icon-next {
    width: 30px;
    height: 30px;
    margin-top: -10px;
    font-size: 30px;
  }
  .'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'glyphicon-chevron-left,
  .'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'icon-prev {
    margin-left: -10px;
  }
  .'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'glyphicon-chevron-right,
  .'.CSS_PREFIX.'carousel-control .'.CSS_PREFIX.'icon-next {
    margin-right: -10px;
  }
  .'.CSS_PREFIX.'carousel-caption {
    right: 20%;
    left: 20%;
    padding-bottom: 30px;
  }
  .'.CSS_PREFIX.'carousel-indicators {
    bottom: 20px;
  }
}
.'.CSS_PREFIX.'clearfix:before,
.'.CSS_PREFIX.'clearfix:after,
.'.CSS_PREFIX.'dl-horizontal dd:before,
.'.CSS_PREFIX.'dl-horizontal dd:after,
.'.CSS_PREFIX.'container:before,
.'.CSS_PREFIX.'container:after,
.'.CSS_PREFIX.'container-fluid:before,
.'.CSS_PREFIX.'container-fluid:after,
.'.CSS_PREFIX.'row:before,
.'.CSS_PREFIX.'row:after,
.'.CSS_PREFIX.'form-horizontal .'.CSS_PREFIX.'form-group:before,
.'.CSS_PREFIX.'form-horizontal .'.CSS_PREFIX.'form-group:after,
.'.CSS_PREFIX.'btn-toolbar:before,
.'.CSS_PREFIX.'btn-toolbar:after,
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn-group:before,
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn-group:after,
.'.CSS_PREFIX.'nav:before,
.'.CSS_PREFIX.'nav:after,
.'.CSS_PREFIX.'navbar:before,
.'.CSS_PREFIX.'navbar:after,
.'.CSS_PREFIX.'navbar-header:before,
.'.CSS_PREFIX.'navbar-header:after,
.'.CSS_PREFIX.'navbar-collapse:before,
.'.CSS_PREFIX.'navbar-collapse:after,
.'.CSS_PREFIX.'pager:before,
.'.CSS_PREFIX.'pager:after,
.'.CSS_PREFIX.'panel-body:before,
.'.CSS_PREFIX.'panel-body:after,
.'.CSS_PREFIX.'modal-header:before,
.'.CSS_PREFIX.'modal-header:after,
.'.CSS_PREFIX.'modal-footer:before,
.'.CSS_PREFIX.'modal-footer:after {
  display: table;
  content: " ";
}
.'.CSS_PREFIX.'clearfix:after,
.'.CSS_PREFIX.'dl-horizontal dd:after,
.'.CSS_PREFIX.'container:after,
.'.CSS_PREFIX.'container-fluid:after,
.'.CSS_PREFIX.'row:after,
.'.CSS_PREFIX.'form-horizontal .'.CSS_PREFIX.'form-group:after,
.'.CSS_PREFIX.'btn-toolbar:after,
.'.CSS_PREFIX.'btn-group-vertical > .'.CSS_PREFIX.'btn-group:after,
.'.CSS_PREFIX.'nav:after,
.'.CSS_PREFIX.'navbar:after,
.'.CSS_PREFIX.'navbar-header:after,
.'.CSS_PREFIX.'navbar-collapse:after,
.'.CSS_PREFIX.'pager:after,
.'.CSS_PREFIX.'panel-body:after,
.'.CSS_PREFIX.'modal-header:after,
.'.CSS_PREFIX.'modal-footer:after {
  clear: both;
}
.'.CSS_PREFIX.'center-block {
  display: block;
  margin-right: auto;
  margin-left: auto;
}
.'.CSS_PREFIX.'pull-right {
  float: right !important;
}
.'.CSS_PREFIX.'pull-left {
  float: left !important;
}
.'.CSS_PREFIX.'hide {
  display: none !important;
}
.'.CSS_PREFIX.'show {
  display: block !important;
}
.'.CSS_PREFIX.'invisible {
  visibility: hidden;
}
.'.CSS_PREFIX.'text-hide {
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}
.'.CSS_PREFIX.'hidden {
  display: none !important;
}
.'.CSS_PREFIX.'affix {
  position: fixed;
}
@-ms-viewport {
  width: device-width;
}
.'.CSS_PREFIX.'visible-xs,
.'.CSS_PREFIX.'visible-sm,
.'.CSS_PREFIX.'visible-md,
.'.CSS_PREFIX.'visible-lg {
  display: none !important;
}
.'.CSS_PREFIX.'visible-xs-block,
.'.CSS_PREFIX.'visible-xs-inline,
.'.CSS_PREFIX.'visible-xs-inline-block,
.'.CSS_PREFIX.'visible-sm-block,
.'.CSS_PREFIX.'visible-sm-inline,
.'.CSS_PREFIX.'visible-sm-inline-block,
.'.CSS_PREFIX.'visible-md-block,
.'.CSS_PREFIX.'visible-md-inline,
.'.CSS_PREFIX.'visible-md-inline-block,
.'.CSS_PREFIX.'visible-lg-block,
.'.CSS_PREFIX.'visible-lg-inline,
.'.CSS_PREFIX.'visible-lg-inline-block {
  display: none !important;
}
@media (max-width: 767px) {
  .'.CSS_PREFIX.'visible-xs {
    display: block !important;
  }
  table.'.CSS_PREFIX.'visible-xs {
    display: table !important;
  }
  tr.'.CSS_PREFIX.'visible-xs {
    display: table-row !important;
  }
  th.'.CSS_PREFIX.'visible-xs,
  td.'.CSS_PREFIX.'visible-xs {
    display: table-cell !important;
  }
}
@media (max-width: 767px) {
  .'.CSS_PREFIX.'visible-xs-block {
    display: block !important;
  }
}
@media (max-width: 767px) {
  .'.CSS_PREFIX.'visible-xs-inline {
    display: inline !important;
  }
}
@media (max-width: 767px) {
  .'.CSS_PREFIX.'visible-xs-inline-block {
    display: inline-block !important;
  }
}
@media (min-width: 768px) and (max-width: 991px) {
  .'.CSS_PREFIX.'visible-sm {
    display: block !important;
  }
  table.'.CSS_PREFIX.'visible-sm {
    display: table !important;
  }
  tr.'.CSS_PREFIX.'visible-sm {
    display: table-row !important;
  }
  th.'.CSS_PREFIX.'visible-sm,
  td.'.CSS_PREFIX.'visible-sm {
    display: table-cell !important;
  }
}
@media (min-width: 768px) and (max-width: 991px) {
  .'.CSS_PREFIX.'visible-sm-block {
    display: block !important;
  }
}
@media (min-width: 768px) and (max-width: 991px) {
  .'.CSS_PREFIX.'visible-sm-inline {
    display: inline !important;
  }
}
@media (min-width: 768px) and (max-width: 991px) {
  .'.CSS_PREFIX.'visible-sm-inline-block {
    display: inline-block !important;
  }
}
@media (min-width: 992px) and (max-width: 1199px) {
  .'.CSS_PREFIX.'visible-md {
    display: block !important;
  }
  table.'.CSS_PREFIX.'visible-md {
    display: table !important;
  }
  tr.'.CSS_PREFIX.'visible-md {
    display: table-row !important;
  }
  th.'.CSS_PREFIX.'visible-md,
  td.'.CSS_PREFIX.'visible-md {
    display: table-cell !important;
  }
}
@media (min-width: 992px) and (max-width: 1199px) {
  .'.CSS_PREFIX.'visible-md-block {
    display: block !important;
  }
}
@media (min-width: 992px) and (max-width: 1199px) {
  .'.CSS_PREFIX.'visible-md-inline {
    display: inline !important;
  }
}
@media (min-width: 992px) and (max-width: 1199px) {
  .'.CSS_PREFIX.'visible-md-inline-block {
    display: inline-block !important;
  }
}
@media (min-width: 1200px) {
  .'.CSS_PREFIX.'visible-lg {
    display: block !important;
  }
  table.'.CSS_PREFIX.'visible-lg {
    display: table !important;
  }
  tr.'.CSS_PREFIX.'visible-lg {
    display: table-row !important;
  }
  th.'.CSS_PREFIX.'visible-lg,
  td.'.CSS_PREFIX.'visible-lg {
    display: table-cell !important;
  }
}
@media (min-width: 1200px) {
  .'.CSS_PREFIX.'visible-lg-block {
    display: block !important;
  }
}
@media (min-width: 1200px) {
  .'.CSS_PREFIX.'visible-lg-inline {
    display: inline !important;
  }
}
@media (min-width: 1200px) {
  .'.CSS_PREFIX.'visible-lg-inline-block {
    display: inline-block !important;
  }
}
@media (max-width: 767px) {
  .'.CSS_PREFIX.'hidden-xs {
    display: none !important;
  }
}
@media (min-width: 768px) and (max-width: 991px) {
  .'.CSS_PREFIX.'hidden-sm {
    display: none !important;
  }
}
@media (min-width: 992px) and (max-width: 1199px) {
  .'.CSS_PREFIX.'hidden-md {
    display: none !important;
  }
}
@media (min-width: 1200px) {
  .'.CSS_PREFIX.'hidden-lg {
    display: none !important;
  }
}
.'.CSS_PREFIX.'visible-print {
  display: none !important;
}
@media print {
  .'.CSS_PREFIX.'visible-print {
    display: block !important;
  }
  table.'.CSS_PREFIX.'visible-print {
    display: table !important;
  }
  tr.'.CSS_PREFIX.'visible-print {
    display: table-row !important;
  }
  th.'.CSS_PREFIX.'visible-print,
  td.'.CSS_PREFIX.'visible-print {
    display: table-cell !important;
  }
}
.'.CSS_PREFIX.'visible-print-block {
  display: none !important;
}
@media print {
  .'.CSS_PREFIX.'visible-print-block {
    display: block !important;
  }
}
.'.CSS_PREFIX.'visible-print-inline {
  display: none !important;
}
@media print {
  .'.CSS_PREFIX.'visible-print-inline {
    display: inline !important;
  }
}
.'.CSS_PREFIX.'visible-print-inline-block {
  display: none !important;
}
@media print {
  .'.CSS_PREFIX.'visible-print-inline-block {
    display: inline-block !important;
  }
}
@media print {
  .'.CSS_PREFIX.'hidden-print {
    display: none !important;
  }
}
/*# sourceMappingURL=bootstrap.css.map */';
