<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <?php if(hasOption($db,'webmaster')) echo getOption($db,'webmaster')->opt_value; ?>

  <?php if($p=='index' || $p=='page') { ?>

  <title><?php echo $blogname?> <?php echo trim($URL['args'][1]) !== '' ? 'on Page '.$URL['args'][1] : ''; ?> | <?php echo $blogdesc;?></title>

  <?php $limit = 18; ?>
  <?php $page = trim($URL['args'][1]) !== '' ? $URL['args'][1] : 1; ?>
  <?php $offset = ($page - 1)  * $limit; ?>

  <?php $kwx = $db->get_results( "SELECT * FROM search_terms AS r1 JOIN (SELECT (RAND() * (SELECT MAX(id) FROM search_terms)) AS id) AS r2 WHERE r1.id >= r2.id AND r1.type='parent'  LIMIT 20" ); ?>
  <?php if(count($kwx) < 20 ) $kwx = $db->get_results( "SELECT * FROM search_terms where type='parent' order by ID DESC limit 20" ); ?>
  <?php 

    $d = ''; 

    if( count($kwx) > 0 ):       
      $i=0; 
      foreach ($kwx as $kv) : 
         $d .= removeSpecial(ucwords($kv->term)).'. ';
         if($i > 5) break; 
         $i++; 
      endforeach; 
    endif; 

  ?>

  <meta name="description" content="<?php echo count($kwx); ?><?php echo $d ;?>">
  <meta name="keywords" content="<?php echo $d ;?>">
  <meta name="robots" content="index, follow">
  <meta name="googlebot" content="index,follow,imageindex">
  <link rel="canonical" href="http://<?php echo $_SERVER['SERVER_NAME']; ?>">
  <script src="<?php echo AGCL_TEMPLATE_URL ?>lib/js/lazysizes.min.js" async=""></script>

  <?php } elseif ($p=='image') { ?>

  <?php

        $imgs = array();

        $cache = new Cache();
        $key = md5('single_post_'.$item->ID);

        if($cache->isCached($key)){

          $imgs = $cache->retrieve($key);

        }else{

            foreach($datas as $a) 
            {

                $im = $db->get_row("SELECT * FROM term_images where parent_term='".$a->ID."'");

                if($im){

                  $title = trim($a->term) == '' || ( count(explode(' ', $a->term)) < 2 ) ? ucwords($q) : ucwords($a->term);
                  
                  $childImgs = getChildImages($db,$im);

                  $sql_fields = array(

                      'term'  => $title,
                      'url'  => SITE_URL.'imgs/'.$im->url,                  
                      'childs' => $childImgs,                  
                      'height' =>$im->height,
                      'width' => $im->width,
                      'thumb' => $im->thumb,
                      'type'    => $im->type

                      ); 

                  $imgs[] = $sql_fields;   
                               
                }

            }

          $imgs = is_term_safe_bulk($imgs);
          $cache->setCache($key)->store($key, $imgs);

        }

  ?>
  
  <?php

    $d = ''; 
    if( $imgs ): 
      $i=0; 
      foreach ($imgs as $kv) : 
         $d .= ucwords($kv['term']).'. ';
         if($i > 5) break; 
         $i++; 
      endforeach; 
    endif; 
  
  ?>

  <title><?php echo ucwords($q);?> Images. <?php echo $d; ?></title>
  <meta itemprop="name" content="<?php echo ucwords($q);?> Images. <?php echo $d; ?>">
  
  <meta name="description" content="<?php echo $q;?>. Gallery images of <?php echo ucwords($q);?> Pictures.  <?php echo $d; ?>">
  <meta name="keywords" content="<?php echo $q;?> photos. <?php echo $q;?> images. <?php echo $q;?> pictures.  <?php echo $d; ?>">
  
  <meta name="robots" content="all,index,follow">
  <meta name="googlebot" content="index,follow,imageindex">
  <meta name="googlebot-Image" content="index,follow">
  <style type="text/css">
     .caption p, .caption strong, .caption em, .caption u{

        display: inline;
        font-style: initial;
        font-weight: 400;
        margin: 0;
        font-size: 16px;
        text-decoration: none;

    }
    .single-header{
        display: inline;
        font-style: initial;
        font-weight: 400;
        font-size: 16px;
        margin: 0;
        text-decoration: none;
    }
  </style>
  <link rel="canonical" href="<?php echo _a_url_q( $q ); ?>">
  <script src="<?php echo AGCL_TEMPLATE_URL ?>lib/js/lazysizes.min.js" async=""></script>

  <?php } elseif ($p=='attachment') { ?>
  
    <?php

        $imgs = array();

        $cache = new Cache();
        $key = md5('single_post_'.$item->ID);

        if($cache->isCached($key)){

          $imgs = $cache->retrieve($key);

        }else{

            foreach($datas as $a) 
            {

                $im = $db->get_row("SELECT * FROM term_images where parent_term='".$a->ID."'");

                if($im){

                  $title = trim($a->term) == '' || ( count(explode(' ', $a->term)) < 2 ) ? ucwords($q) : ucwords($a->term);
                  
                  $childImgs = getChildImages($db,$im);

                  $sql_fields = array(

                      'term'  => $title,
                      'url'  => SITE_URL.'imgs/'.$im->url,                  
                      'childs' => $childImgs,                  
                      'height' =>$im->height,
                      'width' => $im->width,
                      'thumb' => $im->thumb,
                      'type'    => $im->type

                      ); 

                  $imgs[] = $sql_fields;   
                               
                }

            }

          $imgs = is_term_safe_bulk($imgs);
          $cache->setCache($key)->store($key, $imgs);

        }
      
  ?>

  <title><?php echo ucwords($q);?> - <?php echo ucwords($item->term); ?></title>
  <meta itemprop="name" content="<?php echo ucwords($q);?> - <?php echo ucwords($item->term); ?>">

  <meta name="description" content="<?php echo ucwords($img['term']);?> - <?php echo ucwords($item->term); ?>">
  <meta name="keywords" content="<?php echo ucwords($img['term']);?> - <?php echo ucwords($item->term); ?>">
  
  <meta name="robots" content="index, follow">
  <meta name="googlebot" content="index,follow,imageindex">
  <meta name="googlebot-Image" content="index,follow">
  <style type="text/css">
     .caption p, .caption strong, .caption em, .caption u{

        display: inline;
        font-style: initial;
        font-weight: 400;
        margin: 0;
        font-size: 16px;
        text-decoration: none;

    }
    .single-header{
        display: inline;
        font-style: initial;
        font-weight: 400;
        font-size: 16px;
        margin: 0;
        text-decoration: none;
    }
  </style>

  <link rel="canonical" href="<?php echo to_attachment($item->term,$q); ?>">
  <script src="<?php echo AGCL_TEMPLATE_URL ?>lib/js/lazysizes.min.js" async=""></script>

  <?php } elseif ($p=='info') { ?>

    <title><?php echo $headtitle ?></title>
    <meta name="robots" content="noindex, follow">

  <?php } elseif ($p=='map') { ?>

  <title>Sitemap <?php echo $URL['args'][1]; ?></title>
  <meta name="robots" content="noindex, follow">

  <?php } else {?>

    <title>Page not found!</title>
    <meta name="robots" content="noindex, nofollow">
    
  <?php } ?>
  
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <style type="text/css">
    .panel-transparent{background-color: transparent !important;}
    .footlinks a {display: inline-block;margin-left: 10px;margin-right: 10px;}
    .description p{font-size: 12px !important;}
  </style>
  <style type="text/css">
    h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6{color:#ccc!important}.panel-transparent{background-color:transparent!important}.ads{margin:auto auto 5px;margin-bottom: 15px;}@media (max-width:767px){.rec-300{width:90%!important;margin-left:3px;margin-right:3px;padding:0!important;height:280px}.mrec-xs{width:90%}.leaderboard-xs{width:90%;height:90px}.mobile-xs{width:90%;height:50px}.skyscraper-xs{width:120px;height:600px}.banner-xs{width:100%;height:60px}}@media (min-width:768px) and (max-width:991px){.rec-300{width:336px!important;margin-left:3px;margin-right:3px;padding:0!important;height:280px}.mrec-sm{width:300px}.leaderboard-sm{width:728px;height:90px}.mobile-sm{width:320px;height:50px}.skyscraper-sm{width:120px;height:600px}.banner-sm{width:468px;height:60px}}@media (min-width:992px) and (max-width:1199px){.rec-300{width:336px!important;margin-left:3px;margin-right:3px;padding:0!important;height:280px}.mrec-md{width:300px}.leaderboard-md{width:728px;height:90px}.mobile-md{width:320px;height:50px}.skyscraper-md{width:120px;height:600px}.banner-md{width:468px;height:60px}}@media (min-width:1200px){.rec-300{width:336px!important;margin-left:3px;margin-right:3px;padding:0!important;height:280px}.mrec-lg{width:300px}.leaderboard-lg{width:728px;height:90px}.mobile-lg{width:320px;height:50px}.skyscraper-lg{width:120px;height:600px}.banner-lg{width:468px;height:60px}}
  </style>
  
    <?php echo $header; ?>
  
  </head>
  <body style="background: <?php echo $colors[COLOR]['body']; ?>;">

    <nav class="<?php echo CSS_PREFIX; ?>navbar <?php echo CSS_PREFIX; ?>navbar-default <?php echo CSS_PREFIX; ?>navbar-fixed-top" style="background: <?php echo $colors[COLOR]['header']['main']; ?>;border-color: <?php echo $colors[COLOR]['header']['border']; ?>;">
      <div class="<?php echo CSS_PREFIX; ?>container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="<?php echo CSS_PREFIX; ?>navbar-header">
          <button type="button" class="<?php echo CSS_PREFIX; ?>navbar-toggle <?php echo CSS_PREFIX; ?>collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="<?php echo CSS_PREFIX; ?>sr-only">Toggle navigation</span>
            <span class="<?php echo CSS_PREFIX; ?>icon-bar"></span>
            <span class="<?php echo CSS_PREFIX; ?>icon-bar"></span>
            <span class="<?php echo CSS_PREFIX; ?>icon-bar"></span>
          </button>
          <a id="header-navbar-brand" class="<?php echo CSS_PREFIX; ?>navbar-brand" href="<?php echo SITE_URL ?>" style="color: <?php echo $colors[COLOR]['header']['title']; ?>;">
            <?php echo $_SERVER['SERVER_NAME']; ?>
          </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="<?php echo CSS_PREFIX; ?>collapse <?php echo CSS_PREFIX; ?>navbar-collapse" id="bs-example-navbar-collapse-1">

          <form class="<?php echo CSS_PREFIX; ?>navbar-form <?php echo CSS_PREFIX; ?>navbar-right" method="get" action="<?php echo SITE_URL . "go"; ?>">
            <div class="<?php echo CSS_PREFIX; ?>form-group">
              <input type="text" name="q" class="<?php echo CSS_PREFIX; ?>form-control" placeholder="Search">
              <input type="submit" 
                     style="position: absolute; left: -9999px; width: 1px; height: 1px;"
                     tabindex="-1" />
            </div>
          </form>

        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>