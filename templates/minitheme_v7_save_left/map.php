<?php 

ob_start("ob_html_compress");
$p = 'map';
include AGCL_TEMPLATE_PATH . 'header.php'; 

?>
<div class="margin-top-50"></div>
<div class="<?php echo CSS_PREFIX; ?>container">

	<div class="<?php echo CSS_PREFIX; ?>row <?php echo CSS_PREFIX; ?>text-center">	
	    <h2>Sitemap for Page <?php echo $URL['args'][1];?></h2> 
	    <hr>
	    <?php $limit = 50; ?>
            <?php $offset = ($URL['args'][1] - 1)  * $limit; ?>

	    <?php $kwx = $db->get_results( "SELECT * FROM search_terms where type = 'parent' LIMIT ".$offset.",50" );
	    if( $kwx ): ?>
		    <?php foreach ($kwx as $kv) : ?>

			  <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>text-left">

			      <a href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo ucwords($kv->term);?>"><?php echo ucwords($kv->term);?></a>
			      <ul>
			      	<?php
                        $s = $db->get_results( "SELECT * FROM search_terms where parent_id = ".$kv->ID." AND type='child'" );
                    	foreach($s as $a){
                    ?>
			      	<li>
						<a href="<?php echo to_attachment($kv->term,$a->term); ?>" title="<?php echo $a->term; ?>" rel="bookmark">
			      			<?php echo ucwords($a->term);?>
			      		</a>
			      	</li>
			      	<?php } ?>
			      </ul>
			  </div>

		    <?php endforeach; ?>
	    <?php endif; ?>

	</div><!--.row-->

</div><!--.container-->

<?php include AGCL_TEMPLATE_PATH . 'footer.php'; ?>
<?php ob_end_flush(); ?>
