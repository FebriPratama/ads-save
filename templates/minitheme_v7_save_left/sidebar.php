<div class="<?php echo CSS_PREFIX; ?>panel <?php echo CSS_PREFIX; ?>panel-default" style="background: <?php echo $colors[COLOR]['body']; ?>;">
  <div class="<?php echo CSS_PREFIX; ?>panel-body">
    
    <div class="<?php echo CSS_PREFIX; ?>row">
      <div class="<?php echo CSS_PREFIX; ?>col-md-12">
        <h3 itemprop="name">Popular Posts</h3>
      </div>
    </div>

    <?php 

    $kwx = $db->get_results( "SELECT * FROM search_terms AS r1 JOIN (SELECT (RAND() * (SELECT MAX(id) FROM search_terms)) AS id) AS r2 WHERE r1.id >= r2.id  AND r1.type='parent' LIMIT 32" );
    
    $kv = [];

    ?>

    <?php
    $i = 0 ;
    if( $kwx ): ?>
    <ul>

      <?php foreach ($kwx as $kv) : ?>
        <li>
          <a href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo removeSpecial(ucwords($kv->term));?>"><h3 class="single-header"><?php echo removeSpecial(ucwords($kv->term));?></h3></a>
        </li>
        
        <?php $i++; ?>
        <?php if($i>1) break; ?>
      <?php endforeach; ?>
    </ul>

    <?php endif; ?> 

    <?php if ($p=='image' || $p== 'index') { ?>
    <hr style="height:3px;">
    <div class="<?php echo CSS_PREFIX; ?>row">
        <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>col-xs-12">

            <div class="ads mrec-lg mrec-md mrec-sm mrec-xs">
              <?php if(hasOption($db,'ads160')) echo getOption($db,'ads160')->opt_value; ?>
            </div>   
            
        </div>
    </div>

    <?php } ?>

    <?php
    $i = 0 ;
    if( $kwx ): ?>
    <ul>

      <?php foreach ($kwx as $kv) : ?>

        <?php if($i>1){ ?>
        <li>
          <a href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo removeSpecial(ucwords($kv->term));?>"><h3 class="single-header"><?php echo removeSpecial(ucwords($kv->term));?></h3></a>
        </li>
        <?php } ?>
        <?php $i++; ?>

      <?php endforeach; ?>
    </ul>

    <?php endif; ?> 

  </div>
</div>

<?php $index_term = is_object($kv) ? $kv->term : '';