<div id="s">
        <!-- page -->
        <div class="vn"></div>

        <?php if($p !== 'info' && $p !== '404' && $p !== 'go'){ ?>

        <!-- page -->
        <?php  

            $kwx = $db->get_results( "SELECT * FROM search_terms AS r1 JOIN (SELECT (RAND() * (SELECT MAX(id) FROM search_terms)) AS id) AS r2 WHERE r1.id >= r2.id  AND r1.type='parent' LIMIT 0,4" );

          ?>
        <div class="s">
            <div class="cf">
                <div class="rf">
                    <p>

                      <i class="fa fa-star-o" aria-hidden="true"></i> Awesome Designs</p>

                    <?php $i=0; ?>
                    <?php if( $kwx ) : ?>
                    <?php foreach ($kwx as $kv) : ?>
                        <?php if($i>1){ ?>
                        <?php $img = $db->get_row("select b.* from search_terms as a join term_images as b on b.parent_term = a.ID where a.parent_id = '".$kv->ID."'"); ?>

                        <?php if(is_object($img)){ ?>

                        <?php 

                          $img = array(

                              'term'  => removeSpecial(ucwords($kv->term)),
                              'url'  => SITE_URL.'imgs/'.$img->url,       
                              'height' =>$img->height,
                              'width' => $img->width

                              ); 

                        ?>

                        <div class="di">
                          
                            <a rel="nofollow" href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $img['term']; ?>"></a>

                            <img class="rg" src="<?php echo $img['url']; ?>" alt="<?php echo $img['term']; ?>" width="120" height="120" />

                            <h5 class="hd"><?php echo $img['term']; ?></h5>

                            <div class="cl"></div>

                        </div>
                    
                        <?php } ?>
                      <?php } ?>

                    <?php $i++; ?>
                    <?php endforeach; ?>
                    <?php endif; ?>

                    <div class="cl"></div>
                </div>
            </div>
            <div class="cf">
                <div class="rf">
                    <p><i class="fa fa-heart-o" aria-hidden="true"></i> Lovely Designs</p>

                    <?php $i=0; ?>
                    <?php if( $kwx ) : ?>
                    <?php foreach ($kwx as $kv) : ?>
                        <?php if($i>1) break; ?>
                        <?php $img = $db->get_row("select b.* from search_terms as a join term_images as b on b.parent_term = a.ID where a.parent_id = '".$kv->ID."'"); ?>

                        <?php if(is_object($img)){ ?>

                        <?php 

                          $img = array(

                              'term'  => removeSpecial(ucwords($kv->term)),
                              'url'  => SITE_URL.'imgs/'.$img->url,       
                              'height' =>$img->height,
                              'width' => $img->width,

                              ); 

                        ?>

                        <div class="di">
                            <a rel="nofollow" href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $img['term']; ?>"></a>

                            <img class="rg" src="<?php echo $img['url']; ?>" alt="<?php echo $img['term']; ?>" width="120" height="120" />

                            <h5 class="hd"><?php echo $img['term']; ?></h5>
                            <div class="cl"></div>
                        </div>

                        <?php } ?>

                    <?php $i++; ?>
                    <?php endforeach; ?>
                    <?php endif; ?>
            
                    <div class="cl"></div>
                </div>
            </div>

        </div>
        <?php } ?>

        <div class="r"></div>
    </div>

    <!-- sitemap footer -->
    <div id="st">
        <div class="s">

          <?php if($p !== 'info' && $p !== '404' && $p !== 'go'){ ?>
          <?php $total = $db->get_row( "SELECT count(*) as total FROM  search_terms WHERE type='parent'"); ?>
          <?php $totalPage = ceil($total->total/50); ?> 
          <?php $totalPage = $totalPage < 1 ? 1 : $totalPage ?>
          <?php for($i=1;$i<=$totalPage;$i++){

              echo '<a href="'. SITE_URL . 'map/'. $i . AGCL_URL_SUFFIX. '">.</a>';

              } ?>
            
            <br>
          <?php } ?>
          
            Any content trademark/s or other material that might be found on this simains the copyright of its respective owner/s. While using this site, you agree to have read and accepted our terms of use, cookie and <a rel="nofollow" href="<?php echo SITE_URL . 'info/privacy-policy' . AGCL_URL_SUFFIX ?>">privacy policy</a>. <a rel="nofollow" href="<?php echo SITE_URL . 'info/contact' . AGCL_URL_SUFFIX ?>">Contact Us Here</a>.
            <br>Copyright <a rel="nofollow" href="<?php echo SITE_URL . 'info/copyright' . AGCL_URL_SUFFIX ?>">©</a> <?php echo date('Y'); ?> <?php echo $_SERVER['SERVER_NAME']; ?>. All Rights Reserved.

          </div>

    </div>
    <!-- sitemap footer -->

    <script type="text/javascript" src="<?php echo AGCL_TEMPLATE_URL ?>lib/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo AGCL_TEMPLATE_URL ?>lib/js/jquery.lazyload.min.js">
    </script>
    <script type="text/javascript">
        $(function() {
            $("img.lazy").lazyload({
                effect: "fadeIn"
            });
        });
    </script>
    <?php if(hasOption($db,'stat')) echo getOption($db,'stat')->opt_value; ?>
</body>
</html>