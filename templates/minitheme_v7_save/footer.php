  <footer id="fh5co-footer">
    
    <div class="<?php echo CSS_PREFIX; ?>container">
      <div class="<?php echo CSS_PREFIX; ?>row">
        <div class="<?php echo CSS_PREFIX; ?>col-sm-12 <?php echo CSS_PREFIX; ?>text-center">

          <?php $total = $db->get_row( "SELECT count(*) as total FROM  search_terms WHERE type='parent'"); ?>
          <?php $totalPage = ceil($total->total/50); ?> 
          <?php $totalPage = $totalPage < 1 ? 1 : $totalPage ?>
          <?php for($i=1;$i<=$totalPage;$i++){

              echo '<a href="'. SITE_URL . 'map/'. $i . AGCL_URL_SUFFIX. '">*</a>&nbsp; | &nbsp;';

              } ?>

        </div>
        <div class="<?php echo CSS_PREFIX; ?>col-sm-12 <?php echo CSS_PREFIX; ?>text-center">

         <p class="footlinks">
           <a rel="nofollow" href="<?php echo SITE_URL . 'info/privacy-policy' . AGCL_URL_SUFFIX ?>">Privacy Policy</a> 
           <a rel="nofollow" href="<?php echo SITE_URL . 'info/contact' . AGCL_URL_SUFFIX ?>">Contact</a> 
           <a rel="nofollow" href="<?php echo SITE_URL . 'info/copyright' . AGCL_URL_SUFFIX ?>">DMCA / Copyright</a> 
         </p>

        </div><!--.col-->
      </div><!--.row-->

      <div class="<?php echo CSS_PREFIX; ?>row row-padded">
        <div class="<?php echo CSS_PREFIX; ?>col-md-12 <?php echo CSS_PREFIX; ?>text-center">
          <p><small>&copy; <?php echo $_SERVER['SERVER_NAME']; ?>. All Rights Reserved.</small></p>
        </div>
      </div>
    </div>
    
  </footer>

  <!-- Theme Style -->
  <link rel="stylesheet" href="<?php echo AGCL_TEMPLATE_URL ?>hydrogen/css/style-min.css" as="style" onload="this.rel='stylesheet'">
  <link rel="stylesheet" href="<?php echo AGCL_TEMPLATE_URL ?>lib/css/<?php echo CSS_PREFIX; ?>.css" as="style" onload="this.rel='stylesheet'">

  <?php echo $footer; ?>
  <?php if(hasOption($db,'stat')) echo getOption($db,'stat')->opt_value; ?>
  
</body>
</html>