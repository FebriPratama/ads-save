<?php 

    ob_start("ob_html_compress");

    $p = 'attachment';
    $q = preg_replace('/([^a-z0-9]+)/i', ' ', $URL['args'][1]);

    $term  = $q;
    $term .= "\n" . file_get_contents(ABSPATH . 'term.txt');
    file_put_contents(ABSPATH . 'term.txt', $term);  

    include AGCL_TEMPLATE_PATH . 'header.php';
          
?>

    <div id="a">

            <div id="g"><h1><?php echo ucwords($q);?></h1>

            <div class="ds">
                <?php if(hasOption($db,'ads728')) echo getOption($db,'ads728')->opt_value; ?>
            </div>

            <!-- breadcumb -->
            <div id="bc" itemscope itemtype="http://schema.org/BreadcrumbList">
                <i itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemprop="item" rel="nofollow" href="<?php echo SITE_URL ?>">
                        <span itemprop="name">Home</span>
                    </a>
                    <meta itemprop="position" content="1" />
                </i> &#187; 
                <i itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <a itemprop="item" rel="nofollow" href="<?php echo _a_url_q( $item->term ); ?>">
                        <span itemprop="name"><?php echo $item->term; ?></span>
                    </a>
                    <meta itemprop="position" content="2" />
                </i> &#187; 
                <i itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <i itemprop="item">
                        <span itemprop="name"><?php echo ucwords($q);?></span>
                    </i>
                    <meta itemprop="position" content="3" />
                </i>
            </div>

    <figure>
        <?php

            $title = $img['term'];

            $width = $img['width'];
            $height = $img['height'];
            $imageurl = $img['url'];
            $thumbnail_url = $img['thumb'];

            ?>

            <img src="<?php echo AGCL_TEMPLATE_URL ?>lib/img/loading.png" title="<?php echo $title ?>" class="lazy" data-original="<?php echo $imageurl; ?>" alt="<?php echo $title ?>" width="<?php echo $width;?>" height="<?php echo $height;?>" />


            <noscript>

                <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                    <img title="<?php echo $title ?>" style="width:100%;" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?>" itemprop="contentUrl"/>  

                </i>

                    <?php

                        if(array_key_exists('large', $img['childs']) && is_object($img['childs']['large'])){

                            $width = $img['childs']['large']->width;
                            $height = $img['childs']['large']->height;
                            $imageurl = SITE_URL.'imgs/'.$img['childs']['large']->url;
                            $thumbnail_url = $img['childs']['large']->thumb;

                ?>                  
                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">                     
                            <img title="<?php echo $title ?> Large Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Large Version" itemprop="contentUrl"/>  
                    </i>
                <?php } ?>

                    <?php

                        if(array_key_exists('medium', $img['childs']) && is_object($img['childs']['medium'])){

                            $width = $img['childs']['medium']->width;
                            $height = $img['childs']['medium']->height;
                            $imageurl = SITE_URL.'imgs/'.$img['childs']['medium']->url;
                            $thumbnail_url = $img['childs']['medium']->thumb;

                ?>                  
                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">             
                            <img title="<?php echo $title ?> Medium Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Medium Version" itemprop="contentUrl"/>  
                    </i>
                <?php } ?>

                    <?php

                        if(array_key_exists('thumb', $img['childs']) && is_object($img['childs']['thumb'])){

                            $width = $img['childs']['thumb']->width;
                            $height = $img['childs']['thumb']->height;
                            $imageurl = SITE_URL.'imgs/'.$img['childs']['thumb']->url;
                            $thumbnail_url = $img['childs']['thumb']->thumb;

                ?>              
                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">                         
                            <img title="<?php echo $title ?> Thumbnail Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Thumbnail Version" itemprop="contentUrl"/> 
                    </i>

                <?php } ?>

            </noscript>     
    </figure>
</div>
<div id="i">

    <blockquote>

        <?php
        $kw   = $q;
        $url  = 'http://www.bing.com/search?q='.urlencode(removeSpecial($q)).'&format=rss&count=20';
        $data = simplexml_load_file($url);
        if(@simplexml_load_file($url)){
        $i=0;
        foreach($data->channel->item as $a){
            
            $dom = array('.com','.net','.org','.id','.co.uk');
            $title = str_replace($dom, '', $a->title);
            $title = preg_replace('/([^a-z0-9]+)/i',' ',$title);

            if(is_term_safe(removeSpecial(strtolower($title)))) {

        ?>

        <?php echo ucfirst(removeSpecial(strtolower($a->description)));?>.&nbsp;
        
        <?php if($i%5==0) echo '. '; ?>

        <?php $i++; } } } ?>
        
        <em><?php echo ucwords($q);?></em> - <strong><?php echo $item->term; ?></strong>

    </blockquote>
    
    <div class="ki">

        <div class="mrec-xs mrec-sm mrec-md mrec-lg">

            <?php if(hasOption($db,'ads300')) echo getOption($db,'ads300')->opt_value; ?>

        </div>

    </div>

<?php

if( $imgs ):
    $i=0;
    foreach ($imgs as $img) :

        if($i>0){

        $title = $img['term'];

        if(array_key_exists('medium', $img['childs']) && is_object($img['childs']['medium'])){

            $width = $img['childs']['medium']->width;
            $height = $img['childs']['medium']->height;
            $imageurl = SITE_URL.'imgs/'.$img['childs']['medium']->url;
            $thumbnail_url = $img['childs']['medium']->thumb;

        }else{

            $width = $img['width'];
            $height = $img['height'];
            $imageurl = $img['url'];
            $thumbnail_url = $img['thumb'];

        }

        ?>

        <figure class="s2" itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
            <a target="_blank" href="<?php echo to_attachment($item->term,$title); ?>" title="<?php echo $title ?>">

                <img src="<?php echo AGCL_TEMPLATE_URL ?>lib/img/loading.png" class="lazy" title="<?php echo $title ?>" width="<?php echo $width;?>" height="<?php echo $height;?>" data-original="<?php echo $imageurl; ?>" alt="<?php echo $title ?>"/> 

                <noscript>
                    <?php

                        $width = $img['width'];
                        $height = $img['height'];
                        $imageurl = $img['url'];
                        $thumbnail_url = $img['thumb'];

                    ?>
                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <img title="<?php echo $title ?>" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?>" itemprop="contentUrl"/>  
                    </i>
                    <?php

                        if(array_key_exists('large', $img['childs']) && is_object($img['childs']['large'])){

                            $width = $img['childs']['large']->width;
                            $height = $img['childs']['large']->height;
                            $imageurl = SITE_URL.'imgs/'.$img['childs']['large']->url;
                            $thumbnail_url = $img['childs']['large']->thumb;

                ?>                  
                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">                     
                            <img title="<?php echo $title ?> Large Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Large Version" itemprop="contentUrl"/>  
                    </i>
                <?php } ?>

                    <?php

                        if(array_key_exists('medium', $img['childs']) && is_object($img['childs']['medium'])){

                            $width = $img['childs']['medium']->width;
                            $height = $img['childs']['medium']->height;
                            $imageurl = SITE_URL.'imgs/'.$img['childs']['medium']->url;
                            $thumbnail_url = $img['childs']['medium']->thumb;

                ?>                  
                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">             
                            <img title="<?php echo $title ?> Medium Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Medium Version" itemprop="contentUrl"/>  
                    </i>
                <?php } ?>

                    <?php

                        if(array_key_exists('thumb', $img['childs']) && is_object($img['childs']['thumb'])){

                            $width = $img['childs']['thumb']->width;
                            $height = $img['childs']['thumb']->height;
                            $imageurl = SITE_URL.'imgs/'.$img['childs']['thumb']->url;
                            $thumbnail_url = $img['childs']['thumb']->thumb;

                ?>              
                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">                         
                            <img title="<?php echo $title ?> Thumbnail Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Thumbnail Version" itemprop="contentUrl"/> 
                    </i>

                <?php } ?>

                </noscript>

            </a>
            <figcaption class="h4">
                <?php echo ucwords(suffleTitleSingle($title,$i)); ?> <?php echo ucwords(suffleCat());?>
            </figcaption>
        </figure>

        <?php 
        }
        $i++;   

        endforeach;
    endif;

    ?>

    <?php  

        $kwx = $db->get_results( "SELECT * FROM search_terms AS r1 JOIN (SELECT (RAND() * (SELECT MAX(id) FROM search_terms)) AS id) AS r2 WHERE r1.id >= r2.id  AND r1.type='parent' LIMIT 6" );

      ?>

    <div class="ds">
        <?php if(hasOption($db,'ads728')) echo getOption($db,'ads728')->opt_value; ?>
    </div>

    <div id="rc">
        <p><i class="fa fa-trophy" aria-hidden="true"></i></i> Popular Designs</p>

        <?php $i=0; ?>
        <?php if( $kwx ) : ?>
        <?php foreach ($kwx as $kv) : ?>
            <?php if($i>2) break; ?>
            <?php $img = $db->get_row("select b.* from search_terms as a join term_images as b on b.parent_term = a.ID where a.parent_id = '".$kv->ID."'"); ?>

            <?php if(is_object($img)){ ?>

            <?php 

              $img = array(

                  'term'  => removeSpecial(ucwords($kv->term)),
                  'url'  => SITE_URL.'imgs/'.$img->url,       
                  'height' =>$img->height,
                  'width' => $img->width,
                  'thumb' => $img->thumb,
                  'type'    => $img->type

                  ); 

            ?>

            <div class="di">

                <a rel="nofollow" href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $img['term']; ?>"></a>

                <img class="rg" src="<?php echo $img['url']; ?>" alt="<?php echo $img['term']; ?>" width="120" height="120" />

                <h4 class="hd"><?php echo $img['term']; ?></h4>
                <div class="cl"></div>
            </div>

            <?php } ?>

        <?php $i++; ?>
        <?php endforeach; ?>
        <?php endif; ?>
        
        <div class="cl"></div>

    </div>

    <div id="rc">
        <p><i class="fa fa-puzzle-piece" aria-hidden="true"></i> Beautiful <?php echo ucwords(suffleCat());?> Designs</p>

        <?php $i=0; ?>
        <?php if( $kwx ) : ?>
        <?php foreach ($kwx as $kv) : ?>

            <?php if($i>2){ ?>
            <?php $img = $db->get_row("SELECT b.* from search_terms as a join term_images as b on b.parent_term = a.ID where a.parent_id = '".$kv->ID."'"); ?>

            <?php if(is_object($img)){ ?>

            <?php 

              $img = array(

                  'term'  => removeSpecial(ucwords($kv->term)),
                  'url'  => SITE_URL.'imgs/'.$img->url,       
                  'height' =>$img->height,
                  'width' => $img->width,
                  'thumb' => $img->thumb,
                  'type'    => $img->type

                  ); 

            ?>

            <div class="di">

                <a rel="nofollow" href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $img['term']; ?>"></a>

                <img class="rg" src="<?php echo $img['url']; ?>" alt="<?php echo $img['term']; ?>" width="120" height="120" />

                <h4 class="hd"><?php echo $img['term']; ?></h4>
                <div class="cl"></div>
            </div>

            <?php } ?>
           <?php } ?>

        <?php $i++; ?>
        <?php endforeach; ?>
        <?php endif; ?>

        <div class="cl"></div>
    </div>

</div>
<div id="t">
    <div class="kt">
<!-- Sidebar -->
       <?php include 'sidebar.php'; ?>
<!-- Sidebar -->

    </div>
</div>
</div>

<div class="r"></div>

<?php include AGCL_TEMPLATE_PATH . 'footer.php'; ?>
<?php ob_end_flush(); ?>
