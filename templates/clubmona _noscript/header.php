<!DOCTYPE html>
<!-- saved from url=(0020)$base_URL/ -->
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link href="<?php echo AGCL_TEMPLATE_URL ?>lib/css/mc.css" media="screen" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="#" type="image/x-icon">
    <link href="<?php echo AGCL_TEMPLATE_URL ?>lib/css/css.css" rel="stylesheet">

  <?php if(hasOption($db,'webmaster')) echo getOption($db,'webmaster')->opt_value; ?>

  <?php if($p=='index' || $p=='page') { ?>

  <title><?php echo $blogname?> <?php echo trim($URL['args'][1]) !== '' ? 'on Page '.$URL['args'][1] : ''; ?> | <?php echo $blogdesc;?></title>

  <?php //$kwx = $db->get_results( "SELECT * FROM search_terms AS r1 JOIN (SELECT (RAND() * (SELECT MAX(id) FROM search_terms)) AS id) AS r2 WHERE r1.id >= r2.id AND r1.type='parent'  LIMIT 20" ); ?>

  <?php $limit = 18; ?>
  <?php $page = trim($URL['args'][1]) !== '' ? $URL['args'][1] : 1; ?>
  <?php $offset = ($page - 1)  * $limit; ?>
  <?php $kwx = $db->get_results( "SELECT * FROM search_terms WHERE type='parent' ORDER BY ID DESC LIMIT ".$offset.",18" ); ?>

  <?php 

    $d = ''; 

    if( $kwx ):       
      $i=0; 
      foreach ($kwx as $kv) : 
         $d .= removeSpecial(ucwords($kv->term)).'. ';
         if($i > 5) break; 
         $i++; 
      endforeach; 
    endif; 

  ?>

  <meta name="description" content="<?php echo $d ;?>">
  <meta name="keywords" content="<?php echo $d ;?>">
  <meta name="robots" content="index, follow">
  <meta name="googlebot" content="index,follow,imageindex">
  <link rel="canonical" href="http://<?php echo $_SERVER['SERVER_NAME']; ?>">

  <?php } elseif ($p=='image') { ?>

  <?php

        $imgs = array();

        $cache = new Cache();
        $key = md5('single_post_'.$item->ID);

        if($cache->isCached($key)){

          $imgs = $cache->retrieve($key);

        }else{

            foreach($datas as $a) 
            {

                $im = $db->get_row("SELECT * FROM term_images where parent_term='".$a->ID."'");

                if($im){

                  $title = trim($a->term) == '' || ( count(explode(' ', $a->term)) < 2 ) ? ucwords($q) : ucwords($a->term);
                  
                  $childImgs = getChildImages($db,$im);

                  $sql_fields = array(

                      'term'  => $title,
                      'url'  => SITE_URL.'imgs/'.$im->url,  
                      'childs' => $childImgs,                  
                      'height' =>$im->height,
                      'width' => $im->width,
                      'thumb' => $im->thumb,
                      'type'    => $im->type

                      ); 

                  $imgs[] = $sql_fields;   
                               
                }

            }

          $imgs = is_term_safe_bulk($imgs);
          $cache->setCache($key)->store($key, $imgs);

        }

  ?>
  
  <?php

    $d = ''; 
    if( $imgs ): 
      $i=0; 
      foreach ($imgs as $kv) : 
         $d .= ucwords($kv['term']).'. ';
         if($i > 5) break; 
         $i++; 
      endforeach; 
    endif; 
  
  ?>

  <title><?php echo ucwords(suffleTitleAwal());?> <?php echo ucwords($q);?></title>

  <meta name="description" content="<?php echo ucwords(suffleTitleAwal());?> <?php echo $q;?>. Gallery images of <?php echo ucwords($q);?> Pictures.">
  <meta name="keywords" content="<?php echo ucwords(suffleTitleAwal());?> <?php echo $q;?> photos. <?php echo $q;?> images. <?php echo $q;?> pictures.">
  
  <meta name="robots" content="all,index,follow">
  <meta name="googlebot" content="index,follow,imageindex">
  <meta name="googlebot-Image" content="index,follow">

  <link rel="canonical" href="<?php echo _a_url_q( $q ); ?>">

  <?php } elseif ($p=='attachment') { ?>
  
    <?php

        $imgs = array();

        $cache = new Cache();
        $key = md5('single_post_'.$item->ID);

        if($cache->isCached($key)){

          $imgs = $cache->retrieve($key);

        }else{

            foreach($datas as $a) 
            {

                $im = $db->get_row("SELECT * FROM term_images where parent_term='".$a->ID."'");

                if($im){

                  $title = trim($a->term) == '' || ( count(explode(' ', $a->term)) < 2 ) ? ucwords($q) : ucwords($a->term);
                  
                  $childImgs = getChildImages($db,$im);

                  $sql_fields = array(

                      'term'  => $title,
                      'url'  => SITE_URL.'imgs/'.$im->url,                  
                      'childs' => $childImgs,                  
                      'height' =>$im->height,
                      'width' => $im->width,
                      'thumb' => $im->thumb,
                      'type'    => $im->type

                      ); 

                  $imgs[] = $sql_fields;   
                               
                }

            }

          $imgs = is_term_safe_bulk($imgs);
          $cache->setCache($key)->store($key, $imgs);

        }
      
  ?>

  <title><?php echo ucwords($q);?> - <?php echo ucwords($item->term); ?></title>

  <meta name="description" content="Photo : <?php echo ucwords($img['term']);?> - <?php echo ucwords($item->term); ?>">
  <meta name="keywords" content="<?php echo ucwords($img['term']);?> - <?php echo ucwords($item->term); ?>">
  
  <meta name="robots" content="index, follow">
  <meta name="googlebot" content="index,follow,imageindex">
  <meta name="googlebot-Image" content="index,follow">

  <link rel="canonical" href="<?php echo to_attachment($item->term,$q); ?>">

  <?php } elseif ($p=='info') { ?>

    <title><?php echo $headtitle ?></title>
    <meta name="robots" content="noindex, follow">

  <?php } elseif ($p=='map') { ?>

  <title>Sitemap <?php echo $URL['args'][1]; ?></title>
  <meta name="robots" content="noindex, follow">

  <?php } elseif ($p=='go') { ?>

  <title>Search for <?php echo $URL['args'][1]; ?></title>
  <meta name="robots" content="noindex, follow">

  <?php } else {?>

    <title>Page not found!</title>
    <meta name="robots" content="noindex, nofollow">
    
  <?php } ?>
  
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
    .ads{margin:auto auto 5px}@media (max-width:767px){.mrec-xs{width:300px}.leaderboard-xs{width:728px;height:90px}.mobile-xs{width:320px;height:50px}.skyscraper-xs{width:120px;height:600px}.banner-xs{width:468px;height:60px}}@media (min-width:768px) and (max-width:991px){.mrec-sm{width:300px}.leaderboard-sm{width:728px;height:90px}.mobile-sm{width:320px;height:50px}.skyscraper-sm{width:120px;height:600px}.banner-sm{width:468px;height:60px}}@media (min-width:992px) and (max-width:1199px){.mrec-md{width:300px}.leaderboard-md{width:728px;height:90px}.mobile-md{width:320px;height:50px}.skyscraper-md{width:120px;height:600px}.banner-md{width:468px;height:60px}}@media (min-width:1200px){.mrec-lg{width:300px}.leaderboard-lg{width:728px;height:90px}.mobile-lg{width:320px;height:50px}.skyscraper-lg{width:120px;height:600px}.banner-lg{width:468px;height:60px}}
  </style>
    <?php echo $header; ?>
</head>

<body style="">
<div id="n">
  <div class="m0">
      <div class="h1">

          <a href="<?php echo SITE_URL; ?>"></a>

          <?php if($p=='index'){ ?>
            <h1><?php echo $_SERVER['SERVER_NAME']; ?></h1>
          <?php }else if($p == 'image' || $p == 'attachment'){ ?>
            <h2><?php echo $_SERVER['SERVER_NAME']; ?></h2>
          <?php }else{ ?>
            <h1><?php echo $_SERVER['SERVER_NAME']; ?></h1>
          <?php } ?>

        </div>
      <div id="s1"><i class="fa fa-search" aria-hidden="true"></i>
          <form id="s2" role="search" action="<?php echo SITE_URL . "go"; ?>" method="get">

              <input id="s3" type="text" name="q" value="" placeholder="Type to Search...">
              <input id="s4" type="submit" name="search-type" value="Search">

          </form>
      </div>
  </div>
</div>