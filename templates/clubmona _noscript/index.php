<?php 

  ob_start("ob_html_compress");
  $p = 'index';
  include AGCL_TEMPLATE_PATH . 'header.php'; 

?>
    <div id="a">
        <div id="c">            
            <?php 
            
              if( $kwx ): ?>
              
              <?php shuffle($kwx); ?>
              <?php $w=0; ?>
              <?php foreach ($kwx as $kv) : ?>

            	<!--start Picture -->
                <div class="c is">

                    <?php $datas = $db->get_results( "SELECT * FROM search_terms where parent_id = ".$kv->ID." AND type='child'" ); ?>

                    <?php if(is_array($datas)){ ?>
                    <?php shuffle($datas); ?>
                    <?php } ?>

                    <?php if(is_array($datas)){ ?>
                    <?php foreach($datas as $d){ ?>
                    <?php

                        $cache = new Cache();
                        $key = md5('index_post_'.$d->ID);

                        if($cache->isCached($key)){

                          $img = $cache->retrieve($key);

                        }else{

                          $im = $db->get_row("SELECT * FROM term_images where parent_term='".$d->ID."'");
                          if(!is_object($im)) continue;

                          $childImgs = getChildImages($db,$im);
                          $img = array(

                              'term'  => removeSpecial(ucwords($d->term)),
                              'url'  => SITE_URL.'imgs/'.$im->url,                  
                              'childs' => $childImgs,                  
                              'height' =>$im->height,
                              'width' => $im->width,
                              'thumb' => $im->thumb,
                              'type'    => $im->type

                              ); 

                          $cache->setCache($key)->store($key, $img);

                        }
                    
                    ?>

                    <div class="gi" itemid="<?php echo $img['url']; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                        <a href="<?php echo to_attachment($kv->term,$d->term); ?>" title="<?php echo removeSpecial(ucwords($kv->term));?>"></a>

                        <img class="lazy" data-original="<?php echo $img['url']; ?>" alt="<?php echo $img['term']; ?>" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo AGCL_TEMPLATE_URL ?>lib/img/loading.png" style="display: inline;">
                        
                      <noscript>
                        <i itemid="<?php echo $img['url']; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <img title="<?php echo $img['term']; ?>" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $img['url']; ?>" alt="<?php echo $img['term']; ?>" itemprop="contentUrl"/>  
                        </i>
                      </noscript>
                      <div class="ct"><?php echo ucwords(suffleCat()); ?></div>
                    </div>

                        <?php break; ?>
                      <?php } ?>
                    <?php } ?>

                    <!-- attachmant -->
                    <div class="cp">
                        <div class="ch">

                            <a href="<?php echo _a_url_q( $kv->term ); ?>">
                              <?php echo suffleTitleIndex(removeSpecial(ucwords($kv->term)),$w);?>
                            </a>

                        </div>

                        <div class="ah"><i class="fa fa-clock-o" aria-hidden="true"></i>

                            <?php echo date('F jS, Y',strtotime($kv->last_human_access)); ?>

                        </div>
                        <div class="ah">
                    <?php if(is_array($datas)){ ?>                    
                    <?php $i=0; ?>
                    <?php foreach($datas as $d){ ?>

                        <?php

                            $cache = new Cache();
                            $key = md5('index_post_'.$d->ID);

                            if($cache->isCached($key)){

                              $img = $cache->retrieve($key);

                            }else{

                              $im = $db->get_row("SELECT * FROM term_images where parent_term='".$d->ID."'");
                              if(!is_object($im)) continue;

                              $childImgs = getChildImages($db,$im);

                              $img = array(

                                  'term'  => removeSpecial(ucwords($d->term)),
                                  'url'  => SITE_URL.'imgs/'.$im->url,                  
                                  'childs' => $childImgs,                  
                                  'height' =>$im->height,
                                  'width' => $im->width,
                                  'thumb' => $im->thumb,
                                  'type'    => $im->type

                                  ); 

                              $cache->setCache($key)->store($key, $img);

                            }
                        
                        ?>

                        <?php if($i>1){ ?>

                                <?php if($i < 7){ ?>
                                  <?php if(array_key_exists('thumb', $img['childs']) && is_object($img['childs']['thumb'])){ ?>
                                  <?php 
                                            $width = $img['childs']['thumb']->width;
                                            $height = $img['childs']['thumb']->height;
                                            $imageurl = SITE_URL.'imgs/'.$img['childs']['thumb']->url;
                                            $thumbnail_url = $img['childs']['thumb']->thumb;

                                            ?>

                                    <a href="<?php echo to_attachment($kv->term,$d->term); ?>" title="<?php echo removeSpecial(ucwords($d->term));?>">


                                        <img class="lazy rg" data-original="<?php echo $imageurl; ?>" alt="<?php echo $img['term']; ?>" width="50" height="50" src="<?php echo AGCL_TEMPLATE_URL ?>lib/img/loading.png" style="display: inline;">

                                    </a>
   
                                  <?php } ?>
                                <?php } ?>

                                <noscript>
                                <?php

                                  $title = $img['term'];
                                    $width = $img['width'];
                                    $height = $img['height'];
                                    $imageurl = $img['url'];
                                    $thumbnail_url = $img['thumb'];

                                ?>
                                <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                        <img title="<?php echo $title ?>" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?>" itemprop="contentUrl"/>  
                                </i>
                                <?php

                                    if(array_key_exists('large', $img['childs']) && is_object($img['childs']['large'])){

                                        $width = $img['childs']['large']->width;
                                        $height = $img['childs']['large']->height;
                                        $imageurl = SITE_URL.'imgs/'.$img['childs']['large']->url;
                                        $thumbnail_url = $img['childs']['large']->thumb;

                            ?>                  
                                <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">                     
                                    <img title="<?php echo $title ?> Large Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Large Version" itemprop="contentUrl"/>  
                                </i>
                            <?php } ?>

                                <?php

                                    if(array_key_exists('medium', $img['childs']) && is_object($img['childs']['medium'])){

                                        $width = $img['childs']['medium']->width;
                                        $height = $img['childs']['medium']->height;
                                        $imageurl = SITE_URL.'imgs/'.$img['childs']['medium']->url;
                                        $thumbnail_url = $img['childs']['medium']->thumb;

                            ?>                  
                                <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">             
                                    <img title="<?php echo $title ?> Medium Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Medium Version" itemprop="contentUrl"/>  
                                </i>
                            <?php } ?>

                                <?php

                                    if(array_key_exists('thumb', $img['childs']) && is_object($img['childs']['thumb'])){

                                        $width = $img['childs']['thumb']->width;
                                        $height = $img['childs']['thumb']->height;
                                        $imageurl = SITE_URL.'imgs/'.$img['childs']['thumb']->url;
                                        $thumbnail_url = $img['childs']['thumb']->thumb;

                            ?>              
                                <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">                         
                                    <img title="<?php echo $title ?> Thumbnail Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Thumbnail Version" itemprop="contentUrl"/> 

                                </i>

                            <?php } ?>

                        <?php } ?>

                          </noscript>
                          <?php $i++; ?>

                      <?php } ?>
                    <?php } ?>

                        </div>

                    </div>
                </div>
                <!-- end picture -->

                <?php $w++; ?>
              <?php endforeach; ?>
            
            <?php endif; ?>
            
            <div class="r"></div>

            <div id="ipt">
                <div class="s" >

                  <?php $total = $db->get_row( "SELECT count(*) as total FROM  search_terms WHERE type='parent'"); ?>
                  <?php $totalPage = ceil($total->total/18); ?> 
                  <?php $totalPage = $totalPage < 1 ? 1 : $totalPage ?>
                  <?php for($i=1;$i<=$totalPage;$i++){

                        if(trim($URL['args'][1]) == '' && $i === 1){

                        }else if(trim($URL['args'][1]) !== '' && $URL['args'][1] == $i){

                          echo '.&nbsp;&nbsp;';

                        }else{

                          $nofol = $i == 1 ? 'rel="nofollow"' : '';
                          $link = $i == 1 ? SITE_URL : SITE_URL . 'page/'. $i . AGCL_URL_SUFFIX;

                          echo '<a href="'.$link.'" '.$nofol.'>.</a>&nbsp;&nbsp;';

                        }                  

                  } ?>

                  </div>

            </div>

            <div class="r"></div>
        </div>
    </div>
    <div class="r"></div>

<?php include AGCL_TEMPLATE_PATH . 'footer.php'; ?>
<?php ob_end_flush(); ?>

    