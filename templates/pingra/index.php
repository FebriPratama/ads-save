<?php 

  ob_start("ob_html_compress");
  $p = 'index';
  include AGCL_TEMPLATE_PATH . 'header.php'; 

?>

	<div id="content" class="site-content sidebar-right home-fullwidth">
		
		<div class="inner clearfix">

			<div id="primary" class="content-area content-masonry">
				<main id="main" class="site-main" role="main">
					<div id="masonry-container">
						<div class="masonry">	

			            <?php 
			            
			              if( $kwx ): ?>
			              
			              <?php shuffle($kwx); ?>
			              <?php $w=0; ?>
			              <?php foreach ($kwx as $kv) : ?>

							<article class="item has-post-thumbnail post type-post status-publish hentry category-uncategorized post_format-post-format-video" >
			                    
			                    <?php $i=1; ?>
			                    <?php $datas = $db->get_results( "SELECT * FROM search_terms where parent_id = ".$kv->ID." AND type='child'" ); ?>

			                    <?php if(is_array($datas)){ ?>
			                    <?php shuffle($datas); ?>
			                    <?php } ?>

			                    <?php if(is_array($datas)){ ?>
			                    <?php foreach($datas as $d){ ?>
			                    <?php

			                        $cache = new Cache();
			                        $key = md5('index_post_'.$d->ID);

			                        if($cache->isCached($key)){

			                          $img = $cache->retrieve($key);

			                        }else{

			                          $im = $db->get_row("SELECT * FROM term_images where parent_term='".$d->ID."'");
			                          if(!is_object($im)) continue;

			                          $childImgs = getChildImages($db,$im);
			                          $img = array(

			                              'term'  => removeSpecial(ucwords($d->term)),
			                              'url'  => SITE_URL.'imgs/'.$im->url,                  
			                              'childs' => $childImgs,                  
			                              'height' =>$im->height,
			                              'width' => $im->width,
			                              'thumb' => $im->thumb,
			                              'type'    => $im->type

			                              ); 

			                          $cache->setCache($key)->store($key, $img);

			                        }
			                    
			                    ?>

								<div class="thumbnail" itemid="<?php echo $img['url']; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

									<a href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo removeSpecial(ucwords($kv->term));?>" rel="bookmark">

                        				<img class="lazy-mason attachment-home-thumbnail size-home-thumbnail wp-post-image" data-original="<?php echo $img['url']; ?>" alt="<?php echo $img['term']; ?>" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo AGCL_TEMPLATE_URL ?>lib/img/loading.png">

				                      <noscript>
					                    <?php
					                    
					                    	$title = $img['term'];
					                        $width = $img['width'];
					                        $height = $img['height'];
					                        $imageurl = $img['url'];
					                        $thumbnail_url = $img['thumb'];

					                    ?>
					                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
					                            <img title="<?php echo $title ?>" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?>" itemprop="contentUrl"/>  
					                    </i>
					                    <?php

					                        if(array_key_exists('large', $img['childs']) && is_object($img['childs']['large'])){

					                            $width = $img['childs']['large']->width;
					                            $height = $img['childs']['large']->height;
					                            $imageurl = SITE_URL.'imgs/'.$img['childs']['large']->url;
					                            $thumbnail_url = $img['childs']['large']->thumb;

					                ?>                  
					                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">                     
					                        <img title="<?php echo $title ?> Large Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Large Version" itemprop="contentUrl"/>  
					                    </i>
					                <?php } ?>

					                    <?php

					                        if(array_key_exists('medium', $img['childs']) && is_object($img['childs']['medium'])){

					                            $width = $img['childs']['medium']->width;
					                            $height = $img['childs']['medium']->height;
					                            $imageurl = SITE_URL.'imgs/'.$img['childs']['medium']->url;
					                            $thumbnail_url = $img['childs']['medium']->thumb;

					                ?>                  
					                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">             
					                        <img title="<?php echo $title ?> Medium Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Medium Version" itemprop="contentUrl"/>  
					                    </i>
					                <?php } ?>

					                    <?php

					                        if(array_key_exists('thumb', $img['childs']) && is_object($img['childs']['thumb'])){

					                            $width = $img['childs']['thumb']->width;
					                            $height = $img['childs']['thumb']->height;
					                            $imageurl = SITE_URL.'imgs/'.$img['childs']['thumb']->url;
					                            $thumbnail_url = $img['childs']['thumb']->thumb;

					                ?>              
					                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">                         
					                        <img title="<?php echo $title ?> Thumbnail Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Thumbnail Version" itemprop="contentUrl"/> 

					                    </i>

					                <?php } ?>
				                    </noscript>

									</a>
								</div>
								<?php break ?>
								<?php } ?>
							<?php } ?>

								<div class="item-text">
									<header class="entry-header">
										<div class="entry-title">
											<?php echo suffleTitleFull($kv->term,$imageurl); ?>
										</div>
											<div class="entry-meta">
												<span class="posted-on"> Posted on 
													<time class="entry-date published" datetime="<?php echo $kv->last_human_access; ?>">
														<?php echo date('F jS, Y',strtotime($kv->last_human_access)); ?>
													</time>
												</span>	
											</div><!-- .entry-meta -->
									</header><!-- .entry-header -->

									<?php if(is_array($datas)){ ?>                    
					                    <?php $i=0; ?>
					                    <?php foreach($datas as $d){ ?>

					                        <?php

					                            $cache = new Cache();
					                            $key = md5('index_post_'.$d->ID);

					                            if($cache->isCached($key)){

					                              $img = $cache->retrieve($key);

					                            }else{

					                              $im = $db->get_row("SELECT * FROM term_images where parent_term='".$d->ID."'");
					                              if(!is_object($im)) continue;

					                              $childImgs = getChildImages($db,$im);

					                              $img = array(

					                                  'term'  => removeSpecial(ucwords($d->term)),
					                                  'url'  => SITE_URL.'imgs/'.$im->url,                  
					                                  'childs' => $childImgs,                  
					                                  'height' =>$im->height,
					                                  'width' => $im->width,
					                                  'thumb' => $im->thumb,
					                                  'type'    => $im->type

					                                  ); 

					                              $cache->setCache($key)->store($key, $img);

					                            }
					                        
					                        ?>

					                        <?php if($i>1){ ?>

					                                <?php if($i < 6){ ?>
					                                  <?php if(array_key_exists('thumb', $img['childs']) && is_object($img['childs']['thumb'])){ ?>
					                                  <?php 
					                                            $width = $img['childs']['thumb']->width;
					                                            $height = $img['childs']['thumb']->height;
					                                            $imageurl = SITE_URL.'imgs/'.$img['childs']['thumb']->url;
					                                            $thumbnail_url = $img['childs']['thumb']->thumb;

					                                            ?>
														<a href="<?php echo to_attachment($kv->term,$d->term); ?>" title="<?php echo removeSpecial(ucwords($d->term));?>">
															<img src="<?php echo $imageurl; ?>" width="50px" height="50px" alt="<?php echo to_attachment($kv->term,$d->term); ?>" title="<?php echo removeSpecial(ucwords($d->term)); ?>">
														</a>

														 <?php } ?>
					                                <?php } ?>

					                                	<noscript>
							                                <?php

							                                  $title = $img['term'];
							                                    $width = $img['width'];
							                                    $height = $img['height'];
							                                    $imageurl = $img['url'];
							                                    $thumbnail_url = $img['thumb'];

							                                ?>
							                                <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
							                                        <img title="<?php echo $title ?>" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?>" itemprop="contentUrl"/>  
							                                </i>
							                                <?php

							                                    if(array_key_exists('large', $img['childs']) && is_object($img['childs']['large'])){

							                                        $width = $img['childs']['large']->width;
							                                        $height = $img['childs']['large']->height;
							                                        $imageurl = SITE_URL.'imgs/'.$img['childs']['large']->url;
							                                        $thumbnail_url = $img['childs']['large']->thumb;

							                            ?>                  
							                                <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">                     
							                                    <img title="<?php echo $title ?> Large Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Large Version" itemprop="contentUrl"/>  
							                                </i>
							                            <?php } ?>

							                                <?php

							                                    if(array_key_exists('medium', $img['childs']) && is_object($img['childs']['medium'])){

							                                        $width = $img['childs']['medium']->width;
							                                        $height = $img['childs']['medium']->height;
							                                        $imageurl = SITE_URL.'imgs/'.$img['childs']['medium']->url;
							                                        $thumbnail_url = $img['childs']['medium']->thumb;

							                            ?>                  
							                                <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">             
							                                    <img title="<?php echo $title ?> Medium Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Medium Version" itemprop="contentUrl"/>  
							                                </i>
							                            <?php } ?>

							                                <?php

							                                    if(array_key_exists('thumb', $img['childs']) && is_object($img['childs']['thumb'])){

							                                        $width = $img['childs']['thumb']->width;
							                                        $height = $img['childs']['thumb']->height;
							                                        $imageurl = SITE_URL.'imgs/'.$img['childs']['thumb']->url;
							                                        $thumbnail_url = $img['childs']['thumb']->thumb;

							                            ?>              
							                                <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">                         
							                                    <img title="<?php echo $title ?> Thumbnail Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Thumbnail Version" itemprop="contentUrl"/> 

							                                </i>

							                            <?php } ?>

							                        <?php } ?>

							                          </noscript>
							                          <?php $i++; ?>

							                      <?php } ?>
							                    <?php } ?>
								</div>

							</article><!-- #post-## -->	
							
				          <?php endforeach; ?>
				        
				        <?php endif; ?>

						</div>
					</div>
					<nav class="navigation post-navigation clearfix" role="navigation" style="text-align:center;">

	                  <?php $total = $db->get_row( "SELECT count(*) as total FROM  search_terms WHERE type='parent'"); ?>
	                  <?php $totalPage = ceil($total->total/18); ?> 
	                  <?php $totalPage = $totalPage < 1 ? 1 : $totalPage ?>
	                  <?php for($i=1;$i<=$totalPage;$i++){

	                        if(trim($URL['args'][1]) == '' && $i === 1){

	                        }else if(trim($URL['args'][1]) !== '' && $URL['args'][1] == $i){

	                          echo '.&nbsp;&nbsp;';

	                        }else{

	                          $nofol = $i == 1 ? 'rel="nofollow"' : '';
	                          $link = $i == 1 ? SITE_URL : SITE_URL . 'page/'. $i . AGCL_URL_SUFFIX;

	                          echo '<a href="'.$link.'" '.$nofol.'>.</a>&nbsp;&nbsp;';

	                        }                  

	                  } ?>

					</nav><!-- .navigation -->
				</main><!-- #main -->

			</div><!-- #primary -->

		</div>
	</div><!-- #content -->

<?php include AGCL_TEMPLATE_PATH . 'footer.php'; ?>
<?php ob_end_flush(); ?>
