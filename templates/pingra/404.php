<?php 

ob_start("ob_html_compress");
$p = '404';
include AGCL_TEMPLATE_PATH . 'header.php';

?>

	<div id="content" class="site-content sidebar-right home-fullwidth">
		<div class="inner clearfix">

			<div id="primary" class="content-area content-masonry">

				<article class="page type-page status-publish hentry">

					<div class="content-wrap">
						<header class="entry-header">
							<h1 class="entry-title">Page not found</h1>	
						</header><!-- .entry-header -->
					</div>

				</article>


		</div>
	</div><!-- #content -->

<?php include AGCL_TEMPLATE_PATH . 'footer.php'; ?>
<?php ob_end_flush(); ?>