(function ($) {
	'use strict';

	var $gutter = 0, pingraphy = {

		initReady: function() {
			this.menuAnimate();
			this.searchAnimate();
			this.homepage();
			this.mobileMenu();
			this.scrollTop();
			this.postFavor();
			this.cookieFavor();
			this.socialSharer();

		},

		socialSharer: function() {
			
			var offsetTop = $('#content').offset();

			//$('.social-sharing-left').css('top', offsetTop.top + 130 );

			jQuery('.social-popup').on('click', function(e){
				
				var width = 580;
				var height = 470;
				var leftPosition, topPosition;

				leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
				topPosition = (window.screen.height / 2) - ((height / 2) + 50);

				var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
				var url = $(this).attr('href');

				window.open(url, 'socialSharer', windowFeatures);
				e.preventDefault();

			});

			//pinterest
			$('.social-pinterst').on("click", function(t) {
				t.preventDefault();

				try {
					var e = document.createElement('script');
					e.setAttribute('type', 'text/javascript');
					e.setAttribute('charset', 'UTF-8');
					e.setAttribute('src', '//assets.pinterest.com/js/pinmarklet.js?r=' + Math.random() * 99999999);
					document.body.appendChild(e);
				} catch (e) {

				}

				//record share
				Nova.Social.recordShare($(this).attr('data-socialsite'), $(this).attr('data-location'), Nova.System.articleId);
			});

		},
		
		menuAnimate: function() {
			var self = this;

			$('.section-one .toggle-mobile-menu').on('click', function(e) {
				if($('.section-two .toggle-mobile-menu').hasClass('active')) {
					$('.section-two .toggle-mobile-menu').removeClass('active');
					$('.section-two .toggle-mobile-menu').next('.second-navigation').removeClass('main-nav-open');
				}
				e.preventDefault();
				e.stopPropagation();
				if($(this).hasClass('active')) {
					$(this).removeClass('active');
					$(this).next('.main-navigation').removeClass('main-nav-open');
				} else {
					$(this).addClass('active');
					$(this).next('.main-navigation').addClass('main-nav-open');
				}
			});

			$('.section-two .toggle-mobile-menu').on('click', function(e) {
				e.preventDefault();
				if($('.section-one .toggle-mobile-menu').hasClass('active')) {
					$(this).find('i').addClass('fa-angle-down').removeClass('fa-angle-up');
					$('.section-one .toggle-mobile-menu').removeClass('active');
					$('.section-one .toggle-mobile-menu').next('.main-navigation').removeClass('main-nav-open');
				}
				if($(this).hasClass('active')) {
					$(this).find('i').addClass('fa-angle-down').removeClass('fa-angle-up');
					$(this).removeClass('active');
					$(this).next().removeClass('main-nav-open');
				} else {
					$(this).find('i').addClass('fa-angle-up').removeClass('fa-angle-down');
					$(this).addClass('active');
					$(this).next().addClass('main-nav-open');
				}
			});

			$(document).click(function(e) {
				if($('.main-navigation').hasClass('main-nav-open')) {
					e.stopPropagation();
					$('.main-navigation').removeClass('main-nav-open');
				}
			});

			var catcher = $('#catcher'),
				sticky  = $('#sticky'),
				bodyTop = $('body').offset().top;

			if ( sticky.length ) {
				$(window).scroll(function() {
					pingraphy.stickThatMenu(sticky,catcher,bodyTop);
				});
				$(window).resize(function() {
					pingraphy.stickThatMenu(sticky,catcher,bodyTop);
				});
			}
		},
		isScrolledTo: function(elem,top) {
			var docViewTop = $(window).scrollTop(); //num of pixels hidden above current screen
			var docViewBottom = docViewTop + $(window).height();

			var elemTop = $(elem).offset().top - top; //num of pixels above the elem
			var elemBottom = elemTop + $(elem).height();

			return ((elemTop <= docViewTop));
		},
		stickThatMenu: function(sticky,catcher,top) {
			var self = this;

			if(self.isScrolledTo(sticky,top)) {
				sticky.addClass('sticky-nav');
				catcher.height(sticky.height());
			} 
			var stopHeight = catcher.offset().top;
			if ( stopHeight > sticky.offset().top) {
				sticky.removeClass('sticky-nav');
				catcher.height(0);
			}
		},
		searchAnimate: function() {
			var header = $('.site-header');
			var trigger = $('#trigger-overlay');
			var overlay = header.find('.search-overlay');
			var input = header.find('.hideinput, .header-search .fa-search');
			trigger.click(function(e){
				$(this).hide();
				overlay.addClass('open').find('input').focus();
			});

			$('.overlay-close').click(function(e) {
				$('.site-header .search-overlay').addClass('closed').removeClass('open');
				setTimeout(function() { $('.site-header .search-overlay').removeClass('closed'); }, 400);
				$('#trigger-overlay').show();
			});

			$(document).on('click', function(e) {
				
				var target = $(e.target);
				if (target.is('.search-overlay') || target.closest('.search-overlay').length) return true;

				$('.site-header .search-overlay').addClass('closed').removeClass('open');
				setTimeout(function() { $('.site-header .search-overlay').removeClass('closed'); }, 400);
				$('#trigger-overlay').show();
			});

			$('#trigger-overlay').click(function(e) {
				e.preventDefault();
				e.stopPropagation();
			});
		},

		functionMasonry: function( $masonryClass ) {
			
			if ( $('body').hasClass('right-to-left-language') ) {
				
				$( $masonryClass ).masonry({
					columnWidth: '.item',
					gutter: $gutter,
					isFitWidth: true,
					isOriginLeft: false,
					animationOptions: {
						duration: 700,
						queue: true
					}
				});

			} else {
				
				$( $masonryClass ).masonry({
					columnWidth: '.item',
					gutter: $gutter,
					isFitWidth: true,
					isOriginLeft: true,
					animationOptions: {
						duration: 700,
						queue: true
					}
				});

			}

		},

		homepage: function() {
			
			var $container = $('#masonry-container');
			$container.imagesLoaded(function() {
				var $cont = $( '.masonry' );
				$cont.masonry({
					columnWidth: '.item',
					gutter: 30,
					isFitWidth: true,
					isOriginLeft: false,
					animationOptions: {
						duration: 700,
						queue: true
					}
				});
			    $('.lazy-mason').lazyload({
			        effect: 'fadeIn',
			        load: function() {
			        	$cont.masonry('reload');
			        }
			    });

			});

		},

		mobileMenu: function() {
			$('#masthead .menu-item-has-children > a').append('<i class="fa arrow-sub-menu fa-chevron-right"></i>');
			$('.arrow-sub-menu').on('click', function(e) {
				e.preventDefault();
				e.stopPropagation();

				var active = $(this).hasClass('fa-chevron-down');
				if(!active) {
					$(this).parent().next().addClass('sub-menu-open');
					//.css({'display' : 'block'});
					$(this).removeClass('fa-chevron-right').addClass('fa-chevron-down');
				} else {
					$(this).parent().next().removeClass('sub-menu-open');
					$(this).removeClass('fa-chevron-down').addClass('fa-chevron-right');
				}
				
			});
		},
		scrollTop: function() {
			
			var scrollDes = 'html,body';  
			// Opera does a strange thing if we use 'html' and 'body' together so my solution is to do the UA sniffing thing
			if(navigator.userAgent.match(/opera/i)){
				scrollDes = 'html';
			}
			// Show ,Hide
			$(window).scroll(function () {
				if ($(this).scrollTop() > 130) {
					$('.back-to-top').addClass('filling').removeClass('hiding');
					//$('.sharing-top-float').fadeIn();
				} else {
					$('.back-to-top').removeClass('filling').addClass('hiding');
					//$('.sharing-top-float').fadeOut();
				}
			});
			// Scroll to top when click
			$('.back-to-top').click(function () {
				$(scrollDes).animate({ 
					scrollTop: 0
				},{
					duration :500
				});

			});
		},
		postFavor: function() {
			$('.favor').on('click', function(e) {

				if (!$(this).hasClass('favored')) {
					var that = this;
					$.ajax({
						url: AdminAjaxURL.ajaxurl,
						type: 'POST',
						data: 'action=pingraphy_post_favor&post_id=' + $(this).data('post-id'),
						success: function (data) {
							$(that).children('span').html(data.count);
							Cookies.set('koocie-' + $(that).data('post-id'), $(that).data('post-id'));
							$(that).addClass('favored');
						}
						
					});
				}
				e.preventDefault();
			});
		},
		cookieFavor: function () {

			$('a.favor').each(function(index, ele) {
				var postId = $(ele).data('post-id');
				var cookie = Cookies.get('koocie-' + postId);

				if (typeof cookie != 'undefined') {
					$(ele).addClass('favored');
				}
			});
		}
	};

	$(document).ready(function () {
		pingraphy.initReady();
	});

})(jQuery);