<?php 

ob_start("ob_html_compress");
$p = 'go';
include AGCL_TEMPLATE_PATH . 'header.php';

?>

	<div id="content" class="site-content sidebar-right home-fullwidth">
		<div class="inner clearfix">

			<div id="primary" class="content-area content-masonry">

				<article class="page type-page status-publish hentry">

					<div class="content-wrap">
						<header class="entry-header">
							<h1 class="entry-title">Search For <?php echo $_GET['q'];?></h1>
						</header><!-- .entry-header -->
						<div class="entry-content">
                            
                            <?php $q = preg_replace('/([^a-z0-9]+)/i', ' ', $_GET['q']); ?>
                            <?php $kwx = $db->get_results( "SELECT * FROM search_terms where type = 'parent' AND term Like '%".$q."%'" );
                            if( $kwx ): ?>
                                <?php foreach ($kwx as $kv) : ?>

                                <a href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo ucwords($kv->term);?>"><?php echo ucwords($kv->term);?></a>
                                <br>
                                <?php endforeach; ?>
                            <?php endif; ?>

						</div><!-- .entry-content -->
					</div>

				</article>


		</div>
	</div><!-- #content -->

<?php include AGCL_TEMPLATE_PATH . 'footer.php'; ?>
<?php ob_end_flush(); ?>