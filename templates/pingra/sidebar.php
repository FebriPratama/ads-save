
<aside id="tc-category-posts-3" class="widget tc-category-posts-widget">
	<h2 class="widget-title">Popular Posts</h2>
	<ul class="tc-related-posts tc-sidebar-widget">	
		
		<li class="have-thumb">
			<?php if(hasOption($db,'ads160')) echo getOption($db,'ads160')->opt_value; ?>
		</li>

	    <?php if ($p=='image') { ?>	
	    <?php 

    		$kwx = $db->get_results( "SELECT * FROM search_terms AS r1 JOIN (SELECT (RAND() * (SELECT MAX(id) FROM search_terms)) AS id) AS r2 WHERE r1.id >= r2.id  AND r1.type='parent' LIMIT 32" );

	        $kv = [];

	    ?>
	    <?php
	    if( $kwx ): ?>
	    <?php foreach ($kwx as $kv) : ?>
		<li class="have-thumb">

			<div class="post-data">
				<a href="<?php echo _a_url_q( $kv->term ); ?>"title="<?php echo removeSpecial(ucwords($kv->term));?>"><?php echo removeSpecial(ucwords($kv->term));?></a>
			</div>
			<span class="clear"></span>

		</li>
		<?php endforeach; ?>	
		<?php endif; ?> 
		<?php } ?>

	</ul>

	<?php if ($p=='image') { ?>
	<h2 class="widget-title">Related Posts</h2>
	<ul class="tc-related-posts tc-sidebar-widget">		
	    <?php 

	        $kwx = $db->get_results(
	          "SELECT *, MATCH ( term ) AGAINST ( '". addslashes($q) ."' ) AS relevance ".
	          "FROM search_terms WHERE type='parent' && MATCH ( term ) AGAINST ( '". addslashes($q) ."' ) ".
	          "ORDER BY last_robot_access DESC LIMIT 0,5"
	          );

	        $kv = [];

	    ?>
		<?php if( $kwx ) : ?>
		<?php foreach ($kwx as $kv) : ?>

		<?php $img = $db->get_row("SELECT b.* from search_terms as a join term_images as b on b.parent_term = a.ID where a.parent_id = '".$kv->ID."'"); ?>
		<?php if(is_object($img)){ ?>

		<li class="have-thumb">
		      <?php 

		          $img = array(

		              'term'  => removeSpecial(ucwords($kv->term)),
		              'url'  => SITE_URL.'imgs/'.$img->url,       
		              'height' =>$img->height,
		              'width' => $img->width

		              ); 


		      ?>
			<div class="post-img">
				<a rel="nofollow" href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $kv->term; ?>">						
					<img class="attachment-widget-thumbnail size-widget-thumbnail wp-post-image" src="<?php echo $img['url']; ?>" alt="<?php echo $kv->term; ?>" width="120" height="120" />
				</a>
			</div>									
			<div class="post-data">
				<h4 class="single-header"><?php echo $kv->term; ?></h4>
			</div>
			<span class="clear"></span>

		</li>

		<?php } ?>           
		<?php endforeach; ?>
		<?php endif; ?> 

	</ul>
	<?php } ?>

</aside>	