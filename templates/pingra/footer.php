
	<footer id="colophon" class="site-footer" role="contentinfo">
					
		<?php if($p !== 'info' && $p !== '404' && $p !== 'go'){ ?>	
        <?php  

            $kwx = $db->get_results( "SELECT * FROM search_terms AS r1 JOIN (SELECT (RAND() * (SELECT MAX(id) FROM search_terms)) AS id) AS r2 WHERE r1.id >= r2.id  AND r1.type='parent' LIMIT 0,9" );

          ?>				
		<div class="footer-widgets">
			<div class="inner clearfix">
				<div class="footer-widget footer-column-1">

					<aside class="widget tc-category-posts-widget">
						<h2 class="widget-title">Awesome Designs</h2>
						<ul class="tc-related-posts tc-sidebar-widget">		

		                    <?php $i=0; ?>
		                    <?php if( $kwx ) : ?>
		                    <?php foreach ($kwx as $kv) : ?>

		                        <?php if($i<3){ ?>
		                        <?php $img = $db->get_row("select b.* from search_terms as a join term_images as b on b.parent_term = a.ID where a.parent_id = '".$kv->ID."'"); ?>

		                        <?php if(is_object($img)){ ?>

		                        <?php 

		                          $img = array(

		                              'term'  => removeSpecial(ucwords($kv->term)),
		                              'url'  => SITE_URL.'imgs/'.$img->url,       
		                              'height' =>$img->height,
		                              'width' => $img->width

		                              ); 

		                        ?>
								<li class="have-thumb" itemid="<?php echo $img['url']; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
									<div class="post-img">
										<a rel="nofollow" href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $img['term']; ?>">

											<img width="68" height="68" src="<?php echo AGCL_TEMPLATE_URL ?>lib/img/loading.png"  data-original="<?php echo $img['url']; ?>" alt="<?php echo $img['term']; ?>" class="lazy attachment-widget-thumbnail size-widget-thumbnail wp-post-image">	

					                      <noscript>
					                        <i itemid="<?php echo $img['url']; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
					                            <img title="<?php echo $img['term']; ?>" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $img['url']; ?>" alt="<?php echo $img['term']; ?>" itemprop="contentUrl"/>  
					                        </i>
					                      </noscript>			

										</a>
									</div>
									
									<div class="post-data">
										<a href="http://demo.themecountry.com/pingraphy/this-is-video-insite/" alt="This is video insite"><?php echo $img['term']; ?></a>					
										<div class="post-meta">
											<?php echo date('F jS, Y',strtotime($kv->last_human_access)); ?>	
										</div> <!--end .entry-meta-->

									</div>
									<span class="clear"></span>

								</li>
		                        <?php } ?>
		                      <?php } ?>

		                    <?php $i++; ?>
		                    <?php endforeach; ?>
		                    <?php endif; ?>

						</ul>
					</aside>

				</div>
				<div class="footer-widget footer-column-2">

					<aside class="widget tc-category-posts-widget">
						<h2 class="widget-title">Lovely Designs</h2>
						<ul class="tc-related-posts tc-sidebar-widget">			

		                    <?php $i=0; ?>
		                    <?php if( $kwx ) : ?>
		                    <?php foreach ($kwx as $kv) : ?>

		                        <?php if($i>2 && $i < 6){ ?>
		                        <?php $img = $db->get_row("select b.* from search_terms as a join term_images as b on b.parent_term = a.ID where a.parent_id = '".$kv->ID."'"); ?>

		                        <?php if(is_object($img)){ ?>

		                        <?php 

		                          $img = array(

		                              'term'  => removeSpecial(ucwords($kv->term)),
		                              'url'  => SITE_URL.'imgs/'.$img->url,       
		                              'height' =>$img->height,
		                              'width' => $img->width

		                              ); 

		                        ?>
								<li class="have-thumb" itemid="<?php echo $img['url']; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
									<div class="post-img">
										<a rel="nofollow" href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $img['term']; ?>">

											<img width="68" height="68" src="<?php echo AGCL_TEMPLATE_URL ?>lib/img/loading.png"  data-original="<?php echo $img['url']; ?>" alt="<?php echo $img['term']; ?>" class="lazy attachment-widget-thumbnail size-widget-thumbnail wp-post-image">	

					                      <noscript>
					                        <i itemid="<?php echo $img['url']; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
					                            <img title="<?php echo $img['term']; ?>" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $img['url']; ?>" alt="<?php echo $img['term']; ?>" itemprop="contentUrl"/>  
					                        </i>
					                      </noscript>			

										</a>
									</div>
									
									<div class="post-data">
										<a rel="nofollow" href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $img['term']; ?>"><?php echo $img['term']; ?></a>					
										<div class="post-meta">
											<?php echo date('F jS, Y',strtotime($kv->last_human_access)); ?>	
										</div> <!--end .entry-meta-->

									</div>
									<span class="clear"></span>

								</li>
		                        <?php } ?>
		                      <?php } ?>

		                    <?php $i++; ?>
		                    <?php endforeach; ?>
		                    <?php endif; ?>
						</ul>
					</aside>

				</div>
				<div class="footer-widget footer-column-3">

					<aside class="widget tc-category-posts-widget">
						<h2 class="widget-title">Cool Designs</h2>
						<ul class="tc-related-posts tc-sidebar-widget">			

		                    <?php $i=0; ?>
		                    <?php if( $kwx ) : ?>
		                    <?php foreach ($kwx as $kv) : ?>

		                        <?php if($i>5){ ?>
		                        <?php $img = $db->get_row("select b.* from search_terms as a join term_images as b on b.parent_term = a.ID where a.parent_id = '".$kv->ID."'"); ?>

		                        <?php if(is_object($img)){ ?>

		                        <?php 

		                          $img = array(

		                              'term'  => removeSpecial(ucwords($kv->term)),
		                              'url'  => SITE_URL.'imgs/'.$img->url,       
		                              'height' =>$img->height,
		                              'width' => $img->width

		                              ); 

		                        ?>
								<li class="have-thumb" itemid="<?php echo $img['url']; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
									<div class="post-img">
										<a rel="nofollow" href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $img['term']; ?>">

											<img width="68" height="68" src="<?php echo AGCL_TEMPLATE_URL ?>lib/img/loading.png"  data-original="<?php echo $img['url']; ?>" alt="<?php echo $img['term']; ?>" class="lazy attachment-widget-thumbnail size-widget-thumbnail wp-post-image">	

					                      <noscript>
					                        <i itemid="<?php echo $img['url']; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
					                            <img title="<?php echo $img['term']; ?>" width="<?php echo $img['width'];?>" height="<?php echo $img['height'];?>" src="<?php echo $img['url']; ?>" alt="<?php echo $img['term']; ?>" itemprop="contentUrl"/>  
					                        </i>
					                      </noscript>			

										</a>
									</div>
									
									<div class="post-data">
										<a rel="nofollow" href="<?php echo _a_url_q( $kv->term ); ?>" title="<?php echo $img['term']; ?>"><?php echo $img['term']; ?></a>					
										<div class="post-meta">
											<?php echo date('F jS, Y',strtotime($kv->last_human_access)); ?>	
										</div> <!--end .entry-meta-->

									</div>
									<span class="clear"></span>

								</li>
		                        <?php } ?>
		                      <?php } ?>

		                    <?php $i++; ?>
		                    <?php endforeach; ?>
		                    <?php endif; ?>
						</ul>
					</aside>

				</div>
			</div>
		</div>				
		<?php } ?>

		<div class="site-info">
			<div class="inner clearfix">		
					
					<div class="copyright">
			          <?php if($p !== 'info' && $p !== '404' && $p !== 'go'){ ?>
			          <?php $total = $db->get_row( "SELECT count(*) as total FROM  search_terms WHERE type='parent'"); ?>
			          <?php $totalPage = ceil($total->total/50); ?> 
			          <?php $totalPage = $totalPage < 1 ? 1 : $totalPage ?>
			          <?php for($i=1;$i<=$totalPage;$i++){

			              echo '<a href="'. SITE_URL . 'map/'. $i . AGCL_URL_SUFFIX. '">.</a>';

			              } ?>
			            
			            <br>
			          <?php } ?>
					  <br>
					  Any content trademark/s or other material that might be found on this simains the copyright of its respective owner/s. While using this site, you agree to have read and accepted our terms of use and cookie.
					  <br>Copyright <a rel="nofollow" href="<?php echo SITE_URL . 'info/copyright' . AGCL_URL_SUFFIX ?>">©</a> <?php echo date('Y'); ?> <?php echo $_SERVER['SERVER_NAME']; ?>. All Rights Reserved.
			
					</div>				
				
					<div class="menu-footer">
						<div class="menu-footer-menu-container">
							<ul id="menu-footer-menu" class="menu clearfix">
								<li id="menu-item-45" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45">
									<a rel="nofollow" href="<?php echo SITE_URL . 'info/privacy-policy' . AGCL_URL_SUFFIX ?>">Privacy Policy</a>
								</li>
								<li id="menu-item-46" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-46">
									<a rel="nofollow" href="<?php echo SITE_URL . 'info/contact' . AGCL_URL_SUFFIX ?>">Contact Us</a>
								</li>
							</ul>
						</div>					
					</div>
				
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<!-- Back To Top -->
	<span class="back-to-top"><i class="fa fa-angle-double-up"></i></span>

	<script type="text/javascript" src="<?php echo AGCL_TEMPLATE_URL ?>js/masonry.pkgd.min.js"></script>
	<script type="text/javascript" src="<?php echo AGCL_TEMPLATE_URL ?>js/skip-link-focus-fix.js"></script>
	<script type="text/javascript" src="<?php echo AGCL_TEMPLATE_URL ?>js/wp-embed.min.js"></script>

    <?php if(hasOption($db,'stat')) echo getOption($db,'stat')->opt_value; ?>

</body>
</html>