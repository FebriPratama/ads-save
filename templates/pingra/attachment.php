<?php 

    ob_start("ob_html_compress");

    $p = 'attachment';
    $q = preg_replace('/([^a-z0-9]+)/i', ' ', $URL['args'][1]);

    $term  = $q;
    $term .= "\n" . file_get_contents(ABSPATH . 'term.txt');
    file_put_contents(ABSPATH . 'term.txt', $term);  

    include AGCL_TEMPLATE_PATH . 'header.php';
          
          
?>

	<div id="content" class="site-content sidebar-right home-fullwidth">
		<div class="inner clearfix">

			<div id="primary" class="content-area">

				<main id="main" class="site-main" role="main">
					
		        <?php

		            $title = $img['term'];

		            $width = $img['width'];
		            $height = $img['height'];
		            $imageurl = $img['url'];
		            $thumbnail_url = $img['thumb'];

		            ?>

					<article class="post type-post status-publish format-video has-post-thumbnail hentry category-uncategorized post_format-post-format-video">

						<div class="thumbnail" itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">	

                			<img src="<?php echo AGCL_TEMPLATE_URL ?>lib/img/loading.png" class="lazy attachment-single-thumbnail size-single-thumbnail wp-post-image" title="<?php echo $title ?>" width="<?php echo $width;?>" height="<?php echo $height;?>" data-original="<?php echo $imageurl; ?>" alt="<?php echo $title ?>"/> 
				           <noscript>

				                <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

				                    <img title="<?php echo $title ?>" style="width:100%;" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?>" itemprop="contentUrl"/>  

				                </i>

				                    <?php

				                        if(array_key_exists('large', $img['childs']) && is_object($img['childs']['large'])){

				                            $width = $img['childs']['large']->width;
				                            $height = $img['childs']['large']->height;
				                            $imageurl = SITE_URL.'imgs/'.$img['childs']['large']->url;
				                            $thumbnail_url = $img['childs']['large']->thumb;

				                ?>                  
				                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">                     
				                            <img title="<?php echo $title ?> Large Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Large Version" itemprop="contentUrl"/>  
				                    </i>
				                <?php } ?>

				                    <?php

				                        if(array_key_exists('medium', $img['childs']) && is_object($img['childs']['medium'])){

				                            $width = $img['childs']['medium']->width;
				                            $height = $img['childs']['medium']->height;
				                            $imageurl = SITE_URL.'imgs/'.$img['childs']['medium']->url;
				                            $thumbnail_url = $img['childs']['medium']->thumb;

				                ?>                  
				                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">             
				                            <img title="<?php echo $title ?> Medium Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Medium Version" itemprop="contentUrl"/>  
				                    </i>
				                <?php } ?>

				                    <?php

				                        if(array_key_exists('thumb', $img['childs']) && is_object($img['childs']['thumb'])){

				                            $width = $img['childs']['thumb']->width;
				                            $height = $img['childs']['thumb']->height;
				                            $imageurl = SITE_URL.'imgs/'.$img['childs']['thumb']->url;
				                            $thumbnail_url = $img['childs']['thumb']->thumb;

				                ?>              
				                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">                         
				                            <img title="<?php echo $title ?> Thumbnail Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Thumbnail Version" itemprop="contentUrl"/> 
				                    </i>

				                <?php } ?>

				            </noscript>  

						</div>
					
						<div class="content-wrap">
							<header class="entry-header clearfix">
					
								<div class="breadcrumb">
									 <div id="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
						                <i itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						                    <a itemprop="item" rel="nofollow" href="<?php echo SITE_URL ?>">
						                        <span itemprop="name">Home</span>
						                    </a>
						                    <meta itemprop="position" content="1" />
						                </i> &#187; 
						                <i itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						                    <a itemprop="item" rel="nofollow" href="<?php echo _a_url_q( $item->term ); ?>">
						                        <span itemprop="name"><?php echo $item->term; ?></span>
						                    </a>
						                    <meta itemprop="position" content="2" />
						                </i> &#187; 
						                <i itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
						                    <i itemprop="item">
						                        <span itemprop="name"><?php echo ucwords($q);?></span>
						                    </i>
						                    <meta itemprop="position" content="3" />
						                </i>
									 </div>			
								</div>
								<h1 class="entry-title" itemprop="name"><?php echo ucwords($q);?></h1>
								<div class="entry-meta">
									<span class="posted-on"> Posted on 
										<time class="entry-date published" datetime="<?php echo $child->last_human_access; ?>">
											<?php echo date('F jS, Y',strtotime($child->last_human_access)); ?>
										</time>
									</span>
								</div><!-- .entry-meta -->
					
							</header><!-- .entry-header -->

							<div class="entry-content">
								<?php if(hasOption($db,'ads728')) echo getOption($db,'ads728')->opt_value; ?>
							</div><!-- .entry-content -->

						</div>
			
					</article><!-- #post-## -->	
	
					<article class="page type-page status-publish hentry">

						<div class="content-wrap">
							<header class="entry-header">
								<h3 class="entry-title">Description of <?php echo ucwords($q);?></h3>		
							</header><!-- .entry-header -->
							<div class="entry-content">
								<blockquote>

								    <!-- Start -->
								    <?php
								    $kw   = $q;
								    $url  = 'http://www.bing.com/search?q='.urlencode(removeSpecial($q)).'&format=rss&count=20';
								    $data = simplexml_load_file($url);
								    if(@simplexml_load_file($url)){
								    $i=0;
								    foreach($data->channel->item as $a){
								    	
									    $dom = array('.com','.net','.org','.id','.co.uk');
									    $title = str_replace($dom, '', $a->title);
									    $title = preg_replace('/([^a-z0-9]+)/i',' ',$title);

								    	if(is_term_safe(removeSpecial(strtolower($title)))) { 

								    ?>

								    <?php echo ucfirst(removeSpecial(strtolower($a->description)));?>.&nbsp;
								    
								    <?php if($i%5==0) echo '</p><p>'; ?>

								    <?php $i++; } } } ?>
									</p>
									<p>
										<?php foreach($imgs as $img){ ?>
											<?php echo $img['term']; ?>,
										<?php } ?>
										<strong><?php echo ucwords($q); ?></strong>
									</p>

								</blockquote>
							</div><!-- .entry-content -->

							<div class="entry-content">
								<?php if(hasOption($db,'ads728')) echo getOption($db,'ads728')->opt_value; ?>
							</div><!-- .entry-content -->

						</div>

					</article>

				</main><!-- #main -->
					
				<div class="related-posts clearfix">
					<article class="page type-page status-publish hentry">

						<div class="content-wrap">

							<div class="entry-content">

								<h3 class="entry-title">Related Pictures of <?php echo ucwords($q);?> </h3>
								<div class="item-text" align="center">

								<?php

							if( $imgs ):
							    $i=0;
							    foreach ($imgs as $img) :

							        if($i>0){

							        $title = $img['term'];

							        if(array_key_exists('medium', $img['childs']) && is_object($img['childs']['medium'])){

							            $width = $img['childs']['medium']->width;
							            $height = $img['childs']['medium']->height;
							            $imageurl = SITE_URL.'imgs/'.$img['childs']['medium']->url;
							            $thumbnail_url = $img['childs']['medium']->thumb;

							        }else{

							            $width = $img['width'];
							            $height = $img['height'];
							            $imageurl = $img['url'];
							            $thumbnail_url = $img['thumb'];

							        }

							    ?>

									<a target="_blank" style="display:block;float:left;width:18%;height:100px;margin-right:6px;margin-bottom:6px;overflow:hidden;" href="<?php echo to_attachment($item->term,$title); ?>" title="<?php echo $title ?>">
										<img width="<?php echo $width; ?>" height="<?php echo $height; ?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?>" title="<?php echo $title ?>">
										<noscript>
					                    <?php

					                        $width = $img['width'];
					                        $height = $img['height'];
					                        $imageurl = $img['url'];
					                        $thumbnail_url = $img['thumb'];

					                    ?>
					                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
					                            <img title="<?php echo $title ?>" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?>" itemprop="contentUrl"/>  
					                    </i>
					                    <?php

					                        if(array_key_exists('large', $img['childs']) && is_object($img['childs']['large'])){

					                            $width = $img['childs']['large']->width;
					                            $height = $img['childs']['large']->height;
					                            $imageurl = SITE_URL.'imgs/'.$img['childs']['large']->url;
					                            $thumbnail_url = $img['childs']['large']->thumb;

					                ?>                  
					                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">                     
					                            <img title="<?php echo $title ?> Large Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Large Version" itemprop="contentUrl"/>  
					                    </i>
					                <?php } ?>

					                    <?php

					                        if(array_key_exists('medium', $img['childs']) && is_object($img['childs']['medium'])){

					                            $width = $img['childs']['medium']->width;
					                            $height = $img['childs']['medium']->height;
					                            $imageurl = SITE_URL.'imgs/'.$img['childs']['medium']->url;
					                            $thumbnail_url = $img['childs']['medium']->thumb;

					                ?>                  
					                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">             
					                            <img title="<?php echo $title ?> Medium Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Medium Version" itemprop="contentUrl"/>  
					                    </i>
					                <?php } ?>

					                    <?php

					                        if(array_key_exists('thumb', $img['childs']) && is_object($img['childs']['thumb'])){

					                            $width = $img['childs']['thumb']->width;
					                            $height = $img['childs']['thumb']->height;
					                            $imageurl = SITE_URL.'imgs/'.$img['childs']['thumb']->url;
					                            $thumbnail_url = $img['childs']['thumb']->thumb;

					                ?>              
					                    <i itemid="<?php echo $imageurl; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">                         
					                        <img title="<?php echo $title ?> Thumbnail Version" width="<?php echo $width;?>" height="<?php echo $height;?>" src="<?php echo $imageurl; ?>" alt="<?php echo $title ?> Thumbnail Version" itemprop="contentUrl"/> 
					                    </i>

					                <?php } ?>

					                </noscript>		
									</a>

								 <?php 
							        }
							        $i++;   

							        endforeach;
							    endif;

							    ?>
								</div>
							</div><!-- .entry-content -->
						</div>

					</article>
				</div>	

			</div><!-- #primary -->

			<div id="secondary" class="widget-area" role="complementary">

	            <!-- Sidebar -->
	           <?php include 'sidebar.php'; ?>
	            <!-- Sidebar -->

			</div><!-- #secondary -->		

		</div>
	</div>

<?php include AGCL_TEMPLATE_PATH . 'footer.php'; ?>
<?php ob_end_flush(); ?>

