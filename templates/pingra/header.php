<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <?php if(hasOption($db,'webmaster')) echo getOption($db,'webmaster')->opt_value; ?>

  <?php if($p=='index' || $p=='page') { ?>

  <title><?php echo $blogname?> <?php echo trim($URL['args'][1]) !== '' ? 'on Page '.$URL['args'][1] : ''; ?> | <?php echo $blogdesc;?></title>

  <?php //$kwx = $db->get_results( "SELECT * FROM search_terms AS r1 JOIN (SELECT (RAND() * (SELECT MAX(id) FROM search_terms)) AS id) AS r2 WHERE r1.id >= r2.id AND r1.type='parent'  LIMIT 20" ); ?>

  <?php $limit = 18; ?>
  <?php $page = trim($URL['args'][1]) !== '' ? $URL['args'][1] : 1; ?>
  <?php $offset = ($page - 1)  * $limit; ?>
  <?php $kwx = $db->get_results( "SELECT * FROM search_terms WHERE type='parent' ORDER BY ID DESC LIMIT ".$offset.",18" ); ?>

  <?php 

    $d = ''; 

    if( $kwx ):       
      $i=0; 
      foreach ($kwx as $kv) : 
         $d .= removeSpecial(ucwords($kv->term)).'. ';
         if($i > 5) break; 
         $i++; 
      endforeach; 
    endif; 

  ?>

  <meta name="description" content="<?php echo $d ;?>">
  <meta name="keywords" content="<?php echo $d ;?>">
  <meta name="robots" content="index, follow">
  <meta name="googlebot" content="index,follow,imageindex">
  <link rel="canonical" href="http://<?php echo $_SERVER['SERVER_NAME']; ?>">

  <?php } elseif ($p=='image') { ?>

  <?php

        $imgs = array();

        $cache = new Cache();
        $key = md5('single_post_'.$item->ID);

        if($cache->isCached($key)){

          $imgs = $cache->retrieve($key);

        }else{

            foreach($datas as $a) 
            {

                $im = $db->get_row("SELECT * FROM term_images where parent_term='".$a->ID."'");

                if($im){

                  $title = trim($a->term) == '' || ( count(explode(' ', $a->term)) < 2 ) ? ucwords($q) : ucwords($a->term);
                  
                  $childImgs = getChildImages($db,$im);

                  $sql_fields = array(

                      'term'  => $title,
                      'url'  => SITE_URL.'imgs/'.$im->url,  
                      'childs' => $childImgs,                  
                      'height' =>$im->height,
                      'width' => $im->width,
                      'thumb' => $im->thumb,
                      'type'    => $im->type

                      ); 

                  $imgs[] = $sql_fields;   
                               
                }

            }

          $imgs = is_term_safe_bulk($imgs);
          $cache->setCache($key)->store($key, $imgs);

        }

  ?>
  
  <?php

    $d = ''; 
    if( $imgs ): 
      $i=0; 
      foreach ($imgs as $kv) : 
         $d .= ucwords($kv['term']).'. ';
         if($i > 5) break; 
         $i++; 
      endforeach; 
    endif; 
  
  ?>

  <title><?php echo ucwords(suffleTitleAwal());?> <?php echo ucwords($q);?></title>

  <meta name="description" content="<?php echo ucwords(suffleTitleAwal());?> <?php echo $q;?>. Gallery images of <?php echo ucwords($q);?> Pictures.">
  <meta name="keywords" content="<?php echo ucwords(suffleTitleAwal());?> <?php echo $q;?> photos. <?php echo $q;?> images. <?php echo $q;?> pictures.">
  
  <meta name="robots" content="all,index,follow">
  <meta name="googlebot" content="index,follow,imageindex">
  <meta name="googlebot-Image" content="index,follow">

  <link rel="canonical" href="<?php echo _a_url_q( $q ); ?>">

  <?php } elseif ($p=='attachment') { ?>
  
    <?php

        $imgs = array();

        $cache = new Cache();
        $key = md5('single_post_'.$item->ID);

        if($cache->isCached($key)){

          $imgs = $cache->retrieve($key);

        }else{

            foreach($datas as $a) 
            {

                $im = $db->get_row("SELECT * FROM term_images where parent_term='".$a->ID."'");

                if($im){

                  $title = trim($a->term) == '' || ( count(explode(' ', $a->term)) < 2 ) ? ucwords($q) : ucwords($a->term);
                  
                  $childImgs = getChildImages($db,$im);

                  $sql_fields = array(

                      'term'  => $title,
                      'url'  => SITE_URL.'imgs/'.$im->url,                  
                      'childs' => $childImgs,                  
                      'height' =>$im->height,
                      'width' => $im->width,
                      'thumb' => $im->thumb,
                      'type'    => $im->type

                      ); 

                  $imgs[] = $sql_fields;   
                               
                }

            }

          $imgs = is_term_safe_bulk($imgs);
          $cache->setCache($key)->store($key, $imgs);

        }
      
  ?>

  <title><?php echo ucwords($q);?> - <?php echo ucwords($item->term); ?></title>

  <meta name="description" content="Photo : <?php echo ucwords($img['term']);?> - <?php echo ucwords($item->term); ?>">
  <meta name="keywords" content="<?php echo ucwords($img['term']);?> - <?php echo ucwords($item->term); ?>">
  
  <meta name="robots" content="index, follow">
  <meta name="googlebot" content="index,follow,imageindex">
  <meta name="googlebot-Image" content="index,follow">

  <link rel="canonical" href="<?php echo to_attachment($item->term,$q); ?>">

  <?php } elseif ($p=='info') { ?>

    <title><?php echo $headtitle ?></title>
    <meta name="robots" content="noindex, follow">

  <?php } elseif ($p=='map') { ?>

  <title>Sitemap <?php echo $URL['args'][1]; ?></title>
  <meta name="robots" content="noindex, follow">

  <?php } elseif ($p=='go') { ?>

  <title>Search for <?php echo $URL['args'][1]; ?></title>
  <meta name="robots" content="noindex, follow">

  <?php } else {?>

    <title>Page not found!</title>
    <meta name="robots" content="noindex, nofollow">
    
  <?php } ?>
  
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <?php echo $header; ?>


<style type="text/css">
img.wp-smiley,
img.emoji {
  display: inline !important;
  border: none !important;
  box-shadow: none !important;
  height: 1em !important;
  width: 1em !important;
  margin: 0 .07em !important;
  vertical-align: -0.1em !important;
  background: none !important;
  padding: 0 !important;
}
</style>

<link rel="stylesheet" id="pingraphy-font-awesome-style-css" href="<?php echo AGCL_TEMPLATE_URL ?>css/font-awesome.min.css" type="text/css" media="all">
<link rel="stylesheet" id="pingraphy-style-css" href="<?php echo AGCL_TEMPLATE_URL ?>css/style.css" type="text/css" media="all">
<link rel="stylesheet" id="pingraphy-responsive-style-css" href="<?php echo AGCL_TEMPLATE_URL ?>css/responsive.css" type="text/css" media="all">
<link rel="stylesheet" id="google-typography-font-css" href="<?php echo AGCL_TEMPLATE_URL ?>css/css.css" type="text/css" media="all">

  <script type="text/javascript" src="<?php echo AGCL_TEMPLATE_URL ?>js/jquery.js"></script>
  <script type="text/javascript" src="<?php echo AGCL_TEMPLATE_URL ?>js/jquery-migrate.min.js"></script>
  <script type="text/javascript" src="<?php echo AGCL_TEMPLATE_URL ?>js/imagesloaded.pkgd.min.js"></script>
  <script type="text/javascript" src="<?php echo AGCL_TEMPLATE_URL ?>js/js.cookie.js"></script>
  <script type="text/javascript" src="<?php echo AGCL_TEMPLATE_URL ?>js/jquery.lazyload.min.js"></script>
    <script type="text/javascript">
        jQuery(function() {
            jQuery("img.lazy").lazyload({
                effect: "fadeIn"
            });
        });
    </script>
  <script type="text/javascript" src="<?php echo AGCL_TEMPLATE_URL ?>js/script.js"></script>

<!--[if lt IE 9]>
<script type='text/javascript' src='http://demo.themecountry.com/pingraphy/wp-content/themes/Pingraphy/js/html5shiv.min.js?ver=3.7.3'></script>
<![endif]-->

<meta name="generator" content="WordPress 4.9.3">

<style type="text/css">
.site-header .site-title a, .site-header .site-description{ font-family: "Fira Sans"; font-weight: 700; font-size: 26px; color: #ff6565;  }
.main-navigation ul li a{ font-family: "Fira Sans"; font-weight: normal; font-size: 14px; color: #737373;  }
.section-two .toggle-mobile-menu, .second-navigation ul li a{ font-family: "Fira Sans"; font-weight: normal; font-size: 14px; color: #737373;  }
.site-header .social-media h5, .site-header .social-media ul li a{ font-family: "Fira Sans"; font-weight: normal; font-size: 14px; color: #737373;  }
.item-text .entry-header .entry-title, .item-text .entry-header .entry-title a{ font-family: "Fira Sans"; font-weight: 700; font-size: 18px; color: #282724;  }
.page .entry-title, .single article .entry-title{ font-family: "Fira Sans"; font-weight: 700; font-size: 18px; color: #282724;  }
body, .widget ul li a{ font-family: "Fira Sans"; font-weight: normal; font-size: 14px; color: #555555;  }
.widget h2.widget-title, .related-posts h3.title-related-posts, .comments-area h3.comment-reply-title{ font-family: "Fira Sans"; font-weight: 700; font-size: 18px;  }
.site-footer .site-info{ font-family: "Fira Sans"; font-weight: normal; font-size: 14px; color: #bbbbbb;  }
h1{ font-family: "Fira Sans"; font-weight: 700; font-size: 30px;  }
h2{ font-family: "Fira Sans"; font-weight: 700; font-size: 28px;  }
h3{ font-family: "Fira Sans"; font-weight: 700; font-size: 26px;  }
h4{ font-family: "Fira Sans"; font-weight: 700; font-size: 24px;  }
h5{ font-family: "Fira Sans"; font-weight: 700; font-size: 20px;  }
h6{ font-family: "Fira Sans"; font-weight: 700; font-size: 18px;  }
</style>

  <style type="text/css">
    
          .sticky-nav,
      .site-header {
        background: #ffffff;
      }
      .site-header .section-one .toggle-mobile-menu,
      .search-style-one a i {
        color: #737373;
      }
      .main-navigation ul li a {
        background-color: #ffffff;
        color: #737373;
      }
      .main-navigation ul.menu > li.current-menu-item > a,
      .main-navigation ul li a:hover {
        background-color: #f5f5f5;
      }
      .site-header .section-two,
      .widget_tag_cloud .tagcloud a {
        background: #f5f5f5;
      }
      .site-header .section-two nav ul li.current_page_item > a:before,
      .site-header .section-two nav ul li.current_page_parent > a:before,
      .site-header .section-two nav ul li.current_page_ancestor > a:before,
      .site-header .section-two nav ul li.current-menu-parent > a:before,
      .site-header .section-two nav ul li.current-menu-item > a:before,
      .site-header .section-two nav ul li li:hover > a:before,
      .site-header .section-two nav ul > li > a:before  {
        background-color: #ff6565;
      }
      .site-header .section-two nav ul ul {
        background: #f5f5f5;
      }

      .site-header .social-media h5,
      .site-header .social-media ul li a {
        color: ;
      }
      .site-header .social-media ul li a:hover {
        color: ;
      }
      button,
      input[type="button"],
      input[type="reset"],
      input[type="submit"],
      .comments-area .comment-respond input.submit:focus,
      .comments-area .comment-respond input.submit {
        background: #e5e5e5;
        color: #737373;
      }
      .footer-widgets {
        background: #dddddd;
      }
      .footer-widgets *,
      .footer-widgets .widget ul li *,
      .footer-widgets .widget ul li a {
        color: #737373;
      }
      .footer-widgets a:hover,
      .footer-widgets .widget ul li a:hover {
        color: #ff6565;
      }
      .site-footer .site-info {
        background: #ffffff;
      }
      .site-footer .site-info *,
      .site-footer .site-info a {
        color: #737373;
      }
      .site-footer .site-info a:hover {
        color: #ff6565;
      }
      .site-footer .menu ul li a:after,
      .site-footer .menu-footer ul li a:after {
        background: #737373;
      }
      .archive .nav-links a,
      .blog .nav-links a,
      .home .nav-links a,
      .back-to-top,
      .page-links a,
      #load-more-wrap a,
      .btn-read-more a {
        background: #ff6565;
      }
      .page-links {
        border-color: #ff6565;
      }
      #load-more-wrap a:hover,
      .archive .nav-links span.current,
      .blog .nav-links span.current,
      .home .nav-links span.current,
      .archive .nav-links a:hover,
      .blog .nav-links a:hover,
      .home .nav-links a:hover,
      .back-to-top:hover {
        background: #737373;
      }

        
    ;
  </style>
  <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
  <style type="text/css">
    h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {color:#ccc !important;}.panel-transparent{background-color: transparent !important;}
  </style>
</head>
<body class="home blog">

  <div id="page" class="hfeed site social-share-on">

    <header id="masthead" class="site-header" role="banner">
      <div class="clearfix">

        <div class="section-one">
          <div class="inner">
            
            <div class="site-branding">

              <?php if($p=='index'){ ?>

                <h1 itemprop="headline" class="site-title">
                  <a itemprop="url" href="<?php echo SITE_URL; ?>" rel="home" title="<?php echo $_SERVER['SERVER_NAME']; ?>">
                  <?php echo $_SERVER['SERVER_NAME']; ?></a>
                </h1>
                <h2 class="site-description">Ping Everything You Like.</h2>

              <?php }else if($p == 'image' || $p == 'attachment'){ ?>
              
                <h2 itemprop="headline" class="site-title">
                  <a itemprop="url" href="<?php echo SITE_URL; ?>" rel="home" title="<?php echo $_SERVER['SERVER_NAME']; ?>">
                  <?php echo $_SERVER['SERVER_NAME']; ?></a>
                </h2>
                <h3 class="site-description">Ping Everything You Like.</h3>

              <?php }else{ ?>

                <h1 itemprop="headline" class="site-title">
                  <a itemprop="url" href="<?php echo SITE_URL; ?>" rel="home" title="<?php echo $_SERVER['SERVER_NAME']; ?>">
                  <?php echo $_SERVER['SERVER_NAME']; ?></a>
                </h1>
                <h2 class="site-description">Ping Everything You Like.</h2>

              <?php } ?>

             
            </div><!-- .site-branding -->
            
            <div class="search-style-one">
              <a id="trigger-overlay">
                <i class="fa fa-search"></i>
              </a>
              <div class="search-overlay overlay-slideleft">
                <div class="search-row">
                  <form method="get" id="searchform" class="search-form" action="<?php echo SITE_URL . "go"; ?>" _lpchecked="1">
                    <a ahref="#" class="overlay-close"><i class="fa fa-times"></i></a>
                    <input type="text" name="s" id="s" value="" placeholder="Search Keyword ...">
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>
      <div id="catcher"></div>
    </header><!-- #masthead -->