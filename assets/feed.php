<?php

	if( ! defined('ABSPATH') ) require_once '../bootstrap.php';
	if( $ismysql !== TRUE ) die('Error!! Your $ismysql in config.php is not set to TRUE');

		header("Content-Type: application/rss+xml; charset=ISO-8859-1");
	
	$postingan = $db->get_results( "SELECT * FROM search_terms AS r1 JOIN (SELECT (RAND() * (SELECT MAX(id) FROM search_terms)) AS id) AS r2 WHERE r1.id >= r2.id AND r1.type='parent'  LIMIT 2500" );

?>
<?php echo '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<rss xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:wfw="http://wellformedweb.org/CommentAPI/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/" version="2.0">
	<channel>
		<atom:link href="http://mydesignbed.us/feed" rel="self" type="application/rss+xml" />
		<title><?php echo $_SERVER['SERVER_NAME']; ?> RSS</title>
		<description>Best Place to Find Your Designing Home RSS</description>
		<link><?php echo SITE_URL ?></link>
		<lastBuildDate><?php date("D, d M Y H:i:s O", strtotime(date('Y-m-d H:m:s'))); ?></lastBuildDate>

		<?php if( $postingan ):  ?>
			<?php foreach ($postingan as $kv) : ?>
				<item>
					<title><?php echo removeSpecial(ucwords($kv->term));?></title>
					<link><?php echo _a_url_q( $kv->term ); ?></link>
					<description>
						<![CDATA[<div><?php echo removeSpecial(ucwords($kv->term));?></div>]]>
					</description>
					<pubDate><?php date("D, d M Y H:i:s O", strtotime(date('Y-m-d H:m:s'))); ?></pubDate>
					<guid><?php echo _a_url_q( $kv->term ); ?></guid>
				</item>
	      <?php endforeach; ?>
	    <?php endif; ?>

	</channel>
</rss>
