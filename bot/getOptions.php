<?php

	if( !defined('ABSPATH') ) require_once '../bootstrap.php';
	if( $ismysql !== TRUE ) die('Error!! Your $ismysql in config.php is not set to TRUE');

	header("Content-type:application/json");
	
	$options = array(

			'webmaster' => array(
		      'Code Webmaster',
		      'webmaster',
		      ''
				),
			'stat' => array(
		      'Code Stat',
		      'stat',
		      ''
				),
			'ads728' => array(
		      'Ads Content (728)',
		      'ads728',
		      ''
				),
			'ads300' => array(
		      'Ads Rectangle (300)',
		      'ads300',
		      ''
				),
			'ads160' => array(
		      'Ads Sidebar (160)',
		      'ads160',
		      ''
				),

			);

	foreach($options as $o){

		$c = $db->get_row("SELECT * FROM site_options where opt_code='".$o[1]."'");
		if(!is_object($c)) generateDefaultOptions($db,$o);

	}

	$datas = $db->get_results( "SELECT * FROM site_options" );

	echo json_encode($datas);