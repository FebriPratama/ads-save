<?php

if( ! defined('ABSPATH') ) require_once '../bootstrap.php';
if( $ismysql !== TRUE ) die('Error!! Your $ismysql in config.php is not set to TRUE');

$limit = trim($_GET['limit']) === '' ? 1000 : $_GET['limit'];

// get all the pulished pdfs
// get the amount of all PUBLISHED pdf
$count_allpub = $db->get_var("SELECT COUNT(*) FROM search_terms where type='parent'");

// divide the pdf total amount by number of URLs per sitemap file
$total_stmp_file  = ceil( $count_allpub / $limit );
$stmp_fn_format   = "sitemap-%s.xml";

echo '<h3>Generating XML from SQL with limit '.$limit.' perfile</h3>';

for($i=0;$i < $total_stmp_file;$i++){

  $offset = $i == FALSE ? 0 : ($limit*$i)+1;

  $stmp_suffix_number = ( $offset / $limit ) + 1;

  $allpdfs = $db->get_results("SELECT * FROM search_terms where type = 'parent' ORDER BY ID ASC LIMIT ".$limit." OFFSET ".$offset);

  echo 'Round : '.$i. ', Total : ' .count($allpdfs) .'<br>';

  if( $allpdfs ):

    $xml_output = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
    $xml_output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"' . "\n";
    $xml_output .= 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' . "\n";
    $xml_output .= 'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9' . "\n";
    $xml_output .= 'http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' . "\n";

    $urls_towrite = array();
    foreach( $allpdfs as $allpdf ):
      $urls_towrite[] = '<url><loc>' . _a_url_q( $allpdf->term ) . '</loc><changefreq>daily</changefreq><priority>0.9</priority><lastmod>'.date("c", strtotime(date('Y-m-d H:m:s'))).'</lastmod></url>';
    endforeach;

    // glueing to a string
    $url_towrite = implode("\n", $urls_towrite);

    $xml_output .= $url_towrite;

    $xml_output .= "\n" . '</urlset>';

    // construct the new sitemap file path
    $stmp_fpath   = sprintf(ABSPATH. 'sitemap/' . $stmp_fn_format, $stmp_suffix_number);

    // write the URLs to the sitemap file
    $fh = fopen( $stmp_fpath, "wb+");
    fwrite( $fh, $xml_output );
    fclose( $fh );

  endif;

}

echo '<h3>Generating index xml</h3>';

  // sitemap group
  $xml_output = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
  $xml_output .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";

  $urls_towrite = array();
  $dircontents = scandir( ABSPATH.'sitemap/' );

  foreach ($dircontents as $dc) {
    if( strpos($dc, 'sitemap-') !== FALSE ) {
      $urls_towrite[] = '<sitemap><loc>' . SITE_URL . 'sitemap/' . $dc . '</loc><lastmod>'.date("c", strtotime(date('Y-m-d H:m:s'))).'</lastmod></sitemap>';
    }
  }

  // glueing to a string
  $url_towrite = implode("\n", $urls_towrite);

  $xml_output .= $url_towrite;

  $xml_output .= "\n" . '</sitemapindex>';

  // construct the new sitemap file path
  $stmp_fpath   = sprintf(ABSPATH. 'sitemap.xml', "wb+");

  // write the URLs to the sitemap file
  $fh = fopen( $stmp_fpath, "wb+");
  fwrite( $fh, $xml_output );
  fclose( $fh );

echo '<h3>Generating robots.txt</h3>';

$robots_content  = "User-agent: *\n";
$robots_content .= "Allow: /\n";

$dircontents = scandir( ABSPATH.'sitemap/' );
foreach ($dircontents as $dc) {
  if( strpos($dc, 'sitemap-') !== FALSE ) {
    $robots_content .= "Sitemap: ". SITE_URL . 'sitemap/' . $dc ."\n";
  }
}

// create robots.txt
$fm = fopen( ABSPATH . "robots.txt", "wb+");
fwrite( $fm, $robots_content );
fclose( $fm );
