<?php

require_once '../bootstrap.php';

//----- SETTINGS: EDITING IS ALLOWED -----//
$lstmp_IDf        = ABSPATH . "last_item_start.txt";
$nlines_per_file  = 1000;
$stmp_fn_format   = "sitemap-%s.xml";        // your sitemap file will be http://domain.com/sitemap-pdf-.txt, http://domain.com/sitemap-pdf-2.txt


//----- !! DO NOT EDIT AFTER THIS !! -----//  

$xml_output = '';

// get the amount of all PUBLISHED pdf
$count_allpub = $db->get_var("SELECT COUNT(*) FROM search_terms");

// divide the pdf total amount by number of URLs per sitemap file
$total_stmp_file  = ceil( $count_allpub / $nlines_per_file );
// echo '$total_stmp_file = ' . $total_stmp_file . "<br>";

// get the last queried row by reading $lstmp_IDf
$current_start_row  = file_exists($lstmp_IDf) ? file_get_contents($lstmp_IDf) : "1";

// count suffix sitemap filename based on $current_start_row 100000/50000 = 1+1 = 2
$stmp_suffix_number = ( $current_start_row / $nlines_per_file ) + 1;

// $next_start_row needs to be un-incremented if the suffix number reaches the 
// total sitemap file needed to be generate
// this will ensure the script to always update the last sitemap file, 
// instead of creating new sitemap when the pdf amount is growing
if( $stmp_suffix_number == $total_stmp_file ) {
  $next_start_row = $current_start_row;

} else {
  $next_start_row = $current_start_row + $nlines_per_file;

}

// get all the pulished pdfs
$allpdfs = $db->get_results("SELECT * FROM search_terms ORDER BY ID ASC LIMIT {$current_start_row},{$nlines_per_file}");

if( $allpdfs ):

  $xml_output .= '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
  $xml_output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"' . "\n";
  $xml_output .= 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' . "\n";
  $xml_output .= 'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9' . "\n";
  $xml_output .= 'http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' . "\n";

  $urls_towrite = array();
  foreach( $allpdfs as $allpdf ):
    $urls_towrite[] = '<url><loc>' . _a_url_q( $allpdf->term ) . '</loc><changefreq>daily</changefreq><priority>0.9</priority></url>';
  endforeach;

  // glueing to a string
  $url_towrite = implode("\n", $urls_towrite);

  $xml_output .= $url_towrite;

  $xml_output .= "\n" . '</urlset>';

  // construct the new sitemap file path
  $stmp_fpath   = sprintf(ABSPATH . $stmp_fn_format, $stmp_suffix_number);

  // write the URLs to the sitemap file
  $fh = fopen( $stmp_fpath, "wb+");
  fwrite( $fh, $xml_output );
  fclose( $fh );

  // update the $lstmp_IDf file
  $fk = fopen( $lstmp_IDf, "wb+");
  fwrite( $fk, $next_start_row );
  fclose( $fk );

else:

  // echo '<h1>all URLs are already written</h1>';

endif;

// list already generated sitemap files
$stmp_furls = array();
for($i=0; $i<$stmp_suffix_number; $i++) {
  $j = $i+1;
  $stmp_furl    = sprintf(SITE_URL . $stmp_fn_format, $j);
  $stmp_furls[] = $stmp_furl;
  //echo $stmp_furl . "<br>\n";
}


$robots_content  = "User-agent: *\n";
$robots_content .= "Allow: /\n";

$dircontents = scandir( ABSPATH );
foreach ($dircontents as $dc) {
  if( strpos($dc, 'sitemap-') !== FALSE ) {
    $robots_content .= "Sitemap: ". SITE_URL . $dc ."\n";
  }
}

// create robots.txt
$fm = fopen( ABSPATH . "robots.txt", "wb+");
fwrite( $fm, $robots_content );
fclose( $fm );
