<?php

set_time_limit(180);
ini_set('memory_limit','512M');

if( ! defined('ABSPATH') ) require_once '../bootstrap.php';
if( $ismysql !== TRUE ) die('Error!! Your $ismysql in config.php is not set to TRUE');

require_once ABSPATH . 'vendor/autoload.php';
use Intervention\Image\ImageManagerStatic as Image;

$limit = 1000;

function makeCurlCall($url){

  $curl = curl_init();
  $timeout = 10;
  curl_setopt($curl,CURLOPT_URL,$url);
  curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,$timeout);
  curl_setopt($curl,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

  curl_setopt($curl, CURLOPT_ENCODING,"");   
  curl_setopt($curl, CURLOPT_TIMEOUT,10);
  curl_setopt($curl, CURLOPT_FAILONERROR,true);
  curl_setopt($curl, CURLOPT_VERBOSE, true);

  $output = curl_exec($curl);
  

  if (curl_errno($curl)){

    curl_close($curl);
    return false;

  }
  else {

    curl_close($curl);
    return $output;

  }  

  return false;
}

function generateSingleSitemap(){

  $count_allpub = $db->get_var("SELECT COUNT(*) FROM search_terms where type='parent'");

  // divide the pdf total amount by number of URLs per sitemap file
  $total_stmp_file  = ceil( $count_allpub / $limit );
  $stmp_fn_format   = "sitemap-%s.xml";

  for($i=0;$i < $total_stmp_file;$i++){

    $offset = $i == FALSE ? 0 : ($limit*$i)+1;

    $stmp_suffix_number = ( ceil($offset / $limit) ) + 1;

    $allpdfs = $db->get_results("SELECT * FROM search_terms where type='parent' ORDER BY ID ASC LIMIT ".$limit." OFFSET ".$offset);

    if( $allpdfs ):

      $xml_output = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
      $xml_output .= '<?xml-stylesheet type="text/xsl" href="'.SITE_URL.'assets/single-jetpack.xsl"?>' . "\n";
      $xml_output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"' . "\n";
      $xml_output .= 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' . "\n";
      $xml_output .= 'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9' . "\n";
      $xml_output .= 'http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' . "\n";

      $urls_towrite = array();
      foreach( $allpdfs as $allpdf ):
        $urls_towrite[] = '<url><loc>' . _a_url_q( $allpdf->term ) . '</loc><changefreq>daily</changefreq><priority>0.9</priority></url>';
      endforeach;

      // glueing to a string
      $url_towrite = implode("\n", $urls_towrite);

      $xml_output .= $url_towrite;

      $xml_output .= "\n" . '</urlset>';

      // construct the new sitemap file path
      $stmp_fpath   = sprintf(ABSPATH. 'sitemap/' . $stmp_fn_format, $stmp_suffix_number);

      // write the URLs to the sitemap file
      $fh = fopen( $stmp_fpath, "wb+");
      fwrite( $fh, $xml_output );
      fclose( $fh );

    endif;

  }

}

function getFileExtension($url) {
    $extension = explode('.', $url);
    $extension = end($extension);
    if(empty($extension) || strlen($extension) > 4) {$extension = 'jpg';}
    return $extension;
}

function generateIndexSitemap(){

    // sitemap group
    $xml_output = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
    $xml_output .= '<?xml-stylesheet type="text/xsl" href="'.SITE_URL.'assets/index-jetpack.xsl"?>' . "\n";
    $xml_output .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";

    $urls_towrite = array();
    $dircontents = scandir( ABSPATH.'sitemap/' );

    foreach ($dircontents as $dc) {
      if( strpos($dc, 'sitemap-') !== FALSE ) {
        $urls_towrite[] = '<sitemap><loc>' . SITE_URL . 'sitemap/' . $dc . '</loc></sitemap>';
      }
    }

    // glueing to a string
    $url_towrite = implode("\n", $urls_towrite);

    $xml_output .= $url_towrite;

    $xml_output .= "\n" . '</sitemapindex>';

    // construct the new sitemap file path
    $stmp_fpath   = sprintf(ABSPATH. 'sitemap.xml', "wb+");

    // write the URLs to the sitemap file
    $fh = fopen( $stmp_fpath, "wb+");
    fwrite( $fh, $xml_output );
    fclose( $fh );

    $robots_content  = "User-agent: *\n";
    $robots_content .= "Allow: /\n";

    $dircontents = scandir( ABSPATH.'sitemap/' );
    foreach ($dircontents as $dc) {
      if( strpos($dc, 'sitemap-') !== FALSE ) {
        $robots_content .= "Sitemap: ". SITE_URL . 'sitemap/' . $dc ."\n";
      }
    }

    // create robots.txt
    $fm = fopen( ABSPATH . "robots.txt", "wb+");
    fwrite( $fm, $robots_content );
    fclose( $fm );

}

function getPosts($data = [], $total, $try = 1){

  $rtn = array('posts'=>[],'kreads'=>$data);

  $w = 0;
  for($i=0; $i<$total; $i++) {

    if( isset($rtn['kreads'][$i]) ) {

      if(is_term_safe( $rtn['kreads'][$i] )){

        $rtn['posts'][] = $rtn['kreads'][$i];        
        $w++;

      }
      
      unset( $rtn['kreads'][$i] );

      if(count($rtn['posts']) >= $total){
        break;
        return $rtn;
      }
      
    }

  }
  
  $try++;
  if($try>100) return $rtn;
  if(!$w) return getPosts($rtn['kreads'],$total,$try);

  return $rtn;
  
}

function duplicateCheck($db,$datas,$tmp,$total){

  $data = array('posts'=>array(),'datas'=>array());

  $i=0;
  foreach($tmp as $t){
    
      $t = removeSpecial($t);

      $item = $db->get_row("SELECT * FROM search_terms WHERE slug = '".clean( $t )."'");
      
      if($item && is_object($item)){

        unset($tmp[$i]);

      }
      $i++;
  }

  if(!count($tmp)){
    if(array_key_exists(0, $datas)){
      
      $tmp[] = $datas[0];
      unset($datas[0]);

      return duplicateCheck($db,$datas,$tmp,$total);

    }else{
      
      return $data;

    }
  }

  return array('posts'=>$tmp,'datas'=>$datas);  

}

function filterKeywords($db,$total){

  $r = array();
  $filepath = ABSPATH . 'keywords.txt';

  /*===== settings STOP  =====*/
  $flags_runinject = array();
  $kread = '';

  // check if file exists
  if( ! file_exists( $filepath ) ) {
    $flags_runinject[] = FALSE;
  } else {
    $kread = file_get_contents($filepath);  // read keyword inject file
  }
  
  // check if file is not empty
  if( strlen($kread) < 2 ) {
    $flags_runinject[] = FALSE;
  }

  // only run if no FALSE found in $flags_runinject
  if( ! in_array(FALSE, $flags_runinject) ) {

    $kreads = explode("\n", $kread);
    
    $tmp = array();

    foreach($kreads as $k){
      
      if(is_term_safe( $k )){

        $tmp[] = $k;

      }
    
    } 

    if(count($tmp) > 0){
      
      for($i=0;$i<$total;$i++){

        $r[] = $tmp[$i];
        unset($tmp[$i]);   

      }

    }

    //check duplicate
    $c = duplicateCheck($db,$tmp,$r,$total);

    $r = $c['posts'];
    $tmp = $c['datas'];

    // prepare sliced data for file writing
    $kread_sliced = implode("\n", $tmp);

    $fh = fopen($filepath, "wb+");

    fwrite($fh, $kread_sliced);
    fclose($fh);
    
  }

  return $r;

}

/*===== settings START =====*/
$injectamount = isset($_GET['total']) ? $_GET['total'] : 1;
$imageperpost= isset($_GET['images']) ? $_GET['images'] : 30;
$proxy = isset($_GET['proxy']) ? $_GET['proxy'] : ''; 
$bdays = isset($_GET['bdays']) ? $_GET['bdays'] : 0; 
$sdays = isset($_GET['sdays']) ? $_GET['sdays'] : 0; 
$type = isset($_GET['type']) ? $_GET['type'] : 'backdate'; 
  
$r = filterKeywords($db,$injectamount);

$images = 0;
$errors = 0;
$tmp = array();
$url = array();

// only run if no FALSE found in $flags_runinject
if( count($r) ) {
  
  $awal = array('Stunning','Dazzling','Delightful','Trendy','Wonderful','Lovely','Charming','Good Looking','Outstanding','Attracktive','Engaging','Impressive','Gorgeous','Fascinating','Magnificent','Alluring','Excelent','Elegant','New', 'Cool', 'Unique', 'Nice', 'Luxury', 'Modest', 'Awesome', 'Amazing', 'Fresh', 'Popular', 'Awesome', 'Custom', 'Modern', 'Inspiring' , 'Simple', 'Classic');

  foreach($r as $p){
    
    $p = removeSpecial($p);

    $sql_fields = array(

      'term'  => trim( $p ),
      'slug'  => trim( clean( $p ) ),
      'se'    => 'fileinject',
      'type' => 'parent',
      'last_robot_access' => date('Y-m-d H:m:s'),
      'last_human_access' => date('Y-m-d H:m:s'),
      'access_count'      => '0',
      'term_status'       => '1'

      ); 

    $db->query( construct_query_insert('search_terms', $sql_fields) );

    $item = $db->get_row("SELECT * FROM search_terms WHERE slug = '".clean( $p )."'");

    $tmp[] = $item;

    if($item){

      $datas = doMagicGoogle($p,$proxy);

      $datas = count($datas) < 1 ? $datas : doMagicBing($p);

      $url[] = _a_url_q($item->term);

      $destinationPath = ABSPATH. 'imgs/';

      if(is_array($datas) && count($datas) < 1){

        $db->query("delete from search_terms where ID=".$item->ID);

      }

      $ai = 0;
      foreach($datas as $a) 
      {
        if($ai <= $imageperpost && !strpos($a['url'], '?') && !strpos($a['url'], ';') && !strpos($a['url'], ',') && !strpos($a['url'], '%')){

          $db->hide_errors(); 

          $title = trim($a['title']) == '' || ( count(explode(' ', $a['title'])) < 2 ) ? ucwords($item->term) : ucwords($a['title']);

          $title = removeSpecial($title);
          
          $title = $awal[rand(0,29)].' '.$p.' '.$title;

          $date = strtotime(date('Y-m-d H:m:s'));
          $date = date('Y-m-d H:m:s', strtotime('+'.$bdays.' day', $date));

          if($type == 'backdate'){

            $date = date('Y-m-d H:m:s', strtotime('-'.$bdays.' day', $date));

          }else{

            $date = date('Y-m-d H:m:s', strtotime('+'.$sdays.' day', $date));

          }

          $sqlChild = array(

            'term'  => trim( $title  ),
            'slug'  => trim( clean( $title ) ),
            'se'    => 'fileinject',
            'type' => 'child',
            'parent_id' => $item->ID,
            'last_robot_access' => date('Y-m-d H:m:s'),
            'last_human_access' => $date,
            'access_count'      => '0',
            'term_status'       => '1'

            ); 

          $db->query( construct_query_insert('search_terms', $sqlChild) );

          $itemChild = $db->get_row("SELECT * FROM search_terms WHERE slug = '".clean( $title )."'");

          //save images
          //$url = checkImgNew($a['url']);
/*
          $fileExtentension = pathinfo($a['url']);

          @$fileExtentension = $fileExtentension['extension'];*/

          $fileExtentension = getFileExtension($a['url']);

          $rawImage = makeCurlCall($a['url']);

          if($rawImage && is_object($itemChild)){

            /**
            *
            * IMAGE GRABBER
            * 
            **/

            $imgName = to_prety_url($title.'-'.date('Ymdhms'));

            if(file_put_contents($destinationPath.''.$imgName.'.'.$fileExtentension,$rawImage)){

              $name = $destinationPath.''.$imgName.'.'.$fileExtentension;

              if(filesize($name) > 1000){

                $mime = strtolower(finfo_file(finfo_open(FILEINFO_MIME_TYPE), $name));

                $sqlImg = array(

                    'term'  => $title,
                    'url'  => $imgName.'.'.$fileExtentension,
                    'parent_term' => $itemChild->ID,
                    'height' => $a['height'],
                    'width' => $a['width'],
                    'thumb' => $a['thumb'],
                    'type'    => 'full'

                    ); 

                $db->query( construct_query_insert('term_images', $sqlImg) );

                $parentImg = $db->get_row( "SELECT * FROM term_images where url = '".$imgName.'.'.$fileExtentension."'");

                if($parentImg && ( $mime == 'image/png' || $mime == 'image/x-png' || $mime == 'image/jpg' || $mime == 'image/jpeg'|| $mime == 'image/pjpeg' || $mime == 'image/gif')){

                  try {

                    // Full version
                    $full = Image::make($name);
                    
                      // large
                      $large = Image::make($name);
                      $large->resize(808, null, function ($constraint) {
                          $constraint->aspectRatio();
                      });
                      $large->save($destinationPath.''.$imgName.'-808x'.$large->height().'.'.$fileExtentension,60);  
                      $sqlImg = array(

                          'term'  => $title,
                          'url'  => $imgName.'-808x'.$large->height().'.'.$fileExtentension,
                          'parent_term' => $parentImg->ID,
                          'height' => $large->height(),
                          'width' => $large->width(),
                          'thumb' => $a['thumb'],
                          'type'    => 'large'

                          ); 

                      $db->query( construct_query_insert('term_images', $sqlImg) );   

                      // medium
                      $medium = Image::make($name);
                      $medium->resize(718, null, function ($constraint) {
                          $constraint->aspectRatio();
                      });
                      $medium->save($destinationPath.''.$imgName.'-718x'.$medium->height().'.'.$fileExtentension,60);   
                      $sqlImg = array(

                          'term'  => $title,
                          'url'  => $imgName.'-718x'.$medium->height().'.'.$fileExtentension,
                          'parent_term' => $parentImg->ID,
                          'height' => $medium->height(),
                          'width' => $medium->width(),
                          'thumb' => $a['thumb'],
                          'type'    => 'medium'

                          ); 

                      $db->query( construct_query_insert('term_images', $sqlImg) );   

                      // small
                      $thumb = Image::make($name);
                      $thumb->resize(100, 100);
                      $thumb->save($destinationPath.''.$imgName.'-100x100.'.$fileExtentension,60);     
                      $sqlImg = array(

                          'term'  => $title,
                          'url'  => $imgName.'-100x100.'.$fileExtentension,
                          'parent_term' => $parentImg->ID,
                          'height' => $thumb->height(),
                          'width' => $thumb->width(),
                          'thumb' => $a['thumb'],
                          'type'    => 'thumb'

                          ); 

                      $db->query( construct_query_insert('term_images', $sqlImg) );  

                      $images++;
                      $ai++;

                  } catch (Exception $e) {

                      $errors++;

                  }

                }

              } // filesize

            } // move

          } // url
          
        }// if $ci

      } // foreach datas

      if($ai < 1) $db->query("delete from search_terms where ID=".$item->ID);

    } // if $item

  }

  echo json_encode([ 'post' => $url, 'images' => $images, 'message' => 'Ok', 'erros' => $errors ]);

}else{

  echo json_encode([ 'post' => array(), 'images' => 0, 'message' => 'Keywords Sudah Habis', 'erros' => 0]);

}

header("Content-type:application/json");
