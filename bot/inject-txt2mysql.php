<?php

if( ! defined('ABSPATH') ) require_once '../bootstrap.php';
if( $ismysql !== TRUE ) die('Error!! Your $ismysql in config.php is not set to TRUE');


/*===== settings START =====*/

$filepath = ABSPATH . 'keywords.txt';
$injectamount = 5;

/*===== settings STOP  =====*/

$flags_runinject = array();
$kread = '';

// check if file exists
if( ! file_exists( $filepath ) ) {
  $flags_runinject[] = FALSE;
} else {
  $kread = file_get_contents($filepath);  // read keyword inject file
}

// check if file is not empty
if( strlen($kread) < 2 ) {
  $flags_runinject[] = FALSE;
}


// only run if no FALSE found in $flags_runinject
if( ! in_array(FALSE, $flags_runinject) ) {

  // transform into iterable array
  $kreads = explode("\n", $kread);

  // turn-off error display when duplicate entry insert is performed
  $db->hide_errors();
  $w=0;
  for($i=0; $i<$injectamount; $i++) {
    if( isset($kreads[$i]) ) {

      $sql_fields = array(
        'term'  => trim( $kreads[$i] ),
        'slug'  => trim( clean( $kreads[$i] ) ),
        'se'    => 'fileinject',
        'last_robot_access' => '1991-12-31 02:15:39',
        'last_human_access' => '1993-08-17 10:10:49',
        'access_count'      => '0',
        'term_status'       => '1',
        ); 
      $db->query( construct_query_insert('search_terms', $sql_fields) );
      unset( $kreads[$i] );
      $w++;
    }
  }

  // turn-on error display
  $db->show_errors();

  // prepare sliced data for file writing
  $kread_sliced = implode("\n", $kreads);

  $fh = fopen($filepath, "wb+");
  fwrite($fh, $kread_sliced);
  fclose($fh);
  
} // endif( ! in_array(FALSE, $flags_runinject) )

//----- SETTINGS: EDITING IS ALLOWED -----//
$lstmp_IDf        = ABSPATH . "last_item_start.txt";
$nlines_per_file  = 1000;
$stmp_fn_format   = "sitemap-%s.xml";        // your sitemap file will be http://domain.com/sitemap-pdf-.txt, http://domain.com/sitemap-pdf-2.txt

//----- !! DO NOT EDIT AFTER THIS !! -----//  

$xml_output = '';

// get the amount of all PUBLISHED pdf
$count_allpub = $db->get_var("SELECT COUNT(*) FROM search_terms");

// divide the pdf total amount by number of URLs per sitemap file
$total_stmp_file  = ceil( $count_allpub / $nlines_per_file );
// echo '$total_stmp_file = ' . $total_stmp_file . "<br>";

// get the last queried row by reading $lstmp_IDf
$current_start_row  = file_exists($lstmp_IDf) ? file_get_contents($lstmp_IDf) : "1";

// count suffix sitemap filename based on $current_start_row 100000/50000 = 1+1 = 2
$stmp_suffix_number = ( $current_start_row / $nlines_per_file ) + 1;

// $next_start_row needs to be un-incremented if the suffix number reaches the 
// total sitemap file needed to be generate
// this will ensure the script to always update the last sitemap file, 
// instead of creating new sitemap when the pdf amount is growing
if( $stmp_suffix_number == $total_stmp_file ) {
  $next_start_row = $current_start_row;

} else {
  $next_start_row = $current_start_row + $nlines_per_file;

}

// get all the pulished pdfs
$allpdfs = $db->get_results("SELECT * FROM search_terms ORDER BY ID ASC LIMIT {$current_start_row} OFFSET {$nlines_per_file}");

if( $allpdfs ):

  $xml_output .= '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
  $xml_output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"' . "\n";
  $xml_output .= 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' . "\n";
  $xml_output .= 'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9' . "\n";
  $xml_output .= 'http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' . "\n";

  $urls_towrite = array();
  foreach( $allpdfs as $allpdf ):
    $urls_towrite[] = '<url><loc>' . _a_url_q( $allpdf->term ) . '</loc><changefreq>daily</changefreq><priority>0.9</priority></url>';
  endforeach;

  // glueing to a string
  $url_towrite = implode("\n", $urls_towrite);

  $xml_output .= $url_towrite;

  $xml_output .= "\n" . '</urlset>';

  // construct the new sitemap file path
  $stmp_fpath   = sprintf(ABSPATH. 'sitemap/' . $stmp_fn_format, $stmp_suffix_number);

  // write the URLs to the sitemap file
  $fh = fopen( $stmp_fpath, "wb+");
  fwrite( $fh, $xml_output );
  fclose( $fh );

  // update the $lstmp_IDf file
  $fk = fopen( $lstmp_IDf, "wb+");
  fwrite( $fk, $next_start_row );
  fclose( $fk );

  /* GENERATING SITEMAP.XML */

  $xml_output = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
  $xml_output .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";

  $urls_towrite = array();
  $dircontents = scandir( ABSPATH.'sitemap/' );

  foreach ($dircontents as $dc) {
    if( strpos($dc, 'sitemap-') !== FALSE ) {
      $urls_towrite[] = '<sitemap><loc>' . SITE_URL . 'sitemap/'. $dc . '</loc></sitemap>';
    }
  }

  // glueing to a string
  $url_towrite = implode("\n", $urls_towrite);

  $xml_output .= $url_towrite;

  $xml_output .= "\n" . '</sitemapindex>';

  // construct the new sitemap file path
  $stmp_fpath   = sprintf(ABSPATH. 'sitemap.xml', "wb+");

  // write the URLs to the sitemap file
  $fh = fopen( $stmp_fpath, "wb+");
  fwrite( $fh, $xml_output );
  fclose( $fh );

  /* GENERATING ROBOTS.TXT */
  // list already generated sitemap files
  $stmp_furls = array();
  for($i=0; $i<$stmp_suffix_number; $i++) {
    $j = $i+1;
    $stmp_furl    = sprintf(SITE_URL . 'sitemap/' . $stmp_fn_format, $j);
    $stmp_furls[] = $stmp_furl;
    //echo $stmp_furl . "<br>\n";
  }

  $robots_content  = "User-agent: *\n";
  $robots_content .= "Allow: /\n";

  $dircontents = scandir( ABSPATH.'sitemap/' );
  foreach ($dircontents as $dc) {
    if( strpos($dc, 'sitemap-') !== FALSE ) {
      $robots_content .= "Sitemap: ". SITE_URL . 'sitemap/' . $dc ."\n";
    }
  }

  // create robots.txt
  $fm = fopen( ABSPATH . "robots.txt", "wb+");
  fwrite( $fm, $robots_content );
  fclose( $fm );

else:

  //echo '<h1>all URLs are already written</h1>';

endif;


