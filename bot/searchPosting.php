<?php

	if( ! defined('ABSPATH') ) require_once '../bootstrap.php';
	if( $ismysql !== TRUE ) die('Error!! Your $ismysql in config.php is not set to TRUE');

		$q = $_GET['q'];
		$kwx = $db->get_results( "SELECT * FROM search_terms where type = 'parent' AND term Like '%".$q."%'" );

		$tmp = [];
		if($kwx){
			foreach($kwx as $kw){
				
				$tmp[] = array(
					
					'id' => $kw->ID,
					'judul' => $kw->term,
					'slug' => $kw->slug,
					'url' => _a_url_q($kw->term)

					);

			}
		}
		
		header("Content-type:application/json");
		echo json_encode([ 'posts' => $tmp ]);