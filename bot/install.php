<?php

require_once '../bootstrap.php';

$sql_structure = file_get_contents( AGCL_INC_PATH . "db_structure.sql" );
$sqls = explode("\n", $sql_structure);

foreach( $sqls as $sql ):
  $db->query( $sql );
endforeach;

echo 'Tables installed';
