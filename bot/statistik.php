<?php

	if( ! defined('ABSPATH') ) require_once '../bootstrap.php';
	if( $ismysql !== TRUE ) die('Error!! Your $ismysql in config.php is not set to TRUE');

		$postingan = $db->get_var("SELECT COUNT(*) FROM search_terms where type='parent'");
		$gambar = $db->get_var("SELECT COUNT(*) FROM term_images");

		header("Content-type:application/json");
		echo json_encode([ 'post' => $postingan, 'images' => $gambar, 'message' => 'OK']);