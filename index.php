<?php

$path = dirname(__FILE__) . '/config.php';

if(!file_exists($path)){

	define( 'AGCL_PATH', dirname(__FILE__) );

	if(isset($_POST['singlebutton'])){

		  require_once AGCL_PATH . '/includes/databases/ez_sql_core.php';
		  require_once AGCL_PATH . '/includes/databases/ez_sql_mysqli.php';
		  require_once AGCL_PATH . '/includes/databases/queries.php';
		  
		  function to_prety_url($str){
		      if($str !== mb_convert_encoding( mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32') )
		      $str = mb_convert_encoding($str, 'UTF-8', mb_detect_encoding($str));
		      $str = htmlentities($str, ENT_NOQUOTES, 'UTF-8');
		      $str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\1', $str);
		      $str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
		      $str = preg_replace(array('`[^a-z0-9]`i','`[-]+`'), '-', $str);
		      $str = strtolower( trim($str, '-') );
		      return $str;
		  }

		require_once dirname(__FILE__) . '/install/config-generate.php';

	}else{



		require dirname(__FILE__) . '/install/template.php';

	}

}else{

	require_once dirname(__FILE__) . '/bootstrap.php';
	require_once dirname(__FILE__) . '/template.php';

}

